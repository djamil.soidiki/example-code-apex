#!/bin/bash
# Install vendor & dependencies
cd /var/www/html

if [ "$DEPLOYMENT_GROUP_NAME" == "Staging" ]
then
  aws s3 cp s3://configs-ec2-instance/staging.decrypt.private.php.zip /tmp
  sudo unzip -o /tmp/staging.decrypt.private.php.zip -d /var/www/html/config/secrets/staging
  sudo composer dump-env staging
  sudo rm -rf var/cache/staging
elif [ "$DEPLOYMENT_GROUP_NAME" == "Production" ]
then
  aws s3 cp s3://configs-ec2-instance/prod.decrypt.private.php.zip /tmp
  sudo unzip -o /tmp/prod.decrypt.private.php.zip -d /var/www/html/config/secrets/prod
  sudo composer dump-env prod
  sudo rm -rf var/cache/prod
fi

sudo composer install
sudo yarn install
sudo yarn encore production
sudo php bin/console secrets:decrypt-to-local
sudo php bin/console doctrine:migrations:migrate -n