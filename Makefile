start_dev:
	docker volume create --name=reavant-app-sync
	docker-compose -f docker-compose.yaml up -d
	docker-sync start

stop_dev:
	docker-compose stop
	docker container prune -f
	docker-sync stop
	docker-sync clean