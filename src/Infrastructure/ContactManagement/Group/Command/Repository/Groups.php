<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Group\Command\Repository;

use App\Domain\ContactManagement\Group\Group;
use App\Domain\ContactManagement\Group\Repository\GroupsInterface;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

/**
 * Class Groups
 *
 * @package App\Infrastructure\ContactManagement\Group\Command\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Groups implements GroupsInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * Groups constructor.
     *
     * @param EventStore $eventStore
     * @param EventBus   $eventBus
     * @param array      $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Group::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * @inheritdoc
     */
    public function get(GroupId $groupId): ?Group
    {
        /** @var Group $group */
        $group = $this->eventSourcingRepository->load($groupId->serialize());

        return $group;
    }

    /**
     * @inheritdoc
     */
    public function save(Group $group): void
    {
        $this->eventSourcingRepository->save($group);
    }
}