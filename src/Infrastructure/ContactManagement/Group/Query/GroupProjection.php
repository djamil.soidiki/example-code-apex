<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Group\Query;

use App\Domain\ContactManagement\Group\Event\ContactAdded;
use App\Domain\ContactManagement\Group\Event\GroupCreated;
use App\Domain\ContactManagement\Group\Event\GroupDeleted;
use App\Domain\ContactManagement\Group\Event\GroupDescriptionChanged;
use App\Domain\ContactManagement\Group\Event\GroupNameChanged;
use App\Domain\ContactManagement\Group\Event\ContactRemoved;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Member;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository\Groups;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository\Members;
use Broadway\ReadModel\Projector;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class GroupProjection
 *
 * @package App\Infrastructure\ContactManagement\Group\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GroupProjection extends Projector
{
    /**
     * @var Groups
     */
    private $groups;

    /**
     * @var Members
     */
    private $members;

    /**
     * GroupProjection constructor.
     *
     * @param Groups  $groups
     * @param Members $members
     */
    public function __construct(Groups $groups, Members $members)
    {
        $this->groups  = $groups;
        $this->members = $members;
    }

    /**
     * @param GroupCreated $event
     *
     * @throws Exception
     */
    protected function applyGroupCreated(GroupCreated $event): void
    {
        $group = Group::fromSerializable($event);

        $this->groups->add($group);
    }

    /**
     * @param GroupNameChanged $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyGroupNameChanged(GroupNameChanged $event): void
    {
        /** @var Group $group */
        $group = $this->groups->findGroupByGroupId($event->groupId());

        $group->setName($event->name());
        $group->setUpdatedAt($event->updatedAt());

        $this->groups->apply();
    }

    /**
     * @param GroupDescriptionChanged $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyGroupDescriptionChanged(GroupDescriptionChanged $event): void
    {
        /** @var Group $group */
        $group = $this->groups->findGroupByGroupId($event->groupId());

        $group->setDescription($event->description());
        $group->setUpdatedAt($event->updatedAt());

        $this->groups->apply();
    }

    /**
     * @param ContactAdded $event
     *
     * @throws Exception
     */
    protected function applyContactAdded(ContactAdded $event): void
    {
        $member = Member::fromSerializable($event);

        $this->members->add($member);
    }

    /**
     * @param ContactRemoved $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyContactRemoved(ContactRemoved $event): void
    {
        $member = $this->members->findMemberByGroupIdAndContactId($event->groupId(), $event->contactId());

        $this->members->delete($member);
    }

    /**
     * @param GroupDeleted $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyGroupDeleted(GroupDeleted $event): void
    {
        /** @var Group $group */
        $group = $this->groups->findGroupByGroupId($event->groupId());

        $this->groups->delete($group);
    }
}