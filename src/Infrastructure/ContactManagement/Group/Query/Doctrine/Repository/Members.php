<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\MemberId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Member;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Members
 *
 * @package App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Members extends AbstractRepository
{
    /**
     * Members constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Member::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Member $member
     */
    public function add(Member $member): void
    {
        $this->register($member);
    }

    /**
     * @param MemberId $memberId
     *
     * @return Member
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findMemberByMemberId(MemberId $memberId): Member
    {
        $qb = $this->repository
            ->createQueryBuilder('m')
            ->where('m.memberId = :memberId')
            ->setParameter('memberId', $memberId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param GroupId   $groupId
     * @param ContactId $contactId
     *
     * @return Member
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findMemberByGroupIdAndContactId(GroupId $groupId, ContactId $contactId): Member
    {
        $qb = $this->repository
            ->createQueryBuilder('m')
            ->where('m.groupId = :groupId AND m.contactId = :contactId')
            ->setParameter('groupId', $groupId->serialize())
            ->setParameter('contactId', $contactId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param UserId $ownerId
     *
     * @return array
     */
    public function findMembersByOwnerId(UserId $ownerId): array
    {
        return $this->repository
            ->createQueryBuilder('m')
            ->where('m.ownerId = :ownerId')
            ->orderBy('m.createdAt', 'DESC')
            ->setParameter('ownerId', $ownerId->serialize())
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ContactId $contactId
     *
     * @return array
     */
    public function findMembersByContactId(ContactId $contactId): array
    {
        return $this->repository
            ->createQueryBuilder('m')
            ->where('m.contactId = :contactId')
            ->orderBy('m.createdAt', 'DESC')
            ->setParameter('contactId', $contactId->serialize())
            ->getQuery()
            ->getResult();
    }

    /**
     * @param GroupId $groupId
     *
     * @return array
     */
    public function findMembersByGroupId(GroupId $groupId): array
    {
        return $this->repository
            ->createQueryBuilder('m')
            ->where('m.groupId = :groupId')
            ->orderBy('m.createdAt', 'DESC')
            ->setParameter('groupId', $groupId->serialize())
            ->getQuery()
            ->getResult();
    }


    public function totalByGroupId(GroupId $groupId)
    {
        return $this->repository
            ->createQueryBuilder('m')
            ->select('count(m.memberId) as total')
            ->where('m.groupId = :groupId')
            ->setParameter('groupId', $groupId->serialize())
            ->getQuery()
            ->getResult();
    }
}