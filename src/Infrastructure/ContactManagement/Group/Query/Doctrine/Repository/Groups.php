<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository;

use App\Domain\ContactManagement\Group\Repository\Query\GroupQueryInterface;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Domain\SharedKernel\Generic\Specification\SpecificationInterface;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Assert\Assertion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Groups
 *
 * @package App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Groups extends AbstractRepository implements GroupQueryInterface
{
    /**
     * Groups constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Group::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Group $group
     */
    public function add(Group $group): void
    {
        $this->register($group);
    }

    /**
     * @param GroupId $groupId
     *
     * @return Group
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findGroupByGroupId(GroupId $groupId): Group
    {
        $qb = $this->repository
            ->createQueryBuilder('g')
            ->where('g.groupId = :groupId')
            ->setParameter('groupId', $groupId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param UserId $ownerId
     *
     * @return array
     */
    public function findGroupsByOwnerId(UserId $ownerId): array
    {
        return $this->repository
            ->createQueryBuilder('g')
            ->where('g.ownerId = :ownerId')
            ->orderBy('g.createdAt', 'DESC')
            ->setParameter('ownerId', $ownerId->serialize())
            ->getQuery()
            ->getResult();
    }

    /**
     * @param GroupId[] $groupIds
     *
     * @return array
     */
    public function findGroupsByGroupIds(array $groupIds): array
    {
        Assertion::allIsInstanceOf($groupIds, 'App\Domain\ContactManagement\Group\ValueObject\GroupId');

        $serializedGroupIds = [];

        /** @var GroupId $groupId */
        foreach ($groupIds as $groupId) {
            $serializedGroupIds[] = $groupId->serialize();
        }

        $qb = $this->repository->createQueryBuilder('g');

        $qb->select('g')
            ->where(
                $qb->expr()->in('g.groupId', ':groupIds')
            )
            ->orderBy('g.createdAt', 'DESC')
            ->setParameter('groupIds', $serializedGroupIds);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param SpecificationInterface $specification
     *
     * @return mixed
     */
    public function selectSatisfying(SpecificationInterface $specification)
    {
        return $specification->satisfyingElementsFrom($this);
    }
}