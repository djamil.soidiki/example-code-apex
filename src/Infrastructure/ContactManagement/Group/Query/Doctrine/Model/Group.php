<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model;

use App\Domain\ContactManagement\Group\ValueObject\GroupDescription;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\GroupName;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Group
 *
 * @package App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Group implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var GroupId
     */
    private $groupId;

    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var GroupName
     */
    private $name;

    /**
     * @var GroupDescription|null
     */
    private $description;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->groupId->serialize();
    }

    /**
     * @return GroupId
     */
    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    /**
     * @return UserId
     */
    public function ownerId(): UserId
    {
        return $this->ownerId;
    }

    /**
     * @return GroupName
     */
    public function name(): GroupName
    {
        return $this->name;
    }

    /**
     * @return GroupDescription|null
     */
    public function description(): ?GroupDescription
    {
        return $this->description;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param GroupName $name
     */
    public function setName(GroupName $name): void
    {
        $this->name = $name;
    }

    /**
     * @param GroupDescription|null $description
     */
    public function setDescription(?GroupDescription $description): void
    {
        $this->description = $description;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @param DateTimeImmutable|null $updatedAt
     */
    public function setUpdatedAt(?DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->groupId     = new GroupId($data['group_id']);
        $instance->ownerId     = new UserId($data['owner_id']);
        $instance->name        = new GroupName($data['name']);
        $instance->description = self::deserializeValueOrNull($data, 'description', GroupDescription::class);
        $instance->updatedAt   = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $instance->createdAt   = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'group_id'    => $this->groupId->serialize(),
            'owner_id'    => $this->ownerId->serialize(),
            'name'        => $this->name->serialize(),
            'description' => self::serializeValueOrNull($this->description),
            'updated_at'  => self::serializeValueOrNull($this->updatedAt),
            'created_at'  => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}