<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\MemberId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Member
 *
 * @package App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Member implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var MemberId
     */
    private $memberId;

    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var GroupId
     */
    private $groupId;

    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->memberId->serialize();
    }

    /**
     * @return MemberId
     */
    public function memberId(): MemberId
    {
        return $this->memberId;
    }

    /**
     * @return UserId
     */
    public function ownerId(): UserId
    {
        return $this->ownerId;
    }

    /**
     * @return GroupId
     */
    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->memberId  = new MemberId($data['member_id']);
        $instance->ownerId   = new UserId($data['owner_id']);
        $instance->groupId   = new GroupId($data['group_id']);
        $instance->contactId = new ContactId($data['contact_id']);
        $instance->updatedAt = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $instance->createdAt = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'member_id'  => $this->memberId->serialize(),
            'owner_id'   => $this->ownerId->serialize(),
            'group_id'   => $this->groupId->serialize(),
            'contact_id' => $this->contactId->serialize(),
            'updated_at' => self::serializeValueOrNull($this->updatedAt),
            'created_at' => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}