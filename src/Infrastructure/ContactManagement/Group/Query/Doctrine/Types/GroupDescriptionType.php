<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Group\Query\Doctrine\Types;

use App\Domain\ContactManagement\Group\ValueObject\GroupDescription;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

/**
 * Class GroupDescriptionType
 *
 * @package App\Infrastructure\ContactManagement\Group\Query\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class GroupDescriptionType extends Type
{
    const GROUP_DESCRIPTION = 'contact_management__group_description';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof GroupDescription) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', GroupDescription::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return GroupDescription|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof GroupDescription) {
            return $value;
        }

        try {
            $groupDescription = new GroupDescription($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                self::GROUP_DESCRIPTION
            );
        }

        return $groupDescription;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::GROUP_DESCRIPTION;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}

