<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use App\Domain\SharedKernel\Generic\ValueObject\Phone\PhoneNumber;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Contact
 *
 * @package App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Contact implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var FullName
     */
    private $fullName;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var PhoneNumber|null
     */
    private $phoneNumber;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->contactId->serialize();
    }

    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return UserId
     */
    public function ownerId(): UserId
    {
        return $this->ownerId;
    }

    /**
     * @return FullName
     */
    public function fullName(): FullName
    {
        return $this->fullName;
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return PhoneNumber|null
     */
    public function phoneNumber(): ?PhoneNumber
    {
        return $this->phoneNumber;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param FullName $fullName
     */
    public function setFullName(FullName $fullName): void
    {
        $this->fullName = $fullName;
    }

    /**
     * @param EmailAddress $emailAddress
     */
    public function setEmailAddress(EmailAddress $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @param PhoneNumber|null $phoneNumber
     */
    public function setPhoneNumber(?PhoneNumber $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @param DateTimeImmutable|null $updatedAt
     */
    public function setUpdatedAt(?DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->contactId    = new ContactId($data['contact_id']);
        $instance->ownerId      = new UserId($data['owner_id']);
        $instance->fullName     = new FullName($data['full_name']['first_name'], $data['full_name']['last_name']);
        $instance->emailAddress = new EmailAddress($data['email_address']);
        $instance->phoneNumber  = self::deserializeValueOrNull($data, 'phone_number', PhoneNumber::class);
        $instance->updatedAt    = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $instance->createdAt    = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'contact_id'    => $this->contactId->serialize(),
            'owner_id'      => $this->ownerId->serialize(),
            'full_name'     => $this->fullName->serialize(),
            'email_address' => $this->emailAddress->serialize(),
            'phone_number'  => self::serializeValueOrNull($this->phoneNumber),
            'updated_at'    => self::serializeValueOrNull($this->updatedAt),
            'created_at'    => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}