<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Repository;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\ContactManagement\Contact\Repository\Query\ContactQueryInterface;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Domain\SharedKernel\Generic\Specification\SpecificationInterface;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model\Contact;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Assert\Assertion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Contacts
 *
 * @package App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Contacts extends AbstractRepository implements ContactQueryInterface
{
    /**
     * Contacts constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Contact::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Contact $contact
     */
    public function add(Contact $contact): void
    {
        $this->register($contact);
    }

    /**
     * @param ContactId $contactId
     *
     * @return Contact
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findContactByContactId(ContactId $contactId): Contact
    {
        $qb = $this->repository
            ->createQueryBuilder('c')
            ->where('c.contactId = :contactId')
            ->setParameter('contactId', $contactId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param UserId $ownerId
     *
     * @return array
     */
    public function findContactsByOwnerId(UserId $ownerId): array
    {
        return $this->repository
            ->createQueryBuilder('c')
            ->where('c.ownerId = :ownerId')
            ->orderBy('c.createdAt', 'DESC')
            ->setParameter('ownerId', $ownerId->serialize())
            ->getQuery()
            ->getResult();
    }

    /**
     * @param UserId      $ownerId
     * @param array       $contactIds
     * @param string|null $q
     *
     * @return array
     */
    public function findContactsByOwnerIdAndContactIds(UserId $ownerId, array $contactIds = [], string $q = null): array
    {
        Assertion::allIsInstanceOf($contactIds, ContactId::class);

        $qb = $this->repository->createQueryBuilder('c');

        $qb->where($qb->expr()->eq('c.ownerId', ':ownerId'))
            ->orderBy('c.createdAt', 'DESC')
            ->setParameter('ownerId', $ownerId->serialize());

        if (!empty($contactIds)) {
            $qb->andWhere(
                $qb->expr()->in('c.contactId', ':contactIds')
            )
                ->setParameter('contactIds', $contactIds);
        }

        if ($q !== null) {
            $qb
                ->andWhere('c.fullName.firstName LIKE :q OR c.fullName.lastName LIKE :q OR c.emailAddress LIKE :q')
                ->setParameter('q', "%$q%");
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param UserId      $ownerId
     * @param array       $contactIds
     * @param string|null $q
     *
     * @return array
     */
    public function findContactsByOwnerIdAndNotContactIds(UserId $ownerId, array $contactIds = [], string $q = null): array
    {
        Assertion::allIsInstanceOf($contactIds, ContactId::class);

        $qb = $this->repository->createQueryBuilder('c');

        $qb->where($qb->expr()->eq('c.ownerId', ':ownerId'))
            ->orderBy('c.createdAt', 'DESC')
            ->setParameter('ownerId', $ownerId->serialize());

        if (!empty($contactIds)) {
            $qb->andWhere(
                $qb->expr()->notIn('c.contactId', ':contactIds')
            )
                ->setParameter('contactIds', $contactIds);
        }

        if ($q !== null) {
            $qb
                ->andWhere('c.fullName.firstName LIKE :q OR c.fullName.lastName LIKE :q OR c.emailAddress LIKE :q')
                ->setParameter('q', "%$q%");
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param SpecificationInterface $specification
     *
     * @return mixed
     */
    public function selectSatisfying(SpecificationInterface $specification)
    {
        return $specification->satisfyingElementsFrom($this);
    }
}