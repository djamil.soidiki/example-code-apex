<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Contact\Query;

use App\Domain\ContactManagement\Contact\Event\ContactCreated;
use App\Domain\ContactManagement\Contact\Event\ContactDeleted;
use App\Domain\ContactManagement\Contact\Event\ContactEmailAddressChanged;
use App\Domain\ContactManagement\Contact\Event\ContactFullNameChanged;
use App\Domain\ContactManagement\Contact\Event\ContactPhoneNumberChanged;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model\Contact;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Repository\Contacts;
use Broadway\ReadModel\Projector;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class ContactProjection
 *
 * @package App\Infrastructure\ContactManagement\Contact\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ContactProjection extends Projector
{
    /**
     * @var Contacts
     */
    private $contacts;

    /**
     * ContactProjection constructor.
     *
     * @param Contacts $contacts
     */
    public function __construct(Contacts $contacts)
    {
        $this->contacts = $contacts;
    }

    /**
     * @param ContactCreated $event
     *
     * @throws Exception
     */
    protected function applyContactCreated(ContactCreated $event): void
    {
        $contact = Contact::fromSerializable($event);

        $this->contacts->add($contact);
    }

    /**
     * @param ContactFullNameChanged $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyContactFullNameChanged(ContactFullNameChanged $event): void
    {
        /** @var Contact $contact */
        $contact = $this->contacts->findContactByContactId($event->contactId());

        $contact->setFullName($event->fullName());
        $contact->setUpdatedAt($event->updatedAt());

        $this->contacts->apply();
    }

    /**
     * @param ContactEmailAddressChanged $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyContactEmailAddressChanged(ContactEmailAddressChanged $event): void
    {
        /** @var Contact $contact */
        $contact = $this->contacts->findContactByContactId($event->contactId());

        $contact->setEmailAddress($event->emailAddress());
        $contact->setUpdatedAt($event->updatedAt());

        $this->contacts->apply();
    }

    /**
     * @param ContactPhoneNumberChanged $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyContactPhoneNumberChanged(ContactPhoneNumberChanged $event): void
    {
        /** @var Contact $contact */
        $contact = $this->contacts->findContactByContactId($event->contactId());

        $contact->setPhoneNumber($event->phoneNumber());
        $contact->setUpdatedAt($event->updatedAt());

        $this->contacts->apply();
    }

    /**
     * @param ContactDeleted $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyContactDeleted(ContactDeleted $event): void
    {
        /** @var Contact $contact */
        $contact = $this->contacts->findContactByContactId($event->contactId());

        $this->contacts->delete($contact);
    }
}