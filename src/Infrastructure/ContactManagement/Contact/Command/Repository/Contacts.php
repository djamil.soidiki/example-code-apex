<?php

declare(strict_types=1);

namespace App\Infrastructure\ContactManagement\Contact\Command\Repository;

use App\Domain\ContactManagement\Contact\Contact;
use App\Domain\ContactManagement\Contact\Repository\ContactsInterface;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

/**
 * Class Contacts
 *
 * @package App\Infrastructure\ContactManagement\Contact\Command\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Contacts implements ContactsInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * Contacts constructor.
     *
     * @param EventStore $eventStore
     * @param EventBus   $eventBus
     * @param array      $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Contact::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * @inheritdoc
     */
    public function get(ContactId $contactId): ?Contact
    {
        /** @var Contact $contact */
        $contact = $this->eventSourcingRepository->load($contactId->serialize());

        return $contact;
    }

    /**
     * @inheritdoc
     */
    public function save(Contact $contact): void
    {
        $this->eventSourcingRepository->save($contact);
    }
}