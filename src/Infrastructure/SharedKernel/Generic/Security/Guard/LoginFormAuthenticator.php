<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Security\Guard;

use App\Application\Authentication\Command\Login;
use App\Application\Authentication\Query\GetUserByEmailAddress;
use App\Application\Authorization\Query\FindUserRoles;
use App\Application\Billing\Customer\Query\GetCustomerByUserIdQuery;
use App\Domain\SharedKernel\Authentication\Exception\EmailAddressNotFoundException;
use App\Domain\SharedKernel\Authentication\Exception\InvalidCredentialsException;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\User as UserModel;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\Infrastructure\SharedKernel\Generic\Security\Core\User;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use App\Infrastructure\SharedKernel\Generic\Security\Translator\RoleTranslator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * Class LoginFormAuthenticator
 *
 * @package App\Infrastructure\SharedKernel\Generic\Security\Guard
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;
    use BusTrait;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * LoginFormAuthenticator constructor.
     *
     * @param MessageBusInterface       $commandBus
     * @param MessageBusInterface       $queryBus
     * @param RouterInterface           $router
     * @param CsrfTokenManagerInterface $csrfTokenManager
     */
    public function __construct(
        MessageBusInterface $commandBus,
        MessageBusInterface $queryBus,
        RouterInterface $router,
        CsrfTokenManagerInterface $csrfTokenManager
    )
    {
        $this->commandBus       = $commandBus;
        $this->queryBus         = $queryBus;
        $this->router           = $router;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @inheritDoc
     */
    protected function getLoginUrl()
    {
        return $this->router->generate('authentication_login');
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request)
    {
        return 'authentication_login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    /**
     * @inheritDoc
     */
    public function getCredentials(Request $request)
    {
        $credentials = [
            'email'      => $request->request->get('email'),
            'password'   => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

        return $credentials;
    }

    /**
     * @inheritDoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);

        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        try {
            $this->handleCommand(new Login($credentials['email'], $credentials['password']));

            /** @var UserModel $userModel */
            $userModel = $this->handleQuery(new GetUserByEmailAddress($credentials['email']));

            /** @var Customer $customer */
            $customer = $this->handleQuery(new GetCustomerByUserIdQuery($userModel->getId()));

            $roles = $this->handleQuery(new FindUserRoles($userModel->userId()->serialize()));

            $roleTranslator = new RoleTranslator();

            return new User(
                $userModel->getId(),
                $customer->fullName()->firstName(),
                $customer->fullName()->lastName(),
                $userModel->emailAddress()->serialize(),
                $userModel->password()->serialize(),
                $roleTranslator->toRoleForSymfony($roles)
            );

        } catch (HandlerFailedException $exception) {
            $throwable = $this->getThrowable($exception);

            if ($throwable instanceof EmailAddressNotFoundException)
                throw new CustomUserMessageAuthenticationException('We cannot find an account with that email address');
            else if ($throwable instanceof InvalidCredentialsException)
                throw new CustomUserMessageAuthenticationException('Your password is incorrect');

            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->router->generate('app_dashboard'));
    }
}