<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Security\Core;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 *
 * @package App\Infrastructure\SharedKernel\Generic\Security\Core
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class User implements UserInterface
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var string
     */
    private $password;

    /**
     * @var array
     */
    private $roles;

    /**
     * User constructor.
     *
     * @param string $userId
     * @param string $firstName
     * @param string $lastName
     * @param string $emailAddress
     * @param string $password
     * @param array  $roles
     */
    public function __construct(string $userId, string $firstName, string $lastName, string $emailAddress, string $password, array $roles = ['ROLE_USER'])
    {
        $this->userId       = $userId;
        $this->firstName    = $firstName;
        $this->lastName     = $lastName;
        $this->emailAddress = $emailAddress;
        $this->password     = $password;
        $this->roles        = $roles;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function firstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function lastName(): string
    {
        return $this->lastName;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @inheritDoc
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getUsername(): string
    {
        return $this->emailAddress;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials(): void
    {
    }
}