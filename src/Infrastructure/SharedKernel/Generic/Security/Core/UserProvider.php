<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Security\Core;

use App\Application\Authentication\Query\GetUserByEmailAddress;
use App\Application\Authorization\Query\FindUserRoles;
use App\Application\Billing\Customer\Query\GetCustomerByUserIdQuery;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use App\Infrastructure\SharedKernel\Generic\Security\Translator\RoleTranslator;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class UserProvider
 *
 * @package App\Infrastructure\SharedKernel\Generic\Security\Core
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class UserProvider implements UserProviderInterface
{
    use BusTrait;

    /**
     * UserProvider constructor.
     *
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param string $email
     *
     * @return UserInterface
     */
    public function loadUserByUsername($email): UserInterface
    {
        /** @var \App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\User $user */
        $user = $this->handleQuery(new GetUserByEmailAddress($email));

        /** @var Customer $customer */
        $customer = $this->handleQuery(new GetCustomerByUserIdQuery($user->getId()));

        $roles = $this->handleQuery(new FindUserRoles($user->getId()));

        $roleTranslator = new RoleTranslator();

        return new User(
            $user->getId(),
            $customer->fullName()->firstName(),
            $customer->fullName()->lastName(),
            $user->emailAddress()->serialize(),
            $user->password()->serialize(),
            $roleTranslator->toRoleForSymfony($roles)
        );
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class): bool
    {
        return User::class === $class;
    }
}