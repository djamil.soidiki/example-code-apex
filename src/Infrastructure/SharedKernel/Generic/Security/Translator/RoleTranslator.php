<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Security\Translator;

use App\Domain\SharedKernel\Authorization\Role;
use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model\Role as RoleModel;

/**
 * Class RoleTranslator
 *
 * @package App\Infrastructure\SharedKernel\Generic\Security\Translator
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RoleTranslator
{
    /**
     * @param RoleModel[] $roles
     *
     * @return array
     */
    public function toRoleForSymfony(array $roles): array
    {
        $rolesForSymfony = ['ROLE_USER'];

        foreach ($roles as $role) {
            if ($role->name()->serialize() === Role::SUPER_ADMINISTRATOR)
                $rolesForSymfony[] = 'ROLE_SUPER_ADMIN';

            if ($role->name()->serialize() === Role::ADMINISTRATOR)
                $rolesForSymfony[] = 'ROLE_ADMIN';

            if ($role->name()->serialize() === Role::SUBSCRIBER)
                $rolesForSymfony[] = 'ROLE_SUBSCRIBER';
        }

        return $rolesForSymfony;
    }
}