<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200608185114 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contacts (id CHAR(36) NOT NULL COMMENT \'(DC2Type:contact_id)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:user_id)\', email_address VARCHAR(255) NOT NULL COMMENT \'(DC2Type:email_address)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, phone_number_value VARCHAR(255) DEFAULT NULL, phone_number_country VARCHAR(2) DEFAULT NULL, phone_number_code VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_list_members (id CHAR(36) NOT NULL COMMENT \'(DC2Type:contact_list_member_id)\', contact_list_id CHAR(36) NOT NULL COMMENT \'(DC2Type:contact_list_id)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:user_id)\', contact_id CHAR(36) NOT NULL COMMENT \'(DC2Type:contact_id)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_lists (id CHAR(36) NOT NULL COMMENT \'(DC2Type:contact_list_id)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:user_id)\', name VARCHAR(255) NOT NULL COMMENT \'(DC2Type:contact_list_name)\', description VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:contact_list_description)\', count INT NOT NULL, updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_250C8FAA5E237E06 (name), INDEX name_index (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contacts');
        $this->addSql('DROP TABLE contact_list_members');
        $this->addSql('DROP TABLE contact_lists');
    }
}
