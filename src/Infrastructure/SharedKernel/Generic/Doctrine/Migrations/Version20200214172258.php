<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200214172258 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE customers (id CHAR(36) NOT NULL COMMENT \'(DC2Type:customer_id)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:user_id)\', payment_method_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:payment_method_id)\', email_address VARCHAR(255) NOT NULL COMMENT \'(DC2Type:email_address)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products (id CHAR(36) NOT NULL COMMENT \'(DC2Type:product_id)\', name VARCHAR(255) NOT NULL COMMENT \'(DC2Type:product_name)\', description LONGTEXT NOT NULL COMMENT \'(DC2Type:product_description)\', type VARCHAR(255) NOT NULL COMMENT \'(DC2Type:product_type)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plans (id CHAR(36) NOT NULL COMMENT \'(DC2Type:plan_id)\', product_id CHAR(36) NOT NULL COMMENT \'(DC2Type:product_id)\', name VARCHAR(255) NOT NULL COMMENT \'(DC2Type:plan_name)\', billing_cycle VARCHAR(255) NOT NULL COMMENT \'(DC2Type:billing_cycle)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', amount INT NOT NULL, currency VARCHAR(255) NOT NULL COMMENT \'(DC2Type:currency)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscriptions (id CHAR(36) NOT NULL COMMENT \'(DC2Type:subscription_id)\', customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:customer_id)\', plan_id CHAR(36) NOT NULL COMMENT \'(DC2Type:plan_id)\', status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:subscription_status)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trials (id CHAR(36) NOT NULL COMMENT \'(DC2Type:trial_id)\', customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:customer_id)\', status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:trial_status)\', started_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', end_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE customers');
        $this->addSql('DROP TABLE products');
        $this->addSql('DROP TABLE plans');
        $this->addSql('DROP TABLE subscriptions');
        $this->addSql('DROP TABLE trials');
    }
}
