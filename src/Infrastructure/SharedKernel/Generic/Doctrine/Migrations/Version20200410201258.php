<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200410201258 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_analysis (id CHAR(36) NOT NULL COMMENT \'(DC2Type:project_analysis_id)\', project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:project_id)\', name VARCHAR(255) NOT NULL COMMENT \'(DC2Type:project_analysis_name)\', status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:project_analysis_status)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', after_repair_value_amount DOUBLE PRECISION DEFAULT NULL, after_repair_value_currency VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:currency)\', purchase_price_amount DOUBLE PRECISION DEFAULT NULL, purchase_price_currency VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:currency)\', rehab_cost_type VARCHAR(255) DEFAULT NULL, rehab_cost_lump_sum_amount DOUBLE PRECISION DEFAULT NULL, rehab_cost_lump_sum_currency VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:currency)\', buying_cost_type VARCHAR(255) DEFAULT NULL, buying_cost_percentage_of_purchase DOUBLE PRECISION DEFAULT NULL, buying_cost_lump_sum_amount DOUBLE PRECISION DEFAULT NULL, buying_cost_lump_sum_currency VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:currency)\', selling_cost_type VARCHAR(255) DEFAULT NULL, selling_cost_percentage_of_resale DOUBLE PRECISION DEFAULT NULL, selling_cost_lump_sum_amount DOUBLE PRECISION DEFAULT NULL, selling_cost_lump_sum_currency VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:currency)\', holding_cost_type VARCHAR(255) DEFAULT NULL, holding_cost_number_of_holding_months INT DEFAULT NULL, holding_cost_lump_sum_amount DOUBLE PRECISION DEFAULT NULL, holding_cost_lump_sum_currency VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:currency)\', holding_cost_price_per_month_amount DOUBLE PRECISION DEFAULT NULL, holding_cost_price_per_month_currency VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:currency)\', profit_amount DOUBLE PRECISION DEFAULT NULL, profit_currency VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:currency)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE projects (id CHAR(36) NOT NULL COMMENT \'(DC2Type:project_id)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:user_id)\', name VARCHAR(255) NOT NULL COMMENT \'(DC2Type:project_name)\', strategy VARCHAR(255) NOT NULL COMMENT \'(DC2Type:project_strategy)\', status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:project_status)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', address_line1 VARCHAR(255) NOT NULL, address_line2 VARCHAR(255) DEFAULT NULL, address_city VARCHAR(255) NOT NULL, address_state VARCHAR(255) NOT NULL, address_postal_code VARCHAR(255) NOT NULL, address_country VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE plans CHANGE amount amount DOUBLE PRECISION DEFAULT NULL, CHANGE currency currency VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:currency)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE project_analysis');
        $this->addSql('DROP TABLE projects');
        $this->addSql('ALTER TABLE plans CHANGE amount amount DOUBLE PRECISION NOT NULL, CHANGE currency currency VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci` COMMENT \'(DC2Type:currency)\'');
    }
}
