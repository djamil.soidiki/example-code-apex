<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Doctrine\Types;

use App\Domain\SharedKernel\Generic\ValueObject\Token\Token;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

/**
 * Class TokenType
 *
 * @package App\Infrastructure\SharedKernel\Generic\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class TokenType extends Type
{
    const TOKEN = 'token';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof Token) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', Token::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return Token|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof Token) {
            return $value;
        }

        try {
            $token = new Token($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                self::TOKEN
            );
        }

        return $token;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::TOKEN;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}

