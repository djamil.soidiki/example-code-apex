<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Authorization;

use App\Application\Authorization\Command\AssignRoleToUser;
use App\Application\Authorization\Command\CreateRole;
use App\Domain\SharedKernel\Authorization\Role;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\User;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\AbstractFixtures;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Authentication\AuthenticationFixtures;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\FixtureGroups;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class AuthorizationFixtures
 *
 * @package App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Authorization
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AuthorizationFixtures extends AbstractFixtures implements FixtureGroupInterface
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $this->loadSuperAdministratorRole();
        $this->loadAdministratorRole();
        $this->loadSubscriberRole();
    }

    private function loadSuperAdministratorRole(): void
    {
        $roleId = $this->handleCommand(new CreateRole(Role::SUPER_ADMINISTRATOR));

        /** @var User $user */
        $user = $this->getReference(AuthenticationFixtures::USER_SUPER_ADMIN_REFERENCE);

        $this->handleCommand(new AssignRoleToUser(
            $roleId,
            $user->getId()
        ));
    }

    private function loadAdministratorRole(): void
    {
        $roleId = $this->handleCommand(new CreateRole(Role::ADMINISTRATOR));

        /** @var User $user */
        $user = $this->getReference(AuthenticationFixtures::USER_ADMIN_REFERENCE);

        $this->handleCommand(new AssignRoleToUser(
            $roleId,
            $user->getId()
        ));

    }

    private function loadSubscriberRole(): void
    {
        $roleId = $this->handleCommand(new CreateRole(Role::SUBSCRIBER));

        /** @var User $user */
        $user = $this->getReference(AuthenticationFixtures::USER_SUBSCRIBED_REFERENCE);

        $this->handleCommand(new AssignRoleToUser(
            $roleId,
            $user->getId()
        ));

    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            AuthenticationFixtures::class,
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getGroups(): array
    {
        return [FixtureGroups::GLOBAL];
    }
}