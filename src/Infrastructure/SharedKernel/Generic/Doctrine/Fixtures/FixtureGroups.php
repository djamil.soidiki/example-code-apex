<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures;

/**
 * Class FixtureGroups
 *
 * @package App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FixtureGroups
{
    const GLOBAL   = 'global';
    const BILLING  = 'billing';
    const USE_CASE = 'use_case';
}