<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Authentication;

use App\Application\Authentication\Command\RegisterUser;
use App\Application\Authentication\Query\GetUserByEmailAddress;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\AbstractFixtures;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\FixtureGroups;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class AuthenticationFixtures
 *
 * @package App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Authentication
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AuthenticationFixtures extends AbstractFixtures implements FixtureGroupInterface
{
    public const USER_TRIALING_REFERENCE    = 'user-trialing';
    public const USER_SUBSCRIBED_REFERENCE  = 'user-subscribed';
    public const USER_SUPER_ADMIN_REFERENCE = 'user-super-admin';
    public const USER_ADMIN_REFERENCE       = 'user-admin';

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $this->loadSuperAdmin();
        $this->loadAdmin();
        $this->loadUserTrialing();
        $this->loadUserSubscribed();
    }

    private function loadUserTrialing(): void
    {
        $emailAddress = "trialing@reavant.co";
        $password     = 'test';

        $this->handleCommand(new RegisterUser($emailAddress, $password));
        $user = $this->handleQuery(new GetUserByEmailAddress($emailAddress));

        $this->addReference(self::USER_TRIALING_REFERENCE, $user);
    }

    private function loadUserSubscribed(): void
    {
        $emailAddress = "subscribed@reavant.co";
        $password     = 'test';

        $this->handleCommand(new RegisterUser($emailAddress, $password));
        $user = $this->handleQuery(new GetUserByEmailAddress($emailAddress));

        $this->addReference(self::USER_SUBSCRIBED_REFERENCE, $user);
    }

    private function loadSuperAdmin(): void
    {
        $emailAddress = 'super.admin@reavant.co';
        $password     = "superadmin";

        $this->handleCommand(new RegisterUser($emailAddress, $password));
        $user = $this->handleQuery(new GetUserByEmailAddress($emailAddress));

        $this->addReference(self::USER_SUPER_ADMIN_REFERENCE, $user);
    }

    private function loadAdmin(): void
    {
        $emailAddress = 'admin@reavant.co';
        $password     = "adminadmin";

        $this->handleCommand(new RegisterUser($emailAddress, $password));
        $user = $this->handleQuery(new GetUserByEmailAddress($emailAddress));

        $this->addReference(self::USER_ADMIN_REFERENCE, $user);
    }

    /**
     * @inheritDoc
     */
    public static function getGroups(): array
    {
        return [FixtureGroups::GLOBAL];
    }
}