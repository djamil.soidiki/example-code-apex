<?php

namespace App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\TrialPeriod;

use App\Application\TrialPeriod\Command\StartTrialPeriod;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\AbstractFixtures;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Billing\BillingCustomerFixtures;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\FixtureGroups;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class TrialPeriodFixtures
 *
 * @package App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\TrialPeriod
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TrialPeriodFixtures extends AbstractFixtures implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        /** @var Customer $customer */
        $customer = $this->getReference(BillingCustomerFixtures::CUSTOMER_TRIALING_REFERENCE);
        $this->handleCommand(new StartTrialPeriod($customer->customerId()->serialize()));
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            BillingCustomerFixtures::class,
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getGroups(): array
    {
        return [
            FixtureGroups::GLOBAL,
            FixtureGroups::BILLING
        ];
    }
}