<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures;

use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class AbstractFixtures
 *
 * @package App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractFixtures extends Fixture
{
    use BusTrait;

    /**
     * @var Generator
     */
    protected $faker;

    /**
     * AbstractFixtures constructor.
     *
     * @param MessageBusInterface $commandBus
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $commandBus, MessageBusInterface $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus   = $queryBus;
        $this->faker      = Factory::create();
    }
}