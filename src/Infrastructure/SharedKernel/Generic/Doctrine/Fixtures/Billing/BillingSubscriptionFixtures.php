<?php

namespace App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Billing;

use App\Application\Billing\Subscription\Command\SubscribeToPlan;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Model\Plan;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\AbstractFixtures;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\FixtureGroups;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class BillingSubscriptionFixtures
 *
 * @package App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Billing
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class BillingSubscriptionFixtures extends AbstractFixtures implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        /** @var Customer $customer */
        $customer = $this->getReference(BillingCustomerFixtures::CUSTOMER_SUBSCRIBED_REFERENCE);

        /** @var Plan $plan */
        $plan = $this->getReference(BillingPlanFixtures::MONTHLY_PLAN_REFERENCE);

        $subscriptionId = $this->handleCommand(new SubscribeToPlan(
                                 $customer->customerId()->serialize(),
                                 $plan->planId()->serialize()
                             )
        );
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            BillingPlanFixtures::class,
            BillingCustomerFixtures::class,
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getGroups(): array
    {
        return [FixtureGroups::GLOBAL, FixtureGroups::BILLING];
    }
}