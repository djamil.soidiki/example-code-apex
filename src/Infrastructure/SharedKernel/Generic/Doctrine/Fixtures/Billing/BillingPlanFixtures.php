<?php

namespace App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Billing;

use App\Application\Billing\Plan\Command\CreatePlan;
use App\Application\Billing\Plan\Query\GetPlanByPlanIdQuery;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanBillingCycle;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\AbstractFixtures;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\FixtureGroups;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class BillingPlanFixtures
 *
 * @package App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Billing
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class BillingPlanFixtures extends AbstractFixtures implements FixtureGroupInterface
{
    const MONTHLY_PLAN_REFERENCE = 'monthly-plan';
    const YEARLY_PLAN_REFERENCE  = 'yearly-plan';

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $this->loadMonthlyPlan();
        $this->loadYearlyPlan();
    }

    private function loadMonthlyPlan()
    {
        $productId    = 'prod_IGQTnVYYMaANhx';
        $planName     = "Monthly Plan";
        $planPrice    = 39;
        $planInterval = PlanBillingCycle::MONTH;

        $planId = $this->handleCommand(new CreatePlan($productId, $planName, $planPrice, $planInterval));
        $plan   = $this->handleQuery(new GetPlanByPlanIdQuery($planId));

        $this->addReference(self::MONTHLY_PLAN_REFERENCE, $plan);
    }

    private function loadYearlyPlan()
    {
        $productId    = 'prod_IGQTnVYYMaANhx';
        $planName     = "Yearly Plan";
        $planPrice    = 399;
        $planInterval = PlanBillingCycle::YEAR;

        $planId = $this->handleCommand(new CreatePlan($productId, $planName, $planPrice, $planInterval));
        $plan   = $this->handleQuery(new GetPlanByPlanIdQuery($planId));

        $this->addReference(self::YEARLY_PLAN_REFERENCE, $plan);
    }

    /**
     * @inheritDoc
     */
    public static function getGroups(): array
    {
        return [FixtureGroups::GLOBAL, FixtureGroups::BILLING];
    }
}