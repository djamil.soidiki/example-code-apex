<?php

namespace App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Billing;

use App\Application\Billing\Customer\Command\RegisterCustomer;
use App\Application\Billing\Customer\Query\GetCustomerByUserIdQuery;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\AbstractFixtures;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Authentication\AuthenticationFixtures;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\FixtureGroups;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\User;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class BillingCustomerFixtures
 *
 * @package App\Infrastructure\SharedKernel\Generic\Doctrine\Fixtures\Billing
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class BillingCustomerFixtures extends AbstractFixtures implements DependentFixtureInterface, FixtureGroupInterface
{
    const CUSTOMER_TRIALING_REFERENCE = 'customer-trialing';
    const CUSTOMER_SUBSCRIBED_REFERENCE = 'customer-subscribed';

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $this->loadCustomerTrialing();
        $this->loadCustomerSubscribed();
    }

    private function loadCustomerTrialing(): void
    {
        /** @var User $user */
        $user = $this->getReference(AuthenticationFixtures::USER_TRIALING_REFERENCE);

        $customer = $this->loadCustomer($user);

        $this->addReference(self::CUSTOMER_TRIALING_REFERENCE, $customer);
    }

    private function loadCustomerSubscribed(): void
    {
        /** @var User $user */
        $user = $this->getReference(AuthenticationFixtures::USER_SUBSCRIBED_REFERENCE);

        $customer = $this->loadCustomer($user, true);

        $this->addReference(self::CUSTOMER_SUBSCRIBED_REFERENCE, $customer);
    }

    /**
     * @param User $user
     * @param bool $subscribed
     *
     * @return Customer
     */
    private function loadCustomer(User $user, $subscribed = false): Customer
    {
        $userId       = $user->userId()->serialize();
        $emailAddress = $user->emailAddress()->serialize();
        $firstName    = $this->faker->firstName;
        $lastName     = $this->faker->lastName;

        if ($subscribed === false)
            $this->handleCommand(new RegisterCustomer($userId, $emailAddress, $firstName, $lastName));
        else
            $this->handleCommand(new RegisterCustomer($userId, $emailAddress, $firstName, $lastName, 'pm_card_visa'));

        return $this->handleQuery(new GetCustomerByUserIdQuery($userId));
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            AuthenticationFixtures::class,
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getGroups(): array
    {
        return [FixtureGroups::GLOBAL, FixtureGroups::BILLING];
    }
}