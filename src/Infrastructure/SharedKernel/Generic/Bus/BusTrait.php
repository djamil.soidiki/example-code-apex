<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\Bus;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Messenger\Stamp\StampInterface;
use Throwable;

/**
 * Trait BusTrait
 *
 * @package App\Infrastructure\SharedKernel\Generic\Bus
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
trait BusTrait
{
    /**
     * @var MessageBusInterface
     */
    private $commandBus;

    /**
     * @var MessageBusInterface
     */
    private $queryBus;

    /**
     * @param object $command
     *
     * @return mixed
     */
    protected function handleCommand($command)
    {
        /** @var Envelope $envelope */
        $envelope = $this->commandBus->dispatch($command);

        /** @var StampInterface $handled */
        $handled = $envelope->last(HandledStamp::class);

        return $handled->getResult();
    }

    /**
     * @param object $query
     *
     * @return mixed
     */
    protected function handleQuery($query)
    {
        /** @var Envelope $envelope */
        $envelope = $this->queryBus->dispatch($query);

        /** @var StampInterface $handled */
        $handled = $envelope->last(HandledStamp::class);

        return $handled->getResult();
    }

    /**
     * @param HandlerFailedException $exception
     *
     * @return mixed|Throwable
     */
    public function getThrowable(HandlerFailedException $exception)
    {
        $nestedExceptions = $exception->getNestedExceptions();

        return $nestedExceptions[0];
    }
}