<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Generic\EventStore;

use Broadway\Domain\Metadata;
use Broadway\EventSourcing\MetadataEnrichment\MetadataEnricher;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserMetadataEnricher
 *
 * @package App\Infrastructure\SharedKernel\Generic\EventStore
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class UserMetadataEnricher implements MetadataEnricher
{
    /**
     * @var Security
     */
    private $security;

    /**
     * UserMetadataEnricher constructor.
     *
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @inheritDoc
     */
    public function enrich(Metadata $metadata): Metadata
    {
        /** @var UserInterface $user */
        $user = $this->security->getUser();

        if ($user === null)
            return $metadata;

        return $metadata->merge($metadata->kv('user', [
            'id'    => $user->getId(),
            'email' => $user->getUsername(),
            'roles' => $user->getRoles()
        ]));
    }
}