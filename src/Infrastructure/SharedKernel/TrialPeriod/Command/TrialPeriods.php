<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\TrialPeriod\Command;

use App\Domain\SharedKernel\TrialPeriod\TrialPeriod;
use App\Domain\SharedKernel\TrialPeriod\Repository\TrialPeriodsInterface;
use App\Domain\SharedKernel\TrialPeriod\ValueObject\TrialPeriodId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

/**
 * Class TrialPeriods
 *
 * @package App\Infrastructure\SharedKernel\TrialPeriod\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TrialPeriods implements TrialPeriodsInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * TrialPeriods constructor.
     *
     * @param EventStore $eventStore
     * @param EventBus   $eventBus
     * @param array      $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            TrialPeriod::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * @inheritdoc
     */
    public function get(TrialPeriodId $trialPeriodId): ?TrialPeriod
    {
        /** @var TrialPeriod $trialPeriod */
        $trialPeriod = $this->eventSourcingRepository->load($trialPeriodId->serialize());

        return $trialPeriod;
    }

    /**
     * @inheritdoc
     */
    public function save(TrialPeriod $trialPeriod): void
    {
        $this->eventSourcingRepository->save($trialPeriod);
    }
}