<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Model;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\TrialPeriod\ValueObject\TrialPeriodId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class TrialPeriod
 *
 * @package App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class TrialPeriod implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var TrialPeriodId
     */
    private $id;

    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var DateTimeImmutable
     */
    private $startDate;

    /**
     * @var DateTimeImmutable
     */
    private $endDate;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @return string
     */
    public function idEncoded(): string
    {
        return $this->id->encode();
    }

    /**
     * @return CustomerId
     */
    public function customerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function startDate(): DateTimeImmutable
    {
        return $this->startDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function endDate(): DateTimeImmutable
    {
        return $this->endDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $trialPeriod = new self();

        $trialPeriod->id         = new TrialPeriodId($data['trial_period_id']);
        $trialPeriod->customerId = new CustomerId($data['customer_id']);
        $trialPeriod->startDate  = new DateTimeImmutable($data['start_date']);
        $trialPeriod->endDate    = new DateTimeImmutable($data['end_date']);
        $trialPeriod->updatedAt  = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $trialPeriod->createdAt  = new DateTimeImmutable($data['created_at']);

        return $trialPeriod;
    }

    public function serialize(): array
    {
        return [
            'trial_period_id' => $this->id->serialize(),
            'customer_id'     => $this->customerId->serialize(),
            'start_date'      => $this->startDate->format(DateTimeImmutable::ATOM),
            'end_date'        => $this->endDate->format(DateTimeImmutable::ATOM),
            'updated_at'      => self::serializeValueOrNull($this->updatedAt),
            'created_at'      => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}