<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Repository;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use App\Domain\SharedKernel\TrialPeriod\ValueObject\TrialPeriodId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Model\TrialPeriod;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class TrialPeriods
 *
 * @package App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TrialPeriods extends AbstractRepository
{
    /**
     * TrialPeriods constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = TrialPeriod::class;
        parent::__construct($entityManager);
    }

    /**
     * @param TrialPeriod $trialPeriod
     */
    public function add(TrialPeriod $trialPeriod): void
    {
        $this->register($trialPeriod);
    }

    public function save(): void
    {
        $this->apply();
    }

    /**
     * @param TrialPeriodId $trialPeriodId
     *
     * @return TrialPeriod
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findTrialPeriodByTrialPeriodId(TrialPeriodId $trialPeriodId): TrialPeriod
    {
        $qb = $this->repository
            ->createQueryBuilder('tp')
            ->where('tp.id = :trialPeriodId')
            ->setParameter('trialPeriodId', $trialPeriodId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param CustomerId $customerId
     *
     * @return TrialPeriod
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findTrialPeriodByCustomerId(CustomerId $customerId): TrialPeriod
    {
        $qb = $this->repository
            ->createQueryBuilder('tp')
            ->where('tp.customerId = :customerId')
            ->setParameter('customerId', $customerId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository
            ->createQueryBuilder('tp')
            ->orderBy('tp.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param CustomerId $customerId
     *
     * @return bool
     * @throws NonUniqueResultException
     */
    public function hasTrialPeriod(CustomerId $customerId): bool
    {
        return null !== $this->repository
                ->createQueryBuilder('tp')
                ->select('tp.id')
                ->where('tp.customerId = :customerId')
                ->setParameter('customerId', $customerId->serialize())
                ->getQuery()
                ->setHydrationMode(AbstractQuery::HYDRATE_ARRAY)
                ->getOneOrNullResult();
    }
}