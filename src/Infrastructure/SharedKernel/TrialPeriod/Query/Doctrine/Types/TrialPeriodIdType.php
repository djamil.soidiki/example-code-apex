<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Types;

use App\Domain\SharedKernel\TrialPeriod\ValueObject\TrialPeriodId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use Ramsey\Uuid\Uuid;

/**
 * Class TrialPeriodIdType
 *
 * @package App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TrialPeriodIdType extends Type
{
    const TRIAL_PERIOD_ID = 'trial_period__trial_period_id';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof TrialPeriodId) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', TrialPeriodId::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return TrialPeriodId|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof TrialPeriodId) {
            return $value;
        }

        try {
            $trialPeriodId = new TrialPeriodId($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                Uuid::VALID_PATTERN
            );
        }

        return $trialPeriodId;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::TRIAL_PERIOD_ID;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}