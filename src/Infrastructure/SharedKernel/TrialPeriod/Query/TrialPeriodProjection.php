<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\TrialPeriod\Query;

use App\Domain\SharedKernel\TrialPeriod\Event\TrialPeriodStarted;
use App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Model\TrialPeriod;
use App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Repository\TrialPeriods;
use Broadway\ReadModel\Projector;
use Exception;

/**
 * Class TrialPeriodProjection
 *
 * @package App\Infrastructure\SharedKernel\TrialPeriod\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TrialPeriodProjection extends Projector
{
    /**
     * @var TrialPeriods
     */
    private $trialPeriods;

    /**
     * TrialPeriodProjection constructor.
     *
     * @param TrialPeriods $trialPeriods
     */
    public function __construct(TrialPeriods $trialPeriods)
    {
        $this->trialPeriods = $trialPeriods;
    }

    /**
     * @param TrialPeriodStarted $event
     *
     * @throws Exception
     */
    protected function applyTrialPeriodStarted(TrialPeriodStarted $event): void
    {
        $trialPeriod = TrialPeriod::fromSerializable($event);

        $this->trialPeriods->add($trialPeriod);
    }
}