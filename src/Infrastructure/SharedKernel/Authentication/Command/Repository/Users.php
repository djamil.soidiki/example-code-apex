<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authentication\Command\Repository;

use App\Domain\SharedKernel\Authentication\User;
use App\Domain\SharedKernel\Authentication\Repository\UsersInterface;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

/**
 * Class User
 *
 * @package App\Infrastructure\SharedKernel\Authentication\Command\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Users implements UsersInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * User constructor.
     *
     * @param EventStore $eventStore
     * @param EventBus   $eventBus
     * @param array      $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            User::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * @inheritdoc
     */
    public function get(UserId $userId): ?User
    {
        /** @var User $user */
        $user = $this->eventSourcingRepository->load($userId->serialize());

        return $user;
    }

    /**
     * @inheritdoc
     */
    public function save(User $user): void
    {
        $this->eventSourcingRepository->save($user);
    }
}