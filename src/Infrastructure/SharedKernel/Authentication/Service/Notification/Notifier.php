<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authentication\Service\Notification;

use App\Domain\SharedKernel\Authentication\Entity\PasswordReminder;
use App\Domain\SharedKernel\Authentication\Service\Notification\NotifierInterface;
use App\Domain\SharedKernel\Authentication\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

/**
 * Class Notifier
 *
 * @package App\Infrastructure\SharedKernel\Authentication\Service\Notification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Notifier implements NotifierInterface
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * Notifier constructor.
     *
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @inheritDoc
     *
     * @throws TransportExceptionInterface
     */
    public function sendRegistrationNotification(User $user): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address(self::FROM_NO_REPLY, 'Reavant'))
            ->to($user->emailAddress()->serialize())
            ->subject('Welcome to Reavant House Services')
            ->htmlTemplate('authentication/_notification/registration_email_template.html.twig')
            ->context([
                          'name' => $user->emailAddress(),
                      ]
            );

        $this->mailer->send($email);
    }

    /**
     * @inheritDoc
     *
     * @throws TransportExceptionInterface
     */
    public function sendPasswordResetNotification(User $user, PasswordReminder $reminder): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address(self::FROM_NO_REPLY, 'Reavant'))
            ->to($user->emailAddress()->serialize())
            ->subject('Password Assistance')
            ->htmlTemplate('authentication/_notification/instructions_password_reset_email_template.html.twig')
            ->context([
                          'name' => $user->emailAddress(),
                          'token' => $reminder->token()
                      ]
            );

        $this->mailer->send($email);
    }
}