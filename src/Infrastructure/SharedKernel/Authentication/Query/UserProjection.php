<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authentication\Query;

use App\Domain\SharedKernel\Authentication\Event\UserPasswordChanged;
use App\Domain\SharedKernel\Authentication\Event\UserEmailAddressChanged;
use App\Domain\SharedKernel\Authentication\Event\UserPasswordReset;
use App\Domain\SharedKernel\Authentication\Event\PasswordReminderRequested;
use App\Domain\SharedKernel\Authentication\Event\UserRegistered;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\PasswordReminder;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\User;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Repository\PasswordReminders;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Repository\Users;
use Assert\AssertionFailedException;
use Broadway\ReadModel\Projector;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class UserProjection
 *
 * @package App\Infrastructure\SharedKernel\Authentication\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class UserProjection extends Projector
{
    /**
     * @var Users
     */
    private $users;

    /**
     * @var PasswordReminders
     */
    private $passwordReminders;

    /**
     * UserProjection constructor.
     *
     * @param Users             $users
     * @param PasswordReminders $passwordReminders
     */
    public function __construct(Users $users, PasswordReminders $passwordReminders)
    {
        $this->users             = $users;
        $this->passwordReminders = $passwordReminders;
    }

    /**
     * @param UserRegistered $event
     *
     * @throws AssertionFailedException
     */
    protected function applyUserRegistered(UserRegistered $event): void
    {
        $user = User::fromSerializable($event);

        $this->users->add($user);
    }

    /**
     * @param PasswordReminderRequested $event
     *
     * @throws Exception
     */
    protected function applyPasswordReminderRequested(PasswordReminderRequested $event): void
    {
        $passwordReminder = PasswordReminder::fromSerializable($event);

        $this->passwordReminders->add($passwordReminder);

        /** @var User $user */
        $user = $this->users->findUserByUserId($event->userId());

        $user->setUpdatedAt($event->createdAt());
        $this->users->apply();
    }

    /**
     * @param UserPasswordReset $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyUserPasswordReset(UserPasswordReset $event): void
    {
        /** @var User $user */
        $user = $this->users->findUserByUserId($event->userId());

        $user->setPassword($event->password());
        $user->setUpdatedAt($event->updatedAt());

        $this->users->apply();

        /** @var PasswordReminder $passwordReminder */
        $passwordReminder = $this->passwordReminders->findPasswordReminderByUserId($event->userId());

        $this->passwordReminders->delete($passwordReminder);
    }

    /**
     * @param UserEmailAddressChanged $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyUserEmailAddressChanged(UserEmailAddressChanged $event): void
    {
        /** @var User $user */
        $user = $this->users->findUserByUserId($event->userId());

        $user->setEmailAddress($event->emailAddress());
        $user->setUpdatedAt($event->updatedAt());

        $this->users->apply();
    }

    /**
     * @param UserPasswordChanged $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyUserPasswordChanged(UserPasswordChanged $event): void
    {
        /** @var User $user */
        $user = $this->users->findUserByUserId($event->userId());

        $user->setPassword($event->password());
        $user->setUpdatedAt($event->updatedAt());

        $this->users->apply();
    }
}