<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Types;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use Ramsey\Uuid\Uuid;

/**
 * Class UserIdType
 *
 * @package Namespace App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Types;
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class UserIdType extends Type
{
    const USER_ID = 'authentication__user_id';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof UserId) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', UserId::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return UserId|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof UserId) {
            return $value;
        }

        try {
            $userId = new UserId($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                Uuid::VALID_PATTERN
            );
        }

        return $userId;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::USER_ID;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}