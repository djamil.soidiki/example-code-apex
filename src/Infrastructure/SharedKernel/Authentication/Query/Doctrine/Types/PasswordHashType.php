<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Types;

use App\Domain\SharedKernel\Authentication\ValueObject\PasswordHash;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

/**
 * Class PasswordHashType
 *
 * @package App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Types;
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class PasswordHashType extends Type
{
    const PASSWORD_HASH = 'authentication__password_hash';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof PasswordHash) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', PasswordHash::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return PasswordHash|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof PasswordHash) {
            return $value;
        }

        try {
            $PasswordHash = new PasswordHash($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                self::PASSWORD_HASH
            );
        }

        return $PasswordHash;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::PASSWORD_HASH;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}

