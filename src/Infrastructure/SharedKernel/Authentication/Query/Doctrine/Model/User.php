<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model;

use App\Domain\SharedKernel\Authentication\ValueObject\PasswordHash;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Assert\AssertionFailedException;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class User
 *
 * @package App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model;
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class User implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var PasswordHash
     */
    private $password;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->userId->serialize();
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return PasswordHash
     */
    public function password(): PasswordHash
    {
        return $this->password;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param EmailAddress $emailAddress
     */
    public function setEmailAddress(EmailAddress $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @param PasswordHash $password
     */
    public function setPassword(PasswordHash $password): void
    {
        $this->password = $password;
    }

    /**
     * @param DateTimeImmutable $updatedAt
     */
    public function setUpdatedAt(DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws AssertionFailedException
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     *
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->userId       = new UserId($data['user_id']);
        $instance->emailAddress = new EmailAddress($data['email_address']);
        $instance->password     = new PasswordHash($data['password']);
        $instance->updatedAt    = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $instance->createdAt    = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'user_id'       => $this->userId->serialize(),
            'email_address' => $this->emailAddress->serialize(),
            'password'      => $this->password->serialize(),
            'updated_at'    => self::serializeValueOrNull($this->updatedAt),
            'created_at'    => $this->createdAt->format(DateTimeImmutable::ATOM),
        ];
    }
}