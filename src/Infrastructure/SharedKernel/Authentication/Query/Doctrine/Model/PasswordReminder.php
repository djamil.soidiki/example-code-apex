<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model;

use App\Domain\SharedKernel\Authentication\ValueObject\PasswordReminderId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Token\Token;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class PasswordReminder
 *
 * @package App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model;
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class PasswordReminder implements SerializableReadModel
{
    /**
     * @var PasswordReminderId
     */
    private $passwordReminderId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var Token
     */
    private $token;

    /**
     * @var DateTimeImmutable
     */
    private $expiresAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->passwordReminderId->serialize();
    }

    /**
     * @return PasswordReminderId
     */
    public function passwordReminderId(): PasswordReminderId
    {
        return $this->passwordReminderId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function expiresAt(): DateTimeImmutable
    {
        return $this->expiresAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @param array $data
     *
     * @return PasswordReminder|mixed
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->passwordReminderId = new PasswordReminderId($data['password_reminder_id']);
        $instance->userId             = new UserId($data['user_id']);
        $instance->token              = new Token($data['token']);
        $instance->expiresAt          = new DateTimeImmutable($data['expires_at']);
        $instance->createdAt          = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'password_reminder_id' => $this->passwordReminderId->serialize(),
            'user_id'              => $this->userId->serialize(),
            'token'                => $this->token->serialize(),
            'expires_at'           => $this->expiresAt->format(DateTimeImmutable::ATOM),
            'created_at'           => $this->createdAt->format(DateTimeImmutable::ATOM),
        ];
    }
}