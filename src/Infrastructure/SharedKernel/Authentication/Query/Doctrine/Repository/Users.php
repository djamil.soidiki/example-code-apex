<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Repository;

use App\Domain\SharedKernel\Authentication\Repository\Query\EmailAddressQueryInterface;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\User;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Users
 *
 * @package App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Users extends AbstractRepository implements EmailAddressQueryInterface
{
    /**
     * Users constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = User::class;
        parent::__construct($entityManager);
    }

    /**
     * @param User $user
     */
    public function add(User $user): void
    {
        $this->register($user);
    }

    /**
     * @param UserId $userId
     *
     * @return User
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findUserByUserId(UserId $userId): User
    {
        $qb = $this->repository
            ->createQueryBuilder('u')
            ->where('u.userId = :userId')
            ->setParameter('userId', $userId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository
            ->createQueryBuilder('u')
            ->orderBy('u.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param EmailAddress $emailAddress
     *
     * @return User
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findUserByEmailAddress(EmailAddress $emailAddress): User
    {
        $qb = $this->repository
            ->createQueryBuilder('u')
            ->where('u.emailAddress = :emailAddress')
            ->setParameter('emailAddress', $emailAddress->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     */
    public function isEmailAddressUnique(EmailAddress $emailAddress): bool
    {
        return null === $this->repository
                ->createQueryBuilder('u')
                ->select('u.userId')
                ->where('u.emailAddress = :email')
                ->setParameter('email', $emailAddress->serialize())
                ->getQuery()
                ->setHydrationMode(AbstractQuery::HYDRATE_ARRAY)
                ->getOneOrNullResult();
    }

    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     */
    public function existsEmailAddress(EmailAddress $emailAddress): ?UserId
    {
        return $this->repository
                   ->createQueryBuilder('u')
                   ->select('u.userId')
                   ->where('u.emailAddress = :email')
                   ->setParameter('email', $emailAddress->serialize())
                   ->getQuery()
                   ->setHydrationMode(AbstractQuery::HYDRATE_ARRAY)
                   ->getOneOrNullResult()['userId'] ?? null;
    }

    /**
     * @param EmailAddress $emailAddress
     *
     * @return array
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function getCredentialsByEmailAddress(EmailAddress $emailAddress): array
    {
        /** @var User $user */
        $user = $this->findUserByEmailAddress($emailAddress);

        return [
            $user->userId()->serialize(),
            $user->emailAddress()->serialize(),
            $user->password()->serialize()
        ];
    }
}