<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Repository;

use App\Domain\SharedKernel\Authentication\ValueObject\PasswordReminderId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Domain\SharedKernel\Generic\ValueObject\Token\Token;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\PasswordReminder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class PasswordReminders
 *
 * @package App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PasswordReminders extends AbstractRepository
{
    /**
     * PasswordReminders constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = PasswordReminder::class;
        parent::__construct($entityManager);
    }

    /**
     * @param PasswordReminder $passwordReminderModel
     */
    public function add(PasswordReminder $passwordReminderModel): void
    {
        $this->register($passwordReminderModel);
    }

    /**
     * @param PasswordReminderId $passwordReminderId
     *
     * @return PasswordReminder
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findPasswordReminderByReminderId(PasswordReminderId $passwordReminderId): PasswordReminder
    {
        $qb = $this->repository
            ->createQueryBuilder('pr')
            ->where('pr.passwordReminderId = :passwordReminderId')
            ->setParameter('passwordReminderId', $passwordReminderId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param UserId $userId
     *
     * @return PasswordReminder
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findPasswordReminderByUserId(UserId $userId): PasswordReminder
    {
        $qb = $this->repository
            ->createQueryBuilder('pr')
            ->where('pr.userId = :userId')
            ->setParameter('userId', $userId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param Token $token
     *
     * @return PasswordReminder
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findPasswordReminderByToken(Token $token): PasswordReminder
    {
        $qb = $this->repository
            ->createQueryBuilder('pr')
            ->where('pr.token = :token AND pr.expiresAt > :now')
            ->setParameter('token', $token->serialize())
            ->setParameter('now', new \DateTimeImmutable());

        return $this->oneOrException($qb);
    }
}