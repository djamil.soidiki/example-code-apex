<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Subscription\Command\Repository;

use App\Domain\SharedKernel\Billing\Subscription\Subscription;
use App\Domain\SharedKernel\Billing\Subscription\Repository\SubscriptionsInterface;
use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

/**
 * Class Subscriptions
 *
 * @package App\Infrastructure\SharedKernel\Billing\Subscription\Command\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Subscriptions implements SubscriptionsInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * Subscriptions constructor.
     *
     * @param EventStore $eventStore
     * @param EventBus   $eventBus
     * @param array      $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Subscription::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * @inheritdoc
     */
    public function get(SubscriptionId $subscriptionId): ?Subscription
    {
        /** @var Subscription $subscription */
        $subscription = $this->eventSourcingRepository->load($subscriptionId->serialize());

        return $subscription;
    }

    /**
     * @inheritdoc
     */
    public function save(Subscription $subscription): void
    {
        $this->eventSourcingRepository->save($subscription);
    }   
}