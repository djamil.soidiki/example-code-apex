<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Subscription\Query;

use App\Domain\SharedKernel\Billing\Subscription\Event\SubscribedToPlan;
use App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Model\Subscription;
use App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Repository\Subscriptions;
use Broadway\ReadModel\Projector;
use Exception;

/**
 * Class SubscriptionProjection
 *
 * @package App\Infrastructure\SharedKernel\Billing\Subscription\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class SubscriptionProjection extends Projector
{
    /**
     * @var Subscriptions
     */
    private $subscriptions;

    /**
     * SubscriptionProjection constructor.
     *
     * @param Subscriptions $subscriptions
     */
    public function __construct(Subscriptions $subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }

    /**
     * @param SubscribedToPlan $event
     *
     * @throws Exception
     */
    protected function applySubscribedToPlan(SubscribedToPlan $event): void
    {
        $subscription = Subscription::fromSerializable($event);

        $this->subscriptions->add($subscription);
    }
}