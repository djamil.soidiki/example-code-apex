<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Types;

use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use Ramsey\Uuid\Uuid;

/**
 * Class SubscriptionIdType
 *
 * @package App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class SubscriptionIdType extends Type
{
    const SUBSCRIPTION_ID = 'billing__subscription_id';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof SubscriptionId) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', SubscriptionId::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return SubscriptionId|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof SubscriptionId) {
            return $value;
        }

        try {
            $subscriptionId = new SubscriptionId($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                Uuid::VALID_PATTERN
            );
        }

        return $subscriptionId;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::SUBSCRIPTION_ID;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}