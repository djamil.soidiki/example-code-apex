<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Types;

use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionStatus;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

/**
 * Class SubscriptionStatusType
 *
 * @package App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class SubscriptionStatusType extends Type
{
    const SUBSCRIPTION_STATUS = 'billing__subscription_status';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof SubscriptionStatus) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', SubscriptionStatus::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return SubscriptionStatus|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof SubscriptionStatus) {
            return $value;
        }

        try {
            $subscriptionStatus = new SubscriptionStatus($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                self::SUBSCRIPTION_STATUS
            );
        }

        return $subscriptionStatus;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::SUBSCRIPTION_STATUS;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}

