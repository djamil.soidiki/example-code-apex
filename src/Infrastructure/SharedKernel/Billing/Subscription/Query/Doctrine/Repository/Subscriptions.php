<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Repository;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Model\Subscription;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Subscriptions
 *
 * @package App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Subscriptions extends AbstractRepository
{
    /**
     * Subscriptions constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Subscription::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Subscription $subscriptionModel
     */
    public function add(Subscription $subscriptionModel): void
    {
        $this->register($subscriptionModel);
    }

    /**
     * @param SubscriptionId $subscriptionId
     *
     * @return Subscription
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findSubscriptionBySubscriptionId(SubscriptionId $subscriptionId): Subscription
    {
        $qb = $this->repository
            ->createQueryBuilder('p')
            ->where('p.subscriptionId = :subscriptionId')
            ->setParameter('subscriptionId', $subscriptionId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository
            ->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param CustomerId $customerId
     *
     * @return Subscription
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findSubscriptionByCustomerId(CustomerId $customerId): Subscription
    {
        $qb = $this->repository
            ->createQueryBuilder('p')
            ->where('p.customerId = :customerId')
            ->setParameter('customerId', $customerId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param CustomerId $customerId
     *
     * @return bool
     * @throws NonUniqueResultException
     */
    public function isCustomerSubscribed(CustomerId $customerId): bool
    {
        return null !== $this->repository
                ->createQueryBuilder('p')
                ->select('p.subscriptionId')
                ->where('p.customerId = :customerId')
                ->setParameter('customerId', $customerId->serialize())
                ->getQuery()
                ->setHydrationMode(AbstractQuery::HYDRATE_ARRAY)
                ->getOneOrNullResult();
    }
}