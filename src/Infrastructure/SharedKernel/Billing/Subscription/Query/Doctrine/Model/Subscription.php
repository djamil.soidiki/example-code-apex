<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Model;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionId;
use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionStatus;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Subscription
 *
 * @package App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Subscription implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var SubscriptionId
     */
    private $subscriptionId;

    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var PlanId
     */
    private $planId;

    /**
     * @var DateTimeImmutable
     */
    private $startDate;

    /**
     * @var SubscriptionStatus
     */
    private $status;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->subscriptionId->serialize();
    }

    /**
     * @return SubscriptionId
     */
    public function subscriptionId(): SubscriptionId
    {
        return $this->subscriptionId;
    }

    /**
     * @return CustomerId
     */
    public function customerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return PlanId
     */
    public function planId(): PlanId
    {
        return $this->planId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function startDate(): DateTimeImmutable
    {
        return $this->startDate;
    }

    /**
     * @return SubscriptionStatus
     */
    public function status(): SubscriptionStatus
    {
        return $this->status;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->subscriptionId = new SubscriptionId($data['subscription_id']);
        $instance->customerId     = new CustomerId($data['customer_id']);
        $instance->planId         = new PlanId($data['plan_id']);
        $instance->startDate      = new DateTimeImmutable($data['start_date']);
        $instance->status         = new SubscriptionStatus($data['status']);
        $instance->updatedAt      = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $instance->createdAt      = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'subscription_id' => $this->subscriptionId->serialize(),
            'customer_id'     => $this->customerId->serialize(),
            'plan_id'         => $this->planId->serialize(),
            'start_date'      => $this->startDate->format(DateTimeImmutable::ATOM),
            'status'          => $this->status->serialize(),
            'updated_at'      => self::serializeValueOrNull($this->updatedAt),
            'created_at'      => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}