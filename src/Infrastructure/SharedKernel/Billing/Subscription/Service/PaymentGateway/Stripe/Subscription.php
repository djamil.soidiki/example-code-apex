<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Subscription\Service\PaymentGateway\Stripe;

use App\Application\TrialPeriod\Query\GetTrialPeriodByCustomerId;
use App\Application\TrialPeriod\Query\IsCustomerOnTrialPeriod;
use App\Domain\SharedKernel\Billing\Subscription\Event\SubscribedToPlan;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Model\TrialPeriod;
use Stripe\Exception\ApiErrorException;
use Stripe\Stripe;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class Subscription
 *
 * @package App\Infrastructure\SharedKernel\Billing\Subscription\Service\PaymentGateway\Stripe
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Subscription
{
    use BusTrait;

    /**
     * Subscription constructor.
     *
     * @param string              $stripeSecretKey
     * @param MessageBusInterface $queryBus
     */
    public function __construct(string $stripeSecretKey, MessageBusInterface $queryBus)
    {
        Stripe::setApiKey($stripeSecretKey);
        $this->queryBus = $queryBus;
    }

    /**
     * @param SubscribedToPlan $subscriptionCreated
     *
     * @return array
     * @throws ApiErrorException
     */
    public function create(SubscribedToPlan $subscriptionCreated): array
    {
        $params = [
            'customer' => $subscriptionCreated->customerId()->serialize(),
            'items'    => [
                [
                    'plan' => $subscriptionCreated->planId()->serialize()
                ]
            ],
            'expand'   => ['latest_invoice.payment_intent']
        ];

        try {
            /** @var TrialPeriod $trialPeriod */
            $trialPeriod = $this->handleQuery(new GetTrialPeriodByCustomerId($subscriptionCreated->customerId()->serialize()));

            $isTrialPeriodActive = $this->handleQuery(new IsCustomerOnTrialPeriod($subscriptionCreated->customerId()->serialize()));

            if ($isTrialPeriodActive) {
                $daysRemaining               = $trialPeriod->endDate()->diff(new \DateTimeImmutable())->days;
                $params['trial_period_days'] = $daysRemaining;
            }
        } catch (HandlerFailedException $exception) {
        }

        $subscription = \Stripe\Subscription::create($params);

        return $subscription->serializeParameters();
    }
}