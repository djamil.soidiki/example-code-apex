<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Subscription\Service\PaymentGateway;

use App\Domain\SharedKernel\Billing\Subscription\Event\SubscribedToPlan;
use App\Domain\SharedKernel\Billing\Subscription\Service\PaymentGatewayInterface;
use App\Infrastructure\SharedKernel\Billing\Subscription\Service\PaymentGateway\Stripe\Subscription;
use Stripe\Exception\ApiErrorException;

/**
 * Class PaymentGateway
 *
 * @package App\Infrastructure\SharedKernel\Billing\Subscription\Service\PaymentGateway
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PaymentGateway implements PaymentGatewayInterface
{
    /**
     * @var Subscription
     */
    private $subscriptionStripe;

    /**
     * PaymentGateway constructor.
     *
     * @param Subscription $subscriptionStripe
     */
    public function __construct(Subscription $subscriptionStripe)
    {
        $this->subscriptionStripe = $subscriptionStripe;
    }

    /**
     * @inheritDoc
     * @throws ApiErrorException
     */
    public function createSubscription(SubscribedToPlan $subscriptionCreated): array
    {
        return $this->subscriptionStripe->create($subscriptionCreated);
    }
}