<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Customer\Command\Repository;

use App\Domain\SharedKernel\Billing\Customer\Customer;
use App\Domain\SharedKernel\Billing\Customer\Repository\CustomersInterface;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

/**
 * Class Customers
 *
 * @package App\Infrastructure\SharedKernel\Billing\Customer\Command\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Customers implements CustomersInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * Customers constructor.
     *
     * @param EventStore $eventStore
     * @param EventBus   $eventBus
     * @param array      $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Customer::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * @inheritdoc
     */
    public function get(CustomerId $customerId): ?Customer
    {
        /** @var Customer $customer */
        $customer = $this->eventSourcingRepository->load($customerId->serialize());

        return $customer;
    }

    /**
     * @inheritdoc
     */
    public function save(Customer $customer): void
    {
        $this->eventSourcingRepository->save($customer);
    }   
}