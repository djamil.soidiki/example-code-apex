<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Customer\Service\PaymentGateway;

use App\Domain\SharedKernel\Billing\Customer\Event\CustomerRegistered;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerEmailAddressChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerFullNameChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerPaymentMethodChanged;
use App\Domain\SharedKernel\Billing\Customer\Service\PaymentGatewayInterface;
use App\Infrastructure\SharedKernel\Billing\Customer\Service\PaymentGateway\Stripe\Customer;
use Stripe\Exception\ApiErrorException;

/**
 * Class PaymentGateway
 *
 * @package App\Infrastructure\SharedKernel\Billing\Customer\Service\PaymentGateway
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PaymentGateway implements PaymentGatewayInterface
{
    /**
     * @var Customer
     */
    private $customerStripe;

    /**
     * PaymentGateway constructor.
     *
     * @param Customer $customerStripe
     */
    public function __construct(Customer $customerStripe)
    {
        $this->customerStripe = $customerStripe;
    }

    /**
     * @inheritDoc
     * @throws ApiErrorException
     */
    public function createCustomer(CustomerRegistered $customerRegistered): array
    {
        return $this->customerStripe->create($customerRegistered);
    }

    /**
     * @inheritDoc
     * @throws ApiErrorException
     */
    public function updatePaymentMethod(CustomerPaymentMethodChanged $paymentMethodChanged): array
    {
        return $this->customerStripe->updatePaymentMethod($paymentMethodChanged);
    }

    /**
     * @inheritDoc
     * @throws ApiErrorException
     */
    public function updateName(CustomerFullNameChanged $fullNameChanged): array
    {
        return $this->customerStripe->updateName($fullNameChanged);
    }

    /**
     * @inheritDoc
     * @throws ApiErrorException
     */
    public function updateEmail(CustomerEmailAddressChanged $emailAddressChanged): array
    {
        return $this->customerStripe->updateEmail($emailAddressChanged);
    }
}