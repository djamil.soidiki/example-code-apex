<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Customer\Service\PaymentGateway\Stripe;

use App\Domain\SharedKernel\Billing\Customer\Event\CustomerRegistered;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerEmailAddressChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerFullNameChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerPaymentMethodChanged;
use Stripe\Exception\ApiErrorException;
use Stripe\Stripe;

/**
 * Class Customer
 *
 * @package App\Infrastructure\SharedKernel\Billing\Customer\Service\PaymentGateway\Stripe
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Customer
{
    /**
     * Customer constructor.
     *
     * @param string $stripeSecretKey
     */
    public function __construct(string $stripeSecretKey)
    {
        Stripe::setApiKey($stripeSecretKey);
    }

    /**
     * @param CustomerRegistered $customerRegistered
     *
     * @return array
     * @throws ApiErrorException
     */
    public function create(CustomerRegistered $customerRegistered): array
    {
        $params = [
            'id'    => $customerRegistered->customerId()->serialize(),
            'email' => $customerRegistered->emailAddress()->serialize(),
            'name'  => (string)$customerRegistered->fullName()
        ];

        if ($customerRegistered->paymentMethod() !== null) {
            $params['payment_method']   = $customerRegistered->paymentMethod()->serialize();
            $params['invoice_settings'] = [
                'default_payment_method' => $customerRegistered->paymentMethod()->serialize()
            ];
        }

        $customer = \Stripe\Customer::create($params);

        return $customer->serializeParameters();
    }

    /**
     * @param CustomerPaymentMethodChanged $customerPaymentMethodChanged
     *
     * @return array
     * @throws ApiErrorException
     */
    public function updatePaymentMethod(CustomerPaymentMethodChanged $customerPaymentMethodChanged): array
    {
        $payment_method = \Stripe\PaymentMethod::retrieve($customerPaymentMethodChanged->paymentMethod()->serialize());
        $payment_method->attach(['customer' => $customerPaymentMethodChanged->customerId()->serialize()]);

        $customer = \Stripe\Customer::update(
            $customerPaymentMethodChanged->customerId()->serialize(),
            [
                'invoice_settings' => [
                    'default_payment_method' => $customerPaymentMethodChanged->paymentMethod()->serialize()
                ]
            ]
        );

        return $customer->serializeParameters();
    }

    /**
     * @param CustomerFullNameChanged $fullNameChanged
     *
     * @return array
     * @throws ApiErrorException
     */
    public function updateName(CustomerFullNameChanged $fullNameChanged): array
    {
        $customer = \Stripe\Customer::update(
            $fullNameChanged->customerId()->serialize(),
            [
                'name' => (string)$fullNameChanged->fullName()
            ]
        );

        return $customer->serializeParameters();
    }

    /**
     * @param CustomerEmailAddressChanged $emailAddressChanged
     *
     * @return array
     * @throws ApiErrorException
     */
    public function updateEmail(CustomerEmailAddressChanged $emailAddressChanged): array
    {
        $customer = \Stripe\Customer::update(
            $emailAddressChanged->customerId()->serialize(),
            [
                'email' => $emailAddressChanged->emailAddress()->serialize()
            ]
        );

        return $customer->serializeParameters();
    }
}