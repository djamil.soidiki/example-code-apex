<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Repository;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Billing\Customer\Repository\Query\UserIdQueryInterface;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Customers
 *
 * @package App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Customers  extends AbstractRepository implements UserIdQueryInterface
{
    /**
     * Customers constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Customer::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Customer $customerModel
     */
    public function add(Customer $customerModel): void
    {
        $this->register($customerModel);
    }

    /**
     * @param CustomerId $customerId
     *
     * @return Customer
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findCustomerByCustomerId(CustomerId $customerId): Customer
    {
        $qb = $this->repository
            ->createQueryBuilder('c')
            ->where('c.customerId = :customerId')
            ->setParameter('customerId', $customerId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository
            ->createQueryBuilder('c')
            ->orderBy('c.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param UserId $userId
     *
     * @return Customer
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findCustomerByUserId(UserId $userId): Customer
    {
        $qb = $this->repository
            ->createQueryBuilder('c')
            ->where('c.userId = :userId')
            ->setParameter('userId', $userId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     */
    public function findCustomerIdByUserId(UserId $userId): ?CustomerId
    {
        return $this->repository
                   ->createQueryBuilder('c')
                   ->select('c.customerId')
                   ->where('c.userId = :userId')
                   ->setParameter('userId', $userId->serialize())
                   ->getQuery()
                   ->setHydrationMode(AbstractQuery::HYDRATE_ARRAY)
                   ->getOneOrNullResult()['customerId'] ?? null;
    }


    /**
     * @param UserId $userId
     *
     * @return bool
     * @throws NonUniqueResultException
     */
    public function isUserACustomer(UserId $userId)
    {
        return null !== $this->repository
                ->createQueryBuilder('c')
                ->select('c.customerId')
                ->where('c.userId = :userId')
                ->setParameter('userId', $userId->serialize())
                ->getQuery()
                ->setHydrationMode(AbstractQuery::HYDRATE_ARRAY)
                ->getOneOrNullResult();
    }
}