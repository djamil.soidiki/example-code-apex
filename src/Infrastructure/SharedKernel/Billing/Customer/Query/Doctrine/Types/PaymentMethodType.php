<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Types;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\PaymentMethod;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

/**
 * Class PaymentMethodType
 *
 * @package App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class PaymentMethodType extends Type
{
    const PAYMENT_METHOD = 'billing__payment_method';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof PaymentMethod) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', PaymentMethod::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return PaymentMethod|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof PaymentMethod) {
            return $value;
        }

        try {
            $paymentMethod = new PaymentMethod($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                self::PAYMENT_METHOD
            );
        }

        return $paymentMethod;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::PAYMENT_METHOD;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}