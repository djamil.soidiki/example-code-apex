<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\PaymentMethod;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Customer
 *
 * @package App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Customer implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var PaymentMethod|null
     */
    private $paymentMethod;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var FullName
     */
    private $fullName;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->customerId->serialize();
    }

    /**
     * @return CustomerId
     */
    public function customerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return PaymentMethod|null
     */
    public function paymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return FullName
     */
    public function fullName(): FullName
    {
        return $this->fullName;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param EmailAddress $emailAddress
     */
    public function setEmailAddress(EmailAddress $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @param FullName $fullName
     */
    public function setFullName(FullName $fullName): void
    {
        $this->fullName = $fullName;
    }

    /**
     * @param PaymentMethod|null $paymentMethod
     */
    public function setPaymentMethod(?PaymentMethod $paymentMethod): void
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @param DateTimeImmutable|null $updatedAt
     */
    public function setUpdatedAt(?DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->customerId    = new CustomerId($data['customer_id']);
        $instance->userId        = new UserId($data['user_id']);
        $instance->paymentMethod = self::deserializeValueOrNull($data, 'payment_method', PaymentMethod::class);
        $instance->emailAddress  = new EmailAddress($data['email_address']);
        $instance->fullName      = new FullName($data['full_name']['first_name'], $data['full_name']['last_name']);
        $instance->updatedAt     = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $instance->createdAt     = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'customer_id'    => $this->customerId->serialize(),
            'user_id'        => $this->userId->serialize(),
            'payment_method' => self::serializeValueOrNull($this->paymentMethod),
            'email_address'  => $this->emailAddress->serialize(),
            'full_name'      => $this->fullName->serialize(),
            'updated_at'     => self::serializeValueOrNull($this->updatedAt),
            'created_at'     => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}