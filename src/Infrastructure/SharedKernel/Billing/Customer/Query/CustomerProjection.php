<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Customer\Query;

use App\Domain\SharedKernel\Billing\Customer\Event\CustomerRegistered;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerEmailAddressChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerFullNameChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerPaymentMethodChanged;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Repository\Customers;
use Broadway\ReadModel\Projector;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class CustomerProjection
 *
 * @package App\Infrastructure\SharedKernel\Billing\Customer\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CustomerProjection extends Projector
{
    /**
     * @var Customers
     */
    private $customers;

    /**
     * CustomerProjection constructor.
     *
     * @param Customers $customers
     */
    public function __construct(Customers $customers)
    {
        $this->customers = $customers;
    }

    /**
     * @param CustomerRegistered $event
     *
     * @throws Exception
     */
    protected function applyCustomerRegistered(CustomerRegistered $event): void
    {
        $customer = Customer::fromSerializable($event);

        $this->customers->add($customer);
    }

    /**
     * @param CustomerPaymentMethodChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyCustomerPaymentMethodChanged(CustomerPaymentMethodChanged $event): void
    {
        /** @var Customer $customer */
        $customer = $this->customers->findCustomerByCustomerId($event->customerId());

        $customer->setPaymentMethod($event->paymentMethod());
        $customer->setUpdatedAt($event->updatedAt());

        $this->customers->apply();
    }

    /**
     * @param CustomerFullNameChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyCustomerFullNameChanged(CustomerFullNameChanged $event): void
    {
        /** @var Customer $customer */
        $customer = $this->customers->findCustomerByCustomerId($event->customerId());

        $customer->setFullName($event->fullName());
        $customer->setUpdatedAt($event->updatedAt());

        $this->customers->apply();
    }

    /**
     * @param CustomerEmailAddressChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyCustomerEmailAddressChanged(CustomerEmailAddressChanged $event): void
    {
        /** @var Customer $customer */
        $customer = $this->customers->findCustomerByCustomerId($event->customerId());

        $customer->setEmailAddress($event->emailAddress());
        $customer->setUpdatedAt($event->updatedAt());

        $this->customers->apply();
    }
}