<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Plan\Service\PaymentGateway;

use App\Domain\SharedKernel\Billing\Plan\Event\PlanCreated;
use App\Domain\SharedKernel\Billing\Plan\Service\PaymentGatewayInterface;
use App\Infrastructure\SharedKernel\Billing\Plan\Service\PaymentGateway\Stripe\Plan;
use Stripe\Exception\ApiErrorException;

/**
 * Class PaymentGateway
 *
 * @package App\Infrastructure\SharedKernel\Billing\Plan\Service\PaymentGateway
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PaymentGateway implements PaymentGatewayInterface
{
    /**
     * @var Plan
     */
    private $planStripe;

    /**
     * PaymentGateway constructor.
     *
     * @param Plan $planStripe
     */
    public function __construct(Plan $planStripe)
    {
        $this->planStripe = $planStripe;
    }

    /**
     * @inheritDoc
     * @throws ApiErrorException
     */
    public function createPlan(PlanCreated $planCreated): array
    {
        return $this->planStripe->create($planCreated);
    }
}