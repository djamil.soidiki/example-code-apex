<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Plan\Service\PaymentGateway\Stripe;

use App\Domain\SharedKernel\Billing\Plan\Event\PlanCreated;
use Stripe\Exception\ApiErrorException;
use Stripe\Stripe;

/**
 * Class Plan
 *
 * @package App\Infrastructure\SharedKernel\Billing\Plan\Service\PaymentGateway\Stripe
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Plan
{
    /**
     * Plan constructor.
     *
     * @param string $stripeSecretKey
     */
    public function __construct(string $stripeSecretKey)
    {
        Stripe::setApiKey($stripeSecretKey);
    }

    /**
     * @param PlanCreated $planCreated
     *
     * @return array
     * @throws ApiErrorException
     */
    public function create(PlanCreated $planCreated): array
    {
        $Plan = \Stripe\Plan::create(
            [
                'id'       => $planCreated->planId()->serialize(),
                'amount'   => ($planCreated->price()->amount()),
                'currency' => strtolower($planCreated->price()->currency()->serialize()),
                'interval' => strtolower($planCreated->billingCycle()->serialize()),
                'product'  => $planCreated->productId()->serialize()
            ]
        );

        return $Plan->serializeParameters();
    }
}