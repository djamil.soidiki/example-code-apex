<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Plan\Command\Repository;

use App\Domain\SharedKernel\Billing\Plan\Plan;
use App\Domain\SharedKernel\Billing\Plan\Repository\PlansInterface;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

/**
 * Class Plans
 *
 * @package App\Infrastructure\SharedKernel\Billing\Plan\Command\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Plans implements PlansInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * Plans constructor.
     *
     * @param EventStore $eventStore
     * @param EventBus   $eventBus
     * @param array      $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Plan::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * @inheritdoc
     */
    public function get(PlanId $planId): ?Plan
    {
        /** @var Plan $plan */
        $plan = $this->eventSourcingRepository->load($planId->serialize());

        return $plan;
    }

    /**
     * @inheritdoc
     */
    public function save(Plan $plan): void
    {
        $this->eventSourcingRepository->save($plan);
    }   
}