<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Model;

use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanBillingCycle;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanName;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanProductId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Plan
 *
 * @package App\Infrastructure\SharedKernel\Billing\Plan\Query\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Plan implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var PlanId
     */
    private $planId;

    /**
     * @var PlanProductId
     */
    private $productId;

    /**
     * @var PlanName
     */
    private $name;

    /**
     * @var Money
     */
    private $price;

    /**
     * @var PlanBillingCycle
     */
    private $billingCycle;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->planId->serialize();
    }

    /**
     * @return PlanId
     */
    public function planId(): PlanId
    {
        return $this->planId;
    }

    /**
     * @return PlanProductId
     */
    public function productId(): PlanProductId
    {
        return $this->productId;
    }

    /**
     * @return PlanName
     */
    public function name(): PlanName
    {
        return $this->name;
    }

    /**
     * @return Money
     */
    public function price(): Money
    {
        return $this->price;
    }

    /**
     * @return PlanBillingCycle
     */
    public function billingCycle(): PlanBillingCycle
    {
        return $this->billingCycle;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->planId       = new PlanId($data['plan_id']);
        $instance->productId    = new PlanProductId($data['product_id']);
        $instance->name         = new PlanName($data['name']);
        $instance->price        = new Money($data['price']['amount'], $data['price']['currency']);
        $instance->billingCycle = new PlanBillingCycle($data['billing_cycle']);
        $instance->updatedAt    = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $instance->createdAt    = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'plan_id'       => $this->planId->serialize(),
            'product_id'    => $this->productId->serialize(),
            'name'          => $this->name->serialize(),
            'price'         => $this->price->serialize(),
            'billing_cycle' => $this->billingCycle->serialize(),
            'updated_at'    => self::serializeValueOrNull($this->updatedAt),
            'created_at'    => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}