<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Repository;

use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Model\Plan;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Plans
 *
 * @package App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Plans extends AbstractRepository
{
    /**
     * Plans constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Plan::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Plan $planModel
     */
    public function add(Plan $planModel): void
    {
        $this->register($planModel);
    }

    /**
     * @param PlanId $planId
     *
     * @return Plan
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findOneByPlanId(PlanId $planId): Plan
    {
        $qb = $this->repository
            ->createQueryBuilder('p')
            ->where('p.planId = :planId')
            ->setParameter('planId', $planId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository
            ->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}