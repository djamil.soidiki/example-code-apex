<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Types;

use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanName;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

/**
 * Class PlanNameType
 *
 * @package App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class PlanNameType extends Type
{
    const PLAN_NAME = 'billing__plan_name';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof PlanName) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', PlanName::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return PlanName|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof PlanName) {
            return $value;
        }

        try {
            $planName = new PlanName($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                self::PLAN_NAME
            );
        }

        return $planName;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::PLAN_NAME;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}

