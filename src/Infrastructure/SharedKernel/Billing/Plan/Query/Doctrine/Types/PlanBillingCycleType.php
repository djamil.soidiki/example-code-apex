<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Types;

use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanBillingCycle;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

/**
 * Class PlanBillingCycleType
 *
 * @package App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class PlanBillingCycleType extends Type
{
    const BILLING_CYCLE = 'billing__plan_billing_cycle';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof PlanBillingCycle) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', PlanBillingCycle::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return PlanBillingCycle|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof PlanBillingCycle) {
            return $value;
        }

        try {
            $planInterval = new PlanBillingCycle($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                self::BILLING_CYCLE
            );
        }

        return $planInterval;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::BILLING_CYCLE;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}

