<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Billing\Plan\Query;

use App\Domain\SharedKernel\Billing\Plan\Event\PlanCreated;
use App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Model\Plan;
use App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Repository\Plans;
use Broadway\ReadModel\Projector;
use Exception;

/**
 * Class PlanProjection
 *
 * @package App\Infrastructure\SharedKernel\Billing\Plan\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PlanProjection extends Projector
{
    /**
     * @var Plans
     */
    private $planRepository;

    /**
     * PlanProjection constructor.
     *
     * @param Plans $planRepository
     */
    public function __construct(Plans $planRepository)
    {
        $this->planRepository = $planRepository;
    }

    /**
     * @param PlanCreated $event
     *
     * @throws Exception
     */
    protected function applyPlanCreated(PlanCreated $event): void
    {
        $planModel = Plan::fromSerializable($event);

        $this->planRepository->add($planModel);
    }
}