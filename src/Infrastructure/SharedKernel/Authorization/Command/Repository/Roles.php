<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authorization\Command\Repository;

use App\Domain\SharedKernel\Authorization\Role;
use App\Domain\SharedKernel\Authorization\Repository\RolesInterface;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventSourcing\EventStreamDecorator;
use Broadway\EventStore\EventStore;

/**
 * Class Role
 *
 * @package App\Infrastructure\SharedKernel\Authorization\Command\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Roles implements RolesInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * Roles constructor.
     *
     * @param EventStore           $eventStore
     * @param EventBus             $eventBus
     * @param EventStreamDecorator $decorator
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, EventStreamDecorator $decorator)
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Role::class,
            new PublicConstructorAggregateFactory(),
            [$decorator]
        );
    }

    /**
     * @inheritdoc
     */
    public function get(RoleId $roleId): ?Role
    {
        /** @var Role $role */
        $role = $this->eventSourcingRepository->load($roleId->serialize());

        return $role;
    }

    /**
     * @inheritdoc
     */
    public function save(Role $role): void
    {
        $this->eventSourcingRepository->save($role);
    }
}