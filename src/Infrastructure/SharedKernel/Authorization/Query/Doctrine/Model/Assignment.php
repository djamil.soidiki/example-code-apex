<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authorization\ValueObject\AssignmentId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Assignment
 *
 * @package App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Assignment implements SerializableReadModel
{
    /**
     * @var AssignmentId
     */
    private $assignmentId;

    /**
     * @var RoleId
     */
    private $roleId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->assignmentId->serialize();
    }

    /**
     * @return AssignmentId
     */
    public function assignmentId(): AssignmentId
    {
        return $this->assignmentId;
    }

    /**
     * @return RoleId
     */
    public function roleId(): RoleId
    {
        return $this->roleId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @param array $data
     *
     * @return Assignment|mixed
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->assignmentId = new AssignmentId($data['assignment_id']);
        $instance->roleId       = new RoleId($data['role_id']);
        $instance->userId       = new UserId($data['user_id']);
        $instance->createdAt    = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'assignment_id' => $this->assignmentId->serialize(),
            'role_id'       => $this->roleId->serialize(),
            'user_id'       => $this->userId->serialize(),
            'created_at'    => $this->createdAt->format(DateTimeImmutable::ATOM),
        ];
    }
}