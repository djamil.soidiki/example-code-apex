<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model;

use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleName;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Role
 *
 * @package App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model;
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Role implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var RoleId
     */
    private $roleId;

    /**
     * @var RoleName
     */
    private $name;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->roleId->serialize();
    }

    /**
     * @return RoleId
     */
    public function roleId(): RoleId
    {
        return $this->roleId;
    }

    /**
     * @return RoleName
     */
    public function name(): RoleName
    {
        return $this->name;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeImmutable $updatedAt
     */
    public function setUpdatedAt(DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $instance = new self();

        $instance->roleId    = new RoleId($data['role_id']);
        $instance->name      = new RoleName($data['name']);
        $instance->updatedAt = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $instance->createdAt = new DateTimeImmutable($data['created_at']);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'role_id'    => $this->roleId->serialize(),
            'name'       => $this->name->serialize(),
            'updated_at' => self::serializeValueOrNull($this->updatedAt),
            'created_at' => $this->createdAt->format(DateTimeImmutable::ATOM),
        ];
    }
}