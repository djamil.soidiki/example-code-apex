<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Types;

use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use Ramsey\Uuid\Uuid;

/**
 * Class RoleIdType
 *
 * @package Namespace App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Types;
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class RoleIdType extends Type
{
    const ROLE_ID = 'authorization__role_id';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof RoleId) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', RoleId::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return RoleId|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof RoleId) {
            return $value;
        }

        try {
            $roleId = new RoleId($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                Uuid::VALID_PATTERN
            );
        }

        return $roleId;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::ROLE_ID;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}