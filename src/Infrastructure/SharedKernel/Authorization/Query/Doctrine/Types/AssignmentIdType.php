<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Types;

use App\Domain\SharedKernel\Authorization\ValueObject\AssignmentId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use Ramsey\Uuid\Uuid;

/**
 * Class AssignmentIdType
 *
 * @package Namespace App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Types;
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class AssignmentIdType extends Type
{
    const ASSIGNMENT_ID = 'authorization__assignment_id';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof AssignmentId) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', AssignmentId::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return AssignmentId|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof AssignmentId) {
            return $value;
        }

        try {
            $assignmentId = new AssignmentId($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                Uuid::VALID_PATTERN
            );
        }

        return $assignmentId;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::ASSIGNMENT_ID;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}