<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Repository;

use App\Domain\SharedKernel\Authorization\Repository\Query\RoleNameQueryInterface;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleName;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model\Role;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Roles
 *
 * @package App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Roles extends AbstractRepository implements RoleNameQueryInterface
{
    /**
     * Roles constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Role::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Role $role
     */
    public function add(Role $role): void
    {
        $this->register($role);
    }

    /**
     * @param RoleId $roleId
     *
     * @return Role
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findRoleByRoleId(RoleId $roleId): Role
    {
        $qb = $this->repository
            ->createQueryBuilder('r')
            ->where('r.roleId = :roleId')
            ->setParameter('roleId', $roleId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository
            ->createQueryBuilder('r')
            ->orderBy('r.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     */
    public function isRoleNameUnique(RoleName $roleName): bool
    {
        return null === $this->repository
                ->createQueryBuilder('r')
                ->select('r.roleId')
                ->where('r.name = :name')
                ->setParameter('name', $roleName->serialize())
                ->getQuery()
                ->setHydrationMode(AbstractQuery::HYDRATE_ARRAY)
                ->getOneOrNullResult();
    }
}