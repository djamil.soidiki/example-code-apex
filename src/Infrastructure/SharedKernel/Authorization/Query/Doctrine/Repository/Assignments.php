<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Repository;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authorization\ValueObject\AssignmentId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model\Assignment;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Assignments
 *
 * @package App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Assignments extends AbstractRepository
{
    /**
     * Assignments constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Assignment::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Assignment $assignment
     */
    public function add(Assignment $assignment): void
    {
        $this->register($assignment);
    }

    /**
     * @param AssignmentId $assignmentId
     *
     * @return Assignment
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findAssignmentByAssignmentId(AssignmentId $assignmentId): Assignment
    {
        $qb = $this->repository
            ->createQueryBuilder('a')
            ->where('a.assignmentId = :assignmentId')
            ->setParameter('assignmentId', $assignmentId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param RoleId $roleId
     * @param UserId $userId
     *
     * @return Assignment
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findAssignmentByRoleIdAndUserId(RoleId $roleId, UserId $userId): Assignment
    {
        $qb = $this->repository
            ->createQueryBuilder('a')
            ->where('a.roleId = :roleId AND a.userId = :userId')
            ->setParameter('roleId', $roleId->serialize())
            ->setParameter('userId', $userId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param UserId $userId
     *
     * @return array
     */
    public function findAssignmentsByUserId(UserId $userId): array
    {
        return $this->repository
            ->createQueryBuilder('a')
            ->where('a.userId = :userId')
            ->orderBy('a.createdAt', 'DESC')
            ->setParameter('userId', $userId->serialize())
            ->getQuery()
            ->getResult();
    }
}