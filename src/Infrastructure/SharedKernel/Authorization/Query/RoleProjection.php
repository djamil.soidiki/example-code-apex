<?php

declare(strict_types=1);

namespace App\Infrastructure\SharedKernel\Authorization\Query;

use App\Domain\SharedKernel\Authorization\Event\RoleAssignedToUser;
use App\Domain\SharedKernel\Authorization\Event\RoleCreated;
use App\Domain\SharedKernel\Authorization\Event\RoleRevokedFromUser;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model\Assignment;
use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model\Role;
use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Repository\Assignments;
use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Repository\Roles;
use Broadway\ReadModel\Projector;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class RoleProjection
 *
 * @package App\Infrastructure\SharedKernel\Authorization\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RoleProjection extends Projector
{
    /**
     * @var Roles
     */
    private $roles;

    /**
     * @var Assignments
     */
    private $assignments;

    /**
     * RoleProjection constructor.
     *
     * @param Roles             $roles
     * @param Assignments $assignments
     */
    public function __construct(Roles $roles, Assignments $assignments)
    {
        $this->roles             = $roles;
        $this->assignments = $assignments;
    }

    /**
     * @param RoleCreated $event
     *
     * @throws Exception
     */
    protected function applyRoleCreated(RoleCreated $event): void
    {
        $role = Role::fromSerializable($event);

        $this->roles->add($role);
    }

    /**
     * @param RoleAssignedToUser $event
     *
     * @throws Exception
     */
    protected function applyRoleAssignedToUser(RoleAssignedToUser $event): void
    {
        $assignment = Assignment::fromSerializable($event);

        $this->assignments->add($assignment);
    }

    /**
     * @param RoleRevokedFromUser $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyRoleRevokedFromUser(RoleRevokedFromUser $event): void
    {
        $assignment = $this->assignments->findAssignmentByRoleIdAndUserId($event->roleId(), $event->userId());

        $this->assignments->delete($assignment);
    }
}