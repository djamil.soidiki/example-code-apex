<?php

declare(strict_types=1);

namespace App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Analysis
 *
 * @package App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Analysis implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var AnalysisId
     */
    private $id;

    /**
     * @var PropertyId
     */
    private $propertyId;

    /**
     * @var Money|null
     */
    private $closingCost;

    /**
     * @var Money|null
     */
    private $rehabCost;

    /**
     * @var Money|null
     */
    private $holdingCost;

    /**
     * @var Money|null
     */
    private $sellingCost;

    /**
     * @var Money|null
     */
    private $cashNeeded;

    /**
     * @var Money|null
     */
    private $profit;

    /**
     * @var float|null
     */
    private $returnOnInvestment;

    /**
     * @var float|null
     */
    private $annualizedReturnOnInvestment;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @return string
     */
    public function idEncoded(): string
    {
        return $this->id->encode();
    }

    /**
     * @return PropertyId
     */
    public function propertyId(): PropertyId
    {
        return $this->propertyId;
    }

    /**
     * @return Money
     */
    public function closingCost(): Money
    {
        return $this->closingCost;
    }

    /**
     * @return Money|null
     */
    public function rehabCost(): ?Money
    {
        return $this->rehabCost;
    }

    /**
     * @return Money|null
     */
    public function holdingCost(): ?Money
    {
        return $this->holdingCost;
    }

    /**
     * @return Money|null
     */
    public function sellingCost(): ?Money
    {
        return $this->sellingCost;
    }

    /**
     * @return Money|null
     */
    public function cashNeeded(): ?Money
    {
        return $this->cashNeeded;
    }

    /**
     * @return Money|null
     */
    public function profit(): ?Money
    {
        return $this->profit;
    }

    /**
     * @return float|null
     */
    public function returnOnInvestment(): ?float
    {
        return $this->returnOnInvestment;
    }

    /**
     * @return float|null
     */
    public function annualizedReturnOnInvestment(): ?float
    {
        return $this->annualizedReturnOnInvestment;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param Money|null $closingCost
     */
    public function setClosingCost(?Money $closingCost): void
    {
        $this->closingCost = $closingCost;
    }

    /**
     * @param Money|null $rehabCost
     */
    public function setRehabCost(?Money $rehabCost): void
    {
        $this->rehabCost = $rehabCost;
    }

    /**
     * @param Money|null $holdingCost
     */
    public function setHoldingCost(?Money $holdingCost): void
    {
        $this->holdingCost = $holdingCost;
    }

    /**
     * @param Money|null $sellingCost
     */
    public function setSellingCost(?Money $sellingCost): void
    {
        $this->sellingCost = $sellingCost;
    }

    /**
     * @param Money|null $cashNeeded
     */
    public function setCashNeeded(?Money $cashNeeded): void
    {
        $this->cashNeeded = $cashNeeded;
    }

    /**
     * @param Money|null $profit
     */
    public function setProfit(?Money $profit): void
    {
        $this->profit = $profit;
    }

    /**
     * @param float|null $returnOnInvestment
     */
    public function setReturnOnInvestment(?float $returnOnInvestment): void
    {
        $this->returnOnInvestment = $returnOnInvestment;
    }

    /**
     * @param float|null $annualizedReturnOnInvestment
     */
    public function setAnnualizedReturnOnInvestment(?float $annualizedReturnOnInvestment): void
    {
        $this->annualizedReturnOnInvestment = $annualizedReturnOnInvestment;
    }

    /**
     * @param DateTimeImmutable|null $updatedAt
     */
    public function setUpdatedAt(?DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $analysis = new self();

        $analysis->id                           = new AnalysisId($data['analysis_id']);
        $analysis->propertyId                   = new PropertyId($data['property_id']);
        $analysis->closingCost                  = self::deserializeValueOrNull($data, 'closing_cost', Money::class, true);
        $analysis->rehabCost                    = self::deserializeValueOrNull($data, 'rehab_cost', Money::class, true);
        $analysis->holdingCost                  = self::deserializeValueOrNull($data, 'holding_cost', Money::class, true);
        $analysis->sellingCost                  = self::deserializeValueOrNull($data, 'selling_cost', Money::class, true);
        $analysis->cashNeeded                   = self::deserializeValueOrNull($data, 'cash_needed', Money::class, true);
        $analysis->profit                       = self::deserializeValueOrNull($data, 'profit', Money::class, true);
        $analysis->returnOnInvestment           = self::deserializeValueOrNull($data, 'return_on_investment');
        $analysis->annualizedReturnOnInvestment = self::deserializeValueOrNull($data, 'annualized_return_on_investment');
        $analysis->updatedAt                    = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $analysis->createdAt                    = new DateTimeImmutable($data['created_at']);

        return $analysis;
    }

    public function serialize(): array
    {
        return [
            'analysis_id'                     => $this->id->serialize(),
            'property_id'                     => $this->propertyId->serialize(),
            'closing_cost'                    => self::serializeValueOrNull($this->closingCost),
            'rehab_cost'                      => self::serializeValueOrNull($this->rehabCost),
            'holding_cost'                    => self::serializeValueOrNull($this->holdingCost),
            'selling_cost'                    => self::serializeValueOrNull($this->sellingCost),
            'cash_needed'                     => self::serializeValueOrNull($this->cashNeeded),
            'profit'                          => self::serializeValueOrNull($this->profit),
            'return_on_investment'            => self::serializeValueOrNull($this->returnOnInvestment),
            'annualized_return_on_investment' => self::serializeValueOrNull($this->annualizedReturnOnInvestment),
            'updated_at'                      => self::serializeValueOrNull($this->updatedAt),
            'created_at'                      => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}