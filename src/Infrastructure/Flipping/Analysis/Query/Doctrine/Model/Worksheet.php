<?php

declare(strict_types=1);

namespace App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\SellingCost;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Worksheet
 *
 * @package App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Worksheet implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var WorksheetId
     */
    private $id;

    /**
     * @var AnalysisId
     */
    private $analysisId;

    /**
     * @var Money|null
     */
    private $purchasePrice;

    /**
     * @var ClosingCost|null
     */
    private $closingCost;

    /**
     * @var HoldingCost|null
     */
    private $holdingCost;

    /**
     * @var HoldingPeriod
     */
    private $holdingPeriod;

    /**
     * @var RehabCost|null
     */
    private $rehabCost;

    /**
     * @var Money|null
     */
    private $afterRepairValue;

    /**
     * @var SellingCost|null
     */
    private $sellingCost;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @return string
     */
    public function idEncoded(): string
    {
        return $this->id->encode();
    }

    /**
     * @return AnalysisId
     */
    public function analysisId(): AnalysisId
    {
        return $this->analysisId;
    }

    /**
     * @return Money|null
     */
    public function purchasePrice(): ?Money
    {
        return $this->purchasePrice;
    }

    /**
     * @return ClosingCost|null
     */
    public function closingCost(): ?ClosingCost
    {
        return $this->closingCost;
    }

    /**
     * @return HoldingCost|null
     */
    public function holdingCost(): ?HoldingCost
    {
        return $this->holdingCost;
    }

    /**
     * @return HoldingPeriod
     */
    public function holdingPeriod(): HoldingPeriod
    {
        return $this->holdingPeriod;
    }

    /**
     * @return RehabCost|null
     */
    public function rehabCost(): ?RehabCost
    {
        return $this->rehabCost;
    }

    /**
     * @return Money|null
     */
    public function afterRepairValue(): ?Money
    {
        return $this->afterRepairValue;
    }

    /**
     * @return SellingCost|null
     */
    public function sellingCost(): ?SellingCost
    {
        return $this->sellingCost;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param Money|null $purchasePrice
     */
    public function setPurchasePrice(?Money $purchasePrice): void
    {
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * @param ClosingCost|null $closingCost
     */
    public function setClosingCost(?ClosingCost $closingCost): void
    {
        $this->closingCost = $closingCost;
    }

    /**
     * @param HoldingCost|null $holdingCost
     */
    public function setHoldingCost(?HoldingCost $holdingCost): void
    {
        $this->holdingCost = $holdingCost;
    }

    /**
     * @param HoldingPeriod $holdingPeriod
     */
    public function setHoldingPeriod(HoldingPeriod $holdingPeriod): void
    {
        $this->holdingPeriod = $holdingPeriod;
    }

    /**
     * @param RehabCost|null $rehabCost
     */
    public function setRehabCost(?RehabCost $rehabCost): void
    {
        $this->rehabCost = $rehabCost;
    }

    /**
     * @param Money|null $afterRepairValue
     */
    public function setAfterRepairValue(?Money $afterRepairValue): void
    {
        $this->afterRepairValue = $afterRepairValue;
    }

    /**
     * @param SellingCost|null $sellingCost
     */
    public function setSellingCost(?SellingCost $sellingCost): void
    {
        $this->sellingCost = $sellingCost;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $worksheet = new self();

        $worksheet->id               = new WorksheetId($data['worksheet_id']);
        $worksheet->analysisId       = new AnalysisId($data['analysis_id']);
        $worksheet->purchasePrice    = self::deserializeValueOrNull($data, 'purchase_price', Money::class, true);
        $worksheet->closingCost      = self::deserializeValueOrNull($data, 'closing_cost', ClosingCost::class, true);
        $worksheet->holdingCost      = self::deserializeValueOrNull($data, 'holding_cost', HoldingCost::class, true);
        $worksheet->holdingPeriod    = self::deserializeValueOrNull($data, 'holding_period', HoldingPeriod::class);
        $worksheet->rehabCost        = self::deserializeValueOrNull($data, 'rehab_cost', RehabCost::class, true);
        $worksheet->afterRepairValue = self::deserializeValueOrNull($data, 'after_repair_value', Money::class, true);
        $worksheet->sellingCost      = self::deserializeValueOrNull($data, 'selling_cost', SellingCost::class, true);
        $worksheet->createdAt        = new DateTimeImmutable($data['created_at']);

        return $worksheet;
    }

    public function serialize(): array
    {
        return [
            'worksheet_id'       => $this->id->serialize(),
            'analysis_id'        => $this->analysisId->serialize(),
            'purchase_price'     => self::serializeValueOrNull($this->purchasePrice),
            'closing_cost'       => self::serializeValueOrNull($this->closingCost),
            'holding_cost'       => self::serializeValueOrNull($this->holdingCost),
            'holding_period'     => $this->holdingPeriod->serialize(),
            'rehab_cost'         => self::serializeValueOrNull($this->holdingCost),
            'after_repair_value' => self::serializeValueOrNull($this->afterRepairValue),
            'selling_cost'       => self::serializeValueOrNull($this->sellingCost),
            'created_at'         => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}