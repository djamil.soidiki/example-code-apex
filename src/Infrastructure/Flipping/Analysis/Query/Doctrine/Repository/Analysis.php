<?php

declare(strict_types=1);

namespace App\Infrastructure\Flipping\Analysis\Query\Doctrine\Repository;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Analysis as AnalysisModel;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Analysis
 *
 * @package App\Infrastructure\Flipping\Analysis\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Analysis extends AbstractRepository
{
    /**
     * Analysis constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = AnalysisModel::class;
        parent::__construct($entityManager);
    }

    /**
     * @param AnalysisModel $analysis
     */
    public function add(AnalysisModel $analysis): void
    {
        $this->register($analysis);
    }

    public function save(): void
    {
        $this->apply();
    }

    /**
     * @param AnalysisId $analysisId
     *
     * @return AnalysisModel
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findAnalysisByAnalysisId(AnalysisId $analysisId): AnalysisModel
    {
        $qb = $this->repository
            ->createQueryBuilder('a')
            ->where('a.id = :analysisId')
            ->setParameter('analysisId', $analysisId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param PropertyId $propertyId
     *
     * @return array
     */
    public function findAnalysisByPropertyId(PropertyId $propertyId): array
    {
        return $this->repository
            ->createQueryBuilder('a')
            ->where('a.propertyId = :propertyId')
            ->setParameter('propertyId', $propertyId->serialize())
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository
            ->createQueryBuilder('u')
            ->orderBy('u.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}