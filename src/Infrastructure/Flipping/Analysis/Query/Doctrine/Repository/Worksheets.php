<?php

declare(strict_types=1);

namespace App\Infrastructure\Flipping\Analysis\Query\Doctrine\Repository;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Worksheet;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Worksheets
 *
 * @package App\Infrastructure\Flipping\Analysis\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Worksheets extends AbstractRepository
{
    /**
     * Worksheets constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Worksheet::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Worksheet $worksheet
     */
    public function add(Worksheet $worksheet): void
    {
        $this->register($worksheet);
    }

    public function save(): void
    {
        $this->apply();
    }

    /**
     * @param WorksheetId $worksheetId
     *
     * @return Worksheet
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findWorksheetByWorksheetId(WorksheetId $worksheetId): Worksheet
    {
        $qb = $this->repository
            ->createQueryBuilder('w')
            ->where('w.id = :worksheetId')
            ->setParameter('worksheetId', $worksheetId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param AnalysisId $analysisId
     *
     * @return Worksheet
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findWorksheetByAnalysisId(AnalysisId $analysisId): Worksheet
    {
        $qb = $this->repository
            ->createQueryBuilder('w')
            ->where('w.analysisId = :analysisId')
            ->setParameter('analysisId', $analysisId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository
            ->createQueryBuilder('w')
            ->orderBy('w.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}