<?php

declare(strict_types=1);

namespace App\Infrastructure\Flipping\Analysis\Query;

use App\Domain\Flipping\Analysis\Event\AnalysisAnnualizedReturnOnInvestmentCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisCashNeededCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisClosingCostCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisCreated;
use App\Domain\Flipping\Analysis\Event\AnalysisHoldingCostCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisRehabCostCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisProfitCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisReturnOnInvestmentCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisSellingCostCalculated;
use App\Domain\Flipping\Analysis\Event\WorksheetAfterRepairValueChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetClosingCostChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetHoldingCostChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetHoldingPeriodChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetPurchasePriceChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetRehabCostChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetSellingCostChanged;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Analysis;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Worksheet;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Repository\Analysis as AnalysisRepository;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Repository\Worksheets;
use Broadway\ReadModel\Projector;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class AnalysisProjection
 *
 * @package App\Infrastructure\Flipping\Analysis\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AnalysisProjection extends Projector
{
    /**
     * @var AnalysisRepository
     */
    private $analysisRepository;

    /**
     * @var Worksheets
     */
    private $worksheets;

    /**
     * AnalysisProjection constructor.
     *
     * @param AnalysisRepository $analysisRepository
     * @param Worksheets         $worksheets
     */
    public function __construct(AnalysisRepository $analysisRepository, Worksheets $worksheets)
    {
        $this->analysisRepository = $analysisRepository;
        $this->worksheets         = $worksheets;
    }

    /**
     * @param AnalysisCreated $event
     *
     * @throws Exception
     */
    protected function applyAnalysisCreated(AnalysisCreated $event): void
    {
        $analysis  = Analysis::fromSerializable($event);
        $worksheet = Worksheet::fromSerializable($event);

        $worksheet->setHoldingPeriod(new HoldingPeriod());

        $this->analysisRepository->add($analysis);
        $this->worksheets->add($worksheet);
    }

    /**
     * @param WorksheetPurchasePriceChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyWorksheetPurchasePriceChanged(WorksheetPurchasePriceChanged $event): void
    {
        /** @var Worksheet $worksheet */
        $worksheet = $this->worksheets->findWorksheetByWorksheetId($event->worksheetId());

        $worksheet->setPurchasePrice($event->purchasePrice());

        $this->worksheets->save();
    }

    /**
     * @param WorksheetClosingCostChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyWorksheetClosingCostChanged(WorksheetClosingCostChanged $event): void
    {
        /** @var Worksheet $worksheet */
        $worksheet = $this->worksheets->findWorksheetByWorksheetId($event->worksheetId());

        $worksheet->setClosingCost($event->closingCost());

        $this->worksheets->save();
    }

    /**
     * @param WorksheetHoldingCostChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyWorksheetHoldingCostChanged(WorksheetHoldingCostChanged $event): void
    {
        /** @var Worksheet $worksheet */
        $worksheet = $this->worksheets->findWorksheetByWorksheetId($event->worksheetId());

        $worksheet->setHoldingCost($event->holdingCost());

        $this->worksheets->save();
    }

    /**
     * @param WorksheetHoldingPeriodChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyWorksheetHoldingPeriodChanged(WorksheetHoldingPeriodChanged $event): void
    {
        /** @var Worksheet $worksheet */
        $worksheet = $this->worksheets->findWorksheetByWorksheetId($event->worksheetId());

        $worksheet->setHoldingPeriod($event->holdingPeriod());

        $this->worksheets->save();
    }

    /**
     * @param WorksheetRehabCostChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyWorksheetRehabCostChanged(WorksheetRehabCostChanged $event): void
    {
        /** @var Worksheet $worksheet */
        $worksheet = $this->worksheets->findWorksheetByWorksheetId($event->worksheetId());

        $worksheet->setRehabCost($event->rehabCost());

        $this->worksheets->save();
    }

    /**
     * @param WorksheetAfterRepairValueChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyWorksheetAfterRepairValueChanged(WorksheetAfterRepairValueChanged $event): void
    {
        /** @var Worksheet $worksheet */
        $worksheet = $this->worksheets->findWorksheetByWorksheetId($event->worksheetId());

        $worksheet->setAfterRepairValue($event->afterRepairValue());

        $this->worksheets->save();
    }

    /**
     * @param WorksheetSellingCostChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyWorksheetSellingCostChanged(WorksheetSellingCostChanged $event): void
    {
        /** @var Worksheet $worksheet */
        $worksheet = $this->worksheets->findWorksheetByWorksheetId($event->worksheetId());

        $worksheet->setSellingCost($event->sellingCost());

        $this->worksheets->save();
    }

    /**
     * @param AnalysisClosingCostCalculated $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyAnalysisClosingCostCalculated(AnalysisClosingCostCalculated $event): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->findAnalysisByAnalysisId($event->analysisId());

        $analysis->setClosingCost($event->closingCost());
        $analysis->setUpdatedAt($event->updatedAt());

        $this->analysisRepository->save();
    }

    /**
     * @param AnalysisRehabCostCalculated $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyAnalysisRehabCostCalculated(AnalysisRehabCostCalculated $event): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->findAnalysisByAnalysisId($event->analysisId());

        $analysis->setRehabCost($event->rehabCost());
        $analysis->setUpdatedAt($event->updatedAt());

        $this->analysisRepository->save();
    }

    /**
     * @param AnalysisHoldingCostCalculated $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyAnalysisHoldingCostCalculated(AnalysisHoldingCostCalculated $event): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->findAnalysisByAnalysisId($event->analysisId());

        $analysis->setHoldingCost($event->holdingCost());
        $analysis->setUpdatedAt($event->updatedAt());

        $this->analysisRepository->save();
    }

    /**
     * @param AnalysisSellingCostCalculated $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyAnalysisSellingCostCalculated(AnalysisSellingCostCalculated $event): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->findAnalysisByAnalysisId($event->analysisId());

        $analysis->setSellingCost($event->sellingCost());
        $analysis->setUpdatedAt($event->updatedAt());

        $this->analysisRepository->save();
    }

    /**
     * @param AnalysisCashNeededCalculated $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyAnalysisCashNeededCalculated(AnalysisCashNeededCalculated $event): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->findAnalysisByAnalysisId($event->analysisId());

        $analysis->setCashNeeded($event->cashNeeded());
        $analysis->setUpdatedAt($event->updatedAt());

        $this->analysisRepository->save();
    }

    /**
     * @param AnalysisProfitCalculated $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyAnalysisProfitCalculated(AnalysisProfitCalculated $event): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->findAnalysisByAnalysisId($event->analysisId());

        $analysis->setProfit($event->profit());
        $analysis->setUpdatedAt($event->updatedAt());

        $this->analysisRepository->save();
    }

    /**
     * @param AnalysisReturnOnInvestmentCalculated $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyAnalysisReturnOnInvestmentCalculated(AnalysisReturnOnInvestmentCalculated $event): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->findAnalysisByAnalysisId($event->analysisId());

        $analysis->setReturnOnInvestment($event->returnOnInvestment());
        $analysis->setUpdatedAt($event->updatedAt());

        $this->analysisRepository->save();
    }

    /**
     * @param AnalysisAnnualizedReturnOnInvestmentCalculated $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyAnalysisAnnualizedReturnOnInvestmentCalculated(AnalysisAnnualizedReturnOnInvestmentCalculated $event): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->findAnalysisByAnalysisId($event->analysisId());

        $analysis->setAnnualizedReturnOnInvestment($event->annualizedReturnOnInvestment());
        $analysis->setUpdatedAt($event->updatedAt());

        $this->analysisRepository->save();
    }
}