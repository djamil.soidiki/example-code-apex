<?php

declare(strict_types=1);

namespace App\Infrastructure\Flipping\Analysis\Command;

use App\Domain\Flipping\Analysis\Analysis as AnalysisAggregate;
use App\Domain\Flipping\Analysis\Repository\AnalysisInterface;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

/**
 * Class Analysis
 *
 * @package App\Infrastructure\Flipping\Analysis\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Analysis implements AnalysisInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * Analysis constructor.
     *
     * @param EventStore $eventStore
     * @param EventBus   $eventBus
     * @param array      $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            AnalysisAggregate::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * @inheritdoc
     */
    public function get(AnalysisId $analysisId): ?AnalysisAggregate
    {
        /** @var AnalysisAggregate $analysis */
        $analysis = $this->eventSourcingRepository->load($analysisId->serialize());

        return $analysis;
    }

    /**
     * @inheritdoc
     */
    public function save(AnalysisAggregate $analysis): void
    {
        $this->eventSourcingRepository->save($analysis);
    }
}