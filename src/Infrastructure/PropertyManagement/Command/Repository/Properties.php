<?php

declare(strict_types=1);

namespace App\Infrastructure\PropertyManagement\Command\Repository;

use App\Domain\PropertyManagement\Property;
use App\Domain\PropertyManagement\Repository\PropertiesInterface;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;

/**
 * Class Properties
 *
 * @package App\Infrastructure\PropertyManagement\Command\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Properties implements PropertiesInterface
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    /**
     * Properties constructor.
     *
     * @param EventStore $eventStore
     * @param EventBus   $eventBus
     * @param array      $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Property::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * @inheritdoc
     */
    public function get(PropertyId $propertyId): ?Property
    {
        /** @var Property $property */
        $property = $this->eventSourcingRepository->load($propertyId->serialize());

        return $property;
    }

    /**
     * @inheritdoc
     */
    public function save(Property $property): void
    {
        $this->eventSourcingRepository->save($property);
    }
}