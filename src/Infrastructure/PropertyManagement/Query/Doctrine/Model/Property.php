<?php

declare(strict_types=1);

namespace App\Infrastructure\PropertyManagement\Query\Doctrine\Model;

use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\Domain\PropertyManagement\ValueObject\PropertyDescription;
use App\Domain\PropertyManagement\ValueObject\PropertyDetails;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\PropertyManagement\ValueObject\PropertyName;
use App\Domain\PropertyManagement\ValueObject\PropertyStatus;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class Property
 *
 * @package App\Infrastructure\PropertyManagement\Query\Doctrine\Model
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class Property implements SerializableReadModel
{
    use SerializerValueTrait;

    /**
     * @var PropertyId
     */
    private $id;

    /**
     * @var UserId
     */
    private $managerId;

    /**
     * @var PropertyName
     */
    private $name;

    /**
     * @var PropertyDescription|null
     */
    private $description;

    /**
     * @var PropertyAddress|null
     */
    private $address;

    /**
     * @var PropertyDetails|null
     */
    private $details;

    /**
     * @var PropertyStatus
     */
    private $status;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param Serializable $event
     *
     * @return static
     * @throws Exception
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @return string
     */
    public function idEncoded(): string
    {
        return $this->id->encode();
    }

    /**
     * @return UserId
     */
    public function managerId(): UserId
    {
        return $this->managerId;
    }

    /**
     * @return PropertyName
     */
    public function name(): PropertyName
    {
        return $this->name;
    }

    /**
     * @return PropertyDescription|null
     */
    public function description(): ?PropertyDescription
    {
        return $this->description;
    }

    /**
     * @return PropertyAddress|null
     */
    public function address(): ?PropertyAddress
    {
        return $this->address;
    }

    /**
     * @return PropertyDetails|null
     */
    public function details(): ?PropertyDetails
    {
        return $this->details;
    }

    /**
     * @return PropertyStatus
     */
    public function status(): PropertyStatus
    {
        return $this->status;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param PropertyName $name
     */
    public function setName(PropertyName $name): void
    {
        $this->name = $name;
    }

    /**
     * @param PropertyDescription|null $description
     */
    public function setDescription(?PropertyDescription $description): void
    {
        $this->description = $description;
    }

    /**
     * @param PropertyAddress|null $address
     */
    public function setAddress(?PropertyAddress $address): void
    {
        $this->address = $address;
    }

    /**
     * @param PropertyDetails|null $details
     */
    public function setDetails(?PropertyDetails $details): void
    {
        $this->details = $details;
    }

    /**
     * @param PropertyStatus $status
     */
    public function setStatus(PropertyStatus $status): void
    {
        $this->status = $status;
    }

    /**
     * @param DateTimeImmutable|null $updatedAt
     */
    public function setUpdatedAt(?DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        $property = new self();

        $property->id          = new PropertyId($data['property_id']);
        $property->managerId   = new UserId($data['manager_id']);
        $property->name        = new PropertyName($data['name']);
        $property->description = self::deserializeValueOrNull($data, 'description', PropertyDescription::class);
        $property->address     = self::deserializeValueOrNull($data, 'address', PropertyAddress::class, true);
        $property->details     = self::deserializeValueOrNull($data, 'details', PropertyDetails::class, true);
        $property->status      = new PropertyStatus($data['status']);
        $property->updatedAt   = self::deserializeValueOrNull($data, 'updated_at', DateTimeImmutable::class);
        $property->createdAt   = new DateTimeImmutable($data['created_at']);

        return $property;
    }

    public function serialize(): array
    {
        return [
            'property_id' => $this->id->serialize(),
            'manager_id'  => $this->managerId->serialize(),
            'name'        => $this->name->serialize(),
            'description' => self::serializeValueOrNull($this->description),
            'address'     => self::serializeValueOrNull($this->address),
            'details'     => self::serializeValueOrNull($this->details),
            'status'      => $this->status->serialize(),
            'updated_at'  => self::serializeValueOrNull($this->updatedAt),
            'created_at'  => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}