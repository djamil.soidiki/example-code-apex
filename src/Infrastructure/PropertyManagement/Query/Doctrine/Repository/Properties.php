<?php

declare(strict_types=1);

namespace App\Infrastructure\PropertyManagement\Query\Doctrine\Repository;

use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Generic\Doctrine\Repository\AbstractRepository;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class Properties
 *
 * @package App\Infrastructure\PropertyManagement\Query\Doctrine\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Properties extends AbstractRepository
{
    /**
     * Properties constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = Property::class;
        parent::__construct($entityManager);
    }

    /**
     * @param Property $property
     */
    public function add(Property $property): void
    {
        $this->register($property);
    }

    public function save(): void
    {
        $this->apply();
    }

    /**
     * @param PropertyId $propertyId
     *
     * @return Property
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function findPropertyByPropertyId(PropertyId $propertyId): Property
    {
        $qb = $this->repository
            ->createQueryBuilder('p')
            ->where('p.id = :propertyId')
            ->setParameter('propertyId', $propertyId->serialize());

        return $this->oneOrException($qb);
    }

    /**
     * @param UserId $managerId
     *
     * @return array
     */
    public function findPropertiesByManagerId(UserId $managerId): array
    {
        return $this->repository
            ->createQueryBuilder('p')
            ->where('p.managerId = :managerId')
            ->setParameter('managerId', $managerId->serialize())
            ->orderBy('p.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository
            ->createQueryBuilder('u')
            ->orderBy('u.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}