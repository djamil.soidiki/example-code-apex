<?php

declare(strict_types=1);

namespace App\Infrastructure\PropertyManagement\Query\Doctrine\Types;

use App\Domain\PropertyManagement\ValueObject\PropertyName;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

/**
 * Class PropertyNameType
 *
 * @package App\Infrastructure\PropertyManagement\Query\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyNameType extends Type
{
    const PROPERTY_NAME = 'property_management_property_name';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof PropertyName) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', PropertyName::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return PropertyName|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof PropertyName) {
            return $value;
        }

        try {
            $propertyName = new PropertyName($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                self::PROPERTY_NAME
            );
        }

        return $propertyName;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::PROPERTY_NAME;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}

