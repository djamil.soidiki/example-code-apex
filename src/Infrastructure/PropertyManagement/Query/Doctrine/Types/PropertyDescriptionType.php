<?php

declare(strict_types=1);

namespace App\Infrastructure\PropertyManagement\Query\Doctrine\Types;

use App\Domain\PropertyManagement\ValueObject\PropertyDescription;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

/**
 * Class PropertyDescriptionType
 *
 * @package App\Infrastructure\PropertyManagement\Query\Doctrine\Types
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyDescriptionType extends Type
{
    const PROPERTY_DESCRIPTION = 'property_management_property_description';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if ($value instanceof PropertyDescription) {
            return $value->serialize();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', PropertyDescription::class]
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return PropertyDescription|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof PropertyDescription) {
            return $value;
        }

        try {
            $propertyDescription = new PropertyDescription($value);
        } catch (\Exception $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                self::PROPERTY_DESCRIPTION
            );
        }

        return $propertyDescription;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::PROPERTY_DESCRIPTION;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}

