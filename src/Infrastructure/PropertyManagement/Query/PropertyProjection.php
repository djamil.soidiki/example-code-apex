<?php

declare(strict_types=1);

namespace App\Infrastructure\PropertyManagement\Query;

use App\Domain\PropertyManagement\Event\PropertyAddressChanged;
use App\Domain\PropertyManagement\Event\PropertyArchived;
use App\Domain\PropertyManagement\Event\PropertyCreated;
use App\Domain\PropertyManagement\Event\PropertyDescriptionChanged;
use App\Domain\PropertyManagement\Event\PropertyDetailsChanged;
use App\Domain\PropertyManagement\Event\PropertyNameChanged;
use App\Domain\PropertyManagement\Event\PropertyRestored;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Repository\Properties;
use Broadway\ReadModel\Projector;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class PropertyProjection
 *
 * @package App\Infrastructure\PropertyManagement\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyProjection extends Projector
{
    /**
     * @var Properties
     */
    private $properties;

    /**
     * PropertyProjection constructor.
     *
     * @param Properties $properties
     */
    public function __construct(Properties $properties)
    {
        $this->properties = $properties;
    }

    /**
     * @param PropertyCreated $event
     *
     * @throws Exception
     */
    protected function applyPropertyCreated(PropertyCreated $event): void
    {
        $property = Property::fromSerializable($event);

        $this->properties->add($property);
    }

    /**
     * @param PropertyNameChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyPropertyNameChanged(PropertyNameChanged $event): void
    {
        /** @var Property $property */
        $property = $this->properties->findPropertyByPropertyId($event->propertyId());

        $property->setName($event->name());
        $property->setUpdatedAt($event->updatedAt());

        $this->properties->save();
    }

    /**
     * @param PropertyDescriptionChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyPropertyDescriptionChanged(PropertyDescriptionChanged $event): void
    {
        /** @var Property $property */
        $property = $this->properties->findPropertyByPropertyId($event->propertyId());

        $property->setDescription($event->description());
        $property->setUpdatedAt($event->updatedAt());

        $this->properties->save();
    }

    /**
     * @param PropertyAddressChanged $event
     *
     * @throws NotFoundException
     * @throws NonUniqueResultException
     */
    protected function applyPropertyAddressChanged(PropertyAddressChanged $event): void
    {
        /** @var Property $property */
        $property = $this->properties->findPropertyByPropertyId($event->propertyId());

        $property->setAddress($event->address());
        $property->setUpdatedAt($event->updatedAt());

        $this->properties->save();
    }

    /**
     * @param PropertyDetailsChanged $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyPropertyDetailsChanged(PropertyDetailsChanged $event): void
    {
        /** @var Property $property */
        $property = $this->properties->findPropertyByPropertyId($event->propertyId());

        $property->setDetails($event->details());
        $property->setUpdatedAt($event->updatedAt());

        $this->properties->save();
    }

    /**
     * @param PropertyArchived $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyPropertyArchived(PropertyArchived $event): void
    {
        /** @var Property $property */
        $property = $this->properties->findPropertyByPropertyId($event->propertyId());

        $property->setStatus($event->status());
        $property->setUpdatedAt($event->updatedAt());

        $this->properties->save();
    }

    /**
     * @param PropertyRestored $event
     *
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    protected function applyPropertyRestored(PropertyRestored $event): void
    {
        /** @var Property $property */
        $property = $this->properties->findPropertyByPropertyId($event->propertyId());

        $property->setStatus($event->status());
        $property->setUpdatedAt($event->updatedAt());

        $this->properties->save();
    }
}