<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Service;

use App\Domain\SharedKernel\Billing\Customer\Event\CustomerRegistered;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerEmailAddressChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerFullNameChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerPaymentMethodChanged;

/**
 * Interface PaymentGatewayInterface
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface PaymentGatewayInterface
{
    /**
     * @param CustomerRegistered $customerRegistered
     *
     * @return array
     */
    public function createCustomer(CustomerRegistered $customerRegistered): array;

    /**
     * @param CustomerPaymentMethodChanged $paymentMethodChanged
     *
     * @return array
     */
    public function updatePaymentMethod(CustomerPaymentMethodChanged $paymentMethodChanged): array;

    /**
     * @param CustomerFullNameChanged $fullNameChanged
     *
     * @return array
     */
    public function updateName(CustomerFullNameChanged $fullNameChanged): array;

    /**
     * @param CustomerEmailAddressChanged $emailAddressChanged
     *
     * @return array
     */
    public function updateEmail(CustomerEmailAddressChanged $emailAddressChanged): array;
}