<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Identity\UuidTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;

/**
 * Class PaymentMethod
 *
 * @package App\Domain\Authentication\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PaymentMethod extends AbstractValueObject
{
    use UuidTrait;
    use StringTrait;

    /**
     * @var array
     */
    private static $testValue = [
        'pm_card_visa',
        'pm_card_visa_debit',
        'pm_card_mastercard',
        'pm_card_mastercard_debit',
        'pm_card_mastercard_prepaid',
        'pm_card_amex',
        'pm_card_discover',
        'pm_card_diners',
        'pm_card_jcb',
        'pm_card_unionpay'
    ];

    /**
     * @var string
     */
    private $value;

    /**
     * PaymentMethod constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $this->validateString($value);

        $parts = explode('_', $value);

        if (!in_array($value, self::$testValue)) {
            if (count($parts) !== 2 || $parts[0] !== 'pm')
                throw new \InvalidArgumentException(sprintf('"%s" is not a valid payment method id', $value));
        }
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}