<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Repository;

use App\Domain\SharedKernel\Billing\Customer\Customer;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;

/**
 * Interface CustomersInterface
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface CustomersInterface
{
    /**
     * @param CustomerId $customerId
     *
     * @return Customer|null
     */
    public function get(CustomerId $customerId): ?Customer;

    /**
     * @param Customer $customer
     */
    public function save(Customer $customer): void;
}