<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Repository\Query;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;

/**
 * Interface UserIdQueryInterface
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Repository\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface UserIdQueryInterface
{
    /**
     * @param UserId $userId
     *
     * @return CustomerId|null
     */
    public function findCustomerIdByUserId(UserId $userId): ?CustomerId;
}