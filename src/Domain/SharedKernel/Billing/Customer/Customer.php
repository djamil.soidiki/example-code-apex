<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer;

use App\Domain\SharedKernel\Billing\Customer\Event\CustomerRegistered;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerEmailAddressChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerFullNameChanged;
use App\Domain\SharedKernel\Billing\Customer\Event\CustomerPaymentMethodChanged;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\PaymentMethod;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class Customer
 *
 * @package App\Domain\SharedKernel\Billing\Customer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Customer extends EventSourcedAggregateRoot
{
    /**
     * @var CustomerId
     */
    private $id;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var PaymentMethod|null
     */
    private $paymentMethod;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var FullName
     */
    private $fullName;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param UserId             $userId
     * @param EmailAddress       $emailAddress
     * @param FullName           $fullName
     * @param PaymentMethod|null $paymentMethod
     *
     * @return static
     * @throws Exception
     */
    public static function register(
        UserId $userId,
        EmailAddress $emailAddress,
        FullName $fullName,
        ?PaymentMethod $paymentMethod = null
    ): self
    {
        $customer = new self();

        $customer->apply(new CustomerRegistered(
                new CustomerId(),
                $userId,
                $paymentMethod,
                $emailAddress,
                $fullName,
                new DateTimeImmutable()
            )
        );

        return $customer;
    }

    /**
     * @param PaymentMethod $paymentMethod
     *
     * @throws Exception
     */
    public function changePaymentMethod(PaymentMethod $paymentMethod)
    {
        $this->apply(new CustomerPaymentMethodChanged($this->id, $paymentMethod, new DateTimeImmutable()));
    }

    /**
     * @param FullName $fullName
     *
     * @throws Exception
     */
    public function changeFullName(FullName $fullName)
    {
        if (!$this->fullName->sameValueAs($fullName)) {
            $this->apply(new CustomerFullNameChanged($this->id, $fullName, new DateTimeImmutable()));
        }
    }

    /**
     * @param EmailAddress $emailAddress
     *
     * @throws Exception
     */
    public function changeEmailAddress(EmailAddress $emailAddress)
    {
        if (!$this->emailAddress->sameValueAs($emailAddress)) {
            $this->apply(new CustomerEmailAddressChanged($this->id, $emailAddress, new DateTimeImmutable()));
        }
    }

    /**
     * @param CustomerRegistered $event
     */
    public function applyCustomerRegistered(CustomerRegistered $event): void
    {
        $this->id            = $event->customerId();
        $this->userId        = $event->userId();
        $this->paymentMethod = $event->paymentMethod();
        $this->emailAddress  = $event->emailAddress();
        $this->fullName      = $event->fullName();
        $this->createdAt     = $event->createdAt();
    }

    /**
     * @param CustomerPaymentMethodChanged $event
     */
    public function applyCustomerPaymentMethodChanged(CustomerPaymentMethodChanged $event): void
    {
        $this->id            = $event->customerId();
        $this->paymentMethod = $event->paymentMethod();
        $this->updatedAt     = $event->updatedAt();
    }

    /**
     * @param CustomerFullNameChanged $event
     */
    public function applyCustomerFullNameChanged(CustomerFullNameChanged $event): void
    {
        $this->id        = $event->customerId();
        $this->fullName  = $event->fullName();
        $this->updatedAt = $event->updatedAt();
    }

    /**
     * @param CustomerEmailAddressChanged $event
     */
    public function applyCustomerEmailAddressChanged(CustomerEmailAddressChanged $event): void
    {
        $this->id           = $event->customerId();
        $this->emailAddress = $event->emailAddress();
        $this->updatedAt    = $event->updatedAt();
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}