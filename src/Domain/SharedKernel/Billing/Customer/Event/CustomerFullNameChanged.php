<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Event;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class CustomerFullNameChanged
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CustomerFullNameChanged implements Serializable
{
    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var FullName
     */
    private $fullName;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * CustomerFullNameChanged constructor.
     *
     * @param CustomerId        $customerId
     * @param FullName          $fullName
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(CustomerId $customerId, FullName $fullName, DateTimeImmutable $updatedAt)
    {
        $this->customerId = $customerId;
        $this->fullName   = $fullName;
        $this->updatedAt  = $updatedAt;
    }

    /**
     * @return CustomerId
     */
    public function customerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return FullName
     */
    public function fullName(): FullName
    {
        return $this->fullName;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'customer_id');
        Assertion::keyExists($data, 'full_name');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new CustomerId($data['customer_id']),
            new FullName($data['full_name']['first_name'], $data['full_name']['last_name']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'customer_id' => $this->customerId->serialize(),
            'full_name'   => $this->fullName->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}