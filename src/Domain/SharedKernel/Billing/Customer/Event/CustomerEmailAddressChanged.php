<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Event;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class CustomerEmailAddressChanged
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CustomerEmailAddressChanged implements Serializable
{
    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * CustomerEmailAddressChanged constructor.
     *
     * @param CustomerId        $customerId
     * @param EmailAddress      $emailAddress
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(CustomerId $customerId, EmailAddress $emailAddress, DateTimeImmutable $updatedAt)
    {
        $this->customerId   = $customerId;
        $this->emailAddress = $emailAddress;
        $this->updatedAt    = $updatedAt;
    }

    /**
     * @return CustomerId
     */
    public function customerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'customer_id');
        Assertion::keyExists($data, 'email_address');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new CustomerId($data['customer_id']),
            new EmailAddress($data['email_address']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'customer_id'   => $this->customerId->serialize(),
            'email_address' => $this->emailAddress->serialize(),
            'updated_at'    => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}