<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Event;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\PaymentMethod;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class CustomerPaymentMethodChanged
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CustomerPaymentMethodChanged implements Serializable
{
    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var PaymentMethod
     */
    private $paymentMethod;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * CustomerPaymentMethodChanged constructor.
     *
     * @param CustomerId        $customerId
     * @param PaymentMethod     $paymentMethod
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(
        CustomerId $customerId,
        PaymentMethod $paymentMethod,
        DateTimeImmutable $updatedAt
    )
    {
        $this->customerId    = $customerId;
        $this->paymentMethod = $paymentMethod;
        $this->updatedAt     = $updatedAt;
    }

    /**
     * @return CustomerId
     */
    public function customerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return PaymentMethod
     */
    public function paymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'customer_id');
        Assertion::keyExists($data, 'payment_method');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new CustomerId($data['customer_id']),
            new PaymentMethod($data['payment_method']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'customer_id'    => $this->customerId->serialize(),
            'payment_method' => $this->paymentMethod->serialize(),
            'updated_at'     => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}