<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Event\Handler;

use App\Domain\SharedKernel\Authentication\Event\UserEmailAddressChanged;
use App\Domain\SharedKernel\Billing\Customer\Repository\CustomersInterface;
use App\Domain\SharedKernel\Billing\Customer\Repository\Query\UserIdQueryInterface;
use App\Domain\SharedKernel\Billing\Customer\Service\PaymentGatewayInterface;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use Broadway\Processor\Processor;
use Exception;

/**
 * Class UpdateCustomerEmailAddressWhenUserEmailAddressWasChanged
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Event\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class UpdateCustomerEmailAddressWhenUserEmailAddressWasChanged extends Processor
{
    /**
     * @var UserIdQueryInterface
     */
    private $customersQuery;

    /**
     * @var CustomersInterface
     */
    private $customers;

    /**
     * @var PaymentGatewayInterface
     */
    private $paymentGateway;

    /**
     * UpdateCustomerEmailAddressWhenUserEmailAddressWasChanged constructor.
     *
     * @param CustomersInterface      $customers
     * @param UserIdQueryInterface    $customersQuery
     * @param PaymentGatewayInterface $paymentGateway
     */
    public function __construct(CustomersInterface $customers, UserIdQueryInterface $customersQuery, PaymentGatewayInterface $paymentGateway)
    {
        $this->customers      = $customers;
        $this->customersQuery = $customersQuery;
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param UserEmailAddressChanged $event
     *
     * @throws Exception
     */
    public function handleUserEmailAddressChanged(UserEmailAddressChanged $event): void
    {
        $customerId = $this->customersQuery->findCustomerIdByUserId($event->userId());
        if ($customerId === null)
            throw new NotFoundException();

        $customer = $this->customers->get($customerId);

        $customer->changeEmailAddress($event->emailAddress());

        $this->customers->save($customer);
    }
}