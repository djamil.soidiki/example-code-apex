<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Event\Handler;

use App\Domain\SharedKernel\Billing\Customer\Event\CustomerPaymentMethodChanged;
use App\Domain\SharedKernel\Billing\Customer\Service\PaymentGatewayInterface;
use Broadway\Processor\Processor;

/**
 * Class UpdateStripePaymentMethodWhenPaymentMethodWasChanged
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Event\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class UpdateStripePaymentMethodWhenPaymentMethodWasChanged extends Processor
{
    /**
     * @var PaymentGatewayInterface
     */
    private $paymentGateway;

    /**
     * UpdateStripePaymentMethodWhenPaymentMethodWasChanged constructor.
     *
     * @param PaymentGatewayInterface $paymentGateway
     */
    public function __construct(PaymentGatewayInterface $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param CustomerPaymentMethodChanged $event
     */
    public function handleCustomerPaymentMethodChanged(CustomerPaymentMethodChanged $event): void
    {
        $this->paymentGateway->updatePaymentMethod($event);
    }
}