<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Event\Handler;

use App\Domain\SharedKernel\Billing\Customer\Event\CustomerRegistered;
use App\Domain\SharedKernel\Billing\Customer\Service\PaymentGatewayInterface;
use Broadway\Processor\Processor;

/**
 * Class CreateStripeCustomerWhenCustomerWasRegistered
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Event\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateStripeCustomerWhenCustomerWasRegistered extends Processor
{
    /**
     * @var PaymentGatewayInterface
     */
    private $paymentGateway;

    /**
     * CreateStripeCustomerWhenCustomerWasRegistered constructor.
     *
     * @param PaymentGatewayInterface $paymentGateway
     */
    public function __construct(PaymentGatewayInterface $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param CustomerRegistered $event
     */
    public function handleCustomerRegistered(CustomerRegistered $event): void
    {
        $this->paymentGateway->createCustomer($event);
    }
}