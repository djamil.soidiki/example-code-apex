<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Event\Handler;

use App\Domain\SharedKernel\Billing\Customer\Event\CustomerEmailAddressChanged;
use App\Domain\SharedKernel\Billing\Customer\Service\PaymentGatewayInterface;
use Broadway\Processor\Processor;

/**
 * Class UpdateStripeCustomerEmailAddressWhenEmailAddressWasChanged
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Event\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class UpdateStripeCustomerEmailAddressWhenEmailAddressWasChanged extends Processor
{
    /**
     * @var PaymentGatewayInterface
     */
    private $paymentGateway;

    /**
     * UpdateStripeCustomerEmailAddressWhenEmailAddressWasChanged constructor.
     *
     * @param PaymentGatewayInterface $paymentGateway
     */
    public function __construct(PaymentGatewayInterface $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param CustomerEmailAddressChanged $event
     */
    public function handleCustomerEmailAddressChanged(CustomerEmailAddressChanged $event): void
    {
        $this->paymentGateway->updateEmail($event);
    }
}