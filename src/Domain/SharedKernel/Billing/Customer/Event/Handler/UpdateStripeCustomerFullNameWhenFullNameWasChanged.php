<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Event\Handler;

use App\Domain\SharedKernel\Billing\Customer\Event\CustomerFullNameChanged;
use App\Domain\SharedKernel\Billing\Customer\Service\PaymentGatewayInterface;
use Broadway\Processor\Processor;

/**
 * Class UpdateStripeCustomerFullNameWhenFullNameWasChanged
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Event\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class UpdateStripeCustomerFullNameWhenFullNameWasChanged extends Processor
{
    /**
     * @var PaymentGatewayInterface
     */
    private $paymentGateway;

    /**
     * UpdateStripeCustomerFullNameWhenFullNameWasChanged constructor.
     *
     * @param PaymentGatewayInterface $paymentGateway
     */
    public function __construct(PaymentGatewayInterface $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param CustomerFullNameChanged $event
     */
    public function handleCustomerFullNameChanged(CustomerFullNameChanged $event): void
    {
        $this->paymentGateway->updateName($event);
    }
}