<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Customer\Event;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\PaymentMethod;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class CustomerRegistered
 *
 * @package App\Domain\SharedKernel\Billing\Customer\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CustomerRegistered implements Serializable
{
    use SerializerValueTrait;

    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var PaymentMethod|null
     */
    private $paymentMethod;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var FullName
     */
    private $fullName;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * CustomerRegistered constructor.
     *
     * @param CustomerId         $customerId
     * @param UserId             $userId
     * @param PaymentMethod|null $paymentMethod
     * @param EmailAddress       $emailAddress
     * @param FullName           $fullName
     * @param DateTimeImmutable  $createdAt
     */
    public function __construct(
        CustomerId $customerId,
        UserId $userId,
        ?PaymentMethod $paymentMethod,
        EmailAddress $emailAddress,
        FullName $fullName,
        DateTimeImmutable $createdAt
    )
    {
        $this->customerId    = $customerId;
        $this->userId        = $userId;
        $this->paymentMethod = $paymentMethod;
        $this->emailAddress  = $emailAddress;
        $this->fullName      = $fullName;
        $this->createdAt     = $createdAt;
    }

    /**
     * @return CustomerId
     */
    public function customerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return PaymentMethod|null
     */
    public function paymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return FullName
     */
    public function fullName(): FullName
    {
        return $this->fullName;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'customer_id');
        Assertion::keyExists($data, 'user_id');
        Assertion::keyExists($data, 'payment_method');
        Assertion::keyExists($data, 'email_address');
        Assertion::keyExists($data, 'full_name');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new CustomerId($data['customer_id']),
            new UserId($data['user_id']),
            self::deserializeValueOrNull($data, 'payment_method', PaymentMethod::class),
            new EmailAddress($data['email_address']),
            new FullName($data['full_name']['first_name'], $data['full_name']['last_name']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'customer_id'    => $this->customerId->serialize(),
            'user_id'        => $this->userId->serialize(),
            'payment_method' => self::serializeValueOrNull($this->paymentMethod),
            'email_address'  => $this->emailAddress->serialize(),
            'full_name'      => $this->fullName->serialize(),
            'created_at'     => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}