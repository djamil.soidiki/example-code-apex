<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Subscription;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use App\Domain\SharedKernel\Billing\Subscription\Event\SubscribedToPlan;
use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionId;
use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionStatus;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class Subscription
 *
 * @package App\Domain\SharedKernel\Billing\Subscription
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Subscription extends EventSourcedAggregateRoot
{
    /**
     * @var SubscriptionId
     */
    private $id;

    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var PlanId
     */
    private $planId;

    /**
     * @var DateTimeImmutable
     */
    private $startDate;

    /**
     * @var SubscriptionStatus
     */
    private $status;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param CustomerId             $customerId
     * @param PlanId                 $planId
     * @param DateTimeImmutable|null $startDate
     *
     * @return static
     * @throws Exception
     */
    public static function subscribeToPlan(CustomerId $customerId, PlanId $planId, ?DateTimeImmutable $startDate = null): self
    {
        $subscription = new self();

        $subscription->apply(new SubscribedToPlan(
                new SubscriptionId(),
                $customerId,
                $planId,
                $startDate ?? new DateTimeImmutable(),
                SubscriptionStatus::active(),
                new DateTimeImmutable()
            )
        );

        return $subscription;
    }

    /**
     * @param SubscribedToPlan $event
     */
    protected function applySubscribedToPlan(SubscribedToPlan $event): void
    {
        $this->id         = $event->subscriptionId();
        $this->customerId = $event->customerId();
        $this->planId     = $event->planId();
        $this->status     = $event->status();
        $this->createdAt  = $event->createdAt();
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }
}