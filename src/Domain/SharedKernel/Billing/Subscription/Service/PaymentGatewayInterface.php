<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Subscription\Service;

use App\Domain\SharedKernel\Billing\Subscription\Event\SubscribedToPlan;

/**
 * Interface PaymentGatewayInterface
 *
 * @package App\Domain\SharedKernel\Billing\Subscription\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface PaymentGatewayInterface
{
    /**
     * @param SubscribedToPlan $subscriptionCreated
     *
     * @return array
     */
    public function createSubscription(SubscribedToPlan $subscriptionCreated): array;
}