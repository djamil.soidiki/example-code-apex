<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Subscription\Repository;

use App\Domain\SharedKernel\Billing\Subscription\Subscription;
use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionId;

/**
 * Interface SubscriptionsInterface
 *
 * @package App\Domain\SharedKernel\Billing\Subscription\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface SubscriptionsInterface
{
    /**
     * @param SubscriptionId $subscriptionId
     *
     * @return Subscription|null
     */
    public function get(SubscriptionId $subscriptionId): ?Subscription;

    /**
     * @param Subscription $subscription
     */
    public function save(Subscription $subscription): void;
}