<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Subscription\Event;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionId;
use App\Domain\SharedKernel\Billing\Subscription\ValueObject\SubscriptionStatus;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class SubscribedToPlan
 *
 * @package App\Domain\SharedKernel\Billing\Subscription\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class SubscribedToPlan implements Serializable
{
    /**
     * @var SubscriptionId
     */
    private $subscriptionId;

    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var PlanId
     */
    private $planId;

    /**
     * @var DateTimeImmutable
     */
    private $startDate;

    /**
     * @var SubscriptionStatus
     */
    private $status;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * SubscribedToPlan constructor.
     *
     * @param SubscriptionId     $subscriptionId
     * @param CustomerId         $customerId
     * @param PlanId             $planId
     * @param DateTimeImmutable  $startDate
     * @param SubscriptionStatus $status
     * @param DateTimeImmutable  $createdAt
     */
    public function __construct(
        SubscriptionId $subscriptionId,
        CustomerId $customerId,
        PlanId $planId,
        DateTimeImmutable $startDate,
        SubscriptionStatus $status,
        DateTimeImmutable $createdAt
    )
    {
        $this->subscriptionId = $subscriptionId;
        $this->customerId     = $customerId;
        $this->planId         = $planId;
        $this->startDate      = $startDate;
        $this->status         = $status;
        $this->createdAt      = $createdAt;
    }

    /**
     * @return SubscriptionId
     */
    public function subscriptionId(): SubscriptionId
    {
        return $this->subscriptionId;
    }

    /**
     * @return CustomerId
     */
    public function customerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return PlanId
     */
    public function planId(): PlanId
    {
        return $this->planId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function startDate(): DateTimeImmutable
    {
        return $this->startDate;
    }

    /**
     * @return SubscriptionStatus
     */
    public function status(): SubscriptionStatus
    {
        return $this->status;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'subscription_id');
        Assertion::keyExists($data, 'customer_id');
        Assertion::keyExists($data, 'plan_id');
        Assertion::keyExists($data, 'start_date');
        Assertion::keyExists($data, 'status');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new SubscriptionId($data['subscription_id']),
            new CustomerId($data['customer_id']),
            new PlanId($data['plan_id']),
            new DateTimeImmutable($data['start_date']),
            new SubscriptionStatus($data['status']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'subscription_id' => $this->subscriptionId->serialize(),
            'customer_id'     => $this->customerId->serialize(),
            'plan_id'         => $this->planId->serialize(),
            'start_date'      => $this->startDate->format(DateTimeImmutable::ATOM),
            'status'          => $this->status->serialize(),
            'created_at'      => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}