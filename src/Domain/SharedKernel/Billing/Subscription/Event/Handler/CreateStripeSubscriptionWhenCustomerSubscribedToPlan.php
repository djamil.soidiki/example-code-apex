<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Subscription\Event\Handler;

use App\Domain\SharedKernel\Billing\Subscription\Event\SubscribedToPlan;
use App\Domain\SharedKernel\Billing\Subscription\Service\PaymentGatewayInterface;
use Broadway\Processor\Processor;

/**
 * Class CreateStripeSubscriptionWhenCustomerSubscribedToPlan
 *
 * @package App\Domain\SharedKernel\Billing\Subscription\Event\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateStripeSubscriptionWhenCustomerSubscribedToPlan extends Processor
{
    /**
     * @var PaymentGatewayInterface
     */
    private $paymentGateway;

    /**
     * CreateStripeSubscriptionWhenCustomerSubscribedToPlan constructor.
     *
     * @param PaymentGatewayInterface $paymentGateway
     */
    public function __construct(PaymentGatewayInterface $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param SubscribedToPlan $event
     */
    public function handleSubscribedToPlan(SubscribedToPlan $event): void
    {
        $this->paymentGateway->createSubscription($event);
    }
}