<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Subscription\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Identity\UuidTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use Exception;

/**
 * Class SubscriptionId
 *
 * @package App\Domain\SharedKernel\Billing\Subscription\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class SubscriptionId extends AbstractValueObject
{
    use UuidTrait;
    use StringTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * SubscriptionId constructor.
     *
     * @param string|null $value
     *
     * @throws Exception
     */
    public function __construct(?string $value = null)
    {
        if ($value !== null)
            $this->value = $this->validateUuid($value);
        else
            $this->value = $this->generateUuid4();

        $this->validateString($this->value);
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}