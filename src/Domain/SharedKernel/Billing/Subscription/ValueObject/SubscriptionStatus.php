<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Subscription\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;

/**
 * Class SubscriptionStatus
 *
 * @package App\Domain\SharedKernel\Billing\Subscription\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class SubscriptionStatus extends AbstractValueObject
{
    use StringTrait;

    const ACTIVE    = 'active';
    const SUSPENDED = 'suspended';
    const CANCELED  = 'canceled';

    /**
     * @var string
     */
    private $value;

    /**
     * SubscriptionStatus constructor.
     *
     * @param string $subscriptionStatus
     */
    public function __construct(string $subscriptionStatus)
    {
        if (!defined("static::" . strtoupper($subscriptionStatus)))
            throw new \InvalidArgumentException(sprintf('"%s" does not exist or it is not a valid subscription status', $subscriptionStatus));

        $this->value = $this->validateString($subscriptionStatus);
    }

    /**
     * @return static
     */
    public static function active(): self
    {
        return new self(self::ACTIVE);
    }

    /**
     * @return static
     */
    public static function suspended(): self
    {
        return new self(self::SUSPENDED);
    }

    /**
     * @return static
     */
    public static function canceled(): self
    {
        return new self(self::CANCELED);
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}