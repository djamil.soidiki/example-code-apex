<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Plan\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;

/**
 * Class PlanName
 *
 * @package App\Domain\SharedKernel\Billing\Plan\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PlanName extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * PlanName constructor.
     *
     * @param string $planName
     */
    public function __construct(string $planName)
    {
        $this->value = $this->validateString($planName);
    }

    /**
     * @inheritdoc
     */
    public function serialize(): string
    {
        return $this->value;
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}

