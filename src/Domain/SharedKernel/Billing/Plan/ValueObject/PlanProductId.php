<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Plan\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;

/**
 * Class PlanProductId
 *
 * @package App\Domain\SharedKernel\Billing\Plan\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PlanProductId extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * PlanProductId constructor.
     *
     * @param string $planProductId
     */
    public function __construct(string $planProductId)
    {
        $this->value = $this->validateString($planProductId);
    }

    /**
     * @inheritdoc
     */
    public function serialize(): string
    {
        return $this->value;
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}

