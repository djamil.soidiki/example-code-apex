<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Plan\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;

/**
 * Class PlanBillingCycle
 *
 * @package App\Domain\SharedKernel\Billing\Plan\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PlanBillingCycle extends AbstractValueObject
{
    use StringTrait;

    const DAY   = 'DAY';
    const WEEK  = 'WEEK';
    const MONTH = 'MONTH';
    const YEAR  = 'YEAR';

    /**
     * @var string
     */
    private $value;

    /**
     * PlanBillingCycle constructor.
     *
     * @param string $billingCycle
     */
    public function __construct(string $billingCycle)
    {
        if (!defined("static::$billingCycle"))
            throw new \InvalidArgumentException(sprintf('"%s" does not exist or it is not a valid billing cycle', $billingCycle));

        $this->value = $billingCycle;
    }

    /**
     * @return static
     */
    public static function day(): self
    {
        return new self(self::DAY);
    }

    /**
     * @return static
     */
    public static function week(): self
    {
        return new self(self::WEEK);
    }

    /**
     * @return static
     */
    public static function month(): self
    {
        return new self(self::MONTH);
    }

    /**
     * @return static
     */
    public static function year(): self
    {
        return new self(self::YEAR);
    }

    /**
     * @inheritDoc
     */
    public function serialize(): string
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}