<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Plan\Repository;

use App\Domain\SharedKernel\Billing\Plan\Plan;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;

/**
 * Interface PlansInterface
 *
 * @package App\Domain\SharedKernel\Billing\Plan\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface PlansInterface
{
    /**
     * @param PlanId $planId
     *
     * @return Plan|null
     */
    public function get(PlanId $planId): ?Plan;

    /**
     * @param Plan $plan
     */
    public function save(Plan $plan): void;
}