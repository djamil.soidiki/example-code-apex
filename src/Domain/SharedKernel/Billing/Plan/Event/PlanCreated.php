<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Plan\Event;

use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanBillingCycle;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanName;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanProductId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class PlanCreated
 *
 * @package App\Domain\SharedKernel\Billing\Plan\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PlanCreated implements Serializable
{
    /**
     * @var PlanId
     */
    private $planId;

    /**
     * @var PlanProductId
     */
    private $productId;

    /**
     * @var PlanName
     */
    private $name;

    /**
     * @var Money
     */
    private $price;

    /**
     * @var PlanBillingCycle
     */
    private $billingCycle;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * PlanCreated constructor.
     *
     * @param PlanId            $planId
     * @param PlanProductId     $productId
     * @param PlanName          $name
     * @param Money             $price
     * @param PlanBillingCycle  $billingCycle
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(
        PlanId $planId,
        PlanProductId $productId,
        PlanName $name,
        Money $price,
        PlanBillingCycle $billingCycle,
        DateTimeImmutable $createdAt
    )
    {
        $this->planId       = $planId;
        $this->productId    = $productId;
        $this->name         = $name;
        $this->price        = $price;
        $this->billingCycle = $billingCycle;
        $this->createdAt    = $createdAt;
    }

    /**
     * @return PlanId
     */
    public function planId(): PlanId
    {
        return $this->planId;
    }

    /**
     * @return PlanProductId
     */
    public function productId(): PlanProductId
    {
        return $this->productId;
    }

    /**
     * @return PlanName
     */
    public function name(): PlanName
    {
        return $this->name;
    }

    /**
     * @return Money
     */
    public function price(): Money
    {
        return $this->price;
    }

    /**
     * @return PlanBillingCycle
     */
    public function billingCycle(): PlanBillingCycle
    {
        return $this->billingCycle;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'plan_id');
        Assertion::keyExists($data, 'product_id');
        Assertion::keyExists($data, 'name');
        Assertion::keyExists($data, 'price');
        Assertion::keyExists($data, 'billing_cycle');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new PlanId($data['plan_id']),
            new PlanProductId($data['product_id']),
            new PlanName($data['name']),
            new Money($data['price']['amount'], $data['price']['currency']),
            new PlanBillingCycle($data['billing_cycle']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'plan_id'       => $this->planId->serialize(),
            'product_id'    => $this->productId->serialize(),
            'name'          => $this->name->serialize(),
            'price'         => $this->price->serialize(),
            'billing_cycle' => $this->billingCycle->serialize(),
            'created_at'    => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}