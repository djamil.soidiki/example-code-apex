<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Plan\Event\Handler;

use App\Domain\SharedKernel\Billing\Plan\Event\PlanCreated;
use App\Domain\SharedKernel\Billing\Plan\Service\PaymentGatewayInterface;
use Broadway\Processor\Processor;

/**
 * Class CreateStripePlanWhenPlanWasCreated
 *
 * @package App\Domain\SharedKernel\Billing\Plan\Event\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateStripePlanWhenPlanWasCreated extends Processor
{
    /**
     * @var PaymentGatewayInterface
     */
    private $paymentGateway;

    /**
     * CreateStripePlanWhenPlanWasCreated constructor.
     *
     * @param PaymentGatewayInterface $paymentGateway
     */
    public function __construct(PaymentGatewayInterface $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param PlanCreated $event
     */
    public function handlePlanCreated(PlanCreated $event): void
    {
        $this->paymentGateway->createPlan($event);
    }
}