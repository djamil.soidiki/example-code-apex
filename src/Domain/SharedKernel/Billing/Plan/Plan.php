<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Plan;

use App\Domain\SharedKernel\Billing\Plan\Event\PlanCreated;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanBillingCycle;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanName;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanProductId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class Plan
 *
 * @package App\Domain\SharedKernel\Billing\Plan
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Plan extends EventSourcedAggregateRoot
{
    /**
     * @var PlanId
     */
    private $id;

    /**
     * @var PlanProductId
     */
    private $productId;

    /**
     * @var PlanName
     */
    private $name;

    /**
     * @var Money
     */
    private $price;

    /**
     * @var PlanBillingCycle
     */
    private $billingCycle;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param PlanProductId    $productId
     * @param PlanName         $name
     * @param Money            $price
     * @param PlanBillingCycle $billingCycle
     *
     * @return static
     * @throws Exception
     */
    public static function create(PlanProductId $productId, PlanName $name, Money $price, PlanBillingCycle $billingCycle): self
    {
        $plan = new self();

        $plan->apply(new PlanCreated(
                new PlanId(),
                $productId,
                $name,
                $price,
                $billingCycle,
                new DateTimeImmutable()
            )
        );

        return $plan;
    }

    /**
     * @param PlanCreated $event
     */
    protected function applyPlanCreated(PlanCreated $event): void
    {
        $this->id           = $event->planId();
        $this->productId    = $event->productId();
        $this->name         = $event->name();
        $this->price        = $event->price();
        $this->billingCycle = $event->billingCycle();
        $this->createdAt    = $event->createdAt();
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }
}