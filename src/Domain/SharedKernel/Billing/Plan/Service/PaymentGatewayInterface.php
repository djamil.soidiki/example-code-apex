<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Billing\Plan\Service;

use App\Domain\SharedKernel\Billing\Plan\Event\PlanCreated;

/**
 * Interface PaymentGatewayInterface
 *
 * @package App\Domain\SharedKernel\Billing\Plan\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface PaymentGatewayInterface
{
    /**
     * @param PlanCreated $planCreated
     *
     * @return array
     */
    public function createPlan(PlanCreated $planCreated): array;
}