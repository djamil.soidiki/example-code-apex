<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject;

/**
 * Class AbstractValueObject
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractValueObject
{
    /**
     * Compare two AbstractValueObject and tells whether they can be considered equal
     *
     * @param AbstractValueObject $object
     *
     * @return bool
     */
    public function sameValueAs(AbstractValueObject $object): bool
    {
        return $this->serialize() === $object->serialize();
    }

    /**
     * Returns a mixed (string, integer, array, etc) representation of the object
     *
     * @return mixed
     */
    abstract public function serialize();

    /**
     * Returns a string representation of the object
     *
     * @return string
     */
    abstract public function __toString(): string;
}