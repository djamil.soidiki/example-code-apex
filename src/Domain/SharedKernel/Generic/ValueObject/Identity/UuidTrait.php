<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Identity;

use Exception;
use Ramsey\Uuid\Uuid as BaseUuid;

/**
 * Trait Uuid
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Identity
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
trait UuidTrait
{
    /**
     * Generate an uuid4 based on Ramsey/uuid library
     *
     * @return string
     * @throws Exception
     */
    protected function generateUuid4(): string
    {
        $uuid_str = BaseUuid::uuid4();

        return $uuid_str->serialize();
    }

    /**
     * Validate a given string as a valid uuid
     *
     * @param string $uuid
     *
     * @return string
     */
    protected function validateUuid(string $uuid): string
    {
        $pattern = '/' . BaseUuid::VALID_PATTERN . '/';

        if (!\preg_match($pattern, $uuid)) {
            throw new \InvalidArgumentException(sprintf('"%s" is not a valid uuid. Valid pattern: "%s"', $uuid, $pattern));
        }

        return $uuid;
    }

    /**
     * @return string
     */
    public function encode(): string
    {
        return self::gmp_base_convert(str_replace('-', '', $this->value), 16, 62);
    }

    /**
     * @param string $hashid
     *
     * @return string
     */
    public static function decode(string $hashid): string
    {
        return array_reduce([20, 16, 12, 8], function ($uuid, $offset) {
            return substr_replace($uuid, '-', $offset, 0);
        }, str_pad(static::gmp_base_convert($hashid, 62, 16), 32, '0', STR_PAD_LEFT)
        );
    }

    /**
     * @param string $value
     * @param int    $initialBase
     * @param int    $newBase
     *
     * @return string
     */
    private static function gmp_base_convert(string $value, int $initialBase, int $newBase): string
    {
        return \gmp_strval(\gmp_init($value, $initialBase), $newBase);
    }
}