<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Token;

use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;

/**
 * Class Token
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Token
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Token extends AbstractValueObject
{
    use StringTrait;

    const DEFAULT_LENGTH = 32;

    /**
     * @var string
     */
    private $value;

    /**
     * Token constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->value = $this->validateString($token);
    }

    /**
     * @inheritDoc
     */
    public function serialize(): string
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}