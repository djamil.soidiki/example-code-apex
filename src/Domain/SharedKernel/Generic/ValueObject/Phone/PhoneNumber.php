<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Phone;

use App\Domain\SharedKernel\Generic\ValueObject\Country\CountryCode;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;

/**
 * Class PhoneNumber
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Phone
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PhoneNumber extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var CountryCode
     */
    private $countryCode;

    /**
     * @var string
     */
    private $number;

    /**
     * PhoneNumber constructor.
     *
     * @param string $country
     * @param string $number
     */
    public function __construct(string $country, string $number)
    {
        $this->countryCode = new CountryCode($country);
        $this->number      = $this->validateString($number);
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'country_code' => $this->countryCode->serialize(),
            'number'       => $this->number
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        if ($this->number === null)
            return "";

        return sprintf('%s %s', $this->countryCode, $this->number);
    }
}