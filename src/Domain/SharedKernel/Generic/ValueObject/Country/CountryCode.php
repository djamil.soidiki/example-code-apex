<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Country;

use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use Symfony\Component\Mime\Exception\InvalidArgumentException;

/**
 * Class CountryCode
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Country
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CountryCode extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var array
     */
    private static $countryCodes = [
        'US' => [
            'name'         => 'United States',
            'iso_code'     => 'USA',
            'country_code' => '1'
        ]
    ];

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $code;

    /**
     * CountryCode constructor.
     *
     * @param string $country
     */
    public function __construct(string $country)
    {
        if (!isset(self::$countryCodes[$country]))
            throw new InvalidArgumentException(
                sprintf('Unknown country code "%s"', $country)
            );

        $this->country = $this->validateString($country);
        $this->code    = $this->validateString(self::$countryCodes[$country]['country_code']);
    }

    /**
     * @return array
     */
    public static function countryCodes(): array
    {
        return self::$countryCodes;
    }

    /**
     * @return string
     */
    public function country(): string
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'country' => $this->country,
            'code'    => $this->code,
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        if ($this->code === null)
            return "";

        return sprintf('+%s', $this->code);
    }
}