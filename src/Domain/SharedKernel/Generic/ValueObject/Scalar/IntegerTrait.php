<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Scalar;

/**
 * Trait IntegerTrait
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Scalar
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
trait IntegerTrait
{
    /**
     * @param integer $integer
     *
     * @return integer
     */
    protected function validateInteger(int $integer): int
    {
        if (false === \is_integer($integer)) {
            throw new \InvalidArgumentException(sprintf('"%s" is not a valid integer', $integer));
        }

        return $integer;
    }
}