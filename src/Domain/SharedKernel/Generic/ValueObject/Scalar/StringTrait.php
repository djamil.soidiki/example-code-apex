<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Scalar;

/**
 * Trait StringTrait
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Scalar
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
trait StringTrait
{
    /**
     * @param string $string
     *
     * @return string
     */
    protected function validateString(string $string): string
    {
        if (false === \is_string($string)) {
            throw new \InvalidArgumentException(sprintf('"%s" is not a valid string', $string));
        }

        return $string;
    }
}