<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Scalar;

/**
 * Trait FloatTrait
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Scalar
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
trait FloatTrait
{
    /**
     * @param float $float
     *
     * @return float
     */
    protected function validateFloat(float $float): float
    {
        if (false === \is_float($float)) {
            throw new \InvalidArgumentException(sprintf('"%s" is not a valid float', $float));
        }

        return $float;
    }
}