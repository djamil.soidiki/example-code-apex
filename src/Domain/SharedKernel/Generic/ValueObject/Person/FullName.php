<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Person;

use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;

/**
 * Class FullName
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Person
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FullName extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * FullName constructor.
     *
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct(string $firstName, string $lastName)
    {
        $this->firstName = $this->validateString($firstName);
        $this->lastName  = $this->validateString($lastName);
    }

    /**
     * @return string
     */
    public function firstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function lastName(): string
    {
        return $this->lastName;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'first_name' => $this->firstName,
            'last_name'  => $this->lastName
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return "$this->firstName $this->lastName";
    }
}