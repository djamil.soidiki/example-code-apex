<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Geography;

use App\Domain\SharedKernel\Generic\ValueObject\Scalar\FloatTrait;
use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;

/**
 * Class Coordinates
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Geography
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Coordinates extends AbstractValueObject
{
    use FloatTrait;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $longitude;

    /**
     * Coordinates constructor.
     *
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude  = $this->validateFloat($latitude);
        $this->longitude = $this->validateFloat($longitude);
    }

    /**
     * @return float|null
     */
    public function latitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @return float|null
     */
    public function longitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'latitude'  => $this->latitude,
            'longitude' => $this->longitude
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return \sprintf('( %s;%s )', $this->latitude, $this->longitude);
    }
}