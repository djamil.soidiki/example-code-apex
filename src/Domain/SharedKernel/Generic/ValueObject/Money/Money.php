<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Money;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\FloatTrait;
use Assert\Assertion;
use Assert\AssertionFailedException;
use InvalidArgumentException;

/**
 * Class Money
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Money
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Money extends AbstractValueObject
{
    use FloatTrait;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * Money constructor.
     *
     * @param float           $amount
     * @param Currency|string $currency
     */
    public function __construct(float $amount, $currency)
    {
        $this->amount   = $this->validateFloat($amount);
        $this->currency = self::handleCurrencyArgument($currency);
    }

    /**
     * @param array $money
     *
     * @return static
     * @throws AssertionFailedException
     */
    public static function fromArray(array $money): self
    {
        Assertion::keyExists($money, 'amount');
        Assertion::keyExists($money, 'currency');

        return new self($money['amount'], $money['currency']);
    }

    /**
     * @param $currency
     *
     * @return Currency
     */
    private static function handleCurrencyArgument($currency)
    {
        if (!$currency instanceof Currency && !is_string($currency)) {
            throw new InvalidArgumentException('$currency must be an object of type Currency or a string');
        }
        if (is_string($currency)) {
            $currency = new Currency($currency);
        }
        return $currency;
    }

    /**
     * @return float|null
     */
    public function amount(): ?float
    {
        return $this->amount;
    }

    /**
     * @return Currency|null
     */
    public function currency(): ?Currency
    {
        return $this->currency;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'amount'   => $this->amount,
            'currency' => $this->currency->serialize()
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        if ($this->amount === null) {
            $currency = new Currency('USD');
            return "0 $currency";
        }

        $amount = number_format($this->amount);

        return "$amount $this->currency";
    }
}