<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\ValueObject\Web;

use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;

/**
 * Class EmailAddress
 *
 * @package App\Domain\SharedKernel\Generic\ValueObject\Web
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class EmailAddress extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * EmailAddress constructor.
     *
     * @param string $emailAddress
     */
    public function __construct(string $emailAddress)
    {
        $filteredEmailAddress = filter_var($emailAddress, FILTER_VALIDATE_EMAIL);

        if ($filteredEmailAddress === false) {
            throw new \InvalidArgumentException(sprintf('"%s" is not a valid email address', $emailAddress));
        }

        $this->value = $this->validateString($filteredEmailAddress);
    }

    /**
     * @inheritDoc
     */
    public function serialize(): string
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}