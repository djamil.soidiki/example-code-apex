<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\Exception;

/**
 * Class NotFoundException
 *
 * @package App\Domain\SharedKernel\Generic\Exception
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class NotFoundException extends \Exception
{
    /***
     * NotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('Resource not found');
    }
}