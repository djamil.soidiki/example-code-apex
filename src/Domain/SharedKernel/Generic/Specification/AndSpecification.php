<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\Specification;

/**
 * Class AndSpecification
 *
 * @package App\Domain\SharedKernel\Generic\Specification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AndSpecification extends CompositeSpecification
{
    /**
     * @var SpecificationInterface
     */
    private $specification1;

    /**
     * @var SpecificationInterface
     */
    private $specification2;

    /**
     * AndSpecification constructor.
     *
     * @param $specification1
     * @param $specification2
     */
    public function __construct($specification1, $specification2)
    {
        $this->specification1 = $specification1;
        $this->specification2 = $specification2;
    }

    /**
     * @inheritDoc
     */
    public function isSatisfiedBy($candidate): bool
    {
        return $this->specification1->isSatisfiedBy($candidate)
            && $this->specification2->isSatisfiedBy($candidate);
    }
}