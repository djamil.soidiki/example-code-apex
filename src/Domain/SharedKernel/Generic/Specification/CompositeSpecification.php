<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\Specification;

/**
 * Class CompositeSpecification
 *
 * @package App\Domain\SharedKernel\Generic\Specification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class CompositeSpecification implements SpecificationInterface
{
    /**
     * @inheritDoc
     */
    public abstract function isSatisfiedBy($candidate): bool;

    /**
     * @param SpecificationInterface $specification
     *
     * @return AndSpecification
     */
    public function andX(SpecificationInterface $specification)
    {
        return new AndSpecification($this, $specification);
    }
}