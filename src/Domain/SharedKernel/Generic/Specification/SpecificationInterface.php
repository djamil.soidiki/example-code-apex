<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\Specification;

/**
 * Interface SpecificationInterface
 *
 * @package App\Domain\SharedKernel\Generic\Specification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface SpecificationInterface
{
    /**
     * @param $candidate
     *
     * @return bool
     */
    public function isSatisfiedBy($candidate): bool;
}