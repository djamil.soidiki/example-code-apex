<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\Util;

/**
 * Trait SerializerValueTrait
 *
 * @package App\Domain\SharedKernel\Generic\Util
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
trait SerializerValueTrait
{
    /**
     * @param array       $data
     * @param string      $key
     * @param string|null $class
     * @param bool        $fromArray
     *
     * @return mixed|null
     */
    public static function deserializeValueOrNull(array $data, string $key, ?string $class = null, bool $fromArray = false)
    {
        if (!isset($data[$key]) || $data[$key] === null) {
            return null;
        } else {
            if ($class !== null && $fromArray === true)
                return $class::fromArray($data[$key]);
            if ($class !== null)
                return new $class($data[$key]);
            else
                return $data[$key];
        }
    }

    /**
     * @param null $value
     *
     * @return null
     */
    public static function serializeValueOrNull($value = null)
    {
        if ($value === null)
            return null;

        if ($value instanceof \DateTimeImmutable)
            return $value->format(DateTimeImmutable::ATOM);
        else
            return $value->serialize();
    }
}