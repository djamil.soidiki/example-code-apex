<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Generic\Util;

/**
 * Trait SlugifyTrait
 *
 * @package App\Domain\SharedKernel\Generic\Util
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
trait SlugifyTrait
{
    /**
     * @param string $value
     *
     * @return string
     */
    public function slugify(string $value): string
    {
        // replace non letter or digits by -
        $value = preg_replace('~[^\pL\d]+~u', '-', $value);

        // remove unwanted characters
        $value = preg_replace('~[^-\w]+~', '', $value);

        // trim
        $value = trim($value, '-');

        // remove duplicate -
        $value = preg_replace('~-+~', '-', $value);

        // lowercase
        $value = strtolower($value);

        if (empty($value)) {
            return 'n-a';
        }

        return $value;
    }
}