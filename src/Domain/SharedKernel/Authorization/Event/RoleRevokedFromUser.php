<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization\Event;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class RoleRevokedFromUser
 *
 * @package App\Domain\SharedKernel\Authorization\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RoleRevokedFromUser implements Serializable
{
    /**
     * @var RoleId
     */
    private $roleId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * RoleRevokedFromUser constructor.
     *
     * @param RoleId            $roleId
     * @param UserId            $userId
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(RoleId $roleId, UserId $userId, DateTimeImmutable $updatedAt)
    {
        $this->roleId    = $roleId;
        $this->userId    = $userId;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return RoleId
     */
    public function roleId(): RoleId
    {
        return $this->roleId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     *
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'role_id');
        Assertion::keyExists($data, 'user_id');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new RoleId($data['role_id']),
            new UserId($data['user_id']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'role_id'    => $this->roleId->serialize(),
            'user_id'    => $this->userId->serialize(),
            'updated_at' => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}