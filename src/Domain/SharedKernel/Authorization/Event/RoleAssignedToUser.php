<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization\Event;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authorization\ValueObject\AssignmentId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class RoleAssignedToUser
 *
 * @package App\Domain\SharedKernel\Authorization\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RoleAssignedToUser implements Serializable
{
    /**
     * @var AssignmentId
     */
    private $assignmentId;

    /**
     * @var RoleId
     */
    private $roleId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * RoleAssignedToUser constructor.
     *
     * @param AssignmentId      $assignmentId
     * @param RoleId            $roleId
     * @param UserId            $userId
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(AssignmentId $assignmentId, RoleId $roleId, UserId $userId, DateTimeImmutable $createdAt)
    {
        $this->assignmentId = $assignmentId;
        $this->roleId       = $roleId;
        $this->userId       = $userId;
        $this->createdAt    = $createdAt;
    }

    /**
     * @return AssignmentId
     */
    public function assignmentId(): AssignmentId
    {
        return $this->assignmentId;
    }

    /**
     * @return RoleId
     */
    public function roleId(): RoleId
    {
        return $this->roleId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     *
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'assignment_id');
        Assertion::keyExists($data, 'role_id');
        Assertion::keyExists($data, 'user_id');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new AssignmentId($data['assignment_id']),
            new RoleId($data['role_id']),
            new UserId($data['user_id']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'assignment_id' => $this->assignmentId->serialize(),
            'role_id'       => $this->roleId->serialize(),
            'user_id'       => $this->userId->serialize(),
            'created_at'    => $this->createdAt->format(DateTimeImmutable::ATOM),
        ];
    }
}