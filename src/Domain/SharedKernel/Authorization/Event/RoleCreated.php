<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization\Event;

use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleName;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class RoleCreated
 *
 * @package App\Domain\SharedKernel\Authorization\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RoleCreated implements Serializable
{
    /**
     * @var RoleId
     */
    private $roleId;

    /**
     * @var RoleName
     */
    private $name;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * RoleCreated constructor.
     *
     * @param RoleId            $roleId
     * @param RoleName          $name
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(RoleId $roleId, RoleName $name, DateTimeImmutable $createdAt)
    {
        $this->roleId    = $roleId;
        $this->name      = $name;
        $this->createdAt = $createdAt;
    }

    /**
     * @return RoleId
     */
    public function roleId(): RoleId
    {
        return $this->roleId;
    }

    /**
     * @return RoleName
     */
    public function name(): RoleName
    {
        return $this->name;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     *
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'role_id');
        Assertion::keyExists($data, 'name');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new RoleId($data['role_id']),
            new RoleName($data['name']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'role_id'    => $this->roleId->serialize(),
            'name'       => $this->name->serialize(),
            'created_at' => $this->createdAt->format(DateTimeImmutable::ATOM),
        ];
    }
}