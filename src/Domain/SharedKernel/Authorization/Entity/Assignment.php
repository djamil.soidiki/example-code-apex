<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization\Entity;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authorization\ValueObject\AssignmentId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use Broadway\EventSourcing\SimpleEventSourcedEntity;
use DateTimeImmutable;
use Exception;

/**
 * Class Assignment
 *
 * @package App\Domain\SharedKernel\Authorization\Entity
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Assignment extends SimpleEventSourcedEntity
{
    /**
     * @var AssignmentId
     */
    private $id;

    /**
     * @var RoleId
     */
    private $roleId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * Assignment constructor.
     *
     * @param AssignmentId $id
     * @param RoleId       $roleId
     * @param UserId       $userId
     *
     * @throws Exception
     */
    public function __construct(AssignmentId $id, RoleId $roleId, UserId $userId)
    {
        $this->id        = $id;
        $this->roleId    = $roleId;
        $this->userId    = $userId;
        $this->createdAt = new DateTimeImmutable();
    }
}