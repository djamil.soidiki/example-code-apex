<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization\Exception;

/**
 * Class RoleNameIsNotUniqueException
 *
 * @package App\Domain\SharedKernel\Authorization\Exception
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class RoleNameIsNotUniqueException extends \InvalidArgumentException
{
    /**
     * RoleNameIsNotUniqueException constructor.
     *
     * @param string $roleName
     */
    public function __construct(string $roleName)
    {
        parent::__construct("Role name '$roleName' already exist");
    }
}