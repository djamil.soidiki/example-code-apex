<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization\Specification;

use App\Domain\SharedKernel\Authorization\Repository\Query\RoleNameQueryInterface;
use App\Domain\SharedKernel\Generic\Specification\SpecificationInterface;

/**
 * Class RoleNameIsUniqueSpecification
 *
 * @package App\Domain\SharedKernel\Authorization\Specification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RoleNameIsUniqueSpecification implements SpecificationInterface
{
    /**
     * @var RoleNameQueryInterface
     */
    private $roleNameQuery;

    /**
     * RoleNameIsUniqueSpecification constructor.
     *
     * @param RoleNameQueryInterface $roleNameQuery
     */
    public function __construct(RoleNameQueryInterface $roleNameQuery)
    {
        $this->roleNameQuery = $roleNameQuery;
    }

    /**
     * @inheritDoc
     */
    public function isSatisfiedBy($roleName): bool
    {
        return $this->roleNameQuery->isRoleNameUnique($roleName);
    }
}