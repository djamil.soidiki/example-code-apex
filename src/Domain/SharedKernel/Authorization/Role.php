<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authorization\Entity\Assignment;
use App\Domain\SharedKernel\Authorization\Event\RoleAssignedToUser;
use App\Domain\SharedKernel\Authorization\Event\RoleCreated;
use App\Domain\SharedKernel\Authorization\Event\RoleRevokedFromUser;
use App\Domain\SharedKernel\Authorization\ValueObject\AssignmentId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleName;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class Role
 *
 * @package App\Domain\SharedKernel\Authorization
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Role extends EventSourcedAggregateRoot
{
    const SUPER_ADMINISTRATOR = 'Super Administrator';
    const ADMINISTRATOR       = 'Administrator';
    const SUBSCRIBER          = 'Subscriber';

    /**
     * @var RoleId
     */
    private $id;

    /**
     * @var RoleName
     */
    private $name;

    /**
     * @var Assignment[]
     */
    private $assignments = [];

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param RoleName $name
     *
     * @return static
     * @throws Exception
     */
    public static function createRole(RoleName $name): self
    {
        $role = new self();

        $role->apply(new RoleCreated(
            new RoleId(),
            $name,
            new DateTimeImmutable()
        ));

        return $role;
    }

    /**
     * @param UserId $userId
     *
     * @throws Exception
     */
    public function assignToUser(UserId $userId): void
    {
        $this->apply(new RoleAssignedToUser(
            new AssignmentId(),
            $this->id,
            $userId,
            new DateTimeImmutable()
        ));
    }


    /**
     * @param UserId $userId
     *
     * @throws Exception
     */
    public function revokeFromUser(UserId $userId): void
    {
        if (array_key_exists($userId->serialize(), $this->assignments)) {

            $this->apply(new RoleRevokedFromUser(
                $this->id,
                $userId,
                new DateTimeImmutable()
            ));
        }
    }

    /**
     * @param RoleCreated $event
     */
    protected function applyRoleCreated(RoleCreated $event): void
    {
        $this->id        = $event->roleId();
        $this->name      = $event->name();
        $this->createdAt = $event->createdAt();
    }

    /**
     * @param RoleAssignedToUser $event
     *
     * @throws Exception
     */
    protected function applyRoleAssignedToUser(RoleAssignedToUser $event): void
    {
        $this->id = $event->roleId();

        $this->assignments[$event->userId()->serialize()] = new Assignment(
            $event->assignmentId(),
            $event->roleId(),
            $event->userId()
        );
    }

    /**
     * @param RoleRevokedFromUser $event
     *
     * @throws Exception
     */
    protected function applyRoleRevokedFromUser(RoleRevokedFromUser $event): void
    {
        $this->id = $event->roleId();

        unset($this->assignments[$event->userId()->serialize()]);
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @inheritDoc
     */
    protected function getChildEntities(): array
    {
        return $this->assignments;
    }
}