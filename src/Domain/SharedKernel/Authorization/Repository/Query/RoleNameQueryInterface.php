<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization\Repository\Query;

use App\Domain\SharedKernel\Authorization\ValueObject\RoleName;

/**
 * Interface RoleNameQueryInterface
 *
 * @package App\Domain\SharedKernel\Authorization\Repository\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface RoleNameQueryInterface
{
    /**
     * @param RoleName $roleName
     *
     * @return bool
     */
    public function isRoleNameUnique(RoleName $roleName): bool;
}