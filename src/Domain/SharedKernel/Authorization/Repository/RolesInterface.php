<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization\Repository;

use App\Domain\SharedKernel\Authorization\Role;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;

/**
 * Interface RolesInterface
 *
 * @package App\Domain\SharedKernel\Authorization\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface RolesInterface
{
    /**
     * @param RoleId $permissionId
     *
     * @return Role|null
     */
    public function get(RoleId $permissionId): ?Role;

    /**
     * @param Role $permission
     */
    public function save(Role $permission): void;
}