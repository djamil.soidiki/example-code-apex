<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authorization\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;

/**
 * Class RoleName
 *
 * @package App\Domain\SharedKernel\Authorization\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RoleName extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * RoleName constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $this->validateString($value);
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}