<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\TrialPeriod\Service;

use DateTimeImmutable;

/**
 * Class TrialPeriodDateGenerator
 *
 * @package App\Domain\SharedKernel\TrialPeriod\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TrialPeriodDateGenerator
{
    /**
     * Default trial duration in days
     */
    const DEFAULT_DURATION = 31;

    public static function generate(): array
    {
        $startDate = new DateTimeImmutable();
        $endDate   = new DateTimeImmutable();
        $endDate   = $endDate->modify('+' . self::DEFAULT_DURATION . ' days');

        return [
            $startDate,
            $endDate
        ];
    }
}