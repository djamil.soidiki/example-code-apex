<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\TrialPeriod\Specification;

use App\Domain\SharedKernel\Generic\Specification\SpecificationInterface;
use DateTimeImmutable;
use Exception;

/**
 * Class TrialPeriodIsActiveSpecification
 *
 * @package App\Domain\SharedKernel\TrialPeriod\Specification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TrialPeriodIsActiveSpecification implements SpecificationInterface
{
    /**
     * @param $trialPeriod
     *
     * @return bool
     * @throws Exception
     */
    public function isSatisfiedBy($trialPeriod): bool
    {
        $currentDate = new DateTimeImmutable();

        /** @var DateTimeImmutable $startDate */
        $startDate = $trialPeriod->startDate();

        /** @var DateTimeImmutable $endDate */
        $endDate = $trialPeriod->endDate();

        return ($startDate < $currentDate && $endDate > $currentDate);
    }
}