<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\TrialPeriod;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\TrialPeriod\Event\TrialPeriodStarted;
use App\Domain\SharedKernel\TrialPeriod\ValueObject\TrialPeriodId;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class TrialPeriod
 *
 * @package App\Domain\SharedKernel\TrialPeriod
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TrialPeriod extends EventSourcedAggregateRoot
{
    /**
     * @var TrialPeriodId
     */
    private $id;

    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var DateTimeImmutable
     */
    private $startDate;

    /**
     * @var DateTimeImmutable
     */
    private $endDate;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param CustomerId        $customerId
     * @param DateTimeImmutable $startDate
     * @param DateTimeImmutable $endDate
     *
     * @return static
     * @throws Exception
     */
    public static function start(CustomerId $customerId, DateTimeImmutable $startDate, DateTimeImmutable $endDate): self
    {
        $trialPeriod = new self();

        $trialPeriod->apply(new TrialPeriodStarted(
            new TrialPeriodId(),
            $customerId,
            $startDate,
            $endDate,
            new DateTimeImmutable()
        ));

        return $trialPeriod;
    }

    /**
     * @param TrialPeriodStarted $event
     */
    protected function applyTrialPeriodStarted(TrialPeriodStarted $event): void
    {
        $this->id         = $event->trialPeriodId();
        $this->customerId = $event->customerId();
        $this->startDate  = $event->startDate();
        $this->endDate    = $event->endDate();
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }
}