<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\TrialPeriod\Repository;

use App\Domain\SharedKernel\TrialPeriod\TrialPeriod;
use App\Domain\SharedKernel\TrialPeriod\ValueObject\TrialPeriodId;

/**
 * Interface TrialPeriodsInterface
 *
 * @package App\Domain\SharedKernel\TrialPeriod\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface TrialPeriodsInterface
{
    /**
     * @param TrialPeriodId $trialPeriodId
     *
     * @return TrialPeriod|null
     */
    public function get(TrialPeriodId $trialPeriodId): ?TrialPeriod;

    /**
     * @param TrialPeriod $trialPeriod
     */
    public function save(TrialPeriod $trialPeriod): void;
}