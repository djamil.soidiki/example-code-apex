<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\TrialPeriod\Event;

use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\TrialPeriod\ValueObject\TrialPeriodId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class TrialPeriodStarted
 *
 * @package App\Domain\SharedKernel\TrialPeriod\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TrialPeriodStarted implements Serializable
{
    /**
     * @var TrialPeriodId
     */
    private $trialPeriodId;

    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var DateTimeImmutable
     */
    private $startDate;

    /**
     * @var DateTimeImmutable
     */
    private $endDate;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * TrialPeriodStarted constructor.
     *
     * @param TrialPeriodId     $trialPeriodId
     * @param CustomerId        $customerId
     * @param DateTimeImmutable $startDate
     * @param DateTimeImmutable $endDate
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(
        TrialPeriodId $trialPeriodId,
        CustomerId $customerId,
        DateTimeImmutable $startDate,
        DateTimeImmutable $endDate,
        DateTimeImmutable $createdAt
    )
    {
        $this->trialPeriodId = $trialPeriodId;
        $this->customerId    = $customerId;
        $this->startDate     = $startDate;
        $this->endDate       = $endDate;
        $this->createdAt     = $createdAt;
    }

    /**
     * @return TrialPeriodId
     */
    public function trialPeriodId(): TrialPeriodId
    {
        return $this->trialPeriodId;
    }

    /**
     * @return CustomerId
     */
    public function customerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function startDate(): DateTimeImmutable
    {
        return $this->startDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function endDate(): DateTimeImmutable
    {
        return $this->endDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'trial_period_id');
        Assertion::keyExists($data, 'customer_id');
        Assertion::keyExists($data, 'start_date');
        Assertion::keyExists($data, 'end_date');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new TrialPeriodId($data['trial_period_id']),
            new CustomerId($data['customer_id']),
            new DateTimeImmutable($data['start_date']),
            new DateTimeImmutable($data['end_date']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'trial_period_id' => $this->trialPeriodId->serialize(),
            'customer_id'     => $this->customerId->serialize(),
            'start_date'      => $this->startDate->format(DateTimeImmutable::ATOM),
            'end_date'        => $this->endDate->format(DateTimeImmutable::ATOM),
            'created_at'      => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}