<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication;

use App\Domain\SharedKernel\Authentication\Entity\PasswordReminder;
use App\Domain\SharedKernel\Authentication\Event\UserPasswordChanged;
use App\Domain\SharedKernel\Authentication\Event\UserEmailAddressChanged;
use App\Domain\SharedKernel\Authentication\Event\UserPasswordReset;
use App\Domain\SharedKernel\Authentication\Event\PasswordReminderRequested;
use App\Domain\SharedKernel\Authentication\Event\UserRegistered;
use App\Domain\SharedKernel\Authentication\Exception\InvalidCurrentPasswordException;
use App\Domain\SharedKernel\Authentication\Service\Security\Password\VerifyPassword;
use App\Domain\SharedKernel\Authentication\Service\Token\PasswordReminderTokenGenerator;
use App\Domain\SharedKernel\Authentication\ValueObject\Password;
use App\Domain\SharedKernel\Authentication\ValueObject\PasswordHash;
use App\Domain\SharedKernel\Authentication\ValueObject\PasswordReminderId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class User
 *
 * @package App\Domain\SharedKernel\Authentication
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class User extends EventSourcedAggregateRoot
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var PasswordHash
     */
    private $password;

    /**
     * @var PasswordReminder|null
     */
    private $passwordReminder;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param EmailAddress $emailAddress
     * @param PasswordHash $passwordHash
     *
     * @return static
     * @throws Exception
     */
    public static function register(EmailAddress $emailAddress, PasswordHash $passwordHash): self
    {
        $user = new self();

        $user->apply(new UserRegistered(
                new UserId(),
                $emailAddress,
                $passwordHash,
                new DateTimeImmutable()
            )
        );

        return $user;
    }

    /**
     * @throws Exception
     */
    public function requestPasswordReminder(): void
    {
        $expiresAt = new DateTimeImmutable();
        $expiresAt = $expiresAt->add(new \DateInterval('PT' . PasswordReminder::DEFAULT_LIFETIME . 'M'));

        $this->apply(new PasswordReminderRequested(
                new PasswordReminderId(),
                $this->id,
                PasswordReminderTokenGenerator::generate(),
                $expiresAt,
                new DateTimeImmutable()
            )
        );
    }

    /**
     * @param PasswordHash $passwordHash
     *
     * @throws Exception
     */
    public function resetPassword(PasswordHash $passwordHash): void
    {
        $this->apply(new UserPasswordReset($this->id, $passwordHash, new DateTimeImmutable()));
    }

    /**
     * @param EmailAddress $emailAddress
     *
     * @throws Exception
     */
    public function changeEmailAddress(EmailAddress $emailAddress): void
    {
        if (!$this->emailAddress->sameValueAs($emailAddress))
            $this->apply(new UserEmailAddressChanged($this->id, $emailAddress, new DateTimeImmutable()));
    }

    /**
     * @param Password     $currentPassword
     * @param PasswordHash $newPassword
     *
     * @throws Exception
     */
    public function changePassword(Password $currentPassword, PasswordHash $newPassword): void
    {
        if (!VerifyPassword::verify($currentPassword, $this->password))
            throw new InvalidCurrentPasswordException();

        $this->apply(new UserPasswordChanged($this->id, $newPassword, new DateTimeImmutable()));
    }

    /**
     * @param UserRegistered $event
     */
    public function applyUserRegistered(UserRegistered $event): void
    {
        $this->id           = $event->userId();
        $this->emailAddress = $event->emailAddress();
        $this->password     = $event->password();
        $this->createdAt    = $event->createdAt();
    }

    /**
     * @param PasswordReminderRequested $event
     *
     * @throws Exception
     */
    public function applyPasswordReminderRequested(PasswordReminderRequested $event): void
    {
        $this->id        = $event->userId();
        $this->updatedAt = $event->createdAt();

        $this->passwordReminder = new PasswordReminder(
            $event->passwordReminderId(),
            $event->userId(),
            $event->token(),
            $event->expiresAt()
        );
    }

    /**
     * @param UserPasswordReset $event
     */
    public function applyUserPasswordReset(UserPasswordReset $event): void
    {
        $this->id               = $event->userId();
        $this->password         = $event->password();
        $this->passwordReminder = null;
        $this->updatedAt        = $event->updatedAt();
    }

    /**
     * @param UserEmailAddressChanged $event
     */
    public function applyUserEmailAddressChanged(UserEmailAddressChanged $event): void
    {
        $this->id           = $event->userId();
        $this->emailAddress = $event->emailAddress();
        $this->updatedAt    = $event->updatedAt();
    }

    /**
     * @param UserPasswordChanged $event
     */
    public function applyUserPasswordChanged(UserPasswordChanged $event): void
    {
        $this->id        = $event->userId();
        $this->password  = $event->password();
        $this->updatedAt = $event->updatedAt();
    }

    /**
     * @inheritDoc
     */
    protected function getChildEntities(): array
    {
        return ($this->passwordReminder === null) ? [] : [$this->passwordReminder];
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @todo Remove those getters
     *
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return PasswordHash
     */
    public function password(): PasswordHash
    {
        return $this->password;
    }

    /**
     * @return PasswordReminder
     */
    public function passwordReminder(): PasswordReminder
    {
        return $this->passwordReminder;
    }
}