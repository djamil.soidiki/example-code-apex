<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Exception;

/**
 * Class EmailAddressNotFoundException
 *
 * @package App\Domain\SharedKernel\Authentication\Exception
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class EmailAddressNotFoundException extends \RuntimeException
{
    /**
     * EmailAddressNotFoundException constructor.
     *
     * @param string $emailAddress
     */
    public function __construct(string $emailAddress)
    {
        parent::__construct(sprintf('"%s" not found', $emailAddress));
    }
}