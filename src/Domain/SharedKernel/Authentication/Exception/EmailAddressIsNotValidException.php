<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Exception;

/**
 * Class EmailAddressIsNotValidException
 *
 * @package App\Domain\SharedKernel\Authentication\Exception
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class EmailAddressIsNotValidException extends \InvalidArgumentException
{
    /**
     * EmailAddressIsNotValidException constructor.
     *
     * @param string $emailAddress
     */
    public function __construct(string $emailAddress)
    {
        parent::__construct(sprintf('"%s" is not valid email address', $emailAddress));
    }
}