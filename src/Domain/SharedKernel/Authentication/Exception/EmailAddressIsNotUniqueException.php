<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Exception;

/**
 * Class EmailAddressIsNotUniqueException
 *
 * @package App\Domain\SharedKernel\Authentication\Exception
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class EmailAddressIsNotUniqueException extends \InvalidArgumentException
{
    /**
     * EmailAddressIsNotUniqueException constructor.
     */
    public function __construct()
    {
        parent::__construct('Email address is already taken');
    }
}