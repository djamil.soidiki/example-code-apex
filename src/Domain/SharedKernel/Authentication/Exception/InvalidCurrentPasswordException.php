<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Exception;

/**
 * Class InvalidCurrentPasswordException
 *
 * @package App\Domain\SharedKernel\Authentication\Exception
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class InvalidCurrentPasswordException extends \RuntimeException
{
}