<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Repository;

use App\Domain\SharedKernel\Authentication\User;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;

/**
 * Interface UsersInterface
 *
 * @package App\Domain\SharedKernel\Authentication\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface UsersInterface
{
    /**
     * @param UserId $userId
     *
     * @return User|null
     */
    public function get(UserId $userId): ?User;

    /**
     * @param User $user
     */
    public function save(User $user): void;
}