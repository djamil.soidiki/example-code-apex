<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Repository\Query;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;

/**
 * Interface EmailAddressQueryInterface
 *
 * @package App\Domain\SharedKernel\Authentication\Repository\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface EmailAddressQueryInterface
{
    /**
     * @param EmailAddress $emailAddress
     *
     * @return bool
     */
    public function isEmailAddressUnique(EmailAddress $emailAddress): bool;

    /**
     * @param EmailAddress $emailAddress
     *
     * @return UserId|null
     */
    public function existsEmailAddress(EmailAddress $emailAddress): ?UserId;
}