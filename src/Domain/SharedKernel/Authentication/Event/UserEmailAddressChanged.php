<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Event;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class UserEmailAddressChanged
 *
 * @package App\Domain\SharedKernel\Authentication\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class UserEmailAddressChanged implements Serializable
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * CustomerEmailAddressChanged constructor.
     *
     * @param UserId            $userId
     * @param EmailAddress      $emailAddress
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(UserId $userId, EmailAddress $emailAddress, DateTimeImmutable $updatedAt)
    {
        $this->userId       = $userId;
        $this->emailAddress = $emailAddress;
        $this->updatedAt    = $updatedAt;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'user_id');
        Assertion::keyExists($data, 'email_address');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new UserId($data['user_id']),
            new EmailAddress($data['email_address']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'user_id'       => $this->userId->serialize(),
            'email_address' => $this->emailAddress->serialize(),
            'updated_at'    => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}