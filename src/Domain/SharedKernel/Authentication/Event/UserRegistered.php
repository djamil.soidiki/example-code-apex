<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Event;

use App\Domain\SharedKernel\Authentication\ValueObject\PasswordHash;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class UserRegistered
 *
 * @package App\Domain\SharedKernel\Authentication\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class UserRegistered implements Serializable
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var PasswordHash
     */
    private $password;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * UserRegistered constructor.
     *
     * @param UserId            $userId
     * @param EmailAddress      $emailAddress
     * @param PasswordHash      $password
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(UserId $userId, EmailAddress $emailAddress, PasswordHash $password, DateTimeImmutable $createdAt)
    {
        $this->userId       = $userId;
        $this->emailAddress = $emailAddress;
        $this->password     = $password;
        $this->createdAt    = $createdAt;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return PasswordHash
     */
    public function password(): PasswordHash
    {
        return $this->password;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     *
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'user_id');
        Assertion::keyExists($data, 'email_address');
        Assertion::keyExists($data, 'password');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new UserId($data['user_id']),
            new EmailAddress($data['email_address']),
            new PasswordHash($data['password']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'user_id'       => $this->userId->serialize(),
            'email_address' => $this->emailAddress->serialize(),
            'password'      => $this->password->serialize(),
            'created_at'    => $this->createdAt->format(DateTimeImmutable::ATOM),
        ];
    }
}