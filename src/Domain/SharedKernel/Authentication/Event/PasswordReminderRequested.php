<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Event;

use App\Domain\SharedKernel\Authentication\ValueObject\PasswordReminderId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Token\Token;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class PasswordReminderRequested
 *
 * @package App\Domain\SharedKernel\Authentication\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PasswordReminderRequested implements Serializable
{
    /**
     * @var PasswordReminderId
     */
    private $passwordReminderId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var Token
     */
    private $token;

    /**
     * @var DateTimeImmutable
     */
    private $expiresAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * PasswordReminderRequested constructor.
     *
     * @param PasswordReminderId    $passwordReminderId
     * @param UserId                $userId
     * @param Token $token
     * @param DateTimeImmutable     $expiresAt
     * @param DateTimeImmutable     $createdAt
     */
    public function __construct(
        PasswordReminderId $passwordReminderId,
        UserId $userId,
        Token $token,
        DateTimeImmutable $expiresAt,
        DateTimeImmutable $createdAt
    )
    {
        $this->passwordReminderId = $passwordReminderId;
        $this->userId             = $userId;
        $this->token              = $token;
        $this->expiresAt          = $expiresAt;
        $this->createdAt          = $createdAt;
    }

    /**
     * @return PasswordReminderId
     */
    public function passwordReminderId(): PasswordReminderId
    {
        return $this->passwordReminderId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return Token
     */
    public function token(): Token
    {
        return $this->token;
    }

    /**
     * @return DateTimeImmutable
     */
    public function expiresAt(): DateTimeImmutable
    {
        return $this->expiresAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     *
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'password_reminder_id');
        Assertion::keyExists($data, 'user_id');
        Assertion::keyExists($data, 'token');
        Assertion::keyExists($data, 'expires_at');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new PasswordReminderId($data['password_reminder_id']),
            new UserId($data['user_id']),
            new Token($data['token']),
            new DateTimeImmutable($data['expires_at']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'password_reminder_id' => $this->passwordReminderId->serialize(),
            'user_id'              => $this->userId->serialize(),
            'token'                => $this->token->serialize(),
            'expires_at'           => $this->expiresAt->format(DateTimeImmutable::ATOM),
            'created_at'           => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }

}