<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Event;

use App\Domain\SharedKernel\Authentication\ValueObject\PasswordHash;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class UserPasswordReset
 *
 * @package App\Domain\SharedKernel\Authentication\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class UserPasswordReset implements Serializable
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var PasswordHash
     */
    private $password;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * UserPasswordReset constructor.
     *
     * @param UserId            $userId
     * @param PasswordHash      $password
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(UserId $userId, PasswordHash $password, DateTimeImmutable $updatedAt)
    {
        $this->userId    = $userId;
        $this->password  = $password;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return PasswordHash
     */
    public function password(): PasswordHash
    {
        return $this->password;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     *
     * @return UserPasswordReset|mixed
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'user_id');
        Assertion::keyExists($data, 'password');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new UserId($data['user_id']),
            new PasswordHash($data['password']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'user_id'    => $this->userId->serialize(),
            'password'   => $this->password->serialize(),
            'updated_at' => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}