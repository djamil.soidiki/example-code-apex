<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Event\Handler;

use App\Domain\SharedKernel\Authentication\Event\UserRegistered;
use App\Domain\SharedKernel\Authentication\Repository\UsersInterface;
use App\Domain\SharedKernel\Authentication\Service\Notification\NotifierInterface;
use Broadway\Processor\Processor;

/**
 * Class SendRegistrationNotificationWhenUserWasRegistered
 *
 * @package App\Domain\SharedKernel\Authentication\Event\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class SendRegistrationNotificationWhenUserWasRegistered extends Processor
{
    /**
     * @var UsersInterface
     */
    private $users;

    /**
     * @var NotifierInterface
     */
    private $notifier;

    /**
     * SendRegistrationNotificationWhenUserWasRegistered constructor.
     *
     * @param UsersInterface    $users
     * @param NotifierInterface $notifier
     */
    public function __construct(UsersInterface $users, NotifierInterface $notifier)
    {
        $this->users    = $users;
        $this->notifier = $notifier;
    }

    /**
     * @param UserRegistered $event
     */
    public function handleUserRegistered(UserRegistered $event): void
    {
        $user = $this->users->get($event->userId());

        $this->notifier->sendRegistrationNotification($user);
    }
}