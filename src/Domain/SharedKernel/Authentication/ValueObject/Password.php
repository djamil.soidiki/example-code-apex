<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;

/**
 * Class Password
 *
 * @package App\Domain\SharedKernel\Authentication\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Password extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * Password constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $this->validateString($value);
    }

    /**
     * @inheritdoc
     */
    public function serialize(): string
    {
        return $this->value;
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}