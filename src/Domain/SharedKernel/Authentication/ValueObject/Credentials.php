<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Assert\Assertion;
use Assert\AssertionFailedException;

/**
 * Class Credentials
 *
 * @package App\Domain\SharedKernel\Authentication\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Credentials extends AbstractValueObject
{
    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var PasswordHash
     */
    private $password;

    /**
     * Credentials constructor.
     *
     * @param string $emailAddress
     * @param string $password
     */
    public function __construct(string $emailAddress, string $password)
    {
        $this->emailAddress = new EmailAddress($emailAddress);
        $this->password     = new PasswordHash($password);
    }

    /**
     * @param array $credentials
     *
     * @return static
     * @throws AssertionFailedException
     */
    public static function fromArray(array $credentials): self
    {
        Assertion::keyExists($credentials, 'email_address');
        Assertion::keyExists($credentials, 'password');

        return new self($credentials['email_address'], $credentials['password']);
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return PasswordHash
     */
    public function password(): PasswordHash
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function serialize(): array
    {
        return [
            'email_address' => $this->emailAddress()->serialize(),
            'password'      => $this->password()->serialize()
        ];
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return $this->emailAddress->serialize();
    }
}