<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Specification;

use App\Domain\SharedKernel\Authentication\Repository\Query\EmailAddressQueryInterface;
use App\Domain\SharedKernel\Generic\Specification\SpecificationInterface;

/**
 * Class EmailAddressIsUniqueSpecification
 *
 * @package App\Domain\SharedKernel\Authentication\Specification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class EmailAddressIsUniqueSpecification implements SpecificationInterface
{
    /**
     * @var EmailAddressQueryInterface
     */
    private $emailAddressQuery;

    /**
     * EmailAddressIsUniqueSpecification constructor.
     *
     * @param EmailAddressQueryInterface $emailAddressQuery
     */
    public function __construct(EmailAddressQueryInterface $emailAddressQuery)
    {
        $this->emailAddressQuery = $emailAddressQuery;
    }

    /**
     * @inheritDoc
     */
    public function isSatisfiedBy($emailAddress): bool
    {
        return $this->emailAddressQuery->isEmailAddressUnique($emailAddress);
    }
}