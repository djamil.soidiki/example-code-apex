<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Service\Notification;

use App\Domain\SharedKernel\Authentication\Entity\PasswordReminder;
use App\Domain\SharedKernel\Authentication\User;

/**
 * Interface NotifierInterface
 *
 * @package App\Domain\SharedKernel\Authentication\Service\Notification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface NotifierInterface
{
    const FROM_NO_REPLY = 'no-reply@reavant.co';

    /**
     * @param User $user
     */
    public function sendRegistrationNotification(User $user): void;

    /**
     * @param User             $user
     * @param PasswordReminder $reminder
     */
    public function sendPasswordResetNotification(User $user, PasswordReminder $reminder): void;
}