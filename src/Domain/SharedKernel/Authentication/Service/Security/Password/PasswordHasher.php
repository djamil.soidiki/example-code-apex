<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Service\Security\Password;

use App\Domain\SharedKernel\Authentication\ValueObject\Password;
use App\Domain\SharedKernel\Authentication\ValueObject\PasswordHash;

/**
 * Class PasswordHasher
 *
 * @package App\Domain\SharedKernel\Authentication\Service\Security\Password
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PasswordHasher
{
    private const COST = 8;

    private function __construct()
    {
    }

    /**
     * @param Password $password
     *
     * @return PasswordHash
     */
    public static function hash(Password $password): PasswordHash
    {
        return self::process($password);
    }

    /**
     * @param Password $password
     *
     * @return PasswordHash
     */
    private static function process(Password $password): PasswordHash
    {
        $hashedPassword = password_hash($password->serialize(), PASSWORD_BCRYPT, ['cost' => self::COST]);

        if (is_bool($hashedPassword))
            throw new \RuntimeException('Server error hashing password');

        return new PasswordHash($hashedPassword);
    }
}