<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Service\Security\Password;

use App\Domain\SharedKernel\Authentication\ValueObject\Password;
use App\Domain\SharedKernel\Authentication\ValueObject\PasswordHash;

/**
 * Class VerifyPassword
 *
 * @package App\Domain\SharedKernel\Authentication\Service\Security\Password
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class VerifyPassword
{
    private function __construct()
    {
    }

    /**
     * @param Password     $password
     * @param PasswordHash $passwordHash
     *
     * @return bool
     */
    public static function verify(Password $password, PasswordHash $passwordHash): bool
    {
        return self::process($password, $passwordHash);
    }

    /**
     * @param Password     $password
     * @param PasswordHash $passwordHash
     *
     * @return bool
     */
    private static function process(Password $password, PasswordHash $passwordHash): bool
    {
        return password_verify($password->serialize(), $passwordHash->serialize());
    }
}