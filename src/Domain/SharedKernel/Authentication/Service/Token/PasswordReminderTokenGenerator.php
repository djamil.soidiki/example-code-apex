<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Service\Token;

use App\Domain\SharedKernel\Generic\ValueObject\Token\Token;
use Exception;

/**
 * Class PasswordReminderTokenGenerator
 *
 * @package App\Domain\SharedKernel\Authentication\Service\Token
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PasswordReminderTokenGenerator
{
    private function __construct()
    {
    }

    /**
     * @return Token
     * @throws Exception
     */
    public static function generate(): Token
    {
        $token = bin2hex(random_bytes(Token::DEFAULT_LENGTH));

        return new Token($token);
    }
}