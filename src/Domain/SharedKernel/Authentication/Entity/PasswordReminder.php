<?php

declare(strict_types=1);

namespace App\Domain\SharedKernel\Authentication\Entity;

use App\Domain\SharedKernel\Authentication\ValueObject\PasswordReminderId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Token\Token;
use Broadway\EventSourcing\SimpleEventSourcedEntity;
use DateTimeImmutable;
use Exception;

/**
 * Class PasswordReminder
 *
 * @package App\Domain\SharedKernel\Authentication\Entity
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PasswordReminder extends SimpleEventSourcedEntity
{
    const DEFAULT_LIFETIME = 60;

    /**
     * @var PasswordReminderId
     */
    private $passwordReminderId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var Token
     */
    private $token;

    /**
     * @var DateTimeImmutable
     */
    private $expiresAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * PasswordReminder constructor.
     *
     * @param PasswordReminderId $passwordReminderId
     * @param UserId             $userId
     * @param Token              $token
     * @param DateTimeImmutable  $expiresAt
     *
     * @throws Exception
     */
    public function __construct(
        PasswordReminderId $passwordReminderId,
        UserId $userId,
        Token $token,
        DateTimeImmutable $expiresAt
    )
    {
        $this->passwordReminderId = $passwordReminderId;
        $this->userId             = $userId;
        $this->token              = $token;
        $this->expiresAt          = $expiresAt;
        $this->createdAt          = new DateTimeImmutable();
    }

    /**
     * @return Token
     */
    public function token(): Token
    {
        return $this->token;
    }
}