<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\ValueObject\WorksheetInput;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\FloatTrait;

/**
 * Class HoldingPeriod
 *
 * @package App\Domain\Flipping\Analysis\ValueObject\WorksheetInput
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class HoldingPeriod extends AbstractValueObject
{
    use FloatTrait;

    const DEFAULT_VALUE = 0.5;

    /**
     * @var float
     */
    private $value;

    /**
     * HoldingPeriod constructor.
     *
     * @param float|null $value
     */
    public function __construct(?float $value = null)
    {
        if ($value === null)
            $this->value = $this->validateFloat(self::DEFAULT_VALUE);
        else
            $this->value = $this->validateFloat($value);
    }

    /**
     * @return float
     */
    public function value(): float
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return sprintf("%d", $this->value);
    }
}