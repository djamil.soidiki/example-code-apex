<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\ValueObject\WorksheetInput;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use Assert\Assertion;
use Assert\AssertionFailedException;

/**
 * Class SellingCost
 *
 * @package App\Domain\Flipping\Analysis\ValueObject\WorksheetInput
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class SellingCost extends AbstractValueObject
{
    use StringTrait;

    const LUMP_SUM_METHOD = 'lump_sum';
    const ITEMIZED_METHOD = 'itemized';

    const ITEM_AMOUNT_TYPE  = 'amount';
    const ITEM_PERCENT_TYPE = 'percent';

    /**
     * @var string
     */
    private $inputMethod;

    /**
     * @var string
     */
    private $inputValue;

    /**
     * SellingCost constructor.
     *
     * If inputMethod equal Lump Sum
     * Then inputValue has to be an int between 1 and 100 corresponding to % of After Repair Value
     *
     * If inputMethod equal Itemized
     * Then inputValue has to be int string (json) or an array
     *
     * @todo
     *      Add business rule
     *      When lump sum value is choosed $inputValue has to be an int between 1 and 100
     *
     *
     * @param string       $inputMethod
     * @param string|array $inputValue
     *
     * @throws AssertionFailedException
     */
    public function __construct(string $inputMethod, $inputValue)
    {
        if (!defined("static::" . strtoupper($inputMethod) . "_METHOD"))
            throw new \InvalidArgumentException(sprintf('"%s" does not exist or it is not a valid selling cost method', $inputMethod));

        $this->inputMethod = $this->validateString($inputMethod);

        if ($this->inputMethod === self::ITEMIZED_METHOD) {
            $items = $inputValue;

            if (is_string($inputValue))
                $items = json_decode($inputValue, true);

            foreach ($items as $item => $itemData) {
                Assertion::keyExists($itemData, 'name');
                Assertion::keyExists($itemData, 'type');

                if ($itemData['type'] === self::ITEM_AMOUNT_TYPE)
                    Assertion::keyExists($itemData, 'amount');

                if ($itemData['type'] === self::ITEM_PERCENT_TYPE)
                    Assertion::keyExists($itemData, 'percent');

            }

            if (is_string($inputValue))
                $this->inputValue = $this->validateString($inputValue);
            else
                $this->inputValue = $this->validateString(json_encode($inputValue));
        } else {
            $this->inputValue = $this->validateString($inputValue);
        }
    }

    /**
     * @param array $sellingCost
     *
     * @return static
     * @throws AssertionFailedException
     */
    public static function fromArray(array $sellingCost): self
    {
        Assertion::keyExists($sellingCost, 'input_method');
        Assertion::keyExists($sellingCost, 'input_value');

        return new self(
            $sellingCost['input_method'],
            $sellingCost['input_value']
        );
    }

    /**
     * @return string|null
     */
    public function inputMethod(): ?string
    {
        return $this->inputMethod;
    }

    /**
     * @return string|null
     */
    public function inputValue(): ?string
    {
        return $this->inputValue;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'input_method' => $this->inputMethod,
            'input_value'  => $this->inputValue
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->inputMethod;
    }
}