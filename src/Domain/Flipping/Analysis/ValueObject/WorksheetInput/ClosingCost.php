<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\ValueObject\WorksheetInput;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use Assert\Assertion;
use Assert\AssertionFailedException;

/**
 * Class ClosingCost
 *
 * @package App\Domain\Flipping\Analysis\ValueObject\WorksheetInput
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ClosingCost extends AbstractValueObject
{
    use StringTrait;

    const LUMP_SUM_METHOD                     = 'lump_sum';
    const ITEMIZED_METHOD                     = 'itemized';
    const PERCENTAGE_OF_PURCHASE_PRICE_METHOD = 'percentage_of_purchase_price';

    const ITEM_AMOUNT_TYPE  = 'amount';
    const ITEM_PERCENT_TYPE = 'percent';

    /**
     * @var string
     */
    private $inputMethod;

    /**
     * @var string
     */
    private $inputValue;

    /**
     * ClosingCost constructor.
     *
     * @param string       $inputMethod
     * @param string|array $inputValue
     *
     * @throws AssertionFailedException
     */
    public function __construct(string $inputMethod, $inputValue)
    {
        if (!defined("static::" . strtoupper($inputMethod) . "_METHOD"))
            throw new \InvalidArgumentException(sprintf('"%s" does not exist or it is not a valid closing cost method', $inputMethod));

        $this->inputMethod = $this->validateString($inputMethod);

        if ($this->inputMethod === self::ITEMIZED_METHOD) {
            $items = $inputValue;

            if (is_string($inputValue))
                $items = json_decode($inputValue, true);

            foreach ($items as $key => $item) {
                Assertion::keyExists($item, 'name');
                Assertion::keyExists($item, 'type');

                if ($item['type'] === self::ITEM_AMOUNT_TYPE)
                    Assertion::keyExists($item, 'amount');

                if ($item['type'] === self::ITEM_PERCENT_TYPE)
                    Assertion::keyExists($item, 'percent');
            }

            if (is_string($inputValue))
                $this->inputValue = $this->validateString($inputValue);
            else
                $this->inputValue = $this->validateString(json_encode($inputValue));
        } else {
            $this->inputValue = $this->validateString($inputValue);
        }
    }

    /**
     * @param array $closingCost
     *
     * @return static
     * @throws AssertionFailedException
     */
    public static function fromArray(array $closingCost): self
    {
        Assertion::keyExists($closingCost, 'input_method');
        Assertion::keyExists($closingCost, 'input_value');

        return new self(
            $closingCost['input_method'],
            $closingCost['input_value']
        );
    }

    /**
     * @return string|null
     */
    public function inputMethod(): ?string
    {
        return $this->inputMethod;
    }

    /**
     * @return string|null
     */
    public function inputValue(): ?string
    {
        return $this->inputValue;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'input_method' => $this->inputMethod,
            'input_value'  => $this->inputValue
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->inputMethod;
    }
}