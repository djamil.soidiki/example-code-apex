<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\ValueObject\WorksheetInput;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use Assert\Assertion;
use Assert\AssertionFailedException;

/**
 * Class RehabCost
 *
 * @package App\Domain\Flipping\Analysis\ValueObject\WorksheetInput
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RehabCost extends AbstractValueObject
{
    use StringTrait;

    const LUMP_SUM_METHOD = 'lump_sum';
    const ITEMIZED_METHOD = 'itemized';

    /**
     * @var string
     */
    private $inputMethod;

    /**
     * @var string|array
     */
    private $inputValue;

    /**
     * RehabCost constructor.
     *
     * @param string       $inputMethod
     * @param string|array $inputValue
     *
     * @throws AssertionFailedException
     */
    public function __construct(string $inputMethod, $inputValue)
    {
        if (!defined("static::" . strtoupper($inputMethod) . "_METHOD"))
            throw new \InvalidArgumentException(sprintf('"%s" does not exist or it is not a valid rehab cost method', $inputMethod));

        $this->inputMethod = $this->validateString($inputMethod);

        if ($this->inputMethod === self::ITEMIZED_METHOD) {
            $items = $inputValue;

            if (is_string($inputValue))
                $items = json_decode($inputValue, true);

            foreach ($items as $key => $item) {
                Assertion::keyExists($item, 'name');
                Assertion::keyExists($item, 'amount');
            }

            if (is_string($inputValue))
                $this->inputValue = $this->validateString($inputValue);
            else
                $this->inputValue = $this->validateString(json_encode($inputValue));
        } else {
            $this->inputValue = $this->validateString($inputValue);
        }
    }

    /**
     * @param array $rehabCost
     *
     * @return static
     * @throws AssertionFailedException
     */
    public static function fromArray(array $rehabCost): self
    {
        Assertion::keyExists($rehabCost, 'input_method');
        Assertion::keyExists($rehabCost, 'input_value');

        return new self(
            $rehabCost['input_method'],
            $rehabCost['input_value']
        );
    }

    /**
     * @return string|null
     */
    public function inputMethod(): ?string
    {
        return $this->inputMethod;
    }

    /**
     * @return string|null
     */
    public function inputValue(): ?string
    {
        return $this->inputValue;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'input_method' => $this->inputMethod,
            'input_value'  => $this->inputValue
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->inputMethod;
    }
}