<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis;

use App\Domain\Flipping\Analysis\Entity\Worksheet;
use App\Domain\Flipping\Analysis\Event\AnalysisAnnualizedReturnOnInvestmentCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisCashNeededCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisClosingCostCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisCreated;
use App\Domain\Flipping\Analysis\Event\AnalysisHoldingCostCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisRehabCostCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisProfitCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisReturnOnInvestmentCalculated;
use App\Domain\Flipping\Analysis\Event\AnalysisSellingCostCalculated;
use App\Domain\Flipping\Analysis\Service\AnnualizedReturnOnInvestmentCalculator;
use App\Domain\Flipping\Analysis\Service\CashNeededCalculator;
use App\Domain\Flipping\Analysis\Service\ClosingCostCalculator;
use App\Domain\Flipping\Analysis\Service\HoldingCostCalculator;
use App\Domain\Flipping\Analysis\Service\ProfitCalculator;
use App\Domain\Flipping\Analysis\Service\RehabCostCalculator;
use App\Domain\Flipping\Analysis\Service\ReturnOnInvestmentCalculator;
use App\Domain\Flipping\Analysis\Service\SellingCostCalculator;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\SellingCost;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class Analysis
 *
 * @package App\Domain\Flipping\Analysis
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Analysis extends EventSourcedAggregateRoot
{
    /**
     * @var AnalysisId
     */
    private $id;

    /**
     * @var PropertyId
     */
    private $propertyId;

    /**
     * @var Worksheet
     */
    private $worksheet;

    /**
     * @var Money|null
     */
    private $closingCost;

    /**
     * @var Money|null
     */
    private $rehabCost;

    /**
     * @var Money|null
     */
    private $holdingCost;

    /**
     * @var Money|null
     */
    private $sellingCost;

    /**
     * @var Money|null
     */
    private $cashNeeded;

    /**
     * @var Money|null
     */
    private $profit;

    /**
     * @var float|null
     */
    private $returnOnInvestment;

    /**
     * @var float|null
     */
    private $annualizedReturnOnInvestment;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param PropertyId $propertyId
     *
     * @return static
     * @throws Exception
     */
    public static function create(PropertyId $propertyId): self
    {
        $analysis = new self();

        $analysis->apply(new AnalysisCreated(
                new AnalysisId(),
                $propertyId,
                new WorksheetId(),
                new DateTimeImmutable())
        );

        return $analysis;
    }

    /**
     * @param Money $purchasePrice
     *
     * @throws Exception
     */
    public function changeWorksheetPurchasePrice(Money $purchasePrice): void
    {
        $this->worksheet->changePurchasePrice($purchasePrice);
        $this->calculateClosingCost();
    }

    /**
     * @param ClosingCost $closingCost
     *
     * @throws Exception
     */
    public function changeWorksheetClosingCost(ClosingCost $closingCost): void
    {
        $this->worksheet->changeClosingCost($closingCost);
        $this->calculateClosingCost();
    }

    /**
     * @param HoldingCost $holdingCost
     *
     * @throws Exception
     */
    public function changeWorksheetHoldingCost(HoldingCost $holdingCost): void
    {
        $this->worksheet->changeHoldingCost($holdingCost);
        $this->calculateHoldingCost();
    }

    /**
     * @param HoldingPeriod $holdingPeriod
     *
     * @throws Exception
     */
    public function changeWorksheetHoldingPeriod(HoldingPeriod $holdingPeriod): void
    {
        $this->worksheet->changeHoldingPeriod($holdingPeriod);
        $this->calculateHoldingCost();
    }

    /**
     * @param RehabCost $rehabCost
     *
     * @throws Exception
     */
    public function changeWorksheetRehabCost(RehabCost $rehabCost): void
    {
        $this->worksheet->changeRehabCost($rehabCost);
        $this->calculateRehabCost();
    }

    /**
     * @param Money $afterRepairValue
     *
     * @throws Exception
     */
    public function changeWorksheetAfterRepairValue(Money $afterRepairValue): void
    {
        $this->worksheet->changeAfterRepairValue($afterRepairValue);
        $this->calculateSellingCost();
    }

    /**
     * @param SellingCost $sellingCost
     *
     * @throws Exception
     */
    public function changeWorksheetSellingCost(SellingCost $sellingCost): void
    {
        $this->worksheet->changeSellingCost($sellingCost);
        $this->calculateSellingCost();
    }

    /**
     * @throws Exception
     */
    protected function calculateClosingCost(): void
    {
        $closingCostCalculator = new ClosingCostCalculator(
            $this->worksheet->purchasePrice(),
            $this->worksheet->closingCost()
        );

        $closingCost = $closingCostCalculator->calculate();

        if ($this->closingCost !== null && $this->closingCost->sameValueAs($closingCost))
            return;

        $this->apply(new AnalysisClosingCostCalculated(
            $this->id,
            $closingCost,
            new DateTimeImmutable()
        ));

        $this->calculateCashNeeded();
    }

    /**
     * @throws Exception
     */
    protected function calculateRehabCost(): void
    {
        $rehabCostCalculator = new RehabCostCalculator($this->worksheet->rehabCost());

        $rehabCost = $rehabCostCalculator->calculate();

        if ($this->rehabCost !== null && $this->rehabCost->sameValueAs($rehabCost))
            return;

        $this->apply(new AnalysisRehabCostCalculated(
            $this->id,
            $rehabCost,
            new DateTimeImmutable()
        ));

        $this->calculateCashNeeded();
    }

    /**
     * @throws Exception
     */
    protected function calculateHoldingCost(): void
    {
        $holdingCostCalculator = new HoldingCostCalculator(
            $this->worksheet->holdingCost(),
            $this->worksheet->holdingPeriod(),
            $this->worksheet->purchasePrice()
        );

        $holdingCost = $holdingCostCalculator->calculate();

        if ($this->holdingCost !== null && $this->holdingCost->sameValueAs($holdingCost))
            return;

        $this->apply(new AnalysisHoldingCostCalculated(
            $this->id,
            $holdingCost,
            new DateTimeImmutable()
        ));

        $this->calculateProfit();
    }

    /**
     * @throws Exception
     */
    protected function calculateSellingCost(): void
    {
        $sellingCostCalculator = new SellingCostCalculator(
            $this->worksheet->afterRepairValue(),
            $this->worksheet->sellingCost()
        );

        $sellingCost = $sellingCostCalculator->calculate();

        if ($this->sellingCost !== null && $this->sellingCost->sameValueAs($sellingCost))
            return;

        $this->apply(new AnalysisSellingCostCalculated(
            $this->id,
            $sellingCost,
            new DateTimeImmutable()
        ));

        $this->calculateProfit();
    }

    /**
     * @throws Exception
     */
    protected function calculateCashNeeded(): void
    {
        $cashNeededCalculator = new CashNeededCalculator(
            $this->worksheet->purchasePrice(),
            $this->closingCost,
            $this->rehabCost
        );

        $cashNeeded = $cashNeededCalculator->calculate();

        if ($this->cashNeeded !== null && $this->cashNeeded->sameValueAs($cashNeeded))
            return;

        $this->apply(new AnalysisCashNeededCalculated(
            $this->id,
            $cashNeeded,
            new DateTimeImmutable()
        ));

        $this->calculateProfit();
    }

    /**
     * @throws Exception
     */
    protected function calculateProfit(): void
    {
        $profitCalculator = new ProfitCalculator(
            $this->worksheet,
            $this->sellingCost,
            $this->holdingCost,
            $this->cashNeeded
        );

        $profit = $profitCalculator->calculate();

        if ($this->profit !== null && $this->profit->sameValueAs($profit))
            return;

        $this->apply(new AnalysisProfitCalculated(
            $this->id,
            $profit,
            new DateTimeImmutable()
        ));

        $this->calculateReturnOnInvestment();
    }

    /**
     * @throws Exception
     */
    protected function calculateReturnOnInvestment(): void
    {
        $returnOnInvestmentCalculator = new ReturnOnInvestmentCalculator(
            $this->profit,
            $this->holdingCost,
            $this->cashNeeded
        );

        $returnOnInvestment = $returnOnInvestmentCalculator->calculate();

        if ($this->returnOnInvestment !== null && $this->returnOnInvestment === $returnOnInvestment)
            return;

        $this->apply(new AnalysisReturnOnInvestmentCalculated(
            $this->id,
            $returnOnInvestment,
            new DateTimeImmutable()
        ));

        $this->calculateAnnualizedReturnOnInvestment();
    }

    /**
     * @throws Exception
     */
    protected function calculateAnnualizedReturnOnInvestment(): void
    {
        $annualizedReturnOnInvestmentCalculator = new AnnualizedReturnOnInvestmentCalculator(
            $this->returnOnInvestment,
            $this->worksheet->holdingPeriod()
        );

        $annualizedReturnOnInvestment = $annualizedReturnOnInvestmentCalculator->calculate();

        if ($this->annualizedReturnOnInvestment !== null && $this->annualizedReturnOnInvestment === $annualizedReturnOnInvestment)
            return;

        $this->apply(new AnalysisAnnualizedReturnOnInvestmentCalculated(
            $this->id,
            $annualizedReturnOnInvestment,
            new DateTimeImmutable()
        ));
    }

    /**
     * @param AnalysisCreated $event
     *
     * @throws Exception
     */
    protected function applyAnalysisCreated(AnalysisCreated $event): void
    {
        $this->id         = $event->analysisId();
        $this->propertyId = $event->propertyId();
        $this->worksheet  = new Worksheet($event->worksheetId(), $event->analysisId());
        $this->createdAt  = $event->createdAt();
    }

    /**
     * @param AnalysisClosingCostCalculated $event
     */
    protected function applyAnalysisClosingCostCalculated(AnalysisClosingCostCalculated $event): void
    {
        $this->id          = $event->analysisId();
        $this->closingCost = $event->closingCost();
        $this->updatedAt   = $event->updatedAt();
    }

    /**
     * @param AnalysisRehabCostCalculated $event
     */
    protected function applyAnalysisRehabCostCalculated(AnalysisRehabCostCalculated $event): void
    {
        $this->id        = $event->analysisId();
        $this->rehabCost = $event->rehabCost();
        $this->updatedAt = $event->updatedAt();
    }

    /**
     * @param AnalysisHoldingCostCalculated $event
     */
    protected function applyAnalysisHoldingCostCalculated(AnalysisHoldingCostCalculated $event): void
    {
        $this->id          = $event->analysisId();
        $this->holdingCost = $event->holdingCost();
        $this->updatedAt   = $event->updatedAt();
    }

    /**
     * @param AnalysisSellingCostCalculated $event
     */
    protected function applyAnalysisSellingCostCalculated(AnalysisSellingCostCalculated $event): void
    {
        $this->id          = $event->analysisId();
        $this->sellingCost = $event->sellingCost();
        $this->updatedAt   = $event->updatedAt();
    }

    /**
     * @param AnalysisCashNeededCalculated $event
     */
    protected function applyAnalysisCashNeededCalculated(AnalysisCashNeededCalculated $event): void
    {
        $this->id         = $event->analysisId();
        $this->cashNeeded = $event->cashNeeded();
        $this->updatedAt  = $event->updatedAt();
    }

    /**
     * @param AnalysisProfitCalculated $event
     */
    protected function applyAnalysisProfitCalculated(AnalysisProfitCalculated $event): void
    {
        $this->id        = $event->analysisId();
        $this->profit    = $event->profit();
        $this->updatedAt = $event->updatedAt();
    }

    /**
     * @param AnalysisReturnOnInvestmentCalculated $event
     */
    protected function applyAnalysisReturnOnInvestmentCalculated(AnalysisReturnOnInvestmentCalculated $event): void
    {
        $this->id                 = $event->analysisId();
        $this->returnOnInvestment = $event->returnOnInvestment();
        $this->updatedAt          = $event->updatedAt();
    }

    /**
     * @param AnalysisAnnualizedReturnOnInvestmentCalculated $event
     */
    protected function applyAnalysisAnnualizedReturnOnInvestmentCalculated(AnalysisAnnualizedReturnOnInvestmentCalculated $event): void
    {
        $this->id                           = $event->analysisId();
        $this->annualizedReturnOnInvestment = $event->annualizedReturnOnInvestment();
        $this->updatedAt                    = $event->updatedAt();
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @inheritDoc
     */
    protected function getChildEntities(): array
    {
        return [$this->worksheet];
    }
}