<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Entity;

use App\Domain\Flipping\Analysis\Event\WorksheetAfterRepairValueChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetClosingCostChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetHoldingCostChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetHoldingPeriodChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetPurchasePriceChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetRehabCostChanged;
use App\Domain\Flipping\Analysis\Event\WorksheetSellingCostChanged;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\SellingCost;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Broadway\EventSourcing\SimpleEventSourcedEntity;
use DateTimeImmutable;
use Exception;

/**
 * Class Worksheet
 *
 * @package App\Domain\Flipping\Analysis\Entity
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Worksheet extends SimpleEventSourcedEntity
{
    /**
     * @var WorksheetId
     */
    private $id;

    /**
     * @var AnalysisId
     */
    private $analysisId;

    /**
     * @var Money|null
     */
    private $purchasePrice;

    /**
     * @var ClosingCost|null
     */
    private $closingCost;

    /**
     * @var HoldingCost|null
     */
    private $holdingCost;

    /**
     * @var HoldingPeriod
     */
    private $holdingPeriod;

    /**
     * @var RehabCost|null
     */
    private $rehabCost;

    /**
     * @var Money|null
     */
    private $afterRepairValue;

    /**
     * @var SellingCost|null
     */
    private $sellingCost;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * Worksheet constructor.
     *
     * @param WorksheetId $id
     * @param AnalysisId  $analysisId
     *
     * @throws Exception
     */
    public function __construct(WorksheetId $id, AnalysisId $analysisId)
    {
        $this->id            = $id;
        $this->analysisId    = $analysisId;
        $this->holdingPeriod = new HoldingPeriod();
        $this->createdAt     = new DateTimeImmutable();
    }

    /**
     * @param Money $purchasePrice
     *
     * @throws Exception
     */
    public function changePurchasePrice(Money $purchasePrice): void
    {
        if ($this->purchasePrice !== null && $this->purchasePrice->sameValueAs($purchasePrice))
            return;

        $this->apply(new WorksheetPurchasePriceChanged($this->id, $purchasePrice));
    }

    /**
     * @param ClosingCost $closingCost
     */
    public function changeClosingCost(ClosingCost $closingCost): void
    {
        if ($this->closingCost !== null && $this->closingCost->sameValueAs($closingCost))
            return;

        $this->apply(new WorksheetClosingCostChanged($this->id, $closingCost));
    }

    /**
     * @param HoldingCost $holdingCost
     */
    public function changeHoldingCost(HoldingCost $holdingCost): void
    {
        if ($this->holdingCost !== null && $this->holdingCost->sameValueAs($holdingCost))
            return;

        $this->apply(new WorksheetHoldingCostChanged($this->id, $holdingCost));
    }

    /**
     * @param HoldingPeriod $holdingPeriod
     */
    public function changeHoldingPeriod(HoldingPeriod $holdingPeriod): void
    {
        if ($this->holdingPeriod !== null && $this->holdingPeriod === $holdingPeriod)
            return;

        $this->apply(new WorksheetHoldingPeriodChanged($this->id, $holdingPeriod));
    }

    /**
     * @param RehabCost $rehabCost
     */
    public function changeRehabCost(RehabCost $rehabCost): void
    {
        if ($this->rehabCost !== null && $this->rehabCost->sameValueAs($rehabCost))
            return;

        $this->apply(new WorksheetRehabCostChanged($this->id, $rehabCost));
    }

    /**
     * @param Money $afterRepairValue
     *
     * @throws Exception
     */
    public function changeAfterRepairValue(Money $afterRepairValue): void
    {
        if ($this->afterRepairValue !== null && $this->afterRepairValue->sameValueAs($afterRepairValue))
            return;

        $this->apply(new WorksheetAfterRepairValueChanged($this->id, $afterRepairValue));
    }

    /**
     * @param SellingCost $sellingCost
     */
    public function changeSellingCost(SellingCost $sellingCost): void
    {
        if ($this->sellingCost !== null && $this->sellingCost->sameValueAs($sellingCost))
            return;

        $this->apply(new WorksheetSellingCostChanged($this->id, $sellingCost));
    }

    /**
     * @param WorksheetPurchasePriceChanged $event
     */
    protected function applyWorksheetPurchasePriceChanged(WorksheetPurchasePriceChanged $event): void
    {
        $this->id            = $event->worksheetId();
        $this->purchasePrice = $event->purchasePrice();
    }

    /**
     * @param WorksheetClosingCostChanged $event
     */
    protected function applyWorksheetClosingCostChanged(WorksheetClosingCostChanged $event): void
    {
        $this->id          = $event->worksheetId();
        $this->closingCost = $event->closingCost();
    }

    /**
     * @param WorksheetHoldingCostChanged $event
     */
    protected function applyWorksheetHoldingCostChanged(WorksheetHoldingCostChanged $event): void
    {
        $this->id          = $event->worksheetId();
        $this->holdingCost = $event->holdingCost();
    }

    /**
     * @param WorksheetHoldingPeriodChanged $event
     */
    protected function applyWorksheetHoldingPeriodChanged(WorksheetHoldingPeriodChanged $event): void
    {
        $this->id            = $event->worksheetId();
        $this->holdingPeriod = $event->holdingPeriod();
    }

    /**
     * @param WorksheetRehabCostChanged $event
     */
    protected function applyWorksheetRehabCostChanged(WorksheetRehabCostChanged $event): void
    {
        $this->id        = $event->worksheetId();
        $this->rehabCost = $event->rehabCost();
    }

    /**
     * @param WorksheetAfterRepairValueChanged $event
     */
    protected function applyWorksheetAfterRepairValueChanged(WorksheetAfterRepairValueChanged $event): void
    {
        $this->id               = $event->worksheetId();
        $this->afterRepairValue = $event->afterRepairValue();
    }

    /**
     * @param WorksheetSellingCostChanged $event
     */
    protected function applyWorksheetSellingCostChanged(WorksheetSellingCostChanged $event): void
    {
        $this->id          = $event->worksheetId();
        $this->sellingCost = $event->sellingCost();
    }

    /**
     * @return Money|null
     */
    public function purchasePrice(): ?Money
    {
        return $this->purchasePrice;
    }

    /**
     * @return ClosingCost|null
     */
    public function closingCost(): ?ClosingCost
    {
        return $this->closingCost;
    }

    /**
     * @return HoldingCost|null
     */
    public function holdingCost(): ?HoldingCost
    {
        return $this->holdingCost;
    }

    /**
     * @return HoldingPeriod
     */
    public function holdingPeriod(): HoldingPeriod
    {
        return $this->holdingPeriod;
    }

    /**
     * @return RehabCost|null
     */
    public function rehabCost(): ?RehabCost
    {
        return $this->rehabCost;
    }

    /**
     * @return Money|null
     */
    public function afterRepairValue(): ?Money
    {
        return $this->afterRepairValue;
    }

    /**
     * @return SellingCost|null
     */
    public function sellingCost(): ?SellingCost
    {
        return $this->sellingCost;
    }
}