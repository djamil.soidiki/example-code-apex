<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Service;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\SellingCost;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Currency;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;

/**
 * Class SellingCostCalculator
 *
 * @package App\Domain\Flipping\Analysis\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class SellingCostCalculator
{
    /**
     * @var Money|null
     */
    private $afterRepairValue;

    /**
     * @var SellingCost|null
     */
    private $worksheetSellingCost;

    /**
     * SellingCostCalculator constructor.
     *
     * @param Money|null       $afterRepairValue
     * @param SellingCost|null $worksheetSellingCost
     */
    public function __construct(?Money $afterRepairValue, ?SellingCost $worksheetSellingCost)
    {
        $this->afterRepairValue     = $afterRepairValue;
        $this->worksheetSellingCost = $worksheetSellingCost;
    }

    /**
     * @return Money
     */
    public function calculate(): Money
    {
        $sellingCostAmount = 0;
        $afterRepairValue  = ($this->afterRepairValue === null) ? 0 : $this->afterRepairValue->amount();

        if ($this->worksheetSellingCost !== null) {
            if ($this->worksheetSellingCost->inputMethod() === SellingCost::ITEMIZED_METHOD) {

                $items = json_decode($this->worksheetSellingCost->inputValue(), true);
                foreach ($items as $key => $item) {
                    if ($item['type'] === SellingCost::ITEM_PERCENT_TYPE)
                        $sellingCostAmount += (float)$afterRepairValue * ($item['percent'] / 100);
                    else
                        $sellingCostAmount += (float)$item['amount'];
                }
            } else {
                $sellingCostAmount = (float)$afterRepairValue * ($this->worksheetSellingCost->inputValue() / 100);
            }
        }

        return new Money($sellingCostAmount, new Currency('USD'));
    }
}