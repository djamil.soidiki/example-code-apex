<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Service;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod;

/**
 * Class AnnualizedReturnOnInvestmentCalculator
 *
 * @package App\Domain\Flipping\Analysis\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AnnualizedReturnOnInvestmentCalculator
{
    /**
     * @var float|null
     */
    private $returnOnInvestment;

    /**
     * @var HoldingPeriod
     */
    private $holdingPeriod;

    /**
     * AnnualizedReturnOnInvestmentCalculator constructor.
     *
     * @param float|null    $returnOnInvestment
     * @param HoldingPeriod $holdingPeriod
     */
    public function __construct(?float $returnOnInvestment, HoldingPeriod $holdingPeriod)
    {
        $this->returnOnInvestment = $returnOnInvestment;
        $this->holdingPeriod      = $holdingPeriod;
    }

    /**
     * @return float
     */
    public function calculate(): float
    {
        if ($this->returnOnInvestment === null)
            return 0;

        return (float)(12 * $this->returnOnInvestment) / $this->holdingPeriod->value();
    }
}