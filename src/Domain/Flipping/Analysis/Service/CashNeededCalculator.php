<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Service;

use App\Domain\SharedKernel\Generic\ValueObject\Money\Currency;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;

/**
 * Class CashNeededCalculator
 *
 * @package App\Domain\Flipping\Analysis\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CashNeededCalculator
{
    /**
     * @var Money|null
     */
    private $purchasePrice;

    /**
     * @var Money|null
     */
    private $closingCost;
    
    /**
     * @var Money|null
     */
    private $rehabCost;

    /**
     * CashNeededCalculator constructor.
     *
     * @param Money|null $purchasePrice
     * @param Money|null $closingCost
     * @param Money|null $rehabCost
     */
    public function __construct(?Money $purchasePrice, ?Money $closingCost, ?Money $rehabCost)
    {
        $this->purchasePrice = $purchasePrice;
        $this->closingCost   = $closingCost;
        $this->rehabCost     = $rehabCost;
    }
    
    /**
     * @return Money
     */
    public function calculate(): Money
    {
        $cashNeeded = 0;

        if ($this->purchasePrice !== null)
            $cashNeeded += $this->purchasePrice->amount();

        if ($this->closingCost !== null)
            $cashNeeded += $this->closingCost->amount();

        if ($this->rehabCost !== null)
            $cashNeeded += $this->rehabCost->amount();

        return new Money($cashNeeded, new Currency('USD'));
    }
}