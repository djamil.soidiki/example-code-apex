<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Service;

use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;

/**
 * Class ReturnOnInvestmentCalculator
 *
 * @package App\Domain\Flipping\Analysis\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ReturnOnInvestmentCalculator
{
    /**
     * @var Money|null
     */
    private $profit;

    /**
     * @var Money|null
     */
    private $holdingCost;

    /**
     * @var Money|null
     */
    private $cashNeeded;

    /**
     * ReturnOnInvestmentCalculator constructor.
     *
     * @param Money|null $profit
     * @param Money|null $holdingCost
     * @param Money|null $cashNeeded
     */
    public function __construct(?Money $profit, ?Money $holdingCost, ?Money $cashNeeded)
    {
        $this->profit      = $profit;
        $this->holdingCost = $holdingCost;
        $this->cashNeeded  = $cashNeeded;
    }

    /**
     * @return float
     */
    public function calculate(): float
    {
        if ($this->profit === null || ($this->holdingCost === null && $this->cashNeeded === null))
            return 0;

        $profit      = $this->profit !== null ? $this->profit->amount() : 0;
        $holdingCost = $this->holdingCost !== null ? $this->holdingCost->amount() : 0;
        $cashNeeded  = $this->cashNeeded !== null ? $this->cashNeeded->amount() : 0;

        $costOfInvestment = $holdingCost + $cashNeeded;

        return (float)(($profit / $costOfInvestment) * 100);
    }
}