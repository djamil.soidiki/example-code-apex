<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Service;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Currency;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;

/**
 * Class RehabCostCalculator
 *
 * @package App\Domain\Flipping\Analysis\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RehabCostCalculator
{
    /**
     * @var RehabCost|null
     */
    private $worksheetRehabCost;

    /**
     * RehabCostCalculator constructor.
     *
     * @param RehabCost|null $worksheetRehabCost
     */
    public function __construct(?RehabCost $worksheetRehabCost)
    {
        $this->worksheetRehabCost = $worksheetRehabCost;
    }

    /**
     * @return Money
     */
    public function calculate(): Money
    {
        $rehabCostAmount = 0;

        if ($this->worksheetRehabCost !== null) {
            if ($this->worksheetRehabCost->inputMethod() === RehabCost::ITEMIZED_METHOD) {

                $items = json_decode($this->worksheetRehabCost->inputValue(), true);
                foreach ($items as $key => $item) {
                    $rehabCostAmount += (float)$item['amount'];
                }
            } else {
                $rehabCostAmount = (float)$this->worksheetRehabCost->inputValue();
            }
        }

        return new Money($rehabCostAmount, new Currency('USD'));
    }
}