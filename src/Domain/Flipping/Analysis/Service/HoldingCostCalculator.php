<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Service;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Currency;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;

/**
 * Class HoldingCostCalculator
 *
 * @package App\Domain\Flipping\Analysis\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class HoldingCostCalculator
{
    /**
     * @var HoldingCost|null
     */
    private $worksheetHoldingCost;

    /**
     * @var HoldingPeriod
     */
    private $holdingPeriod;

    /**
     * @var Money|null
     */
    private $purchasePrice;

    /**
     * HoldingCostCalculator constructor.
     *
     * @param HoldingCost|null $worksheetHoldingCost
     * @param HoldingPeriod    $holdingPeriod
     * @param Money|null       $purchasePrice
     */
    public function __construct(?HoldingCost $worksheetHoldingCost, HoldingPeriod $holdingPeriod, ?Money $purchasePrice)
    {
        $this->worksheetHoldingCost = $worksheetHoldingCost;
        $this->holdingPeriod        = $holdingPeriod;
        $this->purchasePrice        = $purchasePrice;
    }

    /**
     * TODO Write algorithm documentation
     *
     * @return Money
     */
    public function calculate(): Money
    {
        $holdingCostAmount = 0;

        if ($this->worksheetHoldingCost !== null) {
            if ($this->worksheetHoldingCost->inputMethod() === HoldingCost::ITEMIZED_METHOD) {

                $items = json_decode($this->worksheetHoldingCost->inputValue(), true);
                foreach ($items as $item => $itemData) {

                    if ($itemData['type'] === HoldingCost::ITEM_AMOUNT_TYPE) {
                        if ($itemData['amount']['recurrence'] === HoldingCost::PER_YEAR_RECURRENCE)
                            $holdingCostAmount += (float)$itemData['amount']['value'] / 12;
                        else
                            $holdingCostAmount += (float)$itemData['amount']['value'];
                    } else if ($itemData['type'] === HoldingCost::ITEM_PERCENT_TYPE) {

                        $holdingCostAmount += (float)(($itemData['percent'] / 100) * $this->purchasePrice->amount()) / 12;
                    }
                }
            } else {
                $holdingCostAmount = (float)$this->worksheetHoldingCost->inputValue();
            }

            $holdingCostAmount = $holdingCostAmount * $this->holdingPeriod->value();
        }

        return new Money($holdingCostAmount, new Currency('USD'));
    }
}