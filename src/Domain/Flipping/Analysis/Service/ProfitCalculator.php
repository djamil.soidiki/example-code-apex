<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Service;

use App\Domain\Flipping\Analysis\Entity\Worksheet;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Currency;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;

/**
 * Class ProfitCalculator
 *
 * @package App\Domain\Flipping\Analysis\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ProfitCalculator
{
    /**
     * @var Worksheet
     */
    private $worksheet;

    /**
     * @var Money|null
     */
    private $sellingCost;

    /**
     * @var Money|null
     */
    private $holdingCost;

    /**
     * @var Money|null
     */
    private $cashNeeded;

    /**
     * ProfitCalculator constructor.
     *
     * @param Worksheet  $worksheet
     * @param Money|null $sellingCost
     * @param Money|null $holdingCost
     * @param Money|null $cashNeeded
     */
    public function __construct(Worksheet $worksheet, ?Money $sellingCost, ?Money $holdingCost, ?Money $cashNeeded)
    {
        $this->worksheet   = $worksheet;
        $this->sellingCost = $sellingCost;
        $this->holdingCost = $holdingCost;
        $this->cashNeeded  = $cashNeeded;
    }

    /**
     * @return Money
     */
    public function calculate(): Money
    {
        $afterRepairValue = $this->worksheet->afterRepairValue() !== null ? $this->worksheet->afterRepairValue()->amount() : 0;

        $sellingCost      = $this->sellingCost !== null ? $this->sellingCost->amount() : 0;
        $holdingCost      = $this->holdingCost !== null ? $this->holdingCost->amount() : 0;
        $cashNeeded       = $this->cashNeeded !== null ? $this->cashNeeded->amount() : 0;

        $saleProceeds = $afterRepairValue - $sellingCost;
        $profit       = $saleProceeds - $holdingCost - $cashNeeded;

        return new Money($profit, new Currency('USD'));
    }
}