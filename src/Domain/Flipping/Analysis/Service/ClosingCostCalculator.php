<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Service;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Currency;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;

/**
 * Class ClosingCostCalculator
 *
 * @package App\Domain\Flipping\Analysis\Service
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ClosingCostCalculator
{
    /**
     * @var Money|null
     */
    private $purchasePrice;

    /**
     * @var ClosingCost|null
     */
    private $worksheetClosingCost;

    /**
     * ClosingCostCalculator constructor.
     *
     * @param Money|null       $purchasePrice
     * @param ClosingCost|null $worksheetClosingCost
     */
    public function __construct(?Money $purchasePrice, ?ClosingCost $worksheetClosingCost)
    {
        $this->purchasePrice        = $purchasePrice;
        $this->worksheetClosingCost = $worksheetClosingCost;
    }

    /**
     * @return Money
     */
    public function calculate(): Money
    {
        $closingCostAmount = 0;
        $purchasePrice     = ($this->purchasePrice === null) ? 0 : $this->purchasePrice->amount();

        if ($this->worksheetClosingCost !== null) {
            if ($this->worksheetClosingCost->inputMethod() === ClosingCost::ITEMIZED_METHOD) {

                $items = json_decode($this->worksheetClosingCost->inputValue(), true);
                foreach ($items as $key => $item) {
                    if ($item['type'] === ClosingCost::ITEM_PERCENT_TYPE)
                        $closingCostAmount += (float)$purchasePrice * ($item['percent'] / 100);
                    else
                        $closingCostAmount += (float)$item['amount'];
                }

            } else if ($this->worksheetClosingCost->inputMethod() === ClosingCost::PERCENTAGE_OF_PURCHASE_PRICE_METHOD) {

                $closingCostAmount = (float)$purchasePrice * ($this->worksheetClosingCost->inputValue() / 100);
            } else {
                $closingCostAmount = (float)$purchasePrice;
            }
        }

        return new Money($closingCostAmount, new Currency('USD'));
    }
}