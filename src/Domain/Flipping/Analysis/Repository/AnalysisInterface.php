<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Repository;

use App\Domain\Flipping\Analysis\Analysis;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;

/**
 * Interface AnalysisInterface
 *
 * @package App\Domain\Flipping\Analysis\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface AnalysisInterface
{
    /**
     * @param AnalysisId $analysisId
     *
     * @return Analysis|null
     */
    public function get(AnalysisId $analysisId): ?Analysis;

    /**
     * @param Analysis $analysis
     */
    public function save(Analysis $analysis): void;
}