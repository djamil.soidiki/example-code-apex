<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use Exception;

/**
 * Class WorksheetClosingCostChanged
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class WorksheetClosingCostChanged implements Serializable
{
    /**
     * @var WorksheetId
     */
    private $worksheetId;

    /**
     * @var ClosingCost
     */
    private $closingCost;

    /**
     * WorksheetClosingCostChanged constructor.
     *
     * @param WorksheetId $worksheetId
     * @param ClosingCost $closingCost
     */
    public function __construct(WorksheetId $worksheetId, ClosingCost $closingCost)
    {
        $this->worksheetId = $worksheetId;
        $this->closingCost = $closingCost;
    }

    /**
     * @return WorksheetId
     */
    public function worksheetId(): WorksheetId
    {
        return $this->worksheetId;
    }

    /**
     * @return ClosingCost
     */
    public function closingCost(): ClosingCost
    {
        return $this->closingCost;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'worksheet_id');
        Assertion::keyExists($data, 'closing_cost');

        return new self(
            new WorksheetId($data['worksheet_id']),
            ClosingCost::fromArray($data['closing_cost'])
        );
    }

    public function serialize(): array
    {
        return [
            'worksheet_id' => $this->worksheetId->serialize(),
            'closing_cost' => $this->closingCost->serialize()
        ];
    }
}