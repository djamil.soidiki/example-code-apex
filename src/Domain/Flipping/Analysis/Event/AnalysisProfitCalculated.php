<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class AnalysisProfitCalculated
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AnalysisProfitCalculated implements Serializable
{
    /**
     * @var AnalysisId
     */
    private $analysisId;

    /**
     * @var Money
     */
    private $profit;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * AnalysisProfitCalculated constructor.
     *
     * @param AnalysisId        $analysisId
     * @param Money             $profit
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(AnalysisId $analysisId, Money $profit, DateTimeImmutable $updatedAt)
    {
        $this->analysisId = $analysisId;
        $this->profit     = $profit;
        $this->updatedAt  = $updatedAt;
    }

    /**
     * @return AnalysisId
     */
    public function analysisId(): AnalysisId
    {
        return $this->analysisId;
    }

    /**
     * @return Money
     */
    public function profit(): Money
    {
        return $this->profit;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'analysis_id');
        Assertion::keyExists($data, 'profit');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new AnalysisId($data['analysis_id']),
            Money::fromArray($data['profit']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'analysis_id' => $this->analysisId->serialize(),
            'profit'      => $this->profit->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}