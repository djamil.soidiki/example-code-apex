<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use Exception;

/**
 * Class WorksheetRehabCostChanged
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class WorksheetRehabCostChanged implements Serializable
{
    /**
     * @var WorksheetId
     */
    private $worksheetId;

    /**
     * @var RehabCost
     */
    private $rehabCost;

    /**
     * WorksheetRehabCostChanged constructor.
     *
     * @param WorksheetId $worksheetId
     * @param RehabCost   $rehabCost
     */
    public function __construct(WorksheetId $worksheetId, RehabCost $rehabCost)
    {
        $this->worksheetId = $worksheetId;
        $this->rehabCost   = $rehabCost;
    }

    /**
     * @return WorksheetId
     */
    public function worksheetId(): WorksheetId
    {
        return $this->worksheetId;
    }

    /**
     * @return RehabCost
     */
    public function rehabCost(): RehabCost
    {
        return $this->rehabCost;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'worksheet_id');
        Assertion::keyExists($data, 'rehab_cost');

        return new self(
            new WorksheetId($data['worksheet_id']),
            RehabCost::fromArray($data['rehab_cost'])
        );
    }

    public function serialize(): array
    {
        return [
            'worksheet_id' => $this->worksheetId->serialize(),
            'rehab_cost'   => $this->rehabCost->serialize()
        ];
    }
}