<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class AnalysisRehabCostCalculated
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AnalysisRehabCostCalculated implements Serializable
{
    /**
     * @var AnalysisId
     */
    private $analysisId;

    /**
     * @var Money
     */
    private $rehabCost;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * AnalysisRehabCostCalculated constructor.
     *
     * @param AnalysisId        $analysisId
     * @param Money             $rehabCost
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(AnalysisId $analysisId, Money $rehabCost, DateTimeImmutable $updatedAt)
    {
        $this->analysisId = $analysisId;
        $this->rehabCost  = $rehabCost;
        $this->updatedAt  = $updatedAt;
    }

    /**
     * @return AnalysisId
     */
    public function analysisId(): AnalysisId
    {
        return $this->analysisId;
    }

    /**
     * @return Money
     */
    public function rehabCost(): Money
    {
        return $this->rehabCost;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'analysis_id');
        Assertion::keyExists($data, 'rehab_cost');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new AnalysisId($data['analysis_id']),
            Money::fromArray($data['rehab_cost']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'analysis_id' => $this->analysisId->serialize(),
            'rehab_cost'  => $this->rehabCost->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}