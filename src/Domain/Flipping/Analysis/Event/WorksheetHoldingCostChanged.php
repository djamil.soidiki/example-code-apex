<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use Exception;

/**
 * Class WorksheetHoldingCostChanged
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class WorksheetHoldingCostChanged implements Serializable
{
    /**
     * @var WorksheetId
     */
    private $worksheetId;

    /**
     * @var HoldingCost
     */
    private $holdingCost;

    /**
     * WorksheetHoldingCostChanged constructor.
     *
     * @param WorksheetId $worksheetId
     * @param HoldingCost $holdingCost
     */
    public function __construct(WorksheetId $worksheetId, HoldingCost $holdingCost)
    {
        $this->worksheetId = $worksheetId;
        $this->holdingCost = $holdingCost;
    }

    /**
     * @return WorksheetId
     */
    public function worksheetId(): WorksheetId
    {
        return $this->worksheetId;
    }

    /**
     * @return HoldingCost
     */
    public function holdingCost(): HoldingCost
    {
        return $this->holdingCost;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'worksheet_id');
        Assertion::keyExists($data, 'holding_cost');

        return new self(
            new WorksheetId($data['worksheet_id']),
            HoldingCost::fromArray($data['holding_cost'])
        );
    }

    public function serialize(): array
    {
        return [
            'worksheet_id' => $this->worksheetId->serialize(),
            'holding_cost' => $this->holdingCost->serialize()
        ];
    }
}