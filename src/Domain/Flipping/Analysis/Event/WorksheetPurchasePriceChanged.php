<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use Exception;

/**
 * Class WorksheetPurchasePriceChanged
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class WorksheetPurchasePriceChanged implements Serializable
{
    /**
     * @var WorksheetId
     */
    private $worksheetId;

    /**
     * @var Money
     */
    private $purchasePrice;

    /**
     * WorksheetPurchasePriceChanged constructor.
     *
     * @param WorksheetId       $worksheetId
     * @param Money             $purchasePrice
     */
    public function __construct(WorksheetId $worksheetId, Money $purchasePrice)
    {
        $this->worksheetId   = $worksheetId;
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * @return WorksheetId
     */
    public function worksheetId(): WorksheetId
    {
        return $this->worksheetId;
    }

    /**
     * @return Money
     */
    public function purchasePrice(): Money
    {
        return $this->purchasePrice;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'worksheet_id');
        Assertion::keyExists($data, 'purchase_price');

        return new self(
            new WorksheetId($data['worksheet_id']),
            Money::fromArray($data['purchase_price'])
        );
    }

    public function serialize(): array
    {
        return [
            'worksheet_id'   => $this->worksheetId->serialize(),
            'purchase_price' => $this->purchasePrice->serialize()
        ];
    }
}