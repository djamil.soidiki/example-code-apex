<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class AnalysisAnnualizedReturnOnInvestmentCalculated
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AnalysisAnnualizedReturnOnInvestmentCalculated implements Serializable
{
    /**
     * @var AnalysisId
     */
    private $analysisId;

    /**
     * @var float
     */
    private $annualizedReturnOnInvestment;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * AnalysisAnnualizedReturnOnInvestmentCalculated constructor.
     *
     * @param AnalysisId        $analysisId
     * @param float             $annualizedReturnOnInvestment
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(AnalysisId $analysisId, float $annualizedReturnOnInvestment, DateTimeImmutable $updatedAt)
    {
        $this->analysisId                   = $analysisId;
        $this->annualizedReturnOnInvestment = $annualizedReturnOnInvestment;
        $this->updatedAt                    = $updatedAt;
    }

    /**
     * @return AnalysisId
     */
    public function analysisId(): AnalysisId
    {
        return $this->analysisId;
    }

    /**
     * @return float
     */
    public function annualizedReturnOnInvestment(): float
    {
        return $this->annualizedReturnOnInvestment;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'analysis_id');
        Assertion::keyExists($data, 'annualized_return_on_investment');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new AnalysisId($data['analysis_id']),
            $data['annualized_return_on_investment'],
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'analysis_id'                     => $this->analysisId->serialize(),
            'annualized_return_on_investment' => $this->annualizedReturnOnInvestment,
            'updated_at'                      => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}