<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use Exception;

/**
 * Class WorksheetHoldingPeriodChanged
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class WorksheetHoldingPeriodChanged implements Serializable
{
    /**
     * @var WorksheetId
     */
    private $worksheetId;

    /**
     * @var HoldingPeriod
     */
    private $holdingPeriod;

    /**
     * WorksheetHoldingPeriodChanged constructor.
     *
     * @param WorksheetId   $worksheetId
     * @param HoldingPeriod $holdingPeriod
     */
    public function __construct(WorksheetId $worksheetId, HoldingPeriod $holdingPeriod)
    {
        $this->worksheetId   = $worksheetId;
        $this->holdingPeriod = $holdingPeriod;
    }

    /**
     * @return WorksheetId
     */
    public function worksheetId(): WorksheetId
    {
        return $this->worksheetId;
    }

    /**
     * @return HoldingPeriod
     */
    public function holdingPeriod(): HoldingPeriod
    {
        return $this->holdingPeriod;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'worksheet_id');
        Assertion::keyExists($data, 'holding_period');

        return new self(
            new WorksheetId($data['worksheet_id']),
            new HoldingPeriod($data['holding_period'])
        );
    }

    public function serialize(): array
    {
        return [
            'worksheet_id'   => $this->worksheetId->serialize(),
            'holding_period' => $this->holdingPeriod->serialize()
        ];
    }
}