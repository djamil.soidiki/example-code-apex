<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\SellingCost;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use Exception;

/**
 * Class WorksheetSellingCostChanged
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class WorksheetSellingCostChanged implements Serializable
{
    /**
     * @var WorksheetId
     */
    private $worksheetId;

    /**
     * @var SellingCost
     */
    private $sellingCost;

    /**
     * WorksheetSellingCostChanged constructor.
     *
     * @param WorksheetId $worksheetId
     * @param SellingCost $sellingCost
     */
    public function __construct(WorksheetId $worksheetId, SellingCost $sellingCost)
    {
        $this->worksheetId = $worksheetId;
        $this->sellingCost = $sellingCost;
    }

    /**
     * @return WorksheetId
     */
    public function worksheetId(): WorksheetId
    {
        return $this->worksheetId;
    }

    /**
     * @return SellingCost
     */
    public function sellingCost(): SellingCost
    {
        return $this->sellingCost;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'worksheet_id');
        Assertion::keyExists($data, 'selling_cost');

        return new self(
            new WorksheetId($data['worksheet_id']),
            SellingCost::fromArray($data['selling_cost'])
        );
    }

    public function serialize(): array
    {
        return [
            'worksheet_id' => $this->worksheetId->serialize(),
            'selling_cost' => $this->sellingCost->serialize()
        ];
    }
}