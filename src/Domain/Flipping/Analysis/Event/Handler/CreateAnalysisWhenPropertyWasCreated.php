<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event\Handler;

use App\Domain\Flipping\Analysis\Analysis;
use App\Domain\Flipping\Analysis\Repository\AnalysisInterface;
use Broadway\Processor\Processor;
use Exception;

/**
 * Class CreateAnalysisWhenPropertyWasCreated
 *
 * @package App\Domain\Flipping\Analysis\Event\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateAnalysisWhenPropertyWasCreated extends Processor
{
    /**
     * @var AnalysisInterface
     */
    private $analysis;

    /**
     * CreateAnalysisWhenPropertyWasCreated constructor.
     *
     * @param AnalysisInterface $analysis
     */
    public function __construct(AnalysisInterface $analysis)
    {
        $this->analysis = $analysis;
    }

    /**
     * @param $event
     *
     * @throws Exception
     */
    public function handlePropertyCreated($event): void
    {
        $analysis = Analysis::create($event->propertyId());

        $this->analysis->save($analysis);
    }
}