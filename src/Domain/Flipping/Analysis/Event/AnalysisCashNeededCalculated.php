<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class AnalysisCashNeededCalculated
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AnalysisCashNeededCalculated implements Serializable
{
    /**
     * @var AnalysisId
     */
    private $analysisId;

    /**
     * @var Money
     */
    private $cashNeeded;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * AnalysisCashNeededCalculated constructor.
     *
     * @param AnalysisId        $analysisId
     * @param Money             $cashNeeded
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(AnalysisId $analysisId, Money $cashNeeded, DateTimeImmutable $updatedAt)
    {
        $this->analysisId = $analysisId;
        $this->cashNeeded = $cashNeeded;
        $this->updatedAt  = $updatedAt;
    }

    /**
     * @return AnalysisId
     */
    public function analysisId(): AnalysisId
    {
        return $this->analysisId;
    }

    /**
     * @return Money
     */
    public function cashNeeded(): Money
    {
        return $this->cashNeeded;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'analysis_id');
        Assertion::keyExists($data, 'cash_needed');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new AnalysisId($data['analysis_id']),
            Money::fromArray($data['cash_needed']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'analysis_id' => $this->analysisId->serialize(),
            'cash_needed' => $this->cashNeeded->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}