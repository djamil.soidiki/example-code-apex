<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use Exception;

/**
 * Class WorksheetAfterRepairValueChanged
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class WorksheetAfterRepairValueChanged implements Serializable
{
    /**
     * @var WorksheetId
     */
    private $worksheetId;

    /**
     * @var Money
     */
    private $afterRepairValue;

    /**
     * WorksheetAfterRepairValueChanged constructor.
     *
     * @param WorksheetId $worksheetId
     * @param Money       $afterRepairValue
     */
    public function __construct(WorksheetId $worksheetId, Money $afterRepairValue)
    {
        $this->worksheetId      = $worksheetId;
        $this->afterRepairValue = $afterRepairValue;
    }

    /**
     * @return WorksheetId
     */
    public function worksheetId(): WorksheetId
    {
        return $this->worksheetId;
    }

    /**
     * @return Money
     */
    public function afterRepairValue(): Money
    {
        return $this->afterRepairValue;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'worksheet_id');
        Assertion::keyExists($data, 'after_repair_value');

        return new self(
            new WorksheetId($data['worksheet_id']),
            Money::fromArray($data['after_repair_value'])
        );
    }

    public function serialize(): array
    {
        return [
            'worksheet_id'       => $this->worksheetId->serialize(),
            'after_repair_value' => $this->afterRepairValue->serialize()
        ];
    }
}