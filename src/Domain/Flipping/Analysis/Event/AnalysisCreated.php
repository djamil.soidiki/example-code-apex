<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class AnalysisCreated
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AnalysisCreated implements Serializable
{
    /**
     * @var AnalysisId
     */
    private $analysisId;

    /**
     * @var PropertyId
     */
    private $propertyId;

    /**
     * @var WorksheetId
     */
    private $worksheetId;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * AnalysisCreated constructor.
     *
     * @param AnalysisId        $analysisId
     * @param PropertyId        $propertyId
     * @param WorksheetId       $worksheetId
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(AnalysisId $analysisId, PropertyId $propertyId, WorksheetId $worksheetId, DateTimeImmutable $createdAt)
    {
        $this->analysisId  = $analysisId;
        $this->propertyId  = $propertyId;
        $this->worksheetId = $worksheetId;
        $this->createdAt   = $createdAt;
    }

    /**
     * @return AnalysisId
     */
    public function analysisId(): AnalysisId
    {
        return $this->analysisId;
    }

    /**
     * @return PropertyId
     */
    public function propertyId(): PropertyId
    {
        return $this->propertyId;
    }

    /**
     * @return WorksheetId
     */
    public function worksheetId(): WorksheetId
    {
        return $this->worksheetId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'analysis_id');
        Assertion::keyExists($data, 'property_id');
        Assertion::keyExists($data, 'worksheet_id');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new AnalysisId($data['analysis_id']),
            new PropertyId($data['property_id']),
            new WorksheetId($data['worksheet_id']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'analysis_id'  => $this->analysisId->serialize(),
            'property_id'  => $this->propertyId->serialize(),
            'worksheet_id' => $this->worksheetId->serialize(),
            'created_at'   => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}