<?php

declare(strict_types=1);

namespace App\Domain\Flipping\Analysis\Event;

use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class AnalysisReturnOnInvestmentCalculated
 *
 * @package App\Domain\Flipping\Analysis\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AnalysisReturnOnInvestmentCalculated implements Serializable
{
    /**
     * @var AnalysisId
     */
    private $analysisId;

    /**
     * @var float
     */
    private $returnOnInvestment;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * AnalysisReturnOnInvestmentCalculated constructor.
     *
     * @param AnalysisId        $analysisId
     * @param float             $returnOnInvestment
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(AnalysisId $analysisId, float $returnOnInvestment, DateTimeImmutable $updatedAt)
    {
        $this->analysisId         = $analysisId;
        $this->returnOnInvestment = $returnOnInvestment;
        $this->updatedAt          = $updatedAt;
    }

    /**
     * @return AnalysisId
     */
    public function analysisId(): AnalysisId
    {
        return $this->analysisId;
    }

    /**
     * @return float
     */
    public function returnOnInvestment(): float
    {
        return $this->returnOnInvestment;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'analysis_id');
        Assertion::keyExists($data, 'return_on_investment');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new AnalysisId($data['analysis_id']),
            $data['return_on_investment'],
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'analysis_id'          => $this->analysisId->serialize(),
            'return_on_investment' => $this->returnOnInvestment,
            'updated_at'           => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}