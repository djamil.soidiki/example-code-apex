<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\ContactManagement\Group\Entity\Member;
use App\Domain\ContactManagement\Group\Event\ContactAdded;
use App\Domain\ContactManagement\Group\Event\GroupCreated;
use App\Domain\ContactManagement\Group\Event\GroupDeleted;
use App\Domain\ContactManagement\Group\Event\GroupDescriptionChanged;
use App\Domain\ContactManagement\Group\Event\GroupNameChanged;
use App\Domain\ContactManagement\Group\Event\ContactRemoved;
use App\Domain\ContactManagement\Group\ValueObject\GroupDescription;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\MemberId;
use App\Domain\ContactManagement\Group\ValueObject\GroupName;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class Group
 *
 * @package App\Domain\ContactManagement\Group
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Group extends EventSourcedAggregateRoot
{
    /**
     * @var GroupId
     */
    private $id;

    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var GroupName
     */
    private $name;

    /**
     * @var GroupDescription|null
     */
    private $description;

    /**
     * @var Member[]
     */
    private $members = [];

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param UserId    $ownerId
     * @param GroupName $name
     *
     * @return static
     * @throws Exception
     */
    public static function create(UserId $ownerId, GroupName $name): self
    {
        $group = new self();

        $group->apply(new GroupCreated(
            new GroupId(),
            $ownerId,
            $name,
            new DateTimeImmutable()
        ));

        return $group;
    }

    /**
     * @param GroupName $name
     *
     * @throws Exception
     */
    public function changeName(GroupName $name): void
    {
        if ($this->name->sameValueAs($name))
            return;

        $this->apply(new GroupNameChanged(
            $this->id,
            $name,
            new DateTimeImmutable()
        ));
    }

    /**
     * @param GroupDescription $description
     *
     * @throws Exception
     */
    public function changeDescription(GroupDescription $description): void
    {
        if ($this->description !== null && $this->description->sameValueAs($description))
            return;

        $this->apply(new GroupDescriptionChanged(
            $this->id,
            $description,
            new DateTimeImmutable()
        ));
    }

    /**
     * @param ContactId $contactId
     *
     * @throws Exception
     */
    public function addContact(ContactId $contactId): void
    {
        if (array_key_exists($contactId->serialize(), $this->members))
            return;

        $this->apply(new ContactAdded(
            new MemberId(),
            $this->ownerId,
            $this->id,
            $contactId,
            new DateTimeImmutable()
        ));
    }

    /**
     * @param ContactId $contactId
     *
     * @throws Exception
     */
    public function removeContact(ContactId $contactId): void
    {
        if (array_key_exists($contactId->serialize(), $this->members)) {

            $this->apply(new ContactRemoved(
                $this->id,
                $contactId,
                new DateTimeImmutable()
            ));
        }
    }

    /**
     * @throws Exception
     */
    public function delete(): void
    {
        if ($this->members !== null) {
            /** @var Member $member */
            foreach ($this->members as $memberId => $member) {
                $this->removeContact($member->contactId());
            }
        }

        $this->apply(new GroupDeleted($this->id, new DateTimeImmutable()));
    }

    /**
     * @param GroupCreated $event
     */
    protected function applyGroupCreated(GroupCreated $event): void
    {
        $this->id        = $event->groupId();
        $this->ownerId   = $event->ownerId();
        $this->name      = $event->name();
        $this->createdAt = $event->createdAt();
    }

    /**
     * @param GroupNameChanged $event
     */
    protected function applyGroupNameChanged(GroupNameChanged $event): void
    {
        $this->id        = $event->groupId();
        $this->name      = $event->name();
        $this->updatedAt = $event->updatedAt();
    }

    /**
     * @param GroupDescriptionChanged $event
     */
    protected function applyGroupDescriptionChanged(GroupDescriptionChanged $event): void
    {
        $this->id          = $event->groupId();
        $this->description = $event->description();
        $this->updatedAt   = $event->updatedAt();
    }

    /**
     * @param ContactAdded $event
     *
     * @throws Exception
     */
    protected function applyContactAdded(ContactAdded $event): void
    {
        $this->id        = $event->groupId();
        $this->updatedAt = $event->createdAt();

        $this->members[$event->contactId()->serialize()] = new Member(
            $event->memberId(),
            $event->ownerId(),
            $event->groupId(),
            $event->contactId()
        );
    }

    /**
     * @param ContactRemoved $event
     */
    protected function applyContactRemoved(ContactRemoved $event): void
    {
        $this->id        = $event->groupId();
        $this->updatedAt = $event->updatedAt();

        unset($this->members[$event->contactId()->serialize()]);
    }

    /**
     * @param GroupDeleted $event
     */
    protected function applyGroupDeleted(GroupDeleted $event): void
    {
        $this->id        = $event->groupId();
        $this->updatedAt = $event->updatedAt();
    }

    /**
     * @inheritDoc
     */
    protected function getChildEntities(): array
    {
        return ($this->members === null) ? [] : $this->members;
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @return UserId
     */
    public function ownerId(): UserId
    {
        return $this->ownerId;
    }
}