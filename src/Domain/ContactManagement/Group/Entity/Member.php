<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Entity;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\MemberId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Broadway\EventSourcing\SimpleEventSourcedEntity;
use DateTimeImmutable;
use Exception;

/**
 * Class Member
 *
 * @package App\Domain\ContactManagement\Group\Entity
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Member extends SimpleEventSourcedEntity
{
    /**
     * @var MemberId
     */
    private $memberId;

    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var GroupId
     */
    private $groupId;

    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * Member constructor.
     *
     * @param MemberId  $memberId
     * @param UserId    $ownerId
     * @param GroupId   $groupId
     * @param ContactId $contactId
     *
     * @throws Exception
     */
    public function __construct(
        MemberId $memberId,
        UserId $ownerId,
        GroupId $groupId,
        ContactId $contactId
    )
    {
        $this->memberId  = $memberId;
        $this->ownerId   = $ownerId;
        $this->groupId   = $groupId;
        $this->contactId = $contactId;
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * @return MemberId
     */
    public function memberId(): MemberId
    {
        return $this->memberId;
    }

    /**
     * @return UserId
     */
    public function ownerId(): UserId
    {
        return $this->ownerId;
    }

    /**
     * @return GroupId
     */
    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}