<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Exception;

/**
 * Class GroupNameIsNotUniqueException
 *
 * @package App\Domain\ContactManagement\Group\Exception
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class GroupNameIsNotUniqueException extends \InvalidArgumentException
{
    /**
     * GroupNameIsNotUniqueException constructor.
     */
    public function __construct()
    {
        parent::__construct('Group name is already used');
    }
}