<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Event;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\MemberId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class ContactRemoved
 *
 * @package App\Domain\ContactManagement\Group\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ContactRemoved implements Serializable
{
    /**
     * @var GroupId
     */
    private $groupId;

    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * ContactRemoved constructor.
     *
     * @param GroupId           $groupId
     * @param ContactId         $contactId
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(
        GroupId $groupId,
        ContactId $contactId,
        DateTimeImmutable $updatedAt
    )
    {
        $this->groupId   = $groupId;
        $this->contactId = $contactId;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return GroupId
     */
    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws Exception
     * @throws AssertionFailedException
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'group_id');
        Assertion::keyExists($data, 'contact_id');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new GroupId($data['group_id']),
            new ContactId($data['contact_id']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'group_id'   => $this->groupId->serialize(),
            'contact_id' => $this->contactId->serialize(),
            'updated_at' => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}