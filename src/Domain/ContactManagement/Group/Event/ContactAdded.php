<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Event;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\MemberId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class ContactAdded
 *
 * @package App\Domain\ContactManagement\Group\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ContactAdded implements Serializable
{
    /**
     * @var MemberId
     */
    private $memberId;

    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var GroupId
     */
    private $groupId;

    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * ContactAdded constructor.
     *
     * @param MemberId          $memberId
     * @param UserId            $ownerId
     * @param GroupId           $groupId
     * @param ContactId         $contactId
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(
        MemberId $memberId,
        UserId $ownerId,
        GroupId $groupId,
        ContactId $contactId,
        DateTimeImmutable $createdAt
    )
    {
        $this->memberId  = $memberId;
        $this->ownerId   = $ownerId;
        $this->groupId   = $groupId;
        $this->contactId = $contactId;
        $this->createdAt = $createdAt;
    }

    /**
     * @return MemberId
     */
    public function memberId(): MemberId
    {
        return $this->memberId;
    }

    /**
     * @return UserId
     */
    public function ownerId(): UserId
    {
        return $this->ownerId;
    }

    /**
     * @return GroupId
     */
    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws Exception
     * @throws AssertionFailedException
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'member_id');
        Assertion::keyExists($data, 'owner_id');
        Assertion::keyExists($data, 'group_id');
        Assertion::keyExists($data, 'contact_id');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new MemberId($data['member_id']),
            new UserId($data['owner_id']),
            new GroupId($data['group_id']),
            new ContactId($data['contact_id']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'member_id'  => $this->memberId->serialize(),
            'owner_id'   => $this->ownerId->serialize(),
            'group_id'   => $this->groupId->serialize(),
            'contact_id' => $this->contactId->serialize(),
            'created_at' => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}