<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Event;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\GroupName;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class GroupCreated
 *
 * @package App\Domain\ContactManagement\Group\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GroupCreated implements Serializable
{
    /**
     * @var GroupId
     */
    private $groupId;

    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var GroupName
     */
    private $name;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * GroupCreated constructor.
     *
     * @param GroupId           $groupId
     * @param UserId            $ownerId
     * @param GroupName         $name
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(
        GroupId $groupId,
        UserId $ownerId,
        GroupName $name,
        DateTimeImmutable $createdAt
    )
    {
        $this->groupId   = $groupId;
        $this->ownerId   = $ownerId;
        $this->name      = $name;
        $this->createdAt = $createdAt;
    }

    /**
     * @return GroupId
     */
    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    /**
     * @return UserId
     */
    public function ownerId(): UserId
    {
        return $this->ownerId;
    }

    /**
     * @return GroupName
     */
    public function name(): GroupName
    {
        return $this->name;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'group_id');
        Assertion::keyExists($data, 'owner_id');
        Assertion::keyExists($data, 'name');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new GroupId($data['group_id']),
            new UserId($data['owner_id']),
            new GroupName($data['name']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'group_id'   => $this->groupId->serialize(),
            'owner_id'   => $this->ownerId->serialize(),
            'name'       => $this->name->serialize(),
            'created_at' => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}