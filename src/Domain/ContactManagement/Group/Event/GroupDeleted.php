<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Event;

use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class GroupDeleted
 *
 * @package App\Domain\ContactManagement\Group\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GroupDeleted implements Serializable
{
    /**
     * @var GroupId
     */
    private $groupId;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * GroupDeleted constructor.
     *
     * @param GroupId           $groupId
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(GroupId $groupId, DateTimeImmutable $updatedAt)
    {
        $this->groupId   = $groupId;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return GroupId
     */
    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'group_id');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new GroupId($data['group_id']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'group_id'   => $this->groupId->serialize(),
            'updated_at' => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}