<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Event;

use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\GroupDescription;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class GroupDescriptionChanged
 *
 * @package App\Domain\ContactManagement\Group\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GroupDescriptionChanged implements Serializable
{
    /**
     * @var GroupId
     */
    private $groupId;

    /**
     * @var GroupDescription
     */
    private $description;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * GroupDescriptionChanged constructor.
     *
     * @param GroupId           $groupId
     * @param GroupDescription  $description
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(GroupId $groupId, GroupDescription $description, DateTimeImmutable $updatedAt)
    {
        $this->groupId     = $groupId;
        $this->description = $description;
        $this->updatedAt   = $updatedAt;
    }

    /**
     * @return GroupId
     */
    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    /**
     * @return GroupDescription
     */
    public function description(): GroupDescription
    {
        return $this->description;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'group_id');
        Assertion::keyExists($data, 'description');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new GroupId($data['group_id']),
            new GroupDescription($data['description']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'group_id'    => $this->groupId->serialize(),
            'description' => $this->description->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}