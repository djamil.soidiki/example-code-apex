<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Event;

use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\GroupName;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class GroupNameChanged
 *
 * @package App\Domain\ContactManagement\Group\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GroupNameChanged implements Serializable
{
    /**
     * @var GroupId
     */
    private $groupId;

    /**
     * @var GroupName
     */
    private $name;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * GroupNameChanged constructor.
     *
     * @param GroupId           $groupId
     * @param GroupName         $name
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(GroupId $groupId, GroupName $name, DateTimeImmutable $updatedAt)
    {
        $this->groupId   = $groupId;
        $this->name      = $name;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return GroupId
     */
    public function groupId(): GroupId
    {
        return $this->groupId;
    }

    /**
     * @return GroupName
     */
    public function name(): GroupName
    {
        return $this->name;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'group_id');
        Assertion::keyExists($data, 'name');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new GroupId($data['group_id']),
            new GroupName($data['name']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'group_id'   => $this->groupId->serialize(),
            'name'       => $this->name->serialize(),
            'updated_at' => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}