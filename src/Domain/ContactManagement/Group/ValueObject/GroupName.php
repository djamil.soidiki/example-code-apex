<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;

/**
 * Class GroupName
 *
 * @package App\Domain\ContactManagement\Group\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GroupName extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * GroupName constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $this->validateString($value);
    }

    /**
     * @inheritdoc
     */
    public function serialize(): string
    {
        return $this->value;
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}

