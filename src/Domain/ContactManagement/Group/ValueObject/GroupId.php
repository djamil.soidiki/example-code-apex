<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Identity\UuidTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use Exception;

/**
 * Class GroupId
 *
 * @package App\Domain\ContactManagement\Group\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GroupId extends AbstractValueObject
{
    use UuidTrait;
    use StringTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * GroupId constructor.
     *
     * @param string|null $value
     *
     * @throws Exception
     */
    public function __construct(?string $value = null)
    {
        if ($value !== null)
            $this->value = $this->validateUuid($value);
        else
            $this->value = $this->generateUuid4();

        $this->validateString($this->value);
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}