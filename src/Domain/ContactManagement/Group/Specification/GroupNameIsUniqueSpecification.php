<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Specification;

use App\Domain\ContactManagement\Group\Repository\Query\GroupQueryInterface;
use App\Domain\ContactManagement\Group\ValueObject\GroupName;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Specification\SpecificationInterface;

/**
 * Class GroupNameIsUniqueSpecification
 *
 * @package App\Domain\ContactManagement\Group\Specification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GroupNameIsUniqueSpecification implements SpecificationInterface
{
    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var GroupName
     */
    private $groupName;

    /**
     * GroupNameIsUniqueSpecification constructor.
     *
     * @param UserId    $ownerId
     * @param GroupName $groupName
     */
    public function __construct(UserId $ownerId, GroupName $groupName)
    {
        $this->ownerId   = $ownerId;
        $this->groupName = $groupName;
    }

    /**
     * @inheritDoc
     */
    public function isSatisfiedBy($group): bool
    {
        return $this->groupName->sameValueAs($group->name());
    }

    /**
     * @param GroupQueryInterface $repository
     *
     * @return array
     */
    public function satisfyingElementsFrom(GroupQueryInterface $repository)
    {
        $groups = $repository->findGroupsByOwnerId($this->ownerId);

        return array_filter($groups, function ($group) {
            return $this->isSatisfiedBy($group);
        });
    }
}