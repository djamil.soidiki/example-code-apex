<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Repository\Query;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;

/**
 * Interface GroupQueryInterface
 *
 * @package App\Domain\ContactManagement\Group\Repository\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface GroupQueryInterface
{
    /**
     * @param UserId $ownerId
     *
     * @return array
     */
    //public function findGroupsByOwnerId(UserId $ownerId): array;
}