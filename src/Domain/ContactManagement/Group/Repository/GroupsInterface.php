<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Group\Repository;

use App\Domain\ContactManagement\Group\Group;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;

/**
 * Interface GroupsInterface
 *
 * @package App\Domain\ContactManagement\Group\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface GroupsInterface
{
    /**
     * @param GroupId $contactListId
     *
     * @return Group|null
     */
    public function get(GroupId $contactListId): ?Group;

    /**
     * @param Group $contactList
     */
    public function save(Group $contactList): void;
}