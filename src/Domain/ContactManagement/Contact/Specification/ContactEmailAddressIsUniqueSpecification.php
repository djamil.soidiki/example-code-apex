<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact\Specification;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\ContactManagement\Contact\Repository\Query\ContactQueryInterface;
use App\Domain\SharedKernel\Generic\Specification\CompositeSpecification;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;

/**
 * Class ContactEmailAddressIsUniqueSpecification
 *
 * @package App\Domain\ContactManagement\Contact\Specification
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ContactEmailAddressIsUniqueSpecification extends CompositeSpecification
{
    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * ContactEmailAddressIsUniqueSpecification constructor.
     *
     * @param UserId       $ownerId
     * @param EmailAddress $emailAddress
     */
    public function __construct(UserId $ownerId, EmailAddress $emailAddress)
    {
        $this->ownerId      = $ownerId;
        $this->emailAddress = $emailAddress;
    }

    /**
     * @inheritDoc
     */
    public function isSatisfiedBy($contact): bool
    {
        return $this->emailAddress->sameValueAs($contact->emailAddress());
    }

    /**
     * @param ContactQueryInterface $repository
     *
     * @return array
     */
    public function satisfyingElementsFrom(ContactQueryInterface $repository)
    {
        $contacts = $repository->findContactsByOwnerId($this->ownerId);

        return array_filter($contacts, function ($contact) {
            return $this->isSatisfiedBy($contact);
        });
    }
}