<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact\Event;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class ContactDeleted
 *
 * @package App\Domain\ContactManagement\Contact\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ContactDeleted implements Serializable
{
    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * ContactDeleted constructor.
     *
     * @param ContactId         $contactId
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(ContactId $contactId, DateTimeImmutable $updatedAt)
    {
        $this->contactId = $contactId;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws Exception
     * @throws AssertionFailedException
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'contact_id');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new ContactId($data['contact_id']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'contact_id' => $this->contactId->serialize(),
            'updated_at' => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}