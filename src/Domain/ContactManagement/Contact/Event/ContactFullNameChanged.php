<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact\Event;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class ContactFullNameChanged
 *
 * @package App\Domain\ContactManagement\Contact\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ContactFullNameChanged implements Serializable
{
    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var FullName
     */
    private $fullName;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * ContactFullNameChanged constructor.
     *
     * @param ContactId         $contactId
     * @param FullName          $fullName
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(ContactId $contactId, FullName $fullName, DateTimeImmutable $updatedAt)
    {
        $this->contactId = $contactId;
        $this->fullName  = $fullName;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return FullName
     */
    public function fullName(): FullName
    {
        return $this->fullName;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws Exception
     * @throws AssertionFailedException
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'contact_id');
        Assertion::keyExists($data, 'full_name');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new ContactId($data['contact_id']),
            new FullName($data['full_name']['first_name'], $data['full_name']['last_name']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'contact_id' => $this->contactId->serialize(),
            'full_name'  => $this->fullName->serialize(),
            'updated_at' => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}