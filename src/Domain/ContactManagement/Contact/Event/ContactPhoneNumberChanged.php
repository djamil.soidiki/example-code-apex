<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact\Event;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\SharedKernel\Generic\ValueObject\Phone\PhoneNumber;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class ContactPhoneNumberChanged
 *
 * @package App\Domain\ContactManagement\Contact\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ContactPhoneNumberChanged implements Serializable
{
    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var PhoneNumber
     */
    private $phoneNumber;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * ContactPhoneNumberChanged constructor.
     *
     * @param ContactId         $contactId
     * @param PhoneNumber       $phoneNumber
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(ContactId $contactId, PhoneNumber $phoneNumber, DateTimeImmutable $updatedAt)
    {
        $this->contactId   = $contactId;
        $this->phoneNumber = $phoneNumber;
        $this->updatedAt   = $updatedAt;
    }

    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return PhoneNumber
     */
    public function phoneNumber(): PhoneNumber
    {
        return $this->phoneNumber;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws Exception
     * @throws AssertionFailedException
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'contact_id');
        Assertion::keyExists($data, 'phone_number');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new ContactId($data['contact_id']),
            new PhoneNumber($data['phone_number']['country_code']['country'], $data['phone_number']['number']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'contact_id'   => $this->contactId->serialize(),
            'phone_number' => $this->phoneNumber->serialize(),
            'updated_at'   => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}