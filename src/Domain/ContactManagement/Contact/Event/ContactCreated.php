<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact\Event;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class ContactCreated
 *
 * @package App\Domain\ContactManagement\Contact\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ContactCreated implements Serializable
{
    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var FullName
     */
    private $fullName;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * ContactCreated constructor.
     *
     * @param ContactId         $contactId
     * @param UserId            $ownerId
     * @param FullName          $fullName
     * @param EmailAddress      $emailAddress
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(
        ContactId $contactId,
        UserId $ownerId,
        FullName $fullName,
        EmailAddress $emailAddress,
        DateTimeImmutable $createdAt
    )
    {
        $this->contactId    = $contactId;
        $this->ownerId      = $ownerId;
        $this->fullName     = $fullName;
        $this->emailAddress = $emailAddress;
        $this->createdAt    = $createdAt;
    }


    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return UserId
     */
    public function ownerId(): UserId
    {
        return $this->ownerId;
    }

    /**
     * @return FullName
     */
    public function fullName(): FullName
    {
        return $this->fullName;
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'contact_id');
        Assertion::keyExists($data, 'owner_id');
        Assertion::keyExists($data, 'full_name');
        Assertion::keyExists($data, 'email_address');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new ContactId($data['contact_id']),
            new UserId($data['owner_id']),
            new FullName($data['full_name']['first_name'], $data['full_name']['last_name']),
            new EmailAddress($data['email_address']),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'contact_id'    => $this->contactId->serialize(),
            'owner_id'      => $this->ownerId->serialize(),
            'full_name'     => $this->fullName->serialize(),
            'email_address' => $this->emailAddress->serialize(),
            'created_at'    => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}