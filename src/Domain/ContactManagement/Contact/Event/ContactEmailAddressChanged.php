<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact\Event;

use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class ContactEmailAddressChanged
 *
 * @package App\Domain\ContactManagement\Contact\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ContactEmailAddressChanged implements Serializable
{
    /**
     * @var ContactId
     */
    private $contactId;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * ContactEmailAddressChanged constructor.
     *
     * @param ContactId         $contactId
     * @param EmailAddress      $emailAddress
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(ContactId $contactId, EmailAddress $emailAddress, DateTimeImmutable $updatedAt)
    {
        $this->contactId    = $contactId;
        $this->emailAddress = $emailAddress;
        $this->updatedAt    = $updatedAt;
    }

    /**
     * @return ContactId
     */
    public function contactId(): ContactId
    {
        return $this->contactId;
    }

    /**
     * @return EmailAddress
     */
    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws Exception
     * @throws AssertionFailedException
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'contact_id');
        Assertion::keyExists($data, 'email_address');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new ContactId($data['contact_id']),
            new EmailAddress($data['email_address']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'contact_id'    => $this->contactId->serialize(),
            'email_address' => $this->emailAddress->serialize(),
            'updated_at'    => $this->updatedAt->format(DateTimeImmutable::ATOM)
        ];
    }
}