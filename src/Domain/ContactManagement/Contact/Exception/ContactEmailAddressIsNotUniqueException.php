<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact\Exception;

/**
 * Class ContactEmailAddressIsNotUniqueException
 *
 * @package App\Domain\ContactManagement\Contact\Exception
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class ContactEmailAddressIsNotUniqueException extends \InvalidArgumentException
{
    /**
     * EmailAddressIsNotUniqueException constructor.
     */
    public function __construct()
    {
        parent::__construct('Email address is already in use by an other contact');
    }
}