<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact\Repository;

use App\Domain\ContactManagement\Contact\Contact;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;

/**
 * Interface ContactsInterface
 *
 * @package App\Domain\ContactManagement\Contact\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface ContactsInterface
{
    /**
     * @param ContactId $contactId
     *
     * @return Contact|null
     */
    public function get(ContactId $contactId): ?Contact;

    /**
     * @param Contact $contact
     */
    public function save(Contact $contact): void;
}