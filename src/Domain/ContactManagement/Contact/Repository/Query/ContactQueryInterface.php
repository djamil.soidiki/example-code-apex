<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact\Repository\Query;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;

/**
 * Interface ContactQueryInterface
 *
 * @package App\Domain\ContactManagement\Contact\Repository\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface ContactQueryInterface
{
    /**
     * @param UserId $ownerId
     *
     * @return array
     */
    public function findContactsByOwnerId(UserId $ownerId): array;
}