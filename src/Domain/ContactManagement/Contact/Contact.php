<?php

declare(strict_types=1);

namespace App\Domain\ContactManagement\Contact;

use App\Domain\ContactManagement\Contact\Event\ContactCreated;
use App\Domain\ContactManagement\Contact\Event\ContactDeleted;
use App\Domain\ContactManagement\Contact\Event\ContactEmailAddressChanged;
use App\Domain\ContactManagement\Contact\Event\ContactFullNameChanged;
use App\Domain\ContactManagement\Contact\Event\ContactPhoneNumberChanged;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use App\Domain\SharedKernel\Generic\ValueObject\Phone\PhoneNumber;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class Contact
 *
 * @package App\Domain\ContactManagement\Contact
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Contact extends EventSourcedAggregateRoot
{
    /**
     * @var ContactId
     */
    private $id;

    /**
     * @var UserId
     */
    private $ownerId;

    /**
     * @var FullName
     */
    private $fullName;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var PhoneNumber|null
     */
    private $phoneNumber;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param UserId       $ownerId
     * @param FullName     $fullName
     * @param EmailAddress $emailAddress
     *
     * @return static
     * @throws Exception
     */
    public static function create(UserId $ownerId, FullName $fullName, EmailAddress $emailAddress): self
    {
        $contact = new self();

        $contact->apply(new ContactCreated(
            new ContactId(),
            $ownerId,
            $fullName,
            $emailAddress,
            new DateTimeImmutable()
        ));

        return $contact;
    }

    /**
     * @param FullName $fullName
     *
     * @throws Exception
     */
    public function changeFullName(FullName $fullName): void
    {
        if ($this->fullName->sameValueAs($fullName))
            return;

        $this->apply(new ContactFullNameChanged($this->id, $fullName, new DateTimeImmutable()));
    }

    /**
     * @param EmailAddress $emailAddress
     *
     * @throws Exception
     */
    public function changeEmailAddress(EmailAddress $emailAddress): void
    {
        if ($this->emailAddress->sameValueAs($emailAddress))
            return;

        $this->apply(new ContactEmailAddressChanged($this->id, $emailAddress, new DateTimeImmutable()));
    }

    /**
     * @param PhoneNumber $phoneNumber
     *
     * @throws Exception
     */
    public function changePhoneNumber(PhoneNumber $phoneNumber): void
    {
        if ($this->phoneNumber !== null && $this->phoneNumber->sameValueAs($phoneNumber))
            return;

        $this->apply(new ContactPhoneNumberChanged($this->id, $phoneNumber, new DateTimeImmutable()));
    }

    /**
     * @throws Exception
     */
    public function delete(): void
    {
        $this->apply(new ContactDeleted($this->id, new DateTimeImmutable()));
    }

    /**
     * @param ContactCreated $event
     */
    protected function applyContactCreated(ContactCreated $event): void
    {
        $this->id           = $event->contactId();
        $this->ownerId      = $event->ownerId();
        $this->fullName     = $event->fullName();
        $this->emailAddress = $event->emailAddress();
        $this->createdAt    = $event->createdAt();
    }

    /**
     * @param ContactFullNameChanged $event
     */
    protected function applyContactFullNameChanged(ContactFullNameChanged $event): void
    {
        $this->id        = $event->contactId();
        $this->fullName  = $event->fullName();
        $this->updatedAt = $event->updatedAt();
    }

    /**
     * @param ContactEmailAddressChanged $event
     */
    protected function applyContactEmailAddressChanged(ContactEmailAddressChanged $event): void
    {
        $this->id           = $event->contactId();
        $this->emailAddress = $event->emailAddress();
        $this->updatedAt    = $event->updatedAt();
    }

    /**
     * @param ContactPhoneNumberChanged $event
     */
    protected function applyContactPhoneNumberChanged(ContactPhoneNumberChanged $event): void
    {
        $this->id          = $event->contactId();
        $this->phoneNumber = $event->phoneNumber();
        $this->updatedAt   = $event->updatedAt();
    }

    /**
     * @param ContactDeleted $event
     */
    protected function applyContactDeleted(ContactDeleted $event): void
    {
        $this->id        = $event->contactId();
        $this->updatedAt = $event->updatedAt();
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @return UserId
     */
    public function ownerId(): UserId
    {
        return $this->ownerId;
    }
}