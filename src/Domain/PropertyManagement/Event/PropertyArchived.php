<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\Event;

use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\PropertyManagement\ValueObject\PropertyStatus;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class PropertyArchived
 *
 * @package App\Domain\PropertyManagement\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyArchived implements Serializable
{
    /**
     * @var PropertyId
     */
    private $propertyId;

    /**
     * @var PropertyStatus
     */
    private $status;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * PropertyArchived constructor.
     *
     * @param PropertyId        $propertyId
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(PropertyId $propertyId, DateTimeImmutable $updatedAt)
    {
        $this->propertyId = $propertyId;
        $this->status     = PropertyStatus::archived();
        $this->updatedAt  = $updatedAt;
    }

    /**
     * @return PropertyId
     */
    public function propertyId(): PropertyId
    {
        return $this->propertyId;
    }

    /**
     * @return PropertyStatus
     */
    public function status(): PropertyStatus
    {
        return $this->status;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'property_id');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new PropertyId($data['property_id']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'property_id' => $this->propertyId->serialize(),
            'status'      => $this->status->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}