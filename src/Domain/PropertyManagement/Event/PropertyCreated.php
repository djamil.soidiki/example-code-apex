<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\Event;

use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\Domain\PropertyManagement\ValueObject\PropertyDescription;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\PropertyManagement\ValueObject\PropertyName;
use App\Domain\PropertyManagement\ValueObject\PropertyStatus;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Util\SerializerValueTrait;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class PropertyCreated
 *
 * @package App\Domain\PropertyManagement\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyCreated implements Serializable
{
    use SerializerValueTrait;

    /**
     * @var PropertyId
     */
    private $propertyId;

    /**
     * @var UserId
     */
    private $managerId;

    /**
     * @var PropertyName
     */
    private $name;

    /**
     * @var PropertyDescription|null
     */
    private $description;

    /**
     * @var PropertyAddress|null
     */
    private $address;

    /**
     * @var PropertyStatus
     */
    private $status;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * PropertyCreated constructor.
     *
     * @param PropertyId               $propertyId
     * @param UserId                   $managerId
     * @param PropertyName             $name
     * @param PropertyDescription|null $description
     * @param PropertyAddress|null     $address
     * @param DateTimeImmutable        $createdAt
     */
    public function __construct(
        PropertyId $propertyId,
        UserId $managerId,
        PropertyName $name,
        ?PropertyDescription $description,
        ?PropertyAddress $address,
        DateTimeImmutable $createdAt
    )
    {
        $this->propertyId  = $propertyId;
        $this->managerId   = $managerId;
        $this->name        = $name;
        $this->description = $description;
        $this->address     = $address;
        $this->status      = PropertyStatus::activated();
        $this->createdAt   = $createdAt;
    }

    /**
     * @return PropertyId
     */
    public function propertyId(): PropertyId
    {
        return $this->propertyId;
    }

    /**
     * @return UserId
     */
    public function managerId(): UserId
    {
        return $this->managerId;
    }

    /**
     * @return PropertyName
     */
    public function name(): PropertyName
    {
        return $this->name;
    }

    /**
     * @return PropertyDescription|null
     */
    public function description(): ?PropertyDescription
    {
        return $this->description;
    }

    /**
     * @return PropertyAddress|null
     */
    public function address(): ?PropertyAddress
    {
        return $this->address;
    }

    /**
     * @return PropertyStatus
     */
    public function status(): PropertyStatus
    {
        return $this->status;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'property_id');
        Assertion::keyExists($data, 'manager_id');
        Assertion::keyExists($data, 'name');
        Assertion::keyExists($data, 'description');
        Assertion::keyExists($data, 'address');
        Assertion::keyExists($data, 'created_at');

        return new self(
            new PropertyId($data['property_id']),
            new UserId($data['manager_id']),
            new PropertyName($data['name']),
            self::deserializeValueOrNull($data, 'description', PropertyDescription::class),
            self::deserializeValueOrNull($data, 'address', PropertyAddress::class, true),
            new DateTimeImmutable($data['created_at'])
        );
    }

    /**
     * @inheritDoc
     */
    public function serialize(): array
    {
        return [
            'property_id' => $this->propertyId->serialize(),
            'manager_id'  => $this->managerId->serialize(),
            'name'        => $this->name->serialize(),
            'description' => self::serializeValueOrNull($this->description),
            'address'     => self::serializeValueOrNull($this->address),
            'status'      => $this->status->serialize(),
            'created_at'  => $this->createdAt->format(DateTimeImmutable::ATOM)
        ];
    }
}