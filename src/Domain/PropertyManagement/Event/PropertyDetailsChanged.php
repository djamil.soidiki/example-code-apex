<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\Event;

use App\Domain\PropertyManagement\ValueObject\PropertyDetails;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class PropertyDetailsChanged
 *
 * @package App\Domain\PropertyManagement\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyDetailsChanged implements Serializable
{
    /**
     * @var PropertyId
     */
    private $propertyId;

    /**
     * @var PropertyDetails
     */
    private $details;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * PropertyDetailsChanged constructor.
     *
     * @param PropertyId        $propertyId
     * @param PropertyDetails   $details
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(PropertyId $propertyId, PropertyDetails $details, DateTimeImmutable $updatedAt)
    {
        $this->propertyId = $propertyId;
        $this->details    = $details;
        $this->updatedAt  = $updatedAt;
    }

    /**
     * @return PropertyId
     */
    public function propertyId(): PropertyId
    {
        return $this->propertyId;
    }

    /**
     * @return PropertyDetails
     */
    public function details(): PropertyDetails
    {
        return $this->details;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'property_id');
        Assertion::keyExists($data, 'details');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new PropertyId($data['property_id']),
            PropertyDetails::fromArray($data['details']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'property_id' => $this->propertyId->serialize(),
            'details'     => $this->details->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}