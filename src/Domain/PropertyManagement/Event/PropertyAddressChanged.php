<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\Event;

use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class PropertyAddressChanged
 *
 * @package App\Domain\PropertyManagement\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyAddressChanged implements Serializable
{
    /**
     * @var PropertyId
     */
    private $propertyId;

    /**
     * @var PropertyAddress
     */
    private $address;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * PropertyAddressChanged constructor.
     *
     * @param PropertyId        $propertyId
     * @param PropertyAddress   $address
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(PropertyId $propertyId, PropertyAddress $address, DateTimeImmutable $updatedAt)
    {
        $this->propertyId = $propertyId;
        $this->address    = $address;
        $this->updatedAt  = $updatedAt;
    }

    /**
     * @return PropertyId
     */
    public function propertyId(): PropertyId
    {
        return $this->propertyId;
    }

    /**
     * @return PropertyAddress
     */
    public function address(): PropertyAddress
    {
        return $this->address;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'property_id');
        Assertion::keyExists($data, 'address');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new PropertyId($data['property_id']),
            PropertyAddress::fromArray($data['address']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'property_id' => $this->propertyId->serialize(),
            'address'     => $this->address->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}