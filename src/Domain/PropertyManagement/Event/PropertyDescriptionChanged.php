<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\Event;

use App\Domain\PropertyManagement\ValueObject\PropertyDescription;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class PropertyDescriptionChanged
 *
 * @package App\Domain\PropertyManagement\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyDescriptionChanged implements Serializable
{
    /**
     * @var PropertyId
     */
    private $propertyId;

    /**
     * @var PropertyDescription
     */
    private $description;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * PropertyDescriptionChanged constructor.
     *
     * @param PropertyId          $propertyId
     * @param PropertyDescription $description
     * @param DateTimeImmutable   $updatedAt
     */
    public function __construct(PropertyId $propertyId, PropertyDescription $description, DateTimeImmutable $updatedAt)
    {
        $this->propertyId  = $propertyId;
        $this->description = $description;
        $this->updatedAt   = $updatedAt;
    }

    /**
     * @return PropertyId
     */
    public function propertyId(): PropertyId
    {
        return $this->propertyId;
    }

    /**
     * @return PropertyDescription
     */
    public function description(): PropertyDescription
    {
        return $this->description;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'property_id');
        Assertion::keyExists($data, 'description');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new PropertyId($data['property_id']),
            new PropertyDescription($data['description']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'property_id' => $this->propertyId->serialize(),
            'description' => $this->description->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}