<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\Event;

use App\Domain\PropertyManagement\ValueObject\PropertyName;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use DateTimeImmutable;
use Exception;

/**
 * Class PropertyNameChanged
 *
 * @package App\Domain\PropertyManagement\Event
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyNameChanged implements Serializable
{
    /**
     * @var PropertyId
     */
    private $propertyId;

    /**
     * @var PropertyName
     */
    private $name;

    /**
     * @var DateTimeImmutable
     */
    private $updatedAt;

    /**
     * PropertyNameChanged constructor.
     *
     * @param PropertyId        $propertyId
     * @param PropertyName      $name
     * @param DateTimeImmutable $updatedAt
     */
    public function __construct(PropertyId $propertyId, PropertyName $name, DateTimeImmutable $updatedAt)
    {
        $this->propertyId = $propertyId;
        $this->name       = $name;
        $this->updatedAt  = $updatedAt;
    }

    /**
     * @return PropertyId
     */
    public function propertyId(): PropertyId
    {
        return $this->propertyId;
    }

    /**
     * @return PropertyName
     */
    public function name(): PropertyName
    {
        return $this->name;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     * @throws Exception
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'property_id');
        Assertion::keyExists($data, 'name');
        Assertion::keyExists($data, 'updated_at');

        return new self(
            new PropertyId($data['property_id']),
            new PropertyName($data['name']),
            new DateTimeImmutable($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'property_id' => $this->propertyId->serialize(),
            'name'        => $this->name->serialize(),
            'updated_at'  => $this->updatedAt->format(DateTimeImmutable::ATOM),
        ];
    }
}