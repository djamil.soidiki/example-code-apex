<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\Repository;

use App\Domain\PropertyManagement\Property;
use App\Domain\PropertyManagement\ValueObject\PropertyId;

/**
 * Interface PropertiesInterface
 *
 * @package App\Domain\PropertyManagement\Repository
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
interface PropertiesInterface
{
    /**
     * @param PropertyId $propertyId
     *
     * @return Property|null
     */
    public function get(PropertyId $propertyId): ?Property;

    /**
     * @param Property $property
     */
    public function save(Property $property): void;
}