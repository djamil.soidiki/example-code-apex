<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;

/**
 * Class PropertyName
 *
 * @package App\Domain\PropertyManagement\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyName extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * PropertyName constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $this->validateString($value);
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}