<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\FloatTrait;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\IntegerTrait;
use Assert\Assertion;
use Assert\AssertionFailedException;

/**
 * Class PropertyDetails
 *
 * @package App\Domain\PropertyManagement\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyDetails extends AbstractValueObject
{
    use IntegerTrait;
    use FloatTrait;

    /**
     * @var int|null
     */
    private $bedrooms;

    /**
     * @var int|null
     */
    private $fullBathrooms;

    /**
     * @var int|null
     */
    private $partialBathrooms;

    /**
     * @var int|null
     */
    private $squareFootage;

    /**
     * @var int|null
     */
    private $yearBuilt;

    /**
     * @var float|null
     */
    private $lotSize;

    /**
     * PropertyDetails constructor.
     *
     * @param int|null   $bedrooms
     * @param int|null   $fullBathrooms
     * @param int|null   $partialBathrooms
     * @param int|null   $squareFootage
     * @param int|null   $yearBuilt
     * @param float|null $lotSize
     */
    public function __construct(
        ?int $bedrooms = null,
        ?int $fullBathrooms = null,
        ?int $partialBathrooms = null,
        ?int $squareFootage = null,
        ?int $yearBuilt = null,
        ?float $lotSize = null
    )
    {
        if ($bedrooms !== null)
            $this->bedrooms = $this->validateInteger($bedrooms);

        if ($fullBathrooms !== null)
            $this->fullBathrooms = $this->validateInteger($fullBathrooms);

        if ($partialBathrooms !== null)
            $this->partialBathrooms = $this->validateInteger($partialBathrooms);

        if ($squareFootage !== null)
            $this->squareFootage = $this->validateInteger($squareFootage);

        if ($yearBuilt !== null)
            $this->yearBuilt = $this->validateInteger($yearBuilt);

        if ($lotSize !== null)
            $this->lotSize = $this->validateFloat($lotSize);
    }

    /**
     * @param array $propertyDetails
     *
     * @return static
     * @throws AssertionFailedException
     */
    public static function fromArray(array $propertyDetails): self
    {
        Assertion::keyExists($propertyDetails, 'bedrooms');
        Assertion::keyExists($propertyDetails, 'full_bathrooms');
        Assertion::keyExists($propertyDetails, 'partial_bathrooms');
        Assertion::keyExists($propertyDetails, 'square_footage');
        Assertion::keyExists($propertyDetails, 'year_built');
        Assertion::keyExists($propertyDetails, 'lot_size');

        return new self(
            $propertyDetails['bedrooms'],
            $propertyDetails['full_bathrooms'],
            $propertyDetails['partial_bathrooms'],
            $propertyDetails['square_footage'],
            $propertyDetails['year_built'],
            $propertyDetails['lot_size']
        );
    }

    /**
     * @return int|null
     */
    public function bedrooms(): ?int
    {
        return $this->bedrooms;
    }

    /**
     * @return int|null
     */
    public function fullBathrooms(): ?int
    {
        return $this->fullBathrooms;
    }

    /**
     * @return int|null
     */
    public function partialBathrooms(): ?int
    {
        return $this->partialBathrooms;
    }

    /**
     * @return int|null
     */
    public function squareFootage(): ?int
    {
        return $this->squareFootage;
    }

    /**
     * @return int|null
     */
    public function yearBuilt(): ?int
    {
        return $this->yearBuilt;
    }

    /**
     * @return float|null
     */
    public function lotSize(): ?float
    {
        return $this->lotSize;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'bedrooms'          => $this->bedrooms,
            'full_bathrooms'    => $this->fullBathrooms,
            'partial_bathrooms' => $this->partialBathrooms,
            'square_footage'    => $this->squareFootage,
            'year_built'        => $this->yearBuilt,
            'lot_size'          => $this->lotSize,
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return sprintf(
            "%s beds",
            $this->bedrooms
        );
    }
}