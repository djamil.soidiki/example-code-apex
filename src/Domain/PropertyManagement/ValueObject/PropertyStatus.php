<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;

/**
 * Class PropertyStatus
 *
 * @package App\Domain\PropertyManagement\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyStatus extends AbstractValueObject
{
    use StringTrait;

    const ACTIVATED = 'activated';
    const ARCHIVED  = 'archived';

    /**
     * @var string
     */
    private $value;

    /**
     * PropertyStatus constructor.
     *
     * @param string $status
     */
    public function __construct(string $status)
    {
        if (!defined("static::" . strtoupper($status)))
            throw new \InvalidArgumentException(sprintf('"%s" does not exist or it is not a valid property status', $status));

        $this->value = $this->validateString($status);
    }

    /**
     * @return static
     */
    public static function activated(): self
    {
        return new self(self::ACTIVATED);
    }

    /**
     * @return static
     */
    public static function archived(): self
    {
        return new self(self::ARCHIVED);
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->value;
    }
}