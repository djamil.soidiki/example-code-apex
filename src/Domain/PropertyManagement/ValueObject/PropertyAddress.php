<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement\ValueObject;

use App\Domain\SharedKernel\Generic\ValueObject\Scalar\StringTrait;
use App\Domain\SharedKernel\Generic\ValueObject\AbstractValueObject;
use Assert\Assertion;
use Assert\AssertionFailedException;

/**
 * Class PropertyAddress
 *
 * @package App\Domain\PropertyManagement\ValueObject
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PropertyAddress extends AbstractValueObject
{
    use StringTrait;

    /**
     * @var string
     */
    private $streetAddress;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $zipCode;

    /**
     * PropertyAddress constructor.
     *
     * @param string $streetAddress
     * @param string $city
     * @param string $state
     * @param string $zipCode
     */
    public function __construct(
        string $streetAddress,
        string $city,
        string $state,
        string $zipCode
    )
    {
        $this->streetAddress = ucwords(strtolower($this->validateString($streetAddress)));
        $this->city          = ucwords(strtolower($this->validateString($city)));
        $this->state         = $this->validateString($state);
        $this->zipCode       = $this->validateString($zipCode);
    }

    /**
     * @param array $propertyAddress
     *
     * @return static
     * @throws AssertionFailedException
     */
    public static function fromArray(array $propertyAddress): self
    {
        Assertion::keyExists($propertyAddress, 'street_address');
        Assertion::keyExists($propertyAddress, 'city');
        Assertion::keyExists($propertyAddress, 'state');
        Assertion::keyExists($propertyAddress, 'zip_code');

        return new self(
            $propertyAddress['street_address'],
            $propertyAddress['city'],
            $propertyAddress['state'],
            $propertyAddress['zip_code']
        );
    }

    /**
     * @return string
     */
    public function streetAddress(): string
    {
        return $this->streetAddress;
    }

    /**
     * @return string
     */
    public function city(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function state(): string
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function zipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        return [
            'street_address' => $this->streetAddress,
            'city'           => $this->city,
            'state'          => $this->state,
            'zip_code'       => $this->zipCode
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return sprintf(
            "%s, %s, %s %s ",
            $this->streetAddress, $this->city, $this->state, $this->zipCode
        );
    }
}