<?php

declare(strict_types=1);

namespace App\Domain\PropertyManagement;

use App\Domain\PropertyManagement\Event\PropertyAddressChanged;
use App\Domain\PropertyManagement\Event\PropertyArchived;
use App\Domain\PropertyManagement\Event\PropertyCreated;
use App\Domain\PropertyManagement\Event\PropertyDescriptionChanged;
use App\Domain\PropertyManagement\Event\PropertyDetailsChanged;
use App\Domain\PropertyManagement\Event\PropertyNameChanged;
use App\Domain\PropertyManagement\Event\PropertyRestored;
use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\Domain\PropertyManagement\ValueObject\PropertyDescription;
use App\Domain\PropertyManagement\ValueObject\PropertyDetails;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\PropertyManagement\ValueObject\PropertyName;
use App\Domain\PropertyManagement\ValueObject\PropertyStatus;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DateTimeImmutable;
use Exception;

/**
 * Class Property
 *
 * @package App\Domain\PropertyManagement
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class Property extends EventSourcedAggregateRoot
{
    /**
     * @var PropertyId
     */
    private $id;

    /**
     * @var UserId
     */
    private $managerId;

    /**
     * @var PropertyName
     */
    private $name;

    /**
     * @var PropertyDescription|null
     */
    private $description;

    /**
     * @var PropertyAddress|null
     */
    private $address;

    /**
     * @var PropertyDetails|null
     */
    private $details;

    /**
     * @var PropertyStatus
     */
    private $status;

    /**
     * @var DateTimeImmutable|null
     */
    private $updatedAt;

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param UserId                   $managerId
     * @param PropertyName             $propertyName
     * @param PropertyDescription|null $propertyDescription
     * @param PropertyAddress|null     $propertyAddress
     *
     * @return static
     * @throws Exception
     */
    public static function create(
        UserId $managerId,
        PropertyName $propertyName,
        ?PropertyDescription $propertyDescription = null,
        ?PropertyAddress $propertyAddress = null
    ): self
    {
        $property = new self();

        $property->apply(new PropertyCreated(
            new PropertyId(),
            $managerId,
            $propertyName,
            $propertyDescription,
            $propertyAddress,
            new DateTimeImmutable()
        ));

        return $property;
    }

    /**
     * @param PropertyName $name
     *
     * @throws Exception
     */
    public function changeName(PropertyName $name): void
    {
        if ($this->name->sameValueAs($name))
            return;

        $this->apply(new PropertyNameChanged(
            $this->id,
            $name,
            new DateTimeImmutable()
        ));
    }

    /**
     * @param PropertyDescription $description
     *
     * @throws Exception
     */
    public function changeDescription(PropertyDescription $description): void
    {
        if ($this->description !== null && $this->description->sameValueAs($description))
            return;

        $this->apply(new PropertyDescriptionChanged(
            $this->id,
            $description,
            new DateTimeImmutable()
        ));
    }

    /**
     * @param PropertyAddress $address
     *
     * @throws Exception
     */
    public function changeAddress(PropertyAddress $address): void
    {
        if ($this->address !== null && $this->address->sameValueAs($address))
            return;

        $this->apply(new PropertyAddressChanged(
            $this->id,
            $address,
            new DateTimeImmutable()
        ));
    }

    /**
     * @param PropertyDetails $details
     *
     * @throws Exception
     */
    public function changeDetails(PropertyDetails $details): void
    {
        if ($this->details !== null && $this->details->sameValueAs($details))
            return;

        $this->apply(new PropertyDetailsChanged(
            $this->id,
            $details,
            new DateTimeImmutable()
        ));
    }

    /**
     * @throws Exception
     */
    public function archive(): void
    {
        if ($this->status->sameValueAs(PropertyStatus::archived()))
            return;

        $this->apply(new PropertyArchived($this->id, new DateTimeImmutable()));
    }

    /**
     * @throws Exception
     */
    public function restore(): void
    {
        if ($this->status->sameValueAs(PropertyStatus::activated()))
            return;

        $this->apply(new PropertyRestored($this->id, new DateTimeImmutable()));
    }

    /**
     * @param PropertyCreated $event
     */
    protected function applyPropertyCreated(PropertyCreated $event): void
    {
        $this->id          = $event->propertyId();
        $this->managerId   = $event->managerId();
        $this->name        = $event->name();
        $this->description = $event->description();
        $this->address     = $event->address();
        $this->status      = $event->status();
        $this->createdAt   = $event->createdAt();
    }

    /**
     * @param PropertyNameChanged $event
     */
    protected function applyPropertyNameChanged(PropertyNameChanged $event): void
    {
        $this->id        = $event->propertyId();
        $this->name      = $event->name();
        $this->updatedAt = $event->propertyId();
    }

    /**
     * @param PropertyDescriptionChanged $event
     */
    protected function applyPropertyDescriptionChanged(PropertyDescriptionChanged $event): void
    {
        $this->id          = $event->propertyId();
        $this->description = $event->description();
        $this->updatedAt   = $event->propertyId();
    }

    /**
     * @param PropertyAddressChanged $event
     */
    protected function applyPropertyAddressChanged(PropertyAddressChanged $event): void
    {
        $this->id        = $event->propertyId();
        $this->address   = $event->address();
        $this->updatedAt = $event->propertyId();
    }

    /**
     * @param PropertyDetailsChanged $event
     */
    protected function applyPropertyDetailsChanged(PropertyDetailsChanged $event): void
    {
        $this->id        = $event->propertyId();
        $this->details   = $event->details();
        $this->updatedAt = $event->propertyId();
    }

    /**
     * @param PropertyArchived $event
     */
    protected function applyPropertyArchived(PropertyArchived $event): void
    {
        $this->id        = $event->propertyId();
        $this->status    = $event->status();
        $this->updatedAt = $event->propertyId();
    }

    /**
     * @param PropertyRestored $event
     */
    protected function applyPropertyRestored(PropertyRestored $event): void
    {
        $this->id        = $event->propertyId();
        $this->status    = $event->status();
        $this->updatedAt = $event->propertyId();
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRootId(): string
    {
        return $this->id->serialize();
    }

    /**
     * @return UserId
     */
    public function managerId(): UserId
    {
        return $this->managerId;
    }
}