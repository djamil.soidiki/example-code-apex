<?php

declare(strict_types=1);

namespace App\Application\Billing\Plan\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\Billing\Plan\Command\CreatePlan;
use App\Domain\SharedKernel\Billing\Plan\Plan;
use App\Domain\SharedKernel\Billing\Plan\Repository\PlansInterface;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanBillingCycle;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanName;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanProductId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Currency;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Exception;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CreatePlanHandler
 *
 * @package App\Application\Billing\Plan\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreatePlanHandler extends AbstractPlanCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * CreatePlanHandler constructor.
     *
     * @param PlansInterface                $plans
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(PlansInterface $plans, AuthorizationCheckerInterface $authorizationChecker)
    {
        parent::__construct($plans);
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param CreatePlan $command
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(CreatePlan $command): string
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN'))
            throw new AccessDeniedException();

        $productId = new PlanProductId($command->productId());
        $name      = new PlanName($command->name());
        $price     = new Money($command->price() * 100, new Currency('USD'));
        $interval  = new PlanBillingCycle($command->interval());

        $plan = Plan::create($productId, $name, $price, $interval);

        $this->plans->save($plan);

        return $plan->getAggregateRootId();
    }
}