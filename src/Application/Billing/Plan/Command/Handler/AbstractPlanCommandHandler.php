<?php

declare(strict_types=1);

namespace App\Application\Billing\Plan\Command\Handler;

use App\Domain\SharedKernel\Billing\Plan\Repository\PlansInterface;

/**
 * Class AbstractPlanCommandHandler
 *
 * @package App\Application\Billing\Plan\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractPlanCommandHandler
{
    /**
     * @var PlansInterface
     */
    protected $plans;

    /**
     * AbstractPlanCommandHandler constructor.
     *
     * @param PlansInterface $plans
     */
    public function __construct(PlansInterface $plans)
    {
        $this->plans = $plans;
    }
}