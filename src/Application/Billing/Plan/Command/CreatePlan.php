<?php

declare(strict_types=1);

namespace App\Application\Billing\Plan\Command;

/**
 * Class CreatePlan
 *
 * @package App\Application\Billing\Plan\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreatePlan
{
    /**
     * @var string
     */
    private $productId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $interval;

    /**
     * CreatePlan constructor.
     *
     * @param string $productId
     * @param string $name
     * @param float  $price
     * @param string $interval
     */
    public function __construct(string $productId, string $name, float $price, string $interval)
    {
        $this->productId = $productId;
        $this->name      = $name;
        $this->price     = $price;
        $this->interval  = $interval;
    }

    /**
     * @return string
     */
    public function productId(): string
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function interval(): string
    {
        return $this->interval;
    }
}