<?php

declare(strict_types=1);

namespace App\Application\Billing\Plan\Query;

/**
 * Class GetPlanByPlanIdQuery
 *
 * @package App\Application\Billing\Plan\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetPlanByPlanIdQuery
{
    /**
     * @var string
     */
    private $planId;

    /**
     * GetPlanByPlanId constructor.
     *
     * @param string $planId
     */
    public function __construct(string $planId)
    {
        $this->planId = $planId;
    }

    /**
     * @return string
     */
    public function planId(): string
    {
        return $this->planId;
    }
}