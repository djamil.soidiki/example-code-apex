<?php

declare(strict_types=1);

namespace App\Application\Billing\Plan\Query\Handler;

use App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Repository\Plans;

/**
 * Class AbstractPlanQueryHandler
 *
 * @package App\Application\Billing\Plan\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractPlanQueryHandler
{
    /**
     * @var Plans
     */
    protected $planRepository;

    /**
     * AbstractPlanQueryHandler constructor.
     *
     * @param Plans $planRepository
     */
    public function __construct(Plans $planRepository)
    {
        $this->planRepository = $planRepository;
    }
}