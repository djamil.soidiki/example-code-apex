<?php

declare(strict_types=1);

namespace App\Application\Billing\Plan\Query\Handler;

use App\Application\Billing\Plan\Query\FindPlansQuery;

/**
 * Class FindPlansHandler
 *
 * @package App\Application\Billing\Plan\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindPlansHandler extends AbstractPlanQueryHandler
{
    /**
     * @param FindPlansQuery $query
     *
     * @return array
     */
    public function __invoke(FindPlansQuery $query): array
    {
        return $this->planRepository->findAll();
    }
}