<?php

declare(strict_types=1);

namespace App\Application\Billing\Plan\Query\Handler;

use App\Application\Billing\Plan\Query\GetPlanByPlanIdQuery;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Billing\Plan\Query\Doctrine\Model\Plan;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class GetPlanByPlanIdHandler
 *
 * @package App\Application\Billing\Plan\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetPlanByPlanIdHandler extends AbstractPlanQueryHandler
{
    /**
     * @param GetPlanByPlanIdQuery $query
     *
     * @return Plan
     * @throws NotFoundException
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function __invoke(GetPlanByPlanIdQuery $query): Plan
    {
        return $this->planRepository->findOneByPlanId(new PlanId($query->planId()));
    }
}