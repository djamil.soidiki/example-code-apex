<?php

declare(strict_types=1);

namespace App\Application\Billing\Plan\Query;

/**
 * Class FindPlansQuery
 *
 * @package App\Application\Billing\Plan\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindPlansQuery
{
}