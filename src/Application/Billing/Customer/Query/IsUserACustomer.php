<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Query;

/**
 * Class IsUserACustomer
 *
 * @package App\Application\Billing\Customer\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class IsUserACustomer
{
    /**
     * @var string
     */
    private $userId;

    /**
     * IsUserACustomer constructor.
     *
     * @param string $userId
     */
    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function userId(): string
    {
        return $this->userId;
    }
}