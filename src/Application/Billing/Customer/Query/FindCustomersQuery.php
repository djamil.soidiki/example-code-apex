<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Query;

/**
 * Class FindCustomersQuery
 *
 * @package App\Application\Billing\Customer\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindCustomersQuery
{
}