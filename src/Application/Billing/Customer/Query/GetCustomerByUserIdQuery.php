<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Query;

/**
 * Class GetCustomerByUserIdQuery
 *
 * @package App\Application\Billing\Customer\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetCustomerByUserIdQuery
{
    /**
     * @var string
     */
    private $userId;

    /**
     * GetCustomerByUserIdQuery constructor.
     *
     * @param string $userId
     */
    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function userId(): string
    {
        return $this->userId;
    }
}