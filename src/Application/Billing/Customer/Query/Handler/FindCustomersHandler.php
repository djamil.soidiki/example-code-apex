<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Query\Handler;

use App\Application\Billing\Customer\Query\FindCustomersQuery;

/**
 * Class FindCustomersHandler
 *
 * @package App\Application\Billing\Customer\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindCustomersHandler extends AbstractCustomerQueryHandler
{
    /**
     * @param FindCustomersQuery $query
     *
     * @return array
     */
    public function __invoke(FindCustomersQuery $query): array
    {
        return $this->customers->findAll();
    }
}