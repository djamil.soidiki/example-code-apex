<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Query\Handler;

use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Repository\Customers;

/**
 * Class AbstractCustomerQueryHandler
 *
 * @package App\Application\Billing\Customer\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractCustomerQueryHandler
{
    /**
     * @var Customers
     */
    protected $customers;

    /**
     * AbstractCustomerQueryHandler constructor.
     *
     * @param Customers $customers
     */
    public function __construct(Customers $customers)
    {
        $this->customers = $customers;
    }
}