<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Query\Handler;

use App\Application\Billing\Customer\Query\GetCustomerByUserIdQuery;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class GetCustomerByUserIdHandler
 *
 * @package App\Application\Billing\Customer\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetCustomerByUserIdHandler extends AbstractCustomerQueryHandler
{
    /**
     * @param GetCustomerByUserIdQuery $query
     *
     * @return Customer
     * @throws NotFoundException
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function __invoke(GetCustomerByUserIdQuery $query): Customer
    {
        return $this->customers->findCustomerByUserId(new UserId($query->userId()));
    }
}