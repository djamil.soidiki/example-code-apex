<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Query\Handler;

use App\Application\Billing\Customer\Query\IsUserACustomer;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class IsUserACustomerHandler
 *
 * @package App\Application\Billing\Customer\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class IsUserACustomerHandler extends AbstractCustomerQueryHandler
{
    /**
     * @param IsUserACustomer $query
     *
     * @return bool
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function __invoke(IsUserACustomer $query): bool
    {
        return $this->customers->isUserACustomer(new UserId($query->userId()));
    }
}