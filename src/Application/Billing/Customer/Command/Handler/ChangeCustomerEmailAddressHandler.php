<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\Billing\Customer\Command\ChangeCustomerEmailAddress;
use App\Domain\SharedKernel\Billing\Customer\Repository\CustomersInterface;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangeUserEmailAddressHandler
 *
 * @package App\Application\Billing\Customer\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeCustomerEmailAddressHandler extends AbstractCustomerCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ChangeCustomerEmailAddressHandler constructor.
     *
     * @param CustomersInterface $customers
     * @param Security           $security
     */
    public function __construct(CustomersInterface $customers, Security $security)
    {
        parent::__construct($customers);
        $this->security = $security;
    }

    /**
     * @param ChangeCustomerEmailAddress $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeCustomerEmailAddress $command): void
    {
        $customer = $this->customers->get(new CustomerId($command->customerId()));

        if ($this->security->getUser()->getId() !== $customer->userId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $customer->changeEmailAddress(new EmailAddress($command->emailAddress()));

        $this->customers->save($customer);
    }
}