<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Command\Handler;

use App\Domain\SharedKernel\Billing\Customer\Repository\CustomersInterface;

/**
 * Class AbstractCustomerCommandHandler
 *
 * @package App\Application\Billing\Customer\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractCustomerCommandHandler
{
    /**
     * @var CustomersInterface
     */
    protected $customers;

    /**
     * AbstractCustomerCommandHandler constructor.
     *
     * @param CustomersInterface $customers
     */
    public function __construct(CustomersInterface $customers)
    {
        $this->customers = $customers;
    }
}