<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\Billing\Customer\Command\ChangeCustomerPaymentMethod;
use App\Domain\SharedKernel\Billing\Customer\Repository\CustomersInterface;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\PaymentMethod;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangeCustomerPaymentMethodHandler
 *
 * @package App\Application\Billing\Customer\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeCustomerPaymentMethodHandler extends AbstractCustomerCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ChangeCustomerEmailAddressHandler constructor.
     *
     * @param CustomersInterface $customers
     * @param Security           $security
     */
    public function __construct(CustomersInterface $customers, Security $security)
    {
        parent::__construct($customers);
        $this->security = $security;
    }

    /**
     * @param ChangeCustomerPaymentMethod $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeCustomerPaymentMethod $command): void
    {
        $customer = $this->customers->get(new CustomerId($command->customerId()));

        if ($this->security->getUser()->getId() !== $customer->userId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $customer->changePaymentMethod(new PaymentMethod($command->paymentMethod()));

        $this->customers->save($customer);
    }
}