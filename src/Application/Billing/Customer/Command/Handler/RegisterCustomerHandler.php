<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Command\Handler;

use App\Application\Billing\Customer\Command\RegisterCustomer;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Billing\Customer\Customer;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\PaymentMethod;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Exception;

/**
 * Class RegisterCustomerHandler
 *
 * @package App\Application\Billing\Customer\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RegisterCustomerHandler extends AbstractCustomerCommandHandler
{
    /**
     * @param RegisterCustomer $command
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(RegisterCustomer $command): string
    {
        $userId       = new UserId($command->userId());
        $emailAddress = new EmailAddress($command->emailAddress());
        $fullName     = new FullName($command->firstName(), $command->lastName());

        if ($command->paymentMethod() === null)
            $customer = Customer::register($userId, $emailAddress, $fullName);
        else
            $customer = Customer::register($userId, $emailAddress, $fullName, new PaymentMethod($command->paymentMethod()));

        $this->customers->save($customer);

        return $customer->getAggregateRootId();
    }
}