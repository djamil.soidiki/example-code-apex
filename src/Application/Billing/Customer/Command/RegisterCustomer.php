<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Command;

/**
 * Class RegisterCustomer
 *
 * @package App\Application\Billing\Customer\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RegisterCustomer
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string|null
     */
    private $paymentMethod;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * RegisterCustomer constructor.
     *
     * @param string      $userId
     * @param string      $emailAddress
     * @param string      $firstName
     * @param string      $lastName
     * @param string|null $paymentMethod
     */
    public function __construct(
        string $userId,
        string $emailAddress,
        string $firstName,
        string $lastName,
        ?string $paymentMethod = null
    )
    {
        $this->userId        = $userId;
        $this->emailAddress  = $emailAddress;
        $this->firstName     = $firstName;
        $this->lastName      = $lastName;
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function userId(): string
    {
        return $this->userId;
    }

    /**
     * @return string|null
     */
    public function paymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    /**
     * @return string
     */
    public function emailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @return string
     */
    public function firstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function lastName(): string
    {
        return $this->lastName;
    }
}