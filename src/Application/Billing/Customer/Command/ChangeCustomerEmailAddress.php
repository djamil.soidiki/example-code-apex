<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Command;

/**
 * Class ChangeContactEmailAddressCommand
 *
 * @package App\Application\Billing\Customer\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeCustomerEmailAddress
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * ChangeContactEmailAddressCommand constructor.
     *
     * @param string $customerId
     * @param string $emailAddress
     */
    public function __construct(string $customerId, string $emailAddress)
    {
        $this->customerId   = $customerId;
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function customerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function emailAddress(): string
    {
        return $this->emailAddress;
    }
}