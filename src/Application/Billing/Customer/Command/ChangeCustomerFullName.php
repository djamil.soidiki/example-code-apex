<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Command;

/**
 * Class ChangeContactFullNameCommand
 *
 * @package App\Application\Billing\Customer\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeCustomerFullName
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * ChangeContactFullNameCommand constructor.
     *
     * @param string $customerId
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct(string $customerId, string $firstName, string $lastName)
    {
        $this->customerId = $customerId;
        $this->firstName  = $firstName;
        $this->lastName   = $lastName;
    }

    /**
     * @return string
     */
    public function customerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function firstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function lastName(): string
    {
        return $this->lastName;
    }
}