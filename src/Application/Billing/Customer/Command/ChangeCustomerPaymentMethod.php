<?php

declare(strict_types=1);

namespace App\Application\Billing\Customer\Command;

/**
 * Class ChangeCustomerPaymentMethod
 *
 * @package App\Application\Billing\Customer\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeCustomerPaymentMethod
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * @var string
     */
    private $paymentMethod;

    /**
     * ChangeCustomerPaymentMethod constructor.
     *
     * @param string $customerId
     * @param string $paymentMethod
     */
    public function __construct(string $customerId, string $paymentMethod)
    {
        $this->customerId    = $customerId;
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function customerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function paymentMethod(): string
    {
        return $this->paymentMethod;
    }
}