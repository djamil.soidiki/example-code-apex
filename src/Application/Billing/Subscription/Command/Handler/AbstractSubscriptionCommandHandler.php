<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Command\Handler;

use App\Domain\SharedKernel\Billing\Subscription\Repository\SubscriptionsInterface;

/**
 * Class AbstractSubscriptionCommandHandler
 *
 * @package App\Application\Billing\Subscription\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractSubscriptionCommandHandler
{
    /**
     * @var SubscriptionsInterface
     */
    protected $subscriptions;

    /**
     * AbstractSubscriptionCommandHandler constructor.
     *
     * @param SubscriptionsInterface $subscriptions
     */
    public function __construct(SubscriptionsInterface $subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }
}