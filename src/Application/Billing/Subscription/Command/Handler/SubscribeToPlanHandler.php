<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Command\Handler;

use App\Application\Billing\Subscription\Command\SubscribeToPlan;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanId;
use App\Domain\SharedKernel\Billing\Subscription\Subscription;
use Exception;

/**
 * Class SubscribeToPlanHandler
 *
 * @package App\Application\Billing\Subscription\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class SubscribeToPlanHandler extends AbstractSubscriptionCommandHandler
{
    /**
     * @param SubscribeToPlan $command
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(SubscribeToPlan $command): string
    {
        $customerId = new CustomerId($command->customerId());
        $planId     = new PlanId($command->planId());

        $subscription = Subscription::subscribeToPlan($customerId, $planId);

        $this->subscriptions->save($subscription);

        return $subscription->getAggregateRootId();
    }
}