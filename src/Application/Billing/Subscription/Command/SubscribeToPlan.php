<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Command;

/**
 * Class SubscribeToPlan
 *
 * @package App\Application\Billing\Subscription\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class SubscribeToPlan
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * @var string
     */
    private $planId;

    /**
     * SubscribeToPlan constructor.
     *
     * @param string $customerId
     * @param string $planId
     */
    public function __construct(string $customerId, string $planId)
    {
        $this->customerId = $customerId;
        $this->planId     = $planId;
    }

    /**
     * @return string
     */
    public function customerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function planId(): string
    {
        return $this->planId;
    }
}