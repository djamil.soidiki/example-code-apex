<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Query\Handler;

use App\Application\Billing\Subscription\Query\GetSubscriptionByCustomerId;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Model\Subscription;
use Exception;

/**
 * Class GetSubscriptionByCustomerIdHandler
 *
 * @package App\Application\Billing\Subscription\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetSubscriptionByCustomerIdHandler extends AbstractSubscriptionQueryHandler
{
    /**
     * @param GetSubscriptionByCustomerId $query
     *
     * @return Subscription
     * @throws Exception
     */
    public function __invoke(GetSubscriptionByCustomerId $query): Subscription
    {
        return $this->subscriptions->findSubscriptionByCustomerId(new CustomerId($query->customerId()));
    }
}