<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Query\Handler;

use App\Application\Billing\Subscription\Query\IsCustomerSubscribed;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class IsCustomerSubscribedHandler
 *
 * @package App\Application\Billing\Subscription\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class IsCustomerSubscribedHandler extends AbstractSubscriptionQueryHandler
{
    /**
     * @param IsCustomerSubscribed $query
     *
     * @return bool
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function __invoke(IsCustomerSubscribed $query): bool
    {
        return $this->subscriptions->isCustomerSubscribed(new CustomerId($query->customerId()));
    }
}