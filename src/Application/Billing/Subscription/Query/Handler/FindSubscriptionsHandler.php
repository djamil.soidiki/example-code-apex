<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Query\Handler;

use App\Application\Billing\Subscription\Query\FindSubscriptions;

/**
 * Class FindSubscriptionsHandler
 *
 * @package App\Application\Billing\Subscription\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindSubscriptionsHandler extends AbstractSubscriptionQueryHandler
{
    /**
     * @param FindSubscriptions $query
     *
     * @return array
     */
    public function __invoke(FindSubscriptions $query): array
    {
        return $this->subscriptions->findAll();
    }
}