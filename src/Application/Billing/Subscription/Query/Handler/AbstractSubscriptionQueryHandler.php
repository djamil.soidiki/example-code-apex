<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Query\Handler;


use App\Infrastructure\SharedKernel\Billing\Subscription\Query\Doctrine\Repository\Subscriptions;

/**
 * Class AbstractSubscriptionQueryHandler
 *
 * @package App\Application\Billing\Subscription\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractSubscriptionQueryHandler
{
    /**
     * @var Subscriptions
     */
    protected $subscriptions;

    /**
     * AbstractSubscriptionQueryHandler constructor.
     *
     * @param Subscriptions $subscriptions
     */
    public function __construct(Subscriptions $subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }
}