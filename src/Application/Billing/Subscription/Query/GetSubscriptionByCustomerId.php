<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Query;

/**
 * Class GetSubscriptionByCustomerId
 *
 * @package App\Application\Billing\Subscription\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetSubscriptionByCustomerId
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * GetSubscriptionByCustomerId constructor.
     *
     * @param string $customerId
     */
    public function __construct(string $customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function customerId(): string
    {
        return $this->customerId;
    }
}