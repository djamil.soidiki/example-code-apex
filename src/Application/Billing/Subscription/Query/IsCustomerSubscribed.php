<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Query;

/**
 * Class IsCustomerSubscribed
 *
 * @package App\Application\Billing\Subscription\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class IsCustomerSubscribed
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * IsCustomerSubscribed constructor.
     *
     * @param string $customerId
     */
    public function __construct(string $customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function customerId(): string
    {
        return $this->customerId;
    }
}