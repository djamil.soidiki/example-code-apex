<?php

declare(strict_types=1);

namespace App\Application\Billing\Subscription\Query;

/**
 * Class FindSubscriptions
 *
 * @package App\Application\Billing\Subscription\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindSubscriptions
{
}