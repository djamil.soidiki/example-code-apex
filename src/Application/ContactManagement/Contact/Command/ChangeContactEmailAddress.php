<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Command;

/**
 * Class ChangeContactEmailAddress
 *
 * @package App\Application\ContactManagement\Contact\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeContactEmailAddress
{
    /**
     * @var string
     */
    private $contactId;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * ChangeContactEmailAddress constructor.
     *
     * @param string $contactId
     * @param string $emailAddress
     */
    public function __construct(string $contactId, string $emailAddress)
    {
        $this->contactId    = $contactId;
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function contactId(): string
    {
        return $this->contactId;
    }

    /**
     * @return string
     */
    public function emailAddress(): string
    {
        return $this->emailAddress;
    }
}