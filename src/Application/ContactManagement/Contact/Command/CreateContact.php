<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Command;

/**
 * Class CreateContact
 *
 * @package App\Application\ContactManagement\Contact\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateContact
{
    /**
     * @var string
     */
    private $ownerId;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * CreateContact constructor.
     *
     * @param string $ownerId
     * @param string $firstName
     * @param string $lastName
     * @param string $emailAddress
     */
    public function __construct(string $ownerId, string $firstName, string $lastName, string $emailAddress)
    {
        $this->ownerId      = $ownerId;
        $this->firstName    = $firstName;
        $this->lastName     = $lastName;
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return mixed
     */
    public function ownerId()
    {
        return $this->ownerId;
    }

    /**
     * @return mixed
     */
    public function firstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function lastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function emailAddress()
    {
        return $this->emailAddress;
    }
}