<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Command;

/**
 * Class DeleteContact
 *
 * @package App\Application\ContactManagement\Contact\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class DeleteContact
{
    /**
     * @var string
     */
    private $contactId;

    /**
     * DeleteContact constructor.
     *
     * @param string $contactId
     */
    public function __construct(string $contactId)
    {
        $this->contactId = $contactId;
    }

    /**
     * @return string
     */
    public function contactId(): string
    {
        return $this->contactId;
    }
}