<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Command;

/**
 * Class ChangeContactFullName
 *
 * @package App\Application\ContactManagement\Contact\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeContactFullName
{
    /**
     * @var string
     */
    private $contactId;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * ChangeContactFullName constructor.
     *
     * @param string $contactId
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct(string $contactId, string $firstName, string $lastName)
    {
        $this->contactId = $contactId;
        $this->firstName = $firstName;
        $this->lastName  = $lastName;
    }

    /**
     * @return string
     */
    public function contactId(): string
    {
        return $this->contactId;
    }

    /**
     * @return string
     */
    public function firstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function lastName(): string
    {
        return $this->lastName;
    }
}