<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Command\Handler;

use App\Domain\ContactManagement\Contact\Repository\ContactsInterface;

/**
 * Class AbstractContactCommandHandler
 *
 * @package App\Application\ContactManagement\Contact\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractContactCommandHandler
{
    /**
     * @var ContactsInterface
     */
    protected $contacts;

    /**
     * AbstractContactCommandHandler constructor.
     *
     * @param ContactsInterface $contacts
     */
    public function __construct(ContactsInterface $contacts)
    {
        $this->contacts = $contacts;
    }
}