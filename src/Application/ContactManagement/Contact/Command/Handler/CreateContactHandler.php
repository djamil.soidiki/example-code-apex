<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Command\Handler;

use App\Application\ContactManagement\Contact\Command\CreateContact;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\ContactManagement\Contact\Contact;
use App\Domain\ContactManagement\Contact\Exception\ContactEmailAddressIsNotUniqueException;
use App\Domain\ContactManagement\Contact\Repository\ContactsInterface;
use App\Domain\ContactManagement\Contact\Specification\ContactEmailAddressIsUniqueSpecification;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Repository\Contacts;
use Exception;

/**
 * Class CreateContactHandler
 *
 * @package App\Application\ContactManagement\Contact\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateContactHandler extends AbstractContactCommandHandler
{
    /**
     * @var Contacts
     */
    private $contactsQuery;

    /**
     * CreateContactHandler constructor.
     *
     * @param ContactsInterface $contacts
     * @param Contacts          $contactsQuery
     */
    public function __construct(ContactsInterface $contacts, Contacts $contactsQuery)
    {
        parent::__construct($contacts);

        $this->contactsQuery = $contactsQuery;
    }

    /**
     * @param CreateContact $command
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(CreateContact $command): string
    {
        $ownerId      = new UserId($command->ownerId());
        $fullName     = new FullName($command->firstName(), $command->lastName());
        $emailAddress = new EmailAddress($command->emailAddress());

        $contactEmailAddressIsUnique = $this->contactsQuery->selectSatisfying(
            new ContactEmailAddressIsUniqueSpecification($ownerId, $emailAddress)
        );

        if (!empty($contactEmailAddressIsUnique))
            throw new ContactEmailAddressIsNotUniqueException();

        $contact = Contact::create($ownerId, $fullName, $emailAddress);

        $this->contacts->save($contact);

        return $contact->getAggregateRootId();
    }
}