<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\ContactManagement\Contact\Command\ChangeContactPhoneNumber;
use App\Domain\ContactManagement\Contact\Contact;
use App\Domain\ContactManagement\Contact\Repository\ContactsInterface;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\SharedKernel\Generic\ValueObject\Phone\PhoneNumber;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangeContactPhoneNumberHandler
 *
 * @package App\Application\ContactManagement\Contact\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeContactPhoneNumberHandler extends AbstractContactCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ChangeContactEmailAddressHandler constructor.
     *
     * @param ContactsInterface $contacts
     * @param Security          $security
     */
    public function __construct(ContactsInterface $contacts, Security $security)
    {
        parent::__construct($contacts);
        $this->security = $security;
    }

    /**
     * @param ChangeContactPhoneNumber $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeContactPhoneNumber $command): void
    {
        $contactId   = new ContactId($command->contactId());
        $phoneNumber = new PhoneNumber($command->country(), $command->phoneNumber());

        /** @var Contact $contact */
        $contact = $this->contacts->get($contactId);

        if ($this->security->getUser()->getId() !== $contact->ownerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $contact->changePhoneNumber($phoneNumber);

        $this->contacts->save($contact);
    }
}