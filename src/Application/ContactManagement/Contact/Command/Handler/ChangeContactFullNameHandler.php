<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\ContactManagement\Contact\Command\ChangeContactFullName;
use App\Domain\ContactManagement\Contact\Contact;
use App\Domain\ContactManagement\Contact\Repository\ContactsInterface;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangeContactFullNameHandler
 *
 * @package App\Application\ContactManagement\Contact\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeContactFullNameHandler extends AbstractContactCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ChangeContactEmailAddressHandler constructor.
     *
     * @param ContactsInterface $contacts
     * @param Security          $security
     */
    public function __construct(ContactsInterface $contacts, Security $security)
    {
        parent::__construct($contacts);
        $this->security = $security;
    }

    /**
     * @param ChangeContactFullName $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeContactFullName $command): void
    {
        $contactId = new ContactId($command->contactId());
        $fullName  = new FullName($command->firstName(), $command->lastName());

        /** @var Contact $contact */
        $contact = $this->contacts->get($contactId);

        if ($this->security->getUser()->getId() !== $contact->ownerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $contact->changeFullName($fullName);

        $this->contacts->save($contact);
    }
}