<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Command;

/**
 * Class ChangeContactPhoneNumber
 *
 * @package App\Application\ContactManagement\Contact\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeContactPhoneNumber
{
    /**
     * @var string
     */
    private $contactId;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * ChangeContactPhoneNumber constructor.
     *
     * @param string $contactId
     * @param string $country
     * @param string $phoneNumber
     */
    public function __construct(string $contactId, string $country, string $phoneNumber)
    {
        $this->contactId   = $contactId;
        $this->country     = $country;
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function contactId(): string
    {
        return $this->contactId;
    }

    /**
     * @return string
     */
    public function country(): string
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function phoneNumber(): string
    {
        return $this->phoneNumber;
    }
}