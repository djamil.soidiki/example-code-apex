<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Query;

/**
 * Class FindContactsByOwnerId
 *
 * @package App\Application\ContactManagement\Contact\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindContactsByOwnerId
{
    /**
     * @var string
     */
    private $ownerId;

    /**
     * @var array
     */
    private $filters;

    /**
     * FindContactsByOwnerId constructor.
     *
     * @param string $ownerId
     * @param array  $filters
     */
    public function __construct(string $ownerId, array $filters = [])
    {
        $this->ownerId = $ownerId;
        $this->filters = $filters;
    }

    /**
     * @return string
     */
    public function ownerId(): string
    {
        return $this->ownerId;
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return $this->filters;
    }
}