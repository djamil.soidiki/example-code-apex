<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Query;

/**
 * Class GetContactByContactId
 *
 * @package App\Application\ContactManagement\Contact\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetContactByContactId
{
    /**
     * @var string
     */
    private $contactId;

    /**
     * GetContactByContactId constructor.
     *
     * @param string $contactId
     */
    public function __construct(string $contactId)
    {
        $this->contactId = $contactId;
    }

    /**
     * @return string
     */
    public function contactId(): string
    {
        return $this->contactId;
    }
}