<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Query\Handler;

use App\Application\ContactManagement\Contact\Query\FindContactsByOwnerId;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Repository\Contacts;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Member;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository\Members;
use Exception;

/**
 * Class FindContactsByOwnerIdHandler
 *
 * @package App\Application\ContactManagement\Contact\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindContactsByOwnerIdHandler extends AbstractContactQueryHandler
{
    /**
     * @var Members
     */
    protected $members;

    /**
     * FindContactsByOwnerIdHandler constructor.
     *
     * @param Contacts $contacts
     * @param Members  $members
     */
    public function __construct(Contacts $contacts, Members $members)
    {
        parent::__construct($contacts);

        $this->members = $members;
    }

    /**
     * @param FindContactsByOwnerId $query
     *
     * @return array
     * @throws Exception
     */
    public function __invoke(FindContactsByOwnerId $query): array
    {
        $ownerId = new UserId($query->ownerId());

        $contactIds = [];
        if (!empty($query->filters()) && isset($query->filters()['listId'])) {

            if ($query->filters()['listId'] === "-1" || $query->filters()['listId'] === "-2") {
                $members = $this->members->findMembersByOwnerId($ownerId);
            } else {
                $groupId = new GroupId($query->filters()['listId']);
                $members = $this->members->findMembersByGroupId($groupId);
            }

            /** @var Member $member */
            foreach ($members as $member) {
                $contactIds[] = $member->contactId();
            }

            if (empty($contactIds))
                return [];
        }

        $queryFunctionName = (isset($query->filters()['listId']) && $query->filters()['listId'] === "-2") ? 'findContactsByOwnerIdAndNotContactIds' : 'findContactsByOwnerIdAndContactIds';

        if (!empty($query->filters() && isset($query->filters()['q'])))
            return $this->contacts->$queryFunctionName($ownerId, $contactIds, $query->filters()['q']);

        return $this->contacts->$queryFunctionName($ownerId, $contactIds);

    }
}