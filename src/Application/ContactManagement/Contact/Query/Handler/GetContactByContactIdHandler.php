<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Query\Handler;

use App\Application\ContactManagement\Contact\Query\GetContactByContactId;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model\Contact;
use Exception;

/**
 * Class GetContactByContactIdHandler
 *
 * @package App\Application\ContactManagement\Contact\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetContactByContactIdHandler extends AbstractContactQueryHandler
{
    /**
     * @param GetContactByContactId $query
     *
     * @return Contact
     * @throws Exception
     */
   public function __invoke(GetContactByContactId $query): Contact
   {
       $contactId = new ContactId($query->contactId());

       return $this->contacts->findContactByContactId($contactId);
   }
}