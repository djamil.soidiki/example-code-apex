<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Contact\Query\Handler;

use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Repository\Contacts;

/**
 * Class AbstractContactQueryHandler
 *
 * @package App\Application\ContactManagement\Contact\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractContactQueryHandler
{
    /**
     * @var Contacts
     */
    protected $contacts;

    /**
     * AbstractContactQueryHandler constructor.
     *
     * @param Contacts $contacts
     */
    public function __construct(Contacts $contacts)
    {
        $this->contacts = $contacts;
    }
}