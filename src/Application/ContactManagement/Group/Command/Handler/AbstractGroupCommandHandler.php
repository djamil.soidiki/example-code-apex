<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command\Handler;

use App\Domain\ContactManagement\Group\Repository\GroupsInterface;

/**
 * Class AbstractGroupCommandHandler
 *
 * @package App\Application\ContactManagement\Group\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractGroupCommandHandler
{
    /**
     * @var GroupsInterface
     */
    protected $groups;

    /**
     * AbstractGroupCommandHandler constructor.
     *
     * @param GroupsInterface $groups
     */
    public function __construct(GroupsInterface $groups)
    {
        $this->groups = $groups;
    }
}