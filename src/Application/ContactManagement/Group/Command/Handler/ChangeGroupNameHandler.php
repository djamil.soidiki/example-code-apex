<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\ContactManagement\Group\Command\ChangeGroupName;
use App\Domain\ContactManagement\Group\Exception\GroupNameIsNotUniqueException;
use App\Domain\ContactManagement\Group\Group;
use App\Domain\ContactManagement\Group\Repository\GroupsInterface;
use App\Domain\ContactManagement\Group\Specification\GroupNameIsUniqueSpecification;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\ContactManagement\Group\ValueObject\GroupName;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository\Groups;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangeGroupNameHandler
 *
 * @package App\Application\ContactManagement\Group\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeGroupNameHandler extends AbstractGroupCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * @var Groups
     */
    private $groupsQuery;

    /**
     * ChangeGroupNameHandler constructor.
     *
     * @param GroupsInterface $groups
     * @param Groups          $groupsQuery
     * @param Security        $security
     */
    public function __construct(GroupsInterface $groups, Groups $groupsQuery, Security $security)
    {
        parent::__construct($groups);
        $this->groupsQuery = $groupsQuery;
        $this->security    = $security;
    }

    /**
     * @param ChangeGroupName $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeGroupName $command)
    {
        $ownerId   = new UserId($command->ownerId());
        $groupId   = new GroupId($command->groupId());
        $groupName = new GroupName($command->name());

        /** @var Group $group */
        $group = $this->groups->get($groupId);

        if ($this->security->getUser()->getId() !== $group->ownerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $groupNameIsUnique = $this->groupsQuery->selectSatisfying(
            new GroupNameIsUniqueSpecification($ownerId, $groupName)
        );

        if (!empty($groupNameIsUnique))
            throw new GroupNameIsNotUniqueException();

        $group->changeName($groupName);

        $this->groups->save($group);
    }
}