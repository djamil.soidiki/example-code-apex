<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\ContactManagement\Group\Command\DeleteGroup;
use App\Domain\ContactManagement\Group\Group;
use App\Domain\ContactManagement\Group\Repository\GroupsInterface;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class DeleteGroupHandler
 *
 * @package App\Application\ContactManagement\Group\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class DeleteGroupHandler extends AbstractGroupCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * AddContactHandler constructor.
     *
     * @param GroupsInterface $groups
     * @param Security        $security
     */
    public function __construct(GroupsInterface $groups, Security $security)
    {
        parent::__construct($groups);
        $this->security = $security;
    }

    /**
     * @param DeleteGroup $command
     *
     * @throws Exception
     */
    public function __invoke(DeleteGroup $command)
    {
        $groupId = new GroupId($command->groupId());

        /** @var Group $group */
        $group = $this->groups->get($groupId);

        if ($this->security->getUser()->getId() !== $group->ownerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $group->delete();

        $this->groups->save($group);
    }
}