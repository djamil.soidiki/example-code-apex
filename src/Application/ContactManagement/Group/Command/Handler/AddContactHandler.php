<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\ContactManagement\Group\Command\AddContactToGroup;
use App\Domain\ContactManagement\Contact\Repository\ContactsInterface;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\ContactManagement\Group\Group;
use App\Domain\ContactManagement\Group\Repository\GroupsInterface;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class AddContactHandler
 *
 * @package App\Application\ContactManagement\Group\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AddContactHandler extends AbstractGroupCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * AddContactHandler constructor.
     *
     * @param GroupsInterface $groups
     * @param Security        $security
     */
    public function __construct(GroupsInterface $groups, Security $security)
    {
        parent::__construct($groups);
        $this->security = $security;
    }

    /**
     * @param AddContactToGroup $command
     *
     * @throws Exception
     */
    public function __invoke(AddContactToGroup $command): void
    {
        $groupId   = new GroupId($command->groupId());
        $contactId = new ContactId($command->contactId());

        /** @var Group $group */
        $group = $this->groups->get($groupId);

        if ($this->security->getUser()->getId() !== $group->ownerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $group->addContact($contactId);

        $this->groups->save($group);
    }
}