<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command\Handler;

use App\Application\ContactManagement\Group\Command\CreateGroup;
use App\Domain\ContactManagement\Group\Exception\GroupNameIsNotUniqueException;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\ContactManagement\Group\Group;
use App\Domain\ContactManagement\Group\Repository\GroupsInterface;
use App\Domain\ContactManagement\Group\Specification\GroupNameIsUniqueSpecification;
use App\Domain\ContactManagement\Group\ValueObject\GroupName;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository\Groups;
use Exception;

/**
 * Class CreateGroupHandler
 *
 * @package App\Application\Contact\Group\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateGroupHandler extends AbstractGroupCommandHandler
{
    /**
     * @var Groups
     */
    private $groupsQuery;

    /**
     * CreateGroupHandler constructor.
     *
     * @param GroupsInterface $groups
     * @param Groups          $groupsQuery
     */
    public function __construct(GroupsInterface $groups, Groups $groupsQuery)
    {
        parent::__construct($groups);

        $this->groupsQuery = $groupsQuery;
    }

    /**
     * @param CreateGroup $command
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(CreateGroup $command): string
    {
        $ownerId   = new UserId($command->ownerId());
        $groupName = new GroupName($command->name());

        $groupNameIsUnique = $this->groupsQuery->selectSatisfying(
            new GroupNameIsUniqueSpecification($ownerId, $groupName)
        );

        if (!empty($groupNameIsUnique))
            throw new GroupNameIsNotUniqueException();

        $group = Group::create($ownerId, $groupName);

        $this->groups->save($group);

        return $group->getAggregateRootId();
    }
}