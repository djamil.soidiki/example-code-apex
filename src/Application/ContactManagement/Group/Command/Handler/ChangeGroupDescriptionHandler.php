<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\ContactManagement\Group\Command\ChangeGroupDescription;
use App\Domain\ContactManagement\Group\Group;
use App\Domain\ContactManagement\Group\Repository\GroupsInterface;
use App\Domain\ContactManagement\Group\ValueObject\GroupDescription;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangeGroupDescriptionHandler
 *
 * @package App\Application\ContactManagement\Group\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeGroupDescriptionHandler extends AbstractGroupCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * AddContactHandler constructor.
     *
     * @param GroupsInterface $groups
     * @param Security        $security
     */
    public function __construct(GroupsInterface $groups, Security $security)
    {
        parent::__construct($groups);
        $this->security = $security;
    }

    /**
     * @param ChangeGroupDescription $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeGroupDescription $command)
    {
        $groupId          = new GroupId($command->groupId());
        $groupDescription = new GroupDescription($command->description());

        /** @var Group $group */
        $group = $this->groups->get($groupId);

        if ($this->security->getUser()->getId() !== $group->ownerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $group->changeDescription($groupDescription);

        $this->groups->save($group);
    }
}