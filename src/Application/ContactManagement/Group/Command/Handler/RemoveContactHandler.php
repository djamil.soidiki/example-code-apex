<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\ContactManagement\Group\Command\RemoveContactFromGroup;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Domain\ContactManagement\Group\Group;
use App\Domain\ContactManagement\Group\Repository\GroupsInterface;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class AddContactToListHandler
 *
 * @package App\Application\Contact\Group\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RemoveContactHandler extends AbstractGroupCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * AddContactHandler constructor.
     *
     * @param GroupsInterface $groups
     * @param Security        $security
     */
    public function __construct(GroupsInterface $groups, Security $security)
    {
        parent::__construct($groups);
        $this->security = $security;
    }

    /**
     * @param RemoveContactFromGroup $command
     *
     * @throws Exception
     */
    public function __invoke(RemoveContactFromGroup $command): void
    {
        $groupId   = new GroupId($command->groupId());
        $contactId = new ContactId($command->contactId());

        /** @var Group $group */
        $group = $this->groups->get($groupId);

        if ($this->security->getUser()->getId() !== $group->ownerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $group->removeContact($contactId);

        $this->groups->save($group);
    }
}