<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command;

/**
 * Class DeleteGroup
 *
 * @package App\Application\ContactManagement\Group\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class DeleteGroup
{
    /**
     * @var string
     */
    private $groupId;

    /**
     * DeleteGroup constructor.
     *
     * @param string $groupId
     */
    public function __construct(string $groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }
}