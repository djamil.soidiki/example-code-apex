<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command;

/**
 * Class ChangeGroupDescription
 *
 * @package App\Application\ContactManagement\Group\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeGroupDescription
{
    /**
     * @var string
     */
    private $groupId;

    /**
     * @var string
     */
    private $description;

    /**
     * ChangeGroupDescription constructor.
     *
     * @param string $groupId
     * @param string $description
     */
    public function __construct(string $groupId, string $description)
    {
        $this->groupId     = $groupId;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }
}