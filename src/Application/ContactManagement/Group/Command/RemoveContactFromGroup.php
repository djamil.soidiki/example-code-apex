<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command;

/**
 * Class RemoveContactFromGroup
 *
 * @package App\Application\ContactManagement\Group\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RemoveContactFromGroup
{
    /**
     * @var string
     */
    private $groupId;

    /**
     * @var string
     */
    private $contactId;

    /**
     * RemoveContactFromGroup constructor.
     *
     * @param string $groupId
     * @param string $contactId
     */
    public function __construct(string $groupId, string $contactId)
    {
        $this->groupId   = $groupId;
        $this->contactId = $contactId;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function contactId(): string
    {
        return $this->contactId;
    }
}