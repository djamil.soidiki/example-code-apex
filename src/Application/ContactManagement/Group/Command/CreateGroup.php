<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command;

/**
 * Class CreateGroup
 *
 * @package App\Application\ContactManagement\Group\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateGroup
{
    /**
     * @var string
     */
    private $ownerId;

    /**
     * @var string
     */
    private $name;

    /**
     * CreateGroup constructor.
     *
     * @param string $ownerId
     * @param string $name
     */
    public function __construct(string $ownerId, string $name)
    {
        $this->ownerId = $ownerId;
        $this->name    = $name;
    }

    /**
     * @return string
     */
    public function ownerId(): string
    {
        return $this->ownerId;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}