<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Command;

/**
 * Class ChangeGroupName
 *
 * @package App\Application\ContactManagement\Group\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeGroupName
{
    /**
     * @var string
     */
    private $ownerId;

    /**
     * @var string
     */
    private $groupId;

    /**
     * @var string
     */
    private $name;

    /**
     * ChangeGroupName constructor.
     *
     * @param string $ownerId
     * @param string $groupId
     * @param string $name
     */
    public function __construct(string $ownerId, string $groupId, string $name)
    {
        $this->ownerId = $ownerId;
        $this->groupId = $groupId;
        $this->name    = $name;
    }

    /**
     * @return string
     */
    public function ownerId(): string
    {
        return $this->ownerId;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}