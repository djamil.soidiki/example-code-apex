<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Query;

/**
 * Class GetGroupByGroupId
 *
 * @package App\Application\ContactManagement\Group\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetGroupByGroupId
{
    /**
     * @var string
     */
    private $groupId;

    /**
     * GetGroupByGroupId constructor.
     *
     * @param string $groupId
     */
    public function __construct(string $groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }
}