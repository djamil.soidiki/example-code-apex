<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Query;

/**
 * Class FindGroupsByOwnerId
 *
 * @package App\Application\ContactManagement\Group\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindGroupsByOwnerId
{
    /**
     * @var string
     */
    private $ownerId;

    /**
     * FindGroupsByOwnerId constructor.
     *
     * @param string $ownerId
     */
    public function __construct(string $ownerId)
    {
        $this->ownerId = $ownerId;
    }

    /**
     * @return string
     */
    public function ownerId(): string
    {
        return $this->ownerId;
    }
}