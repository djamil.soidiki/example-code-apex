<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Query\Handler;

use App\Application\ContactManagement\Group\Query\FindGroupsByContactId;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Member;
use Exception;

/**
 * Class FindGroupsByContactIdHandler
 *
 * @package App\Application\ContactManagement\Group\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindGroupsByContactIdHandler extends AbstractGroupQueryHandler
{
    /**
     * @param FindGroupsByContactId $query
     *
     * @return array
     * @throws Exception
     */
    public function __invoke(FindGroupsByContactId $query): array
    {
        $groupIds  = [];
        $contactId = new ContactId($query->contactId());

        $members = $this->members->findMembersByContactId($contactId);

        /** @var Member $member */
        foreach ($members as $member) {
            $groupIds[] = $member->groupId();
        }

        return $this->groups->findGroupsByGroupIds($groupIds);
    }
}