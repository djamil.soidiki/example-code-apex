<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Query\Handler;

use App\Application\ContactManagement\Group\Query\GetGroupByGroupId;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class GetGroupByGroupIdHandler
 *
 * @package App\Application\ContactManagement\Group\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetGroupByGroupIdHandler extends AbstractGroupQueryHandler
{
    /**
     * @param GetGroupByGroupId $query
     *
     * @return Group
     * @throws NotFoundException
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function __invoke(GetGroupByGroupId $query): Group
    {
        $groupId = new GroupId($query->groupId());

        return $this->groups->findGroupByGroupId($groupId);
    }
}