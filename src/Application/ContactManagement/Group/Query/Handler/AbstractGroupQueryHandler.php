<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Query\Handler;

use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository\Groups;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Repository\Members;

/**
 * Class AbstractGroupQueryHandler
 *
 * @package App\Application\ContactManagement\Group\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractGroupQueryHandler
{
    /**
     * @var Groups
     */
    protected $groups;

    /**
     * @var Members
     */
    protected $members;

    /**
     * AbstractGroupQueryHandler constructor.
     *
     * @param Groups  $groups
     * @param Members $members
     */
    public function __construct(Groups $groups, Members $members)
    {
        $this->groups  = $groups;
        $this->members = $members;
    }
}