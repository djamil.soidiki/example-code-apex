<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Query\Handler;

use App\Application\ContactManagement\Group\Query\FindGroupsByOwnerId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use Exception;

/**
 * Class FindGroupsByOwnerIdHandler
 *
 * @package App\Application\ContactManagement\Group\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindGroupsByOwnerIdHandler extends AbstractGroupQueryHandler
{
    /**
     * @param FindGroupsByOwnerId $query
     *
     * @return array
     * @throws Exception
     */
    public function __invoke(FindGroupsByOwnerId $query): array
    {
        $res     = [];
        $ownerId = new UserId($query->ownerId());
        $groups  = $this->groups->findGroupsByOwnerId($ownerId);

        /** @var Group $group */
        foreach ($groups as $group) {

            $totalMembers = $this->members->totalByGroupId($group->groupId());

            $res[$group->groupId()->serialize()] = [
                'data'  => $group,
                'total' => array_shift($totalMembers)['total']
            ];
        }

        return $res;
    }
}