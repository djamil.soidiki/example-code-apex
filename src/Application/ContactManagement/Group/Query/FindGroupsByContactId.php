<?php

declare(strict_types=1);

namespace App\Application\ContactManagement\Group\Query;

/**
 * Class FindGroupsByContactId
 *
 * @package App\Application\ContactManagement\Group\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindGroupsByContactId
{
    /**
     * @var string
     */
    private $contactId;

    /**
     * FindGroupsByContactId constructor.
     *
     * @param string $contactId
     */
    public function __construct(string $contactId)
    {
        $this->contactId = $contactId;
    }

    /**
     * @return string
     */
    public function contactId(): string
    {
        return $this->contactId;
    }
}