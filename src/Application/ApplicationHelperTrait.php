<?php

declare(strict_types=1);

namespace App\Application;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Trait ApplicationHelperTrait
 *
 * @package App\Application
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
trait ApplicationHelperTrait
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;
}