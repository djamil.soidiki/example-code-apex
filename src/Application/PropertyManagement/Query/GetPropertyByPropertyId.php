<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Query;

/**
 * Class GetPropertyByPropertyId
 *
 * @package App\Application\PropertyManagement\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetPropertyByPropertyId
{
    /**
     * @var string
     */
    private $propertyId;

    /**
     * GetPropertyByPropertyId constructor.
     *
     * @param string $propertyId
     */
    public function __construct(string $propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return string
     */
    public function propertyId(): string
    {
        return $this->propertyId;
    }
}