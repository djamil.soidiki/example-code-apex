<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Query\Handler;

use App\Application\PropertyManagement\Query\GetPropertyByPropertyId;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class GetPropertyByPropertyIdHandler
 *
 * @package App\Application\PropertyManagement\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetPropertyByPropertyIdHandler extends AbstractPropertyManagementQueryHandler
{
    /**
     * @param GetPropertyByPropertyId $query
     *
     * @return Property
     * @throws NotFoundException
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function __invoke(GetPropertyByPropertyId $query): Property
    {
        return $this->properties->findPropertyByPropertyId(new PropertyId($query->propertyId()));
    }
}