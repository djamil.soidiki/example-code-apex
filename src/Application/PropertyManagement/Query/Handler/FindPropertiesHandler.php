<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Query\Handler;

use App\Application\PropertyManagement\Query\FindProperties;

/**
 * Class FindPropertiesHandler
 *
 * @package App\Application\PropertyManagement\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindPropertiesHandler extends AbstractPropertyManagementQueryHandler
{
    /**
     * @param FindProperties $query
     *
     * @return array
     */
    public function __invoke(FindProperties $query): array
    {
        return $this->properties->findAll();
    }
}