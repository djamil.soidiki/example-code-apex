<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Query\Handler;

use App\Infrastructure\PropertyManagement\Query\Doctrine\Repository\Properties;

/**
 * Class AbstractPropertyManagementQueryHandler
 *
 * @package App\Application\PropertyManagement\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractPropertyManagementQueryHandler
{
    /**
     * @var Properties
     */
    protected $properties;

    /**
     * AbstractPropertyManagementQueryHandler constructor.
     *
     * @param Properties $properties
     */
    public function __construct(Properties $properties)
    {
        $this->properties = $properties;
    }
}