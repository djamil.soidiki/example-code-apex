<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Query\Handler;

use App\Application\PropertyManagement\Query\FindPropertiesByManagerId;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Exception;

/**
 * Class FindPropertiesByManagerIdHandler
 *
 * @package App\Application\PropertyManagement\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindPropertiesByManagerIdHandler extends AbstractPropertyManagementQueryHandler
{
    /**
     * @param FindPropertiesByManagerId $query
     *
     * @return array
     * @throws Exception
     */
    public function __invoke(FindPropertiesByManagerId $query): array
    {
        return $this->properties->findPropertiesByManagerId(new UserId($query->managerId()));
    }
}