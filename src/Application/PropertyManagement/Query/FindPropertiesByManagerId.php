<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Query;

/**
 * Class FindPropertiesByManagerId
 *
 * @package App\Application\PropertyManagement\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindPropertiesByManagerId
{
    /**
     * @var string
     */
    private $managerId;

    /**
     * FindPropertiesByManagerId constructor.
     *
     * @param string $managerId
     */
    public function __construct(string $managerId)
    {
        $this->managerId = $managerId;
    }

    /**
     * @return string
     */
    public function managerId(): string
    {
        return $this->managerId;
    }
}