<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command;

/**
 * Class ChangePropertyName
 *
 * @package App\Application\PropertyManagement\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangePropertyName
{
    /**
     * @var string
     */
    private $propertyId;

    /**
     * @var string
     */
    private $name;

    /**
     * ChangePropertyName constructor.
     *
     * @param string $propertyId
     * @param string $name
     */
    public function __construct(string $propertyId, string $name)
    {
        $this->propertyId = $propertyId;
        $this->name       = $name;
    }

    /**
     * @return string
     */
    public function propertyId(): string
    {
        return $this->propertyId;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}