<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command;

use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Assert\Assertion;
use Assert\AssertionFailedException;

/**
 * Class CreateProperty
 *
 * @package App\Application\PropertyManagement\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateProperty
{
    /**
     * @var string
     */
    private $managerId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var array|null
     */
    private $address;

    /**
     * CreateProperty constructor.
     *
     * Address variable is an array containing the following data [street_address, city, state, zip_code]
     *
     * @param string      $managerId
     * @param string      $name
     * @param string|null $description
     * @param array|null  $address
     *
     * @throws AssertionFailedException
     */
    public function __construct(
        string $managerId,
        string $name,
        ?string $description = null,
        ?array $address = null
    )
    {
        if ($address !== null) {
            Assertion::keyExists($address, 'street_address');
            Assertion::keyExists($address, 'city');
            Assertion::keyExists($address, 'state');
            Assertion::keyExists($address, 'zip_code');
        }

        $this->managerId   = $managerId;
        $this->name        = $name;
        $this->description = $description;
        $this->address     = $address;
    }

    /**
     * @return string
     */
    public function managerId(): string
    {
        return $this->managerId;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * @return array|null
     */
    public function address(): ?array
    {
        return $this->address;
    }
}