<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command;

/**
 * Class ChangePropertyDescription
 *
 * @package App\Application\PropertyManagement\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangePropertyDescription
{
    /**
     * @var string
     */
    private $propertyId;

    /**
     * @var string
     */
    private $description;

    /**
     * ChangePropertyDescription constructor.
     *
     * @param string $propertyId
     * @param string $description
     */
    public function __construct(string $propertyId, string $description)
    {
        $this->propertyId  = $propertyId;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function propertyId(): string
    {
        return $this->propertyId;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }
}