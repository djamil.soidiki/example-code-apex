<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command;

/**
 * Class ArchiveProperty
 *
 * @package App\Application\PropertyManagement\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ArchiveProperty
{
    /**
     * @var string
     */
    private $propertyId;

    /**
     * ArchiveProperty constructor.
     *
     * @param string $propertyId
     */
    public function __construct(string $propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return string
     */
    public function propertyId(): string
    {
        return $this->propertyId;
    }
}