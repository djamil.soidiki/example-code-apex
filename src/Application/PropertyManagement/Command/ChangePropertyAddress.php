<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command;

use Assert\Assertion;
use Assert\AssertionFailedException;

/**
 * Class ChangePropertyAddress
 *
 * @package App\Application\PropertyManagement\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangePropertyAddress
{
    /**
     * @var string
     */
    private $propertyId;

    /**
     * @var string
     */
    private $streetAddress;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $zipCode;

    /**
     * ChangePropertyAddress constructor.
     *
     * @param string $propertyId
     * @param string $streetAddress
     * @param string $city
     * @param string $state
     * @param string $zipCode
     */
    public function __construct(string $propertyId, string $streetAddress, string $city, string $state, string $zipCode)
    {
        $this->propertyId    = $propertyId;
        $this->streetAddress = $streetAddress;
        $this->city          = $city;
        $this->state         = $state;
        $this->zipCode       = $zipCode;
    }

    /**
     * @return string
     */
    public function propertyId(): string
    {
        return $this->propertyId;
    }

    /**
     * @return string
     */
    public function streetAddress(): string
    {
        return $this->streetAddress;
    }

    /**
     * @return string
     */
    public function city(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function state(): string
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function zipCode(): string
    {
        return $this->zipCode;
    }
}