<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command;

use Assert\Assertion;
use Assert\AssertionFailedException;

/**
 * Class ChangePropertyDetails
 *
 * @package App\Application\PropertyManagement\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangePropertyDetails
{
    /**
     * @var string
     */
    private $propertyId;

    /**
     * @var int|null
     */
    private $bedrooms;

    /**
     * @var int|null
     */
    private $fullBathrooms;

    /**
     * @var int|null
     */
    private $partialBathrooms;

    /**
     * @var int|null
     */
    private $squareFootage;

    /**
     * @var int|null
     */
    private $yearBuilt;

    /**
     * @var float|null
     */
    private $lotSize;

    /**
     * ChangePropertyDetails constructor.
     *
     * @param string     $propertyId
     * @param int|null   $bedrooms
     * @param int|null   $fullBathrooms
     * @param int|null   $partialBathrooms
     * @param int|null   $squareFootage
     * @param int|null   $yearBuilt
     * @param float|null $lotSize
     */
    public function __construct(
        string $propertyId,
        ?int $bedrooms,
        ?int $fullBathrooms,
        ?int $partialBathrooms,
        ?int $squareFootage,
        ?int $yearBuilt,
        ?float $lotSize
    )
    {
        $this->propertyId       = $propertyId;
        $this->bedrooms         = $bedrooms;
        $this->fullBathrooms    = $fullBathrooms;
        $this->partialBathrooms = $partialBathrooms;
        $this->squareFootage    = $squareFootage;
        $this->yearBuilt        = $yearBuilt;
        $this->lotSize          = $lotSize;
    }

    /**
     * @return string
     */
    public function propertyId(): string
    {
        return $this->propertyId;
    }

    /**
     * @return int|null
     */
    public function bedrooms(): ?int
    {
        return $this->bedrooms;
    }

    /**
     * @return int|null
     */
    public function fullBathrooms(): ?int
    {
        return $this->fullBathrooms;
    }

    /**
     * @return int|null
     */
    public function partialBathrooms(): ?int
    {
        return $this->partialBathrooms;
    }

    /**
     * @return int|null
     */
    public function squareFootage(): ?int
    {
        return $this->squareFootage;
    }

    /**
     * @return int|null
     */
    public function yearBuilt(): ?int
    {
        return $this->yearBuilt;
    }

    /**
     * @return float|null
     */
    public function lotSize(): ?float
    {
        return $this->lotSize;
    }
}