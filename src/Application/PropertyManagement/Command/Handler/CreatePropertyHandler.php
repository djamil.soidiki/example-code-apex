<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command\Handler;

use App\Application\PropertyManagement\Command\CreateProperty;
use App\Domain\PropertyManagement\Property;
use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\Domain\PropertyManagement\ValueObject\PropertyDescription;
use App\Domain\PropertyManagement\ValueObject\PropertyName;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use Assert\AssertionFailedException;
use Exception;

/**
 * Class CreatePropertyHandler
 *
 * @package App\Application\PropertyManagement\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreatePropertyHandler extends AbstractPropertyManagementCommandHandler
{
    /**
     * @param CreateProperty $command
     *
     * @return string
     * @throws Exception
     * @throws AssertionFailedException
     */
    public function __invoke(CreateProperty $command): string
    {
        $managerId           = new UserId($command->managerId());
        $propertyName        = new PropertyName($command->name());
        $propertyDescription = $command->description() !== null ? new PropertyDescription($command->description()) : null;
        $propertyAddress     = $command->address() !== null ? PropertyAddress::fromArray($command->address()) : null;

        if ($propertyDescription !== null || $propertyAddress !== null)
            $property = Property::create($managerId, $propertyName, $propertyDescription, $propertyAddress);
        else
            $property = Property::create($managerId, $propertyName);

        $this->properties->save($property);

        return $property->getAggregateRootId();
    }
}