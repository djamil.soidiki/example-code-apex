<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\PropertyManagement\Command\ChangePropertyDetails;
use App\Domain\PropertyManagement\Property;
use App\Domain\PropertyManagement\Repository\PropertiesInterface;
use App\Domain\PropertyManagement\ValueObject\PropertyDetails;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangePropertyDetailsHandler
 *
 * @package App\Application\PropertyManagement\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangePropertyDetailsHandler extends AbstractPropertyManagementCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ArchivePropertyHandler constructor.
     *
     * @param PropertiesInterface $properties
     * @param Security            $security
     */
    public function __construct(PropertiesInterface $properties, Security $security)
    {
        parent::__construct($properties);
        $this->security = $security;
    }

    /**
     * @param ChangePropertyDetails $command
     *
     * @throws Exception
     */
    public function __invoke(ChangePropertyDetails $command): void
    {
        $propertyId = new PropertyId($command->propertyId());
        $details    = new PropertyDetails(
            $command->bedrooms(),
            $command->fullBathrooms(),
            $command->partialBathrooms(),
            $command->squareFootage(),
            $command->yearBuilt(),
            $command->lotSize()
        );

        /** @var Property $property */
        $property = $this->properties->get($propertyId);

        if ($this->security->getUser()->getId() !== $property->managerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $property->changeDetails($details);

        $this->properties->save($property);
    }
}