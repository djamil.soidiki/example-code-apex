<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\PropertyManagement\Command\ChangePropertyAddress;
use App\Domain\PropertyManagement\Property;
use App\Domain\PropertyManagement\Repository\PropertiesInterface;
use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangePropertyAddressHandler
 *
 * @package App\Application\PropertyManagement\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangePropertyAddressHandler extends AbstractPropertyManagementCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ArchivePropertyHandler constructor.
     *
     * @param PropertiesInterface $properties
     * @param Security            $security
     */
    public function __construct(PropertiesInterface $properties, Security $security)
    {
        parent::__construct($properties);
        $this->security = $security;
    }

    /**
     * @param ChangePropertyAddress $command
     *
     * @throws Exception
     */
    public function __invoke(ChangePropertyAddress $command): void
    {
        $propertyId = new PropertyId($command->propertyId());
        $address    = new PropertyAddress(
            $command->streetAddress(),
            $command->city(),
            $command->state(),
            $command->zipCode()
        );

        /** @var Property $property */
        $property = $this->properties->get($propertyId);

        if ($this->security->getUser()->getId() !== $property->managerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $property->changeAddress($address);

        $this->properties->save($property);
    }
}