<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command\Handler;

use App\Domain\PropertyManagement\Repository\PropertiesInterface;

/**
 * Class AbstractContactCommandHandler
 *
 * @package App\Application\PropertyManagement\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractPropertyManagementCommandHandler
{
    /**
     * @var PropertiesInterface
     */
    protected $properties;

    /**
     * AbstractPropertyManagementCommandHandler constructor.
     *
     * @param PropertiesInterface $properties
     */
    public function __construct(PropertiesInterface $properties)
    {
        $this->properties = $properties;
    }
}