<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command\Handler;

use App\Application\PropertyManagement\Command\RestoreProperty;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Exception;

/**
 * Class RestorePropertyHandler
 *
 * @package App\Application\PropertyManagement\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RestorePropertyHandler extends AbstractPropertyManagementCommandHandler
{
    /**
     * @param RestoreProperty $command
     *
     * @throws Exception
     */
    public function __invoke(RestoreProperty $command): void
    {
        $property = $this->properties->get(new PropertyId($command->propertyId()));

        if ($this->security->getUser()->getId() !== $property->managerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $property->restore();

        $this->properties->save($property);
    }
}