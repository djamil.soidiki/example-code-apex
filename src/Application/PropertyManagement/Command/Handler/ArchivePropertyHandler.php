<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\PropertyManagement\Command\ArchiveProperty;
use App\Domain\PropertyManagement\Repository\PropertiesInterface;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ArchivePropertyHandler
 *
 * @package App\Application\PropertyManagement\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ArchivePropertyHandler extends AbstractPropertyManagementCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ArchivePropertyHandler constructor.
     *
     * @param PropertiesInterface $properties
     * @param Security            $security
     */
    public function __construct(PropertiesInterface $properties, Security $security)
    {
        parent::__construct($properties);
        $this->security = $security;
    }

    /**
     * @param ArchiveProperty $command
     *
     * @throws Exception
     */
    public function __invoke(ArchiveProperty $command): void
    {
        $property = $this->properties->get(new PropertyId($command->propertyId()));

        if ($this->security->getUser()->getId() !== $property->managerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $property->archive();

        $this->properties->save($property);
    }
}