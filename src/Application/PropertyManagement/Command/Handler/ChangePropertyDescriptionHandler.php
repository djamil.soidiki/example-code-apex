<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\PropertyManagement\Command\ChangePropertyDescription;
use App\Domain\PropertyManagement\Repository\PropertiesInterface;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\PropertyManagement\ValueObject\PropertyDescription;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangePropertyDescriptionHandler
 *
 * @package App\Application\PropertyManagement\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangePropertyDescriptionHandler extends AbstractPropertyManagementCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ArchivePropertyHandler constructor.
     *
     * @param PropertiesInterface $properties
     * @param Security            $security
     */
    public function __construct(PropertiesInterface $properties, Security $security)
    {
        parent::__construct($properties);
        $this->security = $security;
    }

    /**
     * @param ChangePropertyDescription $command
     *
     * @throws Exception
     */
    public function __invoke(ChangePropertyDescription $command): void
    {
        $property = $this->properties->get(new PropertyId($command->propertyId()));

        if ($this->security->getUser()->getId() !== $property->managerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $property->changeDescription(new PropertyDescription($command->description()));

        $this->properties->save($property);
    }
}