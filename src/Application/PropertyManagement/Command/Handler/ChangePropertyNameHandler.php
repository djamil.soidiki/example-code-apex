<?php

declare(strict_types=1);

namespace App\Application\PropertyManagement\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\PropertyManagement\Command\ChangePropertyName;
use App\Domain\PropertyManagement\Repository\PropertiesInterface;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Domain\PropertyManagement\ValueObject\PropertyName;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangePropertyNameHandler
 *
 * @package App\Application\PropertyManagement\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangePropertyNameHandler extends AbstractPropertyManagementCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ArchivePropertyHandler constructor.
     *
     * @param PropertiesInterface $properties
     * @param Security            $security
     */
    public function __construct(PropertiesInterface $properties, Security $security)
    {
        parent::__construct($properties);
        $this->security = $security;
    }

    /**
     * @param ChangePropertyName $command
     *
     * @throws Exception
     */
    public function __invoke(ChangePropertyName $command): void
    {
        $property = $this->properties->get(new PropertyId($command->propertyId()));

        if ($this->security->getUser()->getId() !== $property->managerId()->serialize())
            throw new Exception('Not allowed to modify this resource');

        $property->changeName(new PropertyName($command->name()));

        $this->properties->save($property);
    }
}