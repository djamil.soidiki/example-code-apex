<?php

declare(strict_types=1);

namespace App\Application\TrialPeriod\Query\Handler;

use App\Application\TrialPeriod\Query\GetTrialPeriodByCustomerId;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Model\TrialPeriod;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class GetTrialPeriodByCustomerIdHandler
 *
 * @package App\Application\TrialPeriod\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetTrialPeriodByCustomerIdHandler extends AbstractTrialPeriodQueryHandler
{
    /**
     * @param GetTrialPeriodByCustomerId $query
     *
     * @return TrialPeriod
     * @throws NonUniqueResultException
     * @throws NotFoundException
     * @throws Exception
     */
    public function __invoke(GetTrialPeriodByCustomerId $query): TrialPeriod
    {
        return $this->trialPeriods->findTrialPeriodByCustomerId(new CustomerId($query->customerId()));
    }
}