<?php

declare(strict_types=1);

namespace App\Application\TrialPeriod\Query\Handler;

use App\Application\TrialPeriod\Query\IsCustomerOnTrialPeriod;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\TrialPeriod\Specification\TrialPeriodIsActiveSpecification;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class IsCustomerOnTrialPeriodHandler
 *
 * @package App\Application\TrialPeriod\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class IsCustomerOnTrialPeriodHandler extends AbstractTrialPeriodQueryHandler
{
    /**
     * @param IsCustomerOnTrialPeriod $query
     *
     * @return bool
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function __invoke(IsCustomerOnTrialPeriod $query): bool
    {
        $hasTrialPeriod = $this->trialPeriods->hasTrialPeriod(new CustomerId($query->customerId()));

        if ($hasTrialPeriod === false)
            return false;

        $trialPeriod         = $this->trialPeriods->findTrialPeriodByCustomerId(new CustomerId($query->customerId()));
        $trialPeriodIsActive = new TrialPeriodIsActiveSpecification();

        return $trialPeriodIsActive->isSatisfiedBy($trialPeriod);
    }
}