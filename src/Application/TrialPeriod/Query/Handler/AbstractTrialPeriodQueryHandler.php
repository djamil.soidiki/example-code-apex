<?php

declare(strict_types=1);

namespace App\Application\TrialPeriod\Query\Handler;

use App\Infrastructure\SharedKernel\TrialPeriod\Query\Doctrine\Repository\TrialPeriods;

/**
 * Class AbstractTrialPeriodQueryHandler
 *
 * @package App\Application\TrialPeriod\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractTrialPeriodQueryHandler
{
    /**
     * @var TrialPeriods
     */
    protected $trialPeriods;

    /**
     * AbstractTrialPeriodQueryHandler constructor.
     *
     * @param TrialPeriods $trialPeriods
     */
    public function __construct(TrialPeriods $trialPeriods)
    {
        $this->trialPeriods = $trialPeriods;
    }
}