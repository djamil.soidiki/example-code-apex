<?php

declare(strict_types=1);

namespace App\Application\TrialPeriod\Query\Handler;

use App\Application\TrialPeriod\Query\HasTrialPeriod;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class HasTrialPeriodHandler
 *
 * @package App\Application\TrialPeriod\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class HasTrialPeriodHandler extends AbstractTrialPeriodQueryHandler
{
    /**
     * @param HasTrialPeriod $query
     *
     * @return bool
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function __invoke(HasTrialPeriod $query): bool
    {
        return $this->trialPeriods->hasTrialPeriod(new CustomerId($query->customerId()));
    }
}