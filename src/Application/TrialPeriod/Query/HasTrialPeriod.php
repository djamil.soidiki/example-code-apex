<?php

declare(strict_types=1);

namespace App\Application\TrialPeriod\Query;

/**
 * Class HasTrialPeriod
 *
 * @package App\Application\TrialPeriod\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class HasTrialPeriod
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * HasTrialPeriod constructor.
     *
     * @param string $customerId
     */
    public function __construct(string $customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function customerId(): string
    {
        return $this->customerId;
    }
}