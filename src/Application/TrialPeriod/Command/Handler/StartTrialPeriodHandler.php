<?php

declare(strict_types=1);

namespace App\Application\TrialPeriod\Command\Handler;

use App\Application\TrialPeriod\Command\StartTrialPeriod;
use App\Domain\SharedKernel\Billing\Customer\ValueObject\CustomerId;
use App\Domain\SharedKernel\TrialPeriod\Service\TrialPeriodDateGenerator;
use App\Domain\SharedKernel\TrialPeriod\TrialPeriod;
use Exception;

/**
 * Class StartTrialPeriodHandler
 *
 * @package App\Application\TrialPeriod\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class StartTrialPeriodHandler extends AbstractTrialPeriodCommandHandler
{
    /**
     * @param StartTrialPeriod $command
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(StartTrialPeriod $command): string
    {
        $customerId = new CustomerId($command->customerId());

        if ($command->startDate() !== null || $command->endDate()) {

            // todo: check if user has right like if he is an admin

            $startDate = new \DateTimeImmutable($command->startDate());
            $endDate   = new \DateTimeImmutable($command->endDate());
        } else {
            list($startDate, $endDate) = TrialPeriodDateGenerator::generate();
        }

        $trialPeriod = TrialPeriod::start($customerId, $startDate, $endDate);

        $this->trialPeriods->save($trialPeriod);

        return $trialPeriod->getAggregateRootId();
    }
}