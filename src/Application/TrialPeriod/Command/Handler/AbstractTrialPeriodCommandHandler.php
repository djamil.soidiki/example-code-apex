<?php

declare(strict_types=1);

namespace App\Application\TrialPeriod\Command\Handler;

use App\Infrastructure\SharedKernel\TrialPeriod\Command\TrialPeriods;

/**
 * Class AbstractTrialPeriodCommandHandler
 *
 * @package App\Application\TrialPeriod\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractTrialPeriodCommandHandler
{
    /**
     * @var TrialPeriods
     */
    protected $trialPeriods;

    /**
     * AbstractTrialPeriodCommandHandler constructor.
     *
     * @param TrialPeriods $trialPeriods
     */
    public function __construct(TrialPeriods $trialPeriods)
    {
        $this->trialPeriods = $trialPeriods;
    }
}