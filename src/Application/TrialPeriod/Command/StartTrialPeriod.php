<?php

declare(strict_types=1);

namespace App\Application\TrialPeriod\Command;

/**
 * Class StartTrialPeriod
 *
 * @package App\Application\TrialPeriod\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class StartTrialPeriod
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * @var string|null
     */
    private $startDate;

    /**
     * @var string|null
     */
    private $endDate;

    /**
     * StartTrialPeriod constructor.
     *
     * @param string      $customerId
     * @param string|null $startDate
     * @param string|null $endDate
     */
    public function __construct(string $customerId, ?string $startDate = null, ?string $endDate = null)
    {
        $this->customerId = $customerId;
        $this->startDate  = $startDate;
        $this->endDate    = $endDate;
    }

    /**
     * @return string
     */
    public function customerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string|null
     */
    public function startDate(): ?string
    {
        return $this->startDate;
    }

    /**
     * @return string|null
     */
    public function endDate(): ?string
    {
        return $this->endDate;
    }
}