<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command;

/**
 * Class RegisterUser
 *
 * @package App\Application\Authentication\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RegisterUser
{
    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var string
     */
    private $password;

    /**
     * RegisterUser constructor.
     *
     * @param string     $emailAddress
     * @param string     $password
     */
    public function __construct(string $emailAddress, string $password)
    {
        $this->emailAddress = $emailAddress;
        $this->password     = $password;
    }

    /**
     * @return string
     */
    public function emailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->password;
    }
}