<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command\Handler;

use App\Application\Authentication\Command\ResetPassword;
use App\Domain\SharedKernel\Authentication\Service\Security\Password\PasswordHasher;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authentication\ValueObject\Password;
use Exception;

/**
 * Class ResetPasswordHandler
 *
 * @package App\Application\Authentication\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ResetPasswordHandler extends AbstractAuthenticationCommandHandler
{
    /**
     * @param ResetPassword $command
     *
     * @throws Exception
     */
    public function __invoke(ResetPassword $command): void
    {
        $userId         = new UserId($command->userId());
        $password       = new Password($command->password());
        $hashedPassword = PasswordHasher::hash($password);
        $user           = $this->users->get($userId);

        $user->resetPassword($hashedPassword);

        $this->users->save($user);
    }
}