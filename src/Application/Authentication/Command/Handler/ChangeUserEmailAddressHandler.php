<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\Authentication\Command\ChangeUserEmailAddress;
use App\Domain\SharedKernel\Authentication\Repository\UsersInterface;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangeUserEmailAddressHandler
 *
 * @package App\Application\Authentication\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeUserEmailAddressHandler extends AbstractAuthenticationCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ChangeUserEmailAddressHandler constructor.
     *
     * @param UsersInterface $users
     * @param Security       $security
     */
    public function __construct(UsersInterface $users, Security $security)
    {
        parent::__construct($users);
        $this->security = $security;
    }

    /**
     * @param ChangeUserEmailAddress $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeUserEmailAddress $command): void
    {
        if ($this->security->getUser()->getId() !== $command->userId())
            throw new Exception('Not allowed to modify this resource');

        $user = $this->users->get(new UserId($command->userId()));

        $user->changeEmailAddress(new EmailAddress($command->emailAddress()));

        $this->users->save($user);
    }
}