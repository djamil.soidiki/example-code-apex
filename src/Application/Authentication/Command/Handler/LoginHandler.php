<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command\Handler;

use App\Application\Authentication\Command\Login;
use App\Domain\SharedKernel\Authentication\Exception\EmailAddressNotFoundException;
use App\Domain\SharedKernel\Authentication\Exception\InvalidCredentialsException;
use App\Domain\SharedKernel\Authentication\Repository\Query\EmailAddressQueryInterface;
use App\Domain\SharedKernel\Authentication\Repository\UsersInterface;
use App\Domain\SharedKernel\Authentication\Service\Security\Password\VerifyPassword;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authentication\ValueObject\Password;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;

/**
 * Class LoginHandler
 *
 * @package App\Application\Authentication\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class LoginHandler extends AbstractAuthenticationCommandHandler
{
    /**
     * @var EmailAddressQueryInterface
     */
    private $emailAddressQuery;

    /**
     * LoginHandler constructor.
     *
     * @param UsersInterface             $users
     * @param EmailAddressQueryInterface $emailAddressQuery
     */
    public function __construct(UsersInterface $users, EmailAddressQueryInterface $emailAddressQuery)
    {
        parent::__construct($users);
        $this->emailAddressQuery = $emailAddressQuery;
    }

    /**
     * @param Login $command
     */
    public function __invoke(Login $command)
    {
        $emailAddress = new EmailAddress($command->emailAddress());
        $password     = new Password($command->password());

        $user = $this->users->get($this->getUserIdByEmailAddress($emailAddress));

        if (!VerifyPassword::verify($password, $user->password()))
            throw new InvalidCredentialsException('Password is incorrect');
    }

    /**
     * @param EmailAddress $emailAddress
     *
     * @return UserId
     */
    private function getUserIdByEmailAddress(EmailAddress $emailAddress): UserId
    {
        $userId = $this->emailAddressQuery->existsEmailAddress($emailAddress);

        if ($userId === null)
            throw new EmailAddressNotFoundException($emailAddress->serialize());

        return $userId;
    }
}