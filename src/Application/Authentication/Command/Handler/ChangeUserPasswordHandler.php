<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\Authentication\Command\ChangeUserPassword;
use App\Domain\SharedKernel\Authentication\Repository\UsersInterface;
use App\Domain\SharedKernel\Authentication\Service\Security\Password\PasswordHasher;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authentication\ValueObject\Password;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChangeUserPasswordHandler
 *
 * @package App\Application\Authentication\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeUserPasswordHandler extends AbstractAuthenticationCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * ChangeUserPasswordHandler constructor.
     *
     * @param UsersInterface $users
     * @param Security       $security
     */
    public function __construct(UsersInterface $users, Security $security)
    {
        parent::__construct($users);
        $this->security = $security;
    }

    /**
     * @param ChangeUserPassword $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeUserPassword $command): void
    {
        if ($this->security->getUser()->getId() !== $command->userId())
            throw new Exception('Not allowed to modify this resource');

        $userId            = new UserId($command->userId());
        $currentPassword   = new Password($command->currentPassword());
        $newHashedPassword = PasswordHasher::hash(new Password($command->newPassword()));

        $user = $this->users->get($userId);

        $user->changePassword($currentPassword, $newHashedPassword);

        $this->users->save($user);
    }
}