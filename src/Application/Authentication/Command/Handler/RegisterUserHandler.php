<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command\Handler;

use App\Application\Authentication\Command\RegisterUser;
use App\Domain\SharedKernel\Authentication\Exception\EmailAddressIsNotUniqueException;
use App\Domain\SharedKernel\Authentication\Service\Security\Password\PasswordHasher;
use App\Domain\SharedKernel\Authentication\Specification\EmailAddressIsUniqueSpecification;
use App\Domain\SharedKernel\Authentication\User;
use App\Domain\SharedKernel\Authentication\Repository\UsersInterface;
use App\Domain\SharedKernel\Authentication\ValueObject\Password;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Exception;

/**
 * Class RegisterUserHandler
 *
 * @package App\Application\Authentication\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RegisterUserHandler extends AbstractAuthenticationCommandHandler
{
    /**
     * @var EmailAddressIsUniqueSpecification
     */
    private $emailAddressIsUnique;

    /**
     * RegisterUserHandler constructor.
     *
     * @param UsersInterface                    $users
     * @param EmailAddressIsUniqueSpecification $emailAddressIsUnique
     */
    public function __construct(UsersInterface $users, EmailAddressIsUniqueSpecification $emailAddressIsUnique)
    {
        parent::__construct($users);
        $this->emailAddressIsUnique = $emailAddressIsUnique;
    }

    /**
     * @param RegisterUser $command
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(RegisterUser $command): string
    {
        $emailAddress = new EmailAddress($command->emailAddress());

        if (false === $this->emailAddressIsUnique->isSatisfiedBy($emailAddress))
            throw new EmailAddressIsNotUniqueException();

        $password     = new Password($command->password());
        $passwordHash = PasswordHasher::hash($password);

        $user = User::register($emailAddress, $passwordHash);

        $this->users->save($user);

        return $user->getAggregateRootId();
    }
}