<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command\Handler;

use App\Application\Authentication\Command\RequestPasswordReminder;
use App\Domain\SharedKernel\Authentication\Exception\EmailAddressNotFoundException;
use App\Domain\SharedKernel\Authentication\Repository\Query\EmailAddressQueryInterface;
use App\Domain\SharedKernel\Authentication\Repository\UsersInterface;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Exception;

/**
 * Class RequestPasswordReminderHandler
 *
 * @package App\Application\Authentication\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RequestPasswordReminderHandler extends AbstractAuthenticationCommandHandler
{
    /**
     * @var EmailAddressQueryInterface
     */
    private $emailAddressQuery;

    /**
     * RequestPasswordReminderHandler constructor.
     *
     * @param UsersInterface             $users
     * @param EmailAddressQueryInterface $emailAddressQuery
     */
    public function __construct(UsersInterface $users, EmailAddressQueryInterface $emailAddressQuery)
    {
        parent::__construct($users);
        $this->emailAddressQuery = $emailAddressQuery;
    }

    /**
     * @param RequestPasswordReminder $command
     *
     * @throws Exception
     */
    public function __invoke(RequestPasswordReminder $command): void
    {
        $emailAddress = new EmailAddress($command->emailAddress());
        $user         = $this->users->get($this->getUserIdByEmailAddress($emailAddress));

        $user->requestPasswordReminder();

        $this->users->save($user);
    }

    /**
     * @param EmailAddress $emailAddress
     *
     * @return UserId
     */
    private function getUserIdByEmailAddress(EmailAddress $emailAddress): UserId
    {
        $userId = $this->emailAddressQuery->existsEmailAddress($emailAddress);

        if ($userId === null)
            throw new EmailAddressNotFoundException($emailAddress->serialize());

        return $userId;
    }
}