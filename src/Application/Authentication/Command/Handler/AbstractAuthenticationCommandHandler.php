<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command\Handler;

use App\Domain\SharedKernel\Authentication\Repository\UsersInterface;

/**
 * Class AbstractAuthenticationCommandHandler
 *
 * @package App\Application\Authentication\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractAuthenticationCommandHandler
{
    /**
     * @var UsersInterface
     */
    protected $users;

    /**
     * AbstractAuthenticationCommandHandler constructor.
     *
     * @param UsersInterface $users
     */
    public function __construct(UsersInterface $users)
    {
        $this->users = $users;
    }
}