<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command;

/**
 * Class ChangeUserPassword
 *
 * @package App\Application\Authentication\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeUserPassword
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $currentPassword;

    /**
     * @var string
     */
    private $newPassword;

    /**
     * ChangeUserPassword constructor.
     *
     * @param string $userId
     * @param string $currentPassword
     * @param string $newPassword
     */
    public function __construct(string $userId, string $currentPassword, string $newPassword)
    {
        $this->userId          = $userId;
        $this->currentPassword = $currentPassword;
        $this->newPassword     = $newPassword;
    }

    /**
     * @return string
     */
    public function userId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function currentPassword(): string
    {
        return $this->currentPassword;
    }

    /**
     * @return string
     */
    public function newPassword(): string
    {
        return $this->newPassword;
    }
}