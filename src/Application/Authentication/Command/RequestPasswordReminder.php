<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command;

/**
 * Class RequestPasswordReminder
 *
 * @package App\Application\Authentication\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class RequestPasswordReminder
{
    /**
     * @var string
     */
    private $emailAddress;

    /**
     * RequestPasswordReminder constructor.
     *
     * @param string $emailAddress
     */
    public function __construct(string $emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function emailAddress(): string
    {
        return $this->emailAddress;
    }
}