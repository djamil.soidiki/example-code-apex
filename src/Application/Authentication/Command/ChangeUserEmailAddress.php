<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command;

/**
 * Class ChangeContactEmailAddressCommand
 *
 * @package App\Application\Authentication\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeUserEmailAddress
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * ChangeContactEmailAddressCommand constructor.
     *
     * @param string $userId
     * @param string $emailAddress
     */
    public function __construct(string $userId, string $emailAddress)
    {
        $this->userId       = $userId;
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function userId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function emailAddress(): string
    {
        return $this->emailAddress;
    }
}