<?php

declare(strict_types=1);

namespace App\Application\Authentication\Command;

/**
 * Class ResetPassword
 *
 * @package App\Application\Authentication\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ResetPassword
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $password;

    /**
     * ResetPassword constructor.
     *
     * @param string $userId
     * @param string $password
     */
    public function __construct(string $userId, string $password)
    {
        $this->userId   = $userId;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function userId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->password;
    }
}