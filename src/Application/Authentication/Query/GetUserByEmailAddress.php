<?php

declare(strict_types=1);

namespace App\Application\Authentication\Query;

/**
 * Class GetUserByEmailAddress
 *
 * @package App\Application\Authentication\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetUserByEmailAddress
{
    /**
     * @var string
     */
    private $emailAddress;

    /**
     * GetUserByEmailAddress constructor.
     *
     * @param string $emailAddress
     */
    public function __construct(string $emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function emailAddress(): string
    {
        return $this->emailAddress;
    }
}