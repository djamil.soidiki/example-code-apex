<?php

declare(strict_types=1);

namespace App\Application\Authentication\Query\Handler;

use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Repository\PasswordReminders;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Repository\Users;

/**
 * Class AbstractAuthenticationQueryHandler
 *
 * @package App\Application\Authentication\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractAuthenticationQueryHandler
{
    /**
     * @var Users
     */
    protected $users;

    /**
     * @var PasswordReminders
     */
    protected $passwordReminders;

    /**
     * AbstractAuthenticationQueryHandler constructor.
     *
     * @param Users             $users
     * @param PasswordReminders $passwordReminders
     */
    public function __construct(Users $users, PasswordReminders $passwordReminders)
    {
        $this->users             = $users;
        $this->passwordReminders = $passwordReminders;
    }
}