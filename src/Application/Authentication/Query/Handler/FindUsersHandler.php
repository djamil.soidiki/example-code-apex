<?php

declare(strict_types=1);

namespace App\Application\Authentication\Query\Handler;

use App\Application\Authentication\Query\FindUsers;

/**
 * Class FindUsersHandler
 *
 * @package App\Application\Authentication\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindUsersHandler extends AbstractAuthenticationQueryHandler
{
    /**
     * @param FindUsers $query
     *
     * @return array
     */
    public function __invoke(FindUsers $query): array
    {
        return $this->users->findAll();
    }
}