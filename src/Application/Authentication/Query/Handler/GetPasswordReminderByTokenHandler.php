<?php

declare(strict_types=1);

namespace App\Application\Authentication\Query\Handler;

use App\Application\Authentication\Query\GetPasswordReminderByToken;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Domain\SharedKernel\Generic\ValueObject\Token\Token;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\PasswordReminder;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class GetPasswordReminderByTokenHandler
 *
 * @package App\Application\Authentication\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetPasswordReminderByTokenHandler extends AbstractAuthenticationQueryHandler
{
    /**
     * @param GetPasswordReminderByToken $query
     *
     * @return PasswordReminder
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function __invoke(GetPasswordReminderByToken $query): PasswordReminder
    {
        $token = new Token($query->reminderToken());

        return $this->passwordReminders->findPasswordReminderByToken($token);
    }
}