<?php

declare(strict_types=1);

namespace App\Application\Authentication\Query\Handler;

use App\Application\Authentication\Query\GetUserByEmailAddress;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\User;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class GetUserByEmailAddressHandler
 *
 * @package App\Application\Authentication\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetUserByEmailAddressHandler extends AbstractAuthenticationQueryHandler
{
    /**
     * @param GetUserByEmailAddress $query
     *
     * @return User
     * @throws NonUniqueResultException
     * @throws NotFoundException
     */
    public function __invoke(GetUserByEmailAddress $query): User
    {
        $emailAddress = new EmailAddress($query->emailAddress());

        return $this->users->findUserByEmailAddress($emailAddress);
    }
}