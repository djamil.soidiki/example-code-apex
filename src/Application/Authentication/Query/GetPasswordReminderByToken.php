<?php

declare(strict_types=1);

namespace App\Application\Authentication\Query;

/**
 * Class GetPasswordReminderByToken
 *
 * @package App\Application\Authentication\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetPasswordReminderByToken
{
    /**
     * @var string
     */
    private $reminderToken;

    /**
     * GetPasswordReminderByToken constructor.
     *
     * @param string $reminderToken
     */
    public function __construct(string $reminderToken)
    {
        $this->reminderToken = $reminderToken;
    }

    /**
     * @return string
     */
    public function reminderToken(): string
    {
        return $this->reminderToken;
    }
}