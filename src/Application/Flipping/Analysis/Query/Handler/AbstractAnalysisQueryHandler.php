<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Query\Handler;

use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Repository\Analysis;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Repository\Worksheets;

/**
 * Class AbstractAnalysisQueryHandler
 *
 * @package App\Application\Flipping\Analysis\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractAnalysisQueryHandler
{
    /**
     * @var Analysis
     */
    protected $analysisRepository;

    /**
     * @var Worksheets
     */
    protected $worksheets;

    /**
     * AbstractAnalysisQueryHandler constructor.
     *
     * @param Analysis   $analysisRepository
     * @param Worksheets $worksheets
     */
    public function __construct(Analysis $analysisRepository, Worksheets $worksheets)
    {
        $this->analysisRepository = $analysisRepository;
        $this->worksheets         = $worksheets;
    }
}