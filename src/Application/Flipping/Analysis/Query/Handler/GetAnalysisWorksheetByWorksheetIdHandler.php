<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Query\Handler;

use App\Application\Flipping\Analysis\Query\GetAnalysisWorksheetByWorksheetId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Worksheet;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class GetAnalysisWorksheetByWorksheetIdHandler
 *
 * @package App\Application\Flipping\Analysis\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetAnalysisWorksheetByWorksheetIdHandler extends AbstractAnalysisQueryHandler
{
    /**
     * @param GetAnalysisWorksheetByWorksheetId $query
     *
     * @return Worksheet
     * @throws NonUniqueResultException
     * @throws NotFoundException
     * @throws Exception
     */
    public function __invoke(GetAnalysisWorksheetByWorksheetId $query): Worksheet
    {
        $worksheetId = new WorksheetId($query->worksheetId());

        return $this->worksheets->findWorksheetByWorksheetId($worksheetId);
    }
}