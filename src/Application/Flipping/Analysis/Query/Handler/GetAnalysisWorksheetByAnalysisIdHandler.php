<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Query\Handler;

use App\Application\Flipping\Analysis\Query\GetAnalysisWorksheetByAnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Worksheet;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class GetAnalysisWorksheetByAnalysisIdHandler
 *
 * @package App\Application\Flipping\Analysis\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetAnalysisWorksheetByAnalysisIdHandler extends AbstractAnalysisQueryHandler
{
    /**
     * @param GetAnalysisWorksheetByAnalysisId $query
     *
     * @return Worksheet
     * @throws NonUniqueResultException
     * @throws NotFoundException
     * @throws Exception
     */
    public function __invoke(GetAnalysisWorksheetByAnalysisId $query): Worksheet
    {
        $analysisId = new AnalysisId($query->analysisId());

        return $this->worksheets->findWorksheetByAnalysisId($analysisId);
    }
}