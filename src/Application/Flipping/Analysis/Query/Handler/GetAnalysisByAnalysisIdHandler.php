<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Query\Handler;

use App\Application\Flipping\Analysis\Query\GetAnalysisByAnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Analysis;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

/**
 * Class GetAnalysisByAnalysisIdHandler
 *
 * @package App\Application\AnalysisManagement\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetAnalysisByAnalysisIdHandler extends AbstractAnalysisQueryHandler
{
    /**
     * @param GetAnalysisByAnalysisId $query
     *
     * @return Analysis
     * @throws NotFoundException
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function __invoke(GetAnalysisByAnalysisId $query): Analysis
    {
        $analysisId = new AnalysisId($query->analysisId());

        return $this->analysisRepository->findAnalysisByAnalysisId($analysisId);
    }
}