<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Query\Handler;

use App\Application\Flipping\Analysis\Query\FindAnalysisByPropertyId;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use Exception;

/**
 * Class FindAnalysisByPropertyIdHandler
 *
 * @package App\Application\AnalysisManagement\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindAnalysisByPropertyIdHandler extends AbstractAnalysisQueryHandler
{
    /**
     * @param FindAnalysisByPropertyId $query
     *
     * @return array
     * @throws Exception
     */
    public function __invoke(FindAnalysisByPropertyId $query): array
    {
        return $this->analysisRepository->findAnalysisByPropertyId(new PropertyId($query->propertyId()));
    }
}