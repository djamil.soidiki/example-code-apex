<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Query;

/**
 * Class GetAnalysisWorksheetByAnalysisId
 *
 * @package App\Application\Flipping\Analysis\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetAnalysisWorksheetByAnalysisId
{
    /**
     * @var string
     */
    private $analysisId;

    /**
     * GetAnalysisWorksheetByAnalysisId constructor.
     *
     * @param string $analysisId
     */
    public function __construct(string $analysisId)
    {
        $this->analysisId = $analysisId;
    }

    /**
     * @return string
     */
    public function analysisId(): string
    {
        return $this->analysisId;
    }
}