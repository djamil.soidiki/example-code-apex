<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Query;

/**
 * Class GetAnalysisByAnalysisId
 *
 * @package App\Application\Flipping\Analysis\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetAnalysisByAnalysisId
{
    /**
     * @var string
     */
    private $analysisId;

    /**
     * GetAnalysisByAnalysisId constructor.
     *
     * @param string $analysisId
     */
    public function __construct(string $analysisId)
    {
        $this->analysisId = $analysisId;
    }

    /**
     * @return string
     */
    public function analysisId(): string
    {
        return $this->analysisId;
    }
}