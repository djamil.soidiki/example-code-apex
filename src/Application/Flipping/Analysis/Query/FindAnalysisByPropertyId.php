<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Query;

/**
 * Class FindAnalysisByPropertyId
 *
 * @package App\Application\Flipping\Analysis\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindAnalysisByPropertyId
{
    /**
     * @var string
     */
    private $propertyId;

    /**
     * FindAnalysisByPropertyId constructor.
     *
     * @param string $propertyId
     */
    public function __construct(string $propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return string
     */
    public function propertyId(): string
    {
        return $this->propertyId;
    }
}