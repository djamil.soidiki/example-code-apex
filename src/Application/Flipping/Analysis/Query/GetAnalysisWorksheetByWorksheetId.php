<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Query;

/**
 * Class GetAnalysisWorksheetByWorksheetId
 *
 * @package App\Application\Flipping\Analysis\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class GetAnalysisWorksheetByWorksheetId
{
    /**
     * @var string
     */
    private $worksheetId;

    /**
     * GetAnalysisWorksheetByWorksheetId constructor.
     *
     * @param string $worksheetId
     */
    public function __construct(string $worksheetId)
    {
        $this->worksheetId = $worksheetId;
    }

    /**
     * @return string
     */
    public function worksheetId(): string
    {
        return $this->worksheetId;
    }
}