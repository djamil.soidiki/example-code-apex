<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command;

/**
 * Class ChangeWorksheetHoldingPeriod
 *
 * @package App\Application\Flipping\Analysis\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetHoldingPeriod
{
    /**
     * @var string
     */
    private $analysisId;

    /**
     * @var float
     */
    private $holdingPeriod;

    /**
     * ChangeWorksheetHoldingPeriod constructor.
     *
     * @param string $analysisId
     * @param float  $holdingPeriod
     */
    public function __construct(string $analysisId, float $holdingPeriod)
    {
        $this->analysisId    = $analysisId;
        $this->holdingPeriod = $holdingPeriod;
    }

    /**
     * @return string
     */
    public function analysisId(): string
    {
        return $this->analysisId;
    }

    /**
     * @return float
     */
    public function holdingPeriod(): float
    {
        return $this->holdingPeriod;
    }
}