<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command;

/**
 * Class ChangeWorksheetAfterRepairValue
 *
 * @package App\Application\Flipping\Analysis\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetAfterRepairValue
{
    /**
     * @var string
     */
    private $analysisId;

    /**
     * @var int
     */
    private $afterRepairValue;

    /**
     * ChangeWorksheetAfterRepairValue constructor.
     *
     * @param string $analysisId
     * @param int    $afterRepairValue
     */
    public function __construct(string $analysisId, int $afterRepairValue)
    {
        $this->analysisId       = $analysisId;
        $this->afterRepairValue = $afterRepairValue;
    }

    /**
     * @return string
     */
    public function analysisId(): string
    {
        return $this->analysisId;
    }

    /**
     * @return int
     */
    public function afterRepairValue(): int
    {
        return $this->afterRepairValue;
    }
}