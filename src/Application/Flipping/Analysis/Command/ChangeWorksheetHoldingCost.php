<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command;

/**
 * Class ChangeWorksheetHoldingCost
 *
 * @package App\Application\Flipping\Analysis\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetHoldingCost
{
    /**
     * @var string
     */
    private $analysisId;

    /**
     * @var string
     */
    private $inputMethod;

    /**
     * @var string|array
     */
    private $inputValue;

    /**
     * ChangeWorksheetHoldingCost constructor.
     *
     * @param string       $analysisId
     * @param string       $inputMethod
     * @param array|string $inputValue
     */
    public function __construct(string $analysisId, string $inputMethod, $inputValue)
    {
        $this->analysisId  = $analysisId;
        $this->inputMethod = $inputMethod;
        $this->inputValue  = $inputValue;
    }

    /**
     * @return string
     */
    public function analysisId(): string
    {
        return $this->analysisId;
    }

    /**
     * @return string
     */
    public function inputMethod(): string
    {
        return $this->inputMethod;
    }

    /**
     * @return array|string
     */
    public function inputValue()
    {
        return $this->inputValue;
    }
}