<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command;

/**
 * Class ChangeWorksheetPurchasePrice
 *
 * @package App\Application\Flipping\Analysis\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetPurchasePrice
{
    /**
     * @var string
     */
    private $analysisId;

    /**
     * @var int
     */
    private $purchasePrice;

    /**
     * ChangeWorksheetPurchasePrice constructor.
     *
     * @param string $analysisId
     * @param int    $purchasePrice
     */
    public function __construct(string $analysisId, int $purchasePrice)
    {
        $this->analysisId    = $analysisId;
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * @return string
     */
    public function analysisId(): string
    {
        return $this->analysisId;
    }

    /**
     * @return int
     */
    public function purchasePrice(): int
    {
        return $this->purchasePrice;
    }
}