<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command\Handler;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetAfterRepairValue;
use App\Domain\Flipping\Analysis\Analysis;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Currency;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Exception;

/**
 * Class ChangeWorksheetAfterRepairValueHandler
 *
 * @package App\Application\Flipping\Analysis\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetAfterRepairValueHandler extends AbstractAnalysisCommandHandler
{
    /**
     * @param ChangeWorksheetAfterRepairValue $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeWorksheetAfterRepairValue $command): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->get(new AnalysisId($command->analysisId()));

        $analysis->changeWorksheetAfterRepairValue(
            new Money(
                $command->afterRepairValue(),
                new Currency('USD')
            )
        );

        $this->analysisRepository->save($analysis);
    }
}