<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command\Handler;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetHoldingCost;
use App\Domain\Flipping\Analysis\Analysis;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use Assert\AssertionFailedException;
use Exception;

/**
 * Class ChangeWorksheetHoldingCostHandler
 *
 * @package App\Application\Flipping\Analysis\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetHoldingCostHandler extends AbstractAnalysisCommandHandler
{
    /**
     * @param ChangeWorksheetHoldingCost $command
     *
     * @throws AssertionFailedException
     * @throws Exception
     */
    public function __invoke(ChangeWorksheetHoldingCost $command): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->get(new AnalysisId($command->analysisId()));

        $analysis->changeWorksheetHoldingCost(
            new HoldingCost(
                $command->inputMethod(),
                $command->inputValue()
            )
        );

        $this->analysisRepository->save($analysis);
    }
}