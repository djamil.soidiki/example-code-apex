<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command\Handler;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetClosingCost;
use App\Domain\Flipping\Analysis\Analysis;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use Assert\AssertionFailedException;
use Exception;

/**
 * Class ChangeWorksheetClosingCostHandler
 *
 * @package App\Application\Flipping\Analysis\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetClosingCostHandler extends AbstractAnalysisCommandHandler
{
    /**
     * @param ChangeWorksheetClosingCost $command
     *
     * @throws AssertionFailedException
     * @throws Exception
     */
    public function __invoke(ChangeWorksheetClosingCost $command): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->get(new AnalysisId($command->analysisId()));

        $analysis->changeWorksheetClosingCost(
            new ClosingCost(
                $command->inputMethod(),
                $command->inputValue()
            )
        );

        $this->analysisRepository->save($analysis);
    }
}