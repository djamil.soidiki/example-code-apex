<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command\Handler;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetRehabCost;
use App\Domain\Flipping\Analysis\Analysis;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost;
use Assert\AssertionFailedException;
use Exception;

/**
 * Class ChangeWorksheetRehabCostHandler
 *
 * @package App\Application\Flipping\Analysis\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetRehabCostHandler extends AbstractAnalysisCommandHandler
{
    /**
     * @param ChangeWorksheetRehabCost $command
     *
     * @throws AssertionFailedException
     * @throws Exception
     */
    public function __invoke(ChangeWorksheetRehabCost $command): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->get(new AnalysisId($command->analysisId()));

        $analysis->changeWorksheetRehabCost(
            new RehabCost(
                $command->inputMethod(),
                $command->inputValue()
            )
        );

        $this->analysisRepository->save($analysis);
    }
}