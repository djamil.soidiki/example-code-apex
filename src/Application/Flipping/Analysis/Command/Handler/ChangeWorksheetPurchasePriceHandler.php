<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command\Handler;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetPurchasePrice;
use App\Domain\Flipping\Analysis\Analysis;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Currency;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Exception;

/**
 * Class ChangeWorksheetPurchasePriceHandler
 *
 * @package App\Application\Flipping\Analysis\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetPurchasePriceHandler extends AbstractAnalysisCommandHandler
{
    /**
     * @param ChangeWorksheetPurchasePrice $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeWorksheetPurchasePrice $command): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->get(new AnalysisId($command->analysisId()));

        $analysis->changeWorksheetPurchasePrice(
            new Money(
                $command->purchasePrice(),
                new Currency('USD')
            )
        );

        $this->analysisRepository->save($analysis);
    }
}