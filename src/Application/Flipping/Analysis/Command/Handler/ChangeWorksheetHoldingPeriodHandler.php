<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command\Handler;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetHoldingCost;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetHoldingPeriod;
use App\Domain\Flipping\Analysis\Analysis;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod;
use Exception;

/**
 * Class ChangeWorksheetHoldingPeriodHandler
 *
 * @package App\Application\Flipping\Analysis\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetHoldingPeriodHandler extends AbstractAnalysisCommandHandler
{
    /**
     * @param ChangeWorksheetHoldingPeriod $command
     *
     * @throws Exception
     */
    public function __invoke(ChangeWorksheetHoldingPeriod $command): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->get(new AnalysisId($command->analysisId()));

        $holdingPeriod = new HoldingPeriod($command->holdingPeriod());

        $analysis->changeWorksheetHoldingPeriod($holdingPeriod);

        $this->analysisRepository->save($analysis);
    }
}