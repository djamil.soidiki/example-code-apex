<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command\Handler;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetSellingCost;
use App\Domain\Flipping\Analysis\Analysis;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\SellingCost;
use Assert\AssertionFailedException;
use Exception;

/**
 * Class ChangeWorksheetSellingCostHandler
 *
 * @package App\Application\Flipping\Analysis\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class ChangeWorksheetSellingCostHandler extends AbstractAnalysisCommandHandler
{
    /**
     * @param ChangeWorksheetSellingCost $command
     *
     * @throws Exception
     * @throws AssertionFailedException
     */
    public function __invoke(ChangeWorksheetSellingCost $command): void
    {
        /** @var Analysis $analysis */
        $analysis = $this->analysisRepository->get(new AnalysisId($command->analysisId()));

        $analysis->changeWorksheetSellingCost(
            new SellingCost(
                $command->inputMethod(),
                $command->inputValue()
            )
        );

        $this->analysisRepository->save($analysis);
    }
}