<?php

declare(strict_types=1);

namespace App\Application\Flipping\Analysis\Command\Handler;

use App\Domain\Flipping\Analysis\Repository\AnalysisInterface;

/**
 * Class AbstractAnalysisCommandHandler
 *
 * @package App\Application\Flipping\Analysis\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractAnalysisCommandHandler
{
    /**
     * @var AnalysisInterface
     */
    protected $analysisRepository;

    /**
     * AbstractAnalysisCommandHandler constructor.
     *
     * @param AnalysisInterface $analysisRepository
     */
    public function __construct(AnalysisInterface $analysisRepository)
    {
        $this->analysisRepository = $analysisRepository;
    }
}