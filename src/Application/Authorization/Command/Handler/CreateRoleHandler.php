<?php

declare(strict_types=1);

namespace App\Application\Authorization\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\Authorization\Command\CreateRole;
use App\Domain\SharedKernel\Authorization\Exception\RoleNameIsNotUniqueException;
use App\Domain\SharedKernel\Authorization\Repository\RolesInterface;
use App\Domain\SharedKernel\Authorization\Role;
use App\Domain\SharedKernel\Authorization\Specification\RoleNameIsUniqueSpecification;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleName;
use Exception;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CreateRoleHandler
 *
 * @package App\Application\Authorization\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateRoleHandler extends AbstractAuthorizationCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * @var RoleNameIsUniqueSpecification
     */
    private $roleNameIsUnique;

    /**
     * CreateRoleHandler constructor.
     *
     * @param RolesInterface                $roles
     * @param RoleNameIsUniqueSpecification $roleNameIsUnique
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(
        RolesInterface $roles,
        RoleNameIsUniqueSpecification $roleNameIsUnique,
        AuthorizationCheckerInterface $authorizationChecker
    )
    {
        parent::__construct($roles);
        $this->roleNameIsUnique     = $roleNameIsUnique;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param CreateRole $command
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(CreateRole $command): string
    {
        if (!$this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN'))
            throw new AccessDeniedException();

        $roleName = new RoleName($command->name());

        if (false === $this->roleNameIsUnique->isSatisfiedBy($roleName))
            throw new RoleNameIsNotUniqueException($roleName->serialize());

        $role = Role::createRole(new RoleName($command->name()));

        $this->roles->save($role);

        return $role->getAggregateRootId();
    }
}