<?php

declare(strict_types=1);

namespace App\Application\Authorization\Command\Handler;

use App\Domain\SharedKernel\Authorization\Repository\RolesInterface;

/**
 * Class AbstractAuthorizationCommandHandler
 *
 * @package App\Application\Authorization\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractAuthorizationCommandHandler
{
    /**
     * @var RolesInterface
     */
    protected $roles;

    /**
     * AbstractAuthorizationCommandHandler constructor.
     *
     * @param RolesInterface $roles
     */
    public function __construct(RolesInterface $roles)
    {
        $this->roles = $roles;
    }
}