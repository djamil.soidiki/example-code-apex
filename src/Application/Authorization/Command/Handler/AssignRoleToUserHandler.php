<?php

declare(strict_types=1);

namespace App\Application\Authorization\Command\Handler;

use App\Application\ApplicationHelperTrait;
use App\Application\Authorization\Command\AssignRoleToUser;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Domain\SharedKernel\Authorization\Repository\RolesInterface;
use App\Domain\SharedKernel\Authorization\ValueObject\RoleId;
use Exception;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class AssignRoleToUserHandler
 *
 * @package App\Application\Authorization\Command\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AssignRoleToUserHandler extends AbstractAuthorizationCommandHandler
{
    use ApplicationHelperTrait;

    /**
     * CreateRoleHandler constructor.
     *
     * @param RolesInterface                $roles
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(RolesInterface $roles, AuthorizationCheckerInterface $authorizationChecker)
    {
        parent::__construct($roles);
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param AssignRoleToUser $command
     *
     * @throws Exception
     */
    public function __invoke(AssignRoleToUser $command): void
    {
        if (!$this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN'))
            throw new AccessDeniedException();

        $roleId = new RoleId($command->roleId());
        $role   = $this->roles->get($roleId);

        $role->assignToUser(new UserId($command->userId()));

        $this->roles->save($role);
    }
}