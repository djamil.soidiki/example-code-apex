<?php

declare(strict_types=1);

namespace App\Application\Authorization\Command;

/**
 * Class AssignRoleToUser
 *
 * @package App\Application\Authorization\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AssignRoleToUser
{
    /**
     * @var string
     */
    private $roleId;

    /**
     * @var string
     */
    private $userId;

    /**
     * AssignRoleToUser constructor.
     *
     * @param string $roleId
     * @param string $userId
     */
    public function __construct(string $roleId, string $userId)
    {
        $this->roleId = $roleId;
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function roleId(): string
    {
        return $this->roleId;
    }

    /**
     * @return string
     */
    public function userId(): string
    {
        return $this->userId;
    }
}