<?php

declare(strict_types=1);

namespace App\Application\Authorization\Command;

/**
 * Class CreateRole
 *
 * @package App\Application\Authorization\Command
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateRole
{
    /**
     * @var string
     */
    private $name;

    /**
     * CreateRole constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}