<?php

declare(strict_types=1);

namespace App\Application\Authorization\Query;

/**
 * Class FindUserRoles
 *
 * @package App\Application\Authorization\Query
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindUserRoles
{
    /**
     * @var string
     */
    private $userId;

    /**
     * FindUserRoles constructor.
     *
     * @param string $userId
     */
    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function userId(): string
    {
        return $this->userId;
    }
}