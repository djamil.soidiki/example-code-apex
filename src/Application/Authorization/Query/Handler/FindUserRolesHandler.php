<?php

declare(strict_types=1);

namespace App\Application\Authorization\Query\Handler;

use App\Application\Authorization\Query\FindUserRoles;
use App\Domain\SharedKernel\Authentication\ValueObject\UserId;
use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Model\Assignment;
use Exception;

/**
 * Class FindUserRolesHandler
 *
 * @package App\Application\Authorization\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class FindUserRolesHandler extends AbstractAuthorizationQueryHandler
{
    /**
     * @param FindUserRoles $query
     *
     * @return array
     * @throws Exception
     */
    public function __invoke(FindUserRoles $query): array
    {
        $roles = [];

        /** @var Assignment[] $assignments */
        $assignments = $this->assignments->findAssignmentsByUserId(new UserId($query->userId()));

        foreach ($assignments as $assignment) {
            $roles[] = $this->roles->findRoleByRoleId($assignment->roleId());
        }

        return $roles;
    }
}