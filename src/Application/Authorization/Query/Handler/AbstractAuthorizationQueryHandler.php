<?php

declare(strict_types=1);

namespace App\Application\Authorization\Query\Handler;

use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Repository\Assignments;
use App\Infrastructure\SharedKernel\Authorization\Query\Doctrine\Repository\Roles;

/**
 * Class AbstractAuthorizationQueryHandler
 *
 * @package App\Application\Authorization\Query\Handler
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractAuthorizationQueryHandler
{
    /**
     * @var Roles
     */
    protected $roles;

    /**
     * @var Assignments
     */
    protected $assignments;

    /**
     * AbstractAuthorizationQueryHandler constructor.
     *
     * @param Roles       $roles
     * @param Assignments $assignments
     */
    public function __construct(Roles $roles, Assignments $assignments)
    {
        $this->roles       = $roles;
        $this->assignments = $assignments;
    }
}