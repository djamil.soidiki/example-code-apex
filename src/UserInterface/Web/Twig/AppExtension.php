<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Twig;

use App\Domain\PropertyManagement\ValueObject\PropertyStatus;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class AppExtension
 *
 * @package App\UserInterface\Web\Twig
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class AppExtension extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('flash_message', [$this, 'createFlashMessage'], ['is_safe' => ['html']]),
            new TwigFilter('property_status', [$this, 'transformPropertyStatusToBadge'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param $flashMessage
     *
     * @return mixed|string
     */
    public function createFlashMessage($flashMessage)
    {
        $flashMessageElement = explode('#~#', $flashMessage);

        if (count($flashMessageElement) == 2) {
            $title   = $flashMessageElement[0];
            $message = $flashMessageElement[1];

            $htmlTitle   = "<h5 class=\"mb-1 font-weight-bold mt-0\">$title</h5>";
            $htmlMessage = "<span>$message</span>";

            $flashMessage = $htmlTitle . $htmlMessage;
        } else {
            $flashMessage = $flashMessageElement[0];
        }

        return $flashMessage;
    }

    /**
     * @param PropertyStatus $propertyStatus
     *
     * @return string
     */
    public function transformPropertyStatusToBadge(PropertyStatus $propertyStatus)
    {
        if ($propertyStatus->sameValueAs(PropertyStatus::activated()))
            $badge = "<span class=\"tag is-success is-light\">Active</span>";
        else
            $badge = "<span class=\"tag is-solid is-light\">Archived</span>";

        return $badge;
    }
}