<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Admin\Billing\Plan;

use App\Domain\SharedKernel\Billing\Plan\ValueObject\PlanBillingCycle;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class CreatePlanType
 *
 * @package App\UserInterface\Web\Form\Admin\Billing\Plan
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class CreatePlanType extends AbstractType
{
    use BusTrait;

    /**
     * CreatePlanType constructor.
     *
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',
                  TextType::class,
                  [
                      'label' => 'Name'
                  ]
            )
            ->add('price',
                  NumberType::class,
                  [
                      'label' => 'Price'
                  ]
            )
            ->add('product',
                TextType::class,
                  [
                      'label'   => 'Product Id (eg. from Stripe or Paypal)'
                  ]
            )
            ->add('interval',
                  ChoiceType::class,
                  [
                      'label'   => 'Choose the interval',
                      'choices' => [
                          'Day'   => PlanBillingCycle::DAY,
                          'Week'  => PlanBillingCycle::WEEK,
                          'Month' => PlanBillingCycle::MONTH,
                          'Year'  => PlanBillingCycle::YEAR,
                      ]
                  ]
            );
    }
}