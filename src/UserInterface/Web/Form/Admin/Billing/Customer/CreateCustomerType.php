<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Admin\Billing\Customer;

use App\Application\Authentication\Query\FindUsers;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\User;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CreateCustomerType
 *
 * @package App\UserInterface\Web\Form\Admin\Billing\Customer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class CreateCustomerType extends AbstractType
{
    use BusTrait;

    /**
     * CreateCustomerType constructor.
     *
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('emailAddress',
                  EmailType::class,
                  [
                      'label' => 'Name'
                  ]
            )
            ->add('firstName',
                  TextType::class,
                  [
                      'label' => 'First Name'
                  ]
            )
            ->add('lastName',
                  TextType::class,
                  [
                      'label' => 'Last Name'
                  ]
            )
            ->add('user',
                  ChoiceType::class,
                  [
                      'label'   => 'Choose the user associated',
                      'choices' => $options['users']
                  ]
            );
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $users = $this->handleQuery(new FindUsers());

        $choices = [];

        /** @var User $user */
        foreach ($users as $user)
        {
            $choices[$user->emailAddress()->serialize()] = $user->userId()->serialize();
        }

        $resolver->setDefaults(
            [
                // Configure your form options here
                'users' => $choices
            ]
        );
    }
}