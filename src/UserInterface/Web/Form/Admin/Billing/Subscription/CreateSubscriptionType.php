<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Admin\Billing\Subscription;

use App\Application\Billing\Customer\Query\FindCustomersQuery;
use App\Application\Billing\Plan\Query\FindPlansQuery;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Model\Customer;
use App\Infrastructure\SharedKernel\Billing\Plan\Query\Model\Plan;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CreateSubscriptionType
 *
 * @package App\UserInterface\Web\Form\Admin\Billing\Subscription
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class CreateSubscriptionType extends AbstractType
{
    use BusTrait;

    /**
     * CreateSubscriptionType constructor.
     *
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('customer',
                  ChoiceType::class,
                  [
                      'label'   => 'Choose a customer',
                      'choices' => $options['customers']
                  ]
            )
            ->add('plan',
                  ChoiceType::class,
                  [
                      'label'   => 'Choose a plan',
                      'choices' => $options['plans']
                  ]
            );
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $customers = $this->handleQuery(new FindCustomersQuery());
        $plans     = $this->handleQuery(new FindPlansQuery());

        $choicesCustomers = [];
        $choicesPlans     = [];

        /** @var Customer $customer */
        foreach ($customers as $customer) {
            $choicesCustomers[(string)$customer->fullName() . ' - '. $customer->emailAddress()->serialize()] = $customer->customerId()->serialize();
        }

        /** @var Plan $plan */
        foreach ($plans as $plan) {
            $choicesPlans[$plan->name()->serialize()] = $plan->planId()->serialize();
        }

        $resolver->setDefaults(
            [
                // Configure your form options here
                'customers' => $choicesCustomers,
                'plans'     => $choicesPlans,
            ]
        );
    }
}