<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Contact\Group;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CreateGroupType
 *
 * @package App\UserInterface\Web\Form\Contact\Group
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class CreateGroupType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'List Name',
                'attr'  => [
                    'placeholder' => 'eg. Investors, Flippers, etc.'
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label'    => 'List Description',
                'attr'     => [
                    'placeholder' => 'South California Investors.'
                ],
                'help'     => 'This description is personal, it will not be shown to your contacts'
            ]);
    }
}