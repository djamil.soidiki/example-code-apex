<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Contact\Group;

use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EditGroupType
 *
 * @package App\UserInterface\Web\Form\Contact\Group
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class EditGroupType extends AbstractType
{
    /**
     * @var string
     */
    private $groupId;

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Group $group */
        $group = $options['group'];

        $this->groupId = $group->groupId()->serialize();

        $builder
            ->add('name', TextType::class, [
                'label' => 'List Name',
                'attr'  => [
                    'placeholder' => 'eg. Investors, Flippers, etc.'
                ],
                'data'  => ($group !== null) ? $group->name()->serialize() : null
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label'    => 'List Description',
                'attr'     => [
                    'placeholder' => 'South California Investors.'
                ],
                'help'     => 'This description is personal, it will not be shown to your contacts'
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'group' => null
        ]);

        $resolver->setAllowedTypes('group', ['App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group']);
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        return 'edit_group_' . $this->groupId;
    }
}