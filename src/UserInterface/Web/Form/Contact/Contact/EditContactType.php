<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Contact\Contact;

use App\Application\ContactManagement\Contact\Query\GetContactByContactId;
use App\Application\ContactManagement\Group\Query\FindGroupsByContactId;
use App\Application\ContactManagement\Group\Query\FindGroupsByOwnerId;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model\Contact;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EditContactType
 *
 * @package App\UserInterface\Web\Form\Contact\Contact
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class EditContactType extends AbstractType
{
    use BusTrait;

    /**
     * CreateContactType constructor.
     *
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Contact $contact */
        $contact         = $this->handleQuery(new GetContactByContactId($options['contact_id']));
        $groups          = $this->handleQuery(new FindGroupsByOwnerId($options['user_id']));
        $groupsOfContact = $this->handleQuery(new FindGroupsByContactId($options['contact_id']));

        $choices = [];
        $data    = [];

        /** @var Group $group */
        foreach ($groups as $group) {
            $group                                = $group['data'];
            $choices[$group->name()->serialize()] = $group->groupId()->serialize();
        }

        /** @var Group $groupOfContact */
        foreach ($groupsOfContact as $groupOfContact) {
            $data[$groupOfContact->name()->serialize()] = $groupOfContact->groupId()->serialize();
        }

        $builder
            ->add('firstName', TextType::class, [
                'label' => 'First Name',
                'data'  => $contact->fullName()->firstName()
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Last Name',
                'data'  => $contact->fullName()->lastName()
            ])
            ->add('emailAddress', EmailType::class, [
                'label' => 'Email Address',
                'data'  => $contact->emailAddress()->serialize()
            ])
            ->add('phoneNumber', TelType::class, [
                'label'    => 'Phone Number',
                'required' => false,
                'data'     => (string)$contact->phoneNumber()
            ])
            ->add('groups', ChoiceType::class, [
                'label'    => 'Add the contact to groups',
                'choices'  => $choices,
                'expanded' => true,
                'multiple' => true,
                'data'     => $data
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'contact_id' => null,
            'user_id'    => null
        ]);

        $resolver->setAllowedTypes('contact_id', ['string']);
        $resolver->setAllowedTypes('user_id', ['string']);
    }
}