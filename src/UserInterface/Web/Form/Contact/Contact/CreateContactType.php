<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Contact\Contact;

use App\Application\ContactManagement\Group\Query\FindGroupsByOwnerId;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CreateContactType
 *
 * @package App\UserInterface\Web\Form\Contact\Contact
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class CreateContactType extends AbstractType
{
    use BusTrait;

    /**
     * CreateContactType constructor.
     *
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $groups = $this->handleQuery(new FindGroupsByOwnerId($options['user_id']));

        $choices = [];

        /** @var Group $group */
        foreach ($groups as $group) {
            $group = $group['data'];

            $choices[$group->name()->serialize()] = $group->groupId()->serialize();
        }

        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('emailAddress', EmailType::class)
            ->add('groups', ChoiceType::class, [
                'label'    => 'add the contact to groups',
                'choices'  => $choices,
                'expanded' => true,
                'multiple' => true
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'user_id' => null
        ]);

        $resolver->setAllowedTypes('user_id', ['string']);
    }
}