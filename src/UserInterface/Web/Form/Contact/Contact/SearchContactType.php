<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Contact\Contact;

use App\Application\ContactManagement\Group\Query\FindGroupsByOwnerId;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchContactType
 *
 * @package App\UserInterface\Web\Form\Contact\Contact
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class SearchContactType extends AbstractType
{
    use BusTrait;

    /**
     * SearchContactType constructor.
     *
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $groups = $this->handleQuery(new FindGroupsByOwnerId($options['user_id']));

        $choices = [
            'All'  => 0,
            'Any'  => -1,
            'None' => -2
        ];

        /** @var Group $group */
        foreach ($groups as $group) {
            $group = $group['data'];

            $choices[$group->name()->serialize()] = $group->groupId()->serialize();
        }

        $builder
            ->add('groupId', ChoiceType::class, [
                'required' => false,
                'label'    => 'LIST',
                'choices'  => $choices,
                'expanded' => false,
                'multiple' => false,
                'data'     => ($options['group_id'] !== null) ? $options['group_id'] : 0
            ])
            ->add('q', TextType::class, [
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Search contacts'
                ],
                'data'     => ($options['q'] !== null) ? $options['q'] : null
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'user_id'  => null,
            'group_id' => null,
            'q'        => null
        ]);

        $resolver->setAllowedTypes('user_id', ['string']);
        $resolver->setAllowedTypes('group_id', ['null', 'string']);
        $resolver->setAllowedTypes('q', ['null', 'string']);
    }
}