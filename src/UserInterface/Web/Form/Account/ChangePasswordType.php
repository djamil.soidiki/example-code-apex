<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Account;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ChangePasswordType
 *
 * @package App\UserInterface\Web\Form\Account
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class ChangePasswordType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('currentPassword',
                PasswordType::class,
                [
                    'label' => 'Current password'
                ]
            )
            ->add('newPassword',
                RepeatedType::class,
                [
                    'type'            => PasswordType::class,
                    'invalid_message' => 'Password confirmation doesn\'t match the password.',
                    'constraints'     => [
                        new NotBlank([
                                'message' => 'Please enter a password',
                            ]
                        ),
                        new Length([
                                'min'        => 6,
                                'minMessage' => 'Your password should be at least {{ limit }} characters',
                                // max length allowed by Symfony for security reasons
                                'max'        => 4096,
                            ]
                        )
                    ],
                    'required'        => true,
                    'first_options'   => ['label' => 'New password'],
                    'second_options'  => ['label' => 'Reenter new password'],
                ]
            );
    }
}