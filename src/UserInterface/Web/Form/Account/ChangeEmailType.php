<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Account;

use App\Domain\SharedKernel\Generic\ValueObject\Web\EmailAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChangeEmailType
 *
 * @package App\UserInterface\Web\Form\Account
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class ChangeEmailType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['email_address'] instanceof EmailAddress) {
            $builder
                ->add('emailAddress',
                      TextType::class,
                      [
                          'label' => 'Email Address',
                          'attr'  => [
                              'value'       => $options['email_address']->serialize(),
                              'placeholder' => 'Email Address'
                          ]
                      ]
                );
        } else {
            throw new \InvalidArgumentException(sprintf("\$options['email_address'] should be an instance of %s", EmailAddress::class));
        }
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'email_address' => EmailAddress::class // Must be an instance of EmailAddress ValueObject
            ]
        );
    }
}