<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Account;

use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChangeNameType
 *
 * @package App\UserInterface\Web\Form\Account
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class ChangeNameType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['full_name'] instanceof FullName) {
            $builder
                ->add('firstName',
                      TextType::class,
                      [
                          'label' => 'First Name',
                          'attr'  => [
                              'value'       => $options['full_name']->firstName(),
                              'placeholder' => 'First Name'
                          ]
                      ]
                )
                ->add('lastName',
                      TextType::class,
                      [
                          'label' => 'Last Name',
                          'attr'  => [
                              'value'       => $options['full_name']->lastName(),
                              'placeholder' => 'Last Name'
                          ]
                      ]
                );
        } else {
            throw new \InvalidArgumentException(sprintf("\$options['full_name'] should be an instance of %s", FullName::class));
        }
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'full_name' => FullName::class // Must be an instance of FullName ValueObject
            ]
        );
    }
}