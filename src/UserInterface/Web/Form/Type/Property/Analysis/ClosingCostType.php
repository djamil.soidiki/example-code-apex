<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Type\Property\Analysis;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\UserInterface\Web\Form\Type\Property\Analysis\EventListener\AddClosingCostValueFieldListener;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ClosingCostType
 *
 * @package App\UserInterface\Web\Form\Type\Property\Analysis
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class ClosingCostType extends AbstractType
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * ClosingCostType constructor.
     *
     * @param Environment              $twig
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(Environment $twig, EventDispatcherInterface $eventDispatcher)
    {
        $this->twig            = $twig;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ClosingCost $closingCost */
        $closingCost = $options['closing_cost'];

        $closingCostValue = ($closingCost->inputValue() !== null) ? $closingCost->inputValue() : null;

        $builder
            ->add('type', ChoiceType::class, [
                'label'       => 'How do you want to calculate the buying cost?',
                'placeholder' => 'Choose an option',
                'choices'     => [
                    'Lump Sum'            => ClosingCost::LUMP_SUM_METHOD,
                    '% of Purchase Price' => ClosingCost::PERCENTAGE_OF_PURCHASE_PRICE_METHOD
                ],
                'data'        => ($closingCost !== null) ? $closingCost->inputMethod() : null
            ])
            ->get('type')->addEventSubscriber(new AddClosingCostValueFieldListener($closingCostValue));
    }

    /**
     * @inheritDoc
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $javascriptCode = $this->twig->render('_form/type/closing_cost_type.html.twig', [
            'analysisName'     => $view->vars['id'],
            'closingCostType'  => $view->vars['id'] . "_type",
            'closingCostValue' => $view->vars['id'] . "_value"
        ]);

        $this->eventDispatcher->addListener('kernel.response', function ($event) use ($javascriptCode) {

            $response = $event->getResponse();
            $content  = $response->getContent();
            // finding position of </body> tag to add content before the end of the tag
            $pos     = strripos($content, '</body>');
            $content = substr($content, 0, $pos) . $javascriptCode . substr($content, $pos);

            $response->setContent($content);
            $event->setResponse($response);
        });

        parent::buildView($view, $form, $options);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'closing_cost' => null
        ]);

        $resolver->addAllowedTypes('closing_cost', ['null', 'App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost']);
    }
}