<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Type\Property\Analysis\EventListener;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

/**
 * Class AddClosingCostValueFieldListener
 *
 * @package App\UserInterface\Web\Form\Type\Property\Analysis\EventListener
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AddClosingCostValueFieldListener implements EventSubscriberInterface
{
    /**
     * @var mixed|null
     */
    private $closingCostValue;

    /**
     * AddClosingCostValueFieldListener constructor.
     *
     * @param mixed|null $value
     */
    public function __construct($value = null)
    {
        $this->closingCostValue = $value;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::POST_SUBMIT  => 'onPostSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        $this->formModifier($event->getForm()->getParent(), $event->getData());
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSubmit(FormEvent $event)
    {
        $this->formModifier($event->getForm()->getParent(), $event->getForm()->getData());
    }

    /**
     * @param FormInterface $form
     * @param null          $data
     */
    private function formModifier(FormInterface $form, $data = null)
    {
        if ($data === null)
            return;

        $dataInput = ($this->closingCostValue instanceof Money) ? $this->closingCostValue->amount() : $this->closingCostValue;

        if ($data === ClosingCost::LUMP_SUM_METHOD) {
            $form->add('value', MoneyType::class, [
                'label'     => 'Lump Sum Amount',
                'currency'  => 'USD',
                'help'      => 'Amount in <b>US Dollars (USD)</b>.',
                'help_html' => true,
                'data'      => $dataInput
            ]);
        } elseif ($data === ClosingCost::PERCENTAGE_OF_PURCHASE_PRICE_METHOD) {
            $form->add('value', IntegerType::class, [
                'label'     => 'Percentage of Purchase',
                'help'      => 'Amount in <b>Percentage (%)</b>.',
                'help_html' => true,
                'data'      => $dataInput
            ]);
        }
    }
}