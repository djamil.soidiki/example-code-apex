<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Type\Property\Analysis\EventListener;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

/**
 * Class AddHoldingCostValueFieldListener
 *
 * @package App\UserInterface\Web\Form\Type\Property\Analysis\EventListener
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AddHoldingCostValueFieldListener implements EventSubscriberInterface
{
    /**
     * @var mixed|null
     */
    private $holdingCostValue;

    /**
     * AddHoldingCostValueFieldListener constructor.
     *
     * @param mixed|null $value
     */
    public function __construct($value = null)
    {
        $this->holdingCostValue = $value;
    }
    
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::POST_SUBMIT  => 'onPostSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        $this->formModifier($event->getForm()->getParent(), $event->getData());
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSubmit(FormEvent $event)
    {
        $this->formModifier($event->getForm()->getParent(), $event->getForm()->getData());
    }

    /**
     * @param FormInterface $form
     * @param null          $data
     */
    private function formModifier(FormInterface $form, $data = null)
    {
        if ($data === null)
            return;

        if ($data === HoldingCost::LUMP_SUM_METHOD) {
            $form->add('value', MoneyType::class, [
                'label'     => 'Lump Sum Amount',
                'currency'  => 'USD',
                'help'      => 'Amount in <b>US Dollars (USD)</b>.',
                'help_html' => true,
                'data'      => (int)(($this->holdingCostValue === null) ? null : $this->holdingCostValue->amount())
            ]);
        } /*elseif ($data === HoldingCost::PRICE_PER_MONTH_TYPE) {
            $form->add('value', MoneyType::class, [
                'label'     => 'Price per Month',
                'currency'  => 'USD',
                'help'      => 'Amount in <b>US Dollars (USD)</b>.',
                'help_html' => true,
                'data'      => (int)(($this->holdingCostValue === null) ? null : $this->holdingCostValue->amount())
            ]);
        }*/
    }
}