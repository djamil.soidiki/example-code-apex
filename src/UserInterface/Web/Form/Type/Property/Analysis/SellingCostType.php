<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Type\Property\Analysis;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\SellingCost;
use App\UserInterface\Web\Form\Type\Property\Analysis\EventListener\AddSellingCostValueFieldListener;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class SellingCostType
 *
 * @package App\UserInterface\Web\Form\Type\Property\Analysis
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class SellingCostType extends AbstractType
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * SellingCostType constructor.
     *
     * @param Environment              $twig
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(Environment $twig, EventDispatcherInterface $eventDispatcher)
    {
        $this->twig            = $twig;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var SellingCost $sellingCost */
        $sellingCost = $options['selling_cost'];

        $sellingCostValue = ($sellingCost !== null) ? $sellingCost->inputValue() : null;

        $builder
            ->add('type', ChoiceType::class, [
                'label'       => 'How do you want to calculate the selling cost?',
                'placeholder' => 'Choose an option',
                'choices'     => [
                    'Lump Sum'          => SellingCost::LUMP_SUM_METHOD,
                    //'% of Resale Price' => SellingCost::MET
                ],
                'data'        => ($sellingCost !== null) ? $sellingCost->inputMethod() : null
            ])
            ->get('type')->addEventSubscriber(new AddSellingCostValueFieldListener($sellingCostValue));
    }

    /**
     * @inheritDoc
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $javascriptCode = $this->twig->render('_form/type/selling_cost_type.html.twig', [
            'analysisName'     => $view->vars['id'],
            'sellingCostType'  => $view->vars['id'] . "_type",
            'sellingCostValue' => $view->vars['id'] . "_value"
        ]);

        $this->eventDispatcher->addListener('kernel.response', function ($event) use ($javascriptCode) {

            $response = $event->getResponse();
            $content  = $response->getContent();
            // finding position of </body> tag to add content before the end of the tag
            $pos     = strripos($content, '</body>');
            $content = substr($content, 0, $pos) . $javascriptCode . substr($content, $pos);

            $response->setContent($content);
            $event->setResponse($response);
        });

        parent::buildView($view, $form, $options);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'selling_cost' => null
        ]);

        $resolver->addAllowedTypes('selling_cost', ['null', 'App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\SellingCost']);
    }
}