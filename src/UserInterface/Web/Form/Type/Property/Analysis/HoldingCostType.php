<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Type\Property\Analysis;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod;
use App\UserInterface\Web\Form\Type\Property\Analysis\EventListener\AddHoldingCostValueFieldListener;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class HoldingCostType
 *
 * @package App\UserInterface\Web\Form\Type\Property\Analysis
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class HoldingCostType extends AbstractType
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * SellingCostType constructor.
     *
     * @param Environment              $twig
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(Environment $twig, EventDispatcherInterface $eventDispatcher)
    {
        $this->twig            = $twig;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var HoldingCost $holdingCost */
        $holdingCost = $options['holding_cost'];

        /** @var HoldingPeriod $holdingPeriod */
        $holdingPeriod = $options['holding_period'];

        $holdingCostValue = ($holdingCost !== null) ? $holdingCost->inputValue() : null;

        $holdingPeriodChoices = [];

        for ($month = 0.5; $month < 25; $month += 0.5) {
            $holdingPeriodChoices[$month] = $month;
        }

        $builder
            ->add('type', ChoiceType::class, [
                'label'       => 'How do you want to calculate the holding price?',
                'placeholder' => 'Choose an option',
                'choices'     => [
                    'Lump Sum' => HoldingCost::LUMP_SUM_METHOD,
                    'Itemized' => HoldingCost::ITEMIZED_METHOD
                ],
                'data'        => ($holdingCost !== null) ? $holdingCost->inputMethod() : null
            ])
            ->add('holdingPeriod', ChoiceType::class, [
                'label'   => 'Number of Holding Months',
                'choices' => $holdingPeriodChoices,
                'data'    => ($holdingPeriod !== null) ? $holdingPeriod->value() : null
            ])
            ->get('type')->addEventSubscriber(new AddHoldingCostValueFieldListener($holdingCostValue));
    }

    /**
     * @inheritDoc
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $javascriptCode = $this->twig->render('_form/type/holding_cost_type.html.twig', [
            'analysisName'     => $view->vars['id'],
            'holdingCostType'  => $view->vars['id'] . "_type",
            'holdingCostValue' => $view->vars['id'] . "_value"
        ]);

        $this->eventDispatcher->addListener('kernel.response', function ($event) use ($javascriptCode) {

            $response = $event->getResponse();
            $content  = $response->getContent();
            // finding position of </body> tag to add content before the end of the tag
            $pos     = strripos($content, '</body>');
            $content = substr($content, 0, $pos) . $javascriptCode . substr($content, $pos);

            $response->setContent($content);
            $event->setResponse($response);
        });

        parent::buildView($view, $form, $options);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'holding_cost' => null,
            'holding_period' => null
        ]);

        $resolver->addAllowedTypes('holding_cost', ['null', 'App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost']);
        $resolver->addAllowedTypes('holding_cost', ['null', 'App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingPeriod']);
    }
}