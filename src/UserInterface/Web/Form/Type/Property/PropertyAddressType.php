<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Type\Property;

use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\UserInterface\Web\Form\Type\Address\StateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PropertyAddressType
 *
 * @package App\UserInterface\Web\Form\Type\Property
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class PropertyAddressType extends AbstractType implements DataMapperInterface
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('streetAddress', TextType::class, [
                'label' => 'Street Address',
                'attr'  => [
                    'placeholder' => '1234 Main St'
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'City',
                'attr'  => [
                    'placeholder' => 'Los Angeles'
                ]
            ])
            ->add('state', StateType::class, [
                'label' => 'State',
                'attr'  => [
                    'placeholder' => 'CA'
                ]
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'Zip Code',
                'attr'  => [
                    'placeholder' => '92069'
                ]
            ]);

        $builder->setDataMapper($this);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertyAddress::class,
            'empty_data' => null
        ]);
    }

    /**
     * @inheritDoc
     */
    public function mapDataToForms($viewData, $forms)
    {
        if (null === $viewData)
            return;

        if (!$viewData instanceof PropertyAddress)
            throw new UnexpectedTypeException($viewData, PropertyAddress::class);

        $forms = iterator_to_array($forms);

        $forms['streetAddress']->setData($viewData->streetAddress());
        $forms['city']->setData($viewData->city());
        $forms['state']->setData($viewData->state());
        $forms['zipCode']->setData($viewData->zipCode());
    }

    /**
     * @inheritDoc
     */
    public function mapFormsToData($forms, &$viewData)
    {
        $forms = iterator_to_array($forms);

        $viewData = new PropertyAddress(
            $forms['streetAddress']->getData(),
            $forms['city']->getData(),
            $forms['state']->getData(),
            $forms['zipCode']->getData()
        );
    }
}