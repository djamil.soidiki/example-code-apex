<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Type\Address;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddressType
 *
 * @package App\UserInterface\Web\Form\Type\Address
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class AddressType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'line1',
                TextType::class,
                [
                    'label' => 'Address',
                    'attr'  => [
                        'placeholder' => '1234 Main St'
                    ]
                ]
            )
            ->add(
                'line2',
                TextType::class,
                [
                    'label'    => 'Address 2',
                    'attr'     => [
                        'placeholder' => 'Apartment, studio, or floor'
                    ],
                    'required' => false
                ]
            )
            ->add(
                'city',
                TextType::class,
                [
                    'label' => 'City'
                ]
            )
            ->add('state',
                  StateType::class,
                  [
                      'label' => 'State'
                  ]
            )
            ->add(
                'postalCode',
                TextType::class,
                [
                    'label' => 'Postal Code'
                ]
            );
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'address' => null, // Must be an instance of Address
            ]
        );
    }
}