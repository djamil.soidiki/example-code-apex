<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Home;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class TryType
 *
 * @package App\UserInterface\Web\Form\Home
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class TryType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'firstName',
                TextType::class,
                [
                    'label'       => 'First Name',
                    'attr'        => [
                        'placeholder' => 'First Name'
                    ],
                    'constraints' => [
                        new NotBlank([
                                         'message' => 'Your first name is required.'
                                     ]
                        )
                    ]
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'label'       => 'Last Name',
                    'attr'        => [
                        'placeholder' => 'Last Name'
                    ],
                    'constraints' => [
                        new NotBlank([
                                         'message' => 'Your last name is required.'
                                     ]
                        )
                    ]
                ]
            )
            ->add(
                'emailAddress',
                EmailType::class,
                [
                    'label' => 'Email Address',
                    'attr'  => [
                        'placeholder' => 'Email Address'
                    ],

                    'constraints' => [
                        new NotBlank([
                                         'message' => 'Your email address is required.'
                                     ]
                        ),
                        new Email()
                    ]
                ]
            )
            ->add(
                'password',
                PasswordType::class,
                [
                    'label'       => 'Password',
                    'attr'        => [
                        'placeholder' => 'Password'
                    ],
                    'constraints' => [
                        new NotBlank([
                                         'message' => 'Your must choose a password.'
                                     ]
                        )
                    ]
                ]
            )
            ->add(
                'agreeTerms',
                CheckboxType::class,
                [
                    'label'       => 'I agree to the #TERMS_OF_USE_LINK#.',
                    'required'    => false,
                    'constraints' => [
                        new IsTrue([
                                       'message' => 'You must agree to our terms.'
                                   ]
                        )
                    ]
                ]
            );
    }
}