<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Home;

use App\Application\Billing\Plan\Query\FindPlansQuery;
use App\Domain\SharedKernel\Generic\ValueObject\Person\FullName;
use App\Infrastructure\SharedKernel\Billing\Plan\Query\Model\Plan;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class BuyType
 *
 * @package App\UserInterface\Web\Form\Home
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class BuyType extends AbstractType
{
    use BusTrait;

    /**
     * BuyType constructor.
     *
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userAuthenticated = $options['user_authenticated'];
        /** @var FullName $fullName */
        $fullName = $options['full_name'];

        if ($userAuthenticated === false) {
            $builder
                ->add('emailAddress', EmailType::class, ['label' => 'Email Address'])
                ->add('password', PasswordType::class, ['label' => 'Password']);
        }

        if ($fullName !== null && $fullName instanceof FullName) {
            $firstName = $fullName->firstName();
            $lastName  = $fullName->lastName();
        }

        /** @var Plan[] $plans */
        $plans = $this->handleQuery(new FindPlansQuery());

        $choices        = [];
        $default_choice = null;
        foreach ($plans as $key => $plan) {

            $price        = number_format(($plan->price()->amount() / 100), 2);
            $billingCycle = strtolower($plan->billingCycle()->serialize());

            $choices[$plan->name()->serialize() . "<br /><small>$$price/$billingCycle</small>"] = $plan->getId();

            if ($key === 0)
                $default_choice = $plan->getId();
        }

        $builder
            ->add('plan',
                  ChoiceType::class,
                  [
                      'choices'  => $choices,
                      'expanded' => true,
                      'multiple' => false,
                      'data'     => $default_choice
                  ]
            )
            ->add('firstName',
                  TextType::class,
                  [
                      'label' => 'First Name',
                      'attr'  => [
                          'value' => $firstName ?? null
                      ]
                  ]
            )
            ->add('lastName',
                  TextType::class,
                  [
                      'label' => 'Last Name',
                      'attr'  => [
                          'value' => $lastName ?? null
                      ]
                  ]
            )
            ->add('paymentMethod', HiddenType::class, ['constraints' => [new NotBlank()]])
            ->add(
                'agreeTerms',
                CheckboxType::class,
                [
                    'label'       => 'I agree to the #TERMS_OF_USE_LINK#.',
                    'required'    => false,
                    'constraints' => [
                        new IsTrue([
                                       'message' => 'You must agree to our terms.'
                                   ]
                        )
                    ]
                ]
            );
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                // Configure your form options here
                'user_authenticated' => false,
                'full_name'          => null,
            ]
        );
    }
}