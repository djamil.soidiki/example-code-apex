<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Authentication;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class RequestPasswordResetType
 *
 * @package App\UserInterface\Web\Form\Authentication
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class RequestPasswordResetType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('emailAddress',
                  EmailType::class,
                  [
                      'label' => 'Email Address'
                  ]
            );
    }
}