<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Authentication;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ResetPasswordType
 *
 * @package App\UserInterface\Web\Form\Authentication
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class ResetPasswordType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password',
                  RepeatedType::class,
                  [
                      'type'            => PasswordType::class,
                      'first_options'   => [
                          'label' => 'New Password',
                          'attr'  => [
                              'class' => 'form-control',
                          ]
                      ],
                      'second_options'  => [
                          'label' => 'Confirm New Password',
                          'attr'  => [
                              'class' => 'form-control',
                          ]
                      ],
                      'invalid_message' => 'Password confirmation doesn\'t match the password.',
                      'constraints'     => [
                          new NotBlank([
                                           'message' => 'Please enter a password',
                                       ]
                          ),
                          new Length([
                                         'min'        => 6,
                                         'minMessage' => 'Your password should be at least {{ limit }} characters',
                                         // max length allowed by Symfony for security reasons
                                         'max'        => 4096,
                                     ]
                          )
                      ]
                  ]
            );
    }
}
