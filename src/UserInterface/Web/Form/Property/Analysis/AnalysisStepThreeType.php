<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Property\Analysis;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AnalysisStepThreeType
 *
 * @package App\UserInterface\Web\Form\Property\Analysis
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class AnalysisStepThreeType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var RehabCost $rehabCost */
        $rehabCost = $options['rehab_cost'];

        $builder
            ->add('rehabCost', NumberType::class, [
                'label'      => 'Rehab Cost',
                'label_attr' => [
                    'class' => 'mb-2'
                ],
                'help'       => 'Total selling price of a property which includes the down payment and the principal on the loan. <br /><br />Amount in <b>US Dollars (USD)</b>.',
                'help_html'  => true,
                'data'       => ($rehabCost === null) ? null : $rehabCost->inputValue()
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'rehab_cost' => null,
        ]);

        $resolver->addAllowedTypes('rehab_cost', ['null', 'App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost']);
    }
}