<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Property\Analysis;

use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AnalysisStepOneType
 *
 * @package App\UserInterface\Web\Form\Property\Analysis
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class AnalysisStepOneType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Money $purchasePrice */
        $purchasePrice = $options['purchase_price'];

        /** @var Money $afterRepairValue */
        $afterRepairValue = $options['after_repair_value'];

        $builder
            ->add('purchasePrice', NumberType::class, [
                'label'      => 'Purchase Price',
                'label_attr' => [
                    'class' => 'mb-2'
                ],
                'help'       => 'Total selling price of a property which includes the down payment and the principal on the loan. <br /><br />Amount in <b>US Dollars (USD)</b>.',
                'help_html'  => true,
                'data'       => ($purchasePrice === null) ? null : $purchasePrice->amount()
            ])
            ->add('afterRepairValue', NumberType::class, [
                'label'      => 'After Repair Value',
                'label_attr' => [
                    'class' => 'mb-2'
                ],
                'help'       => 'Predictive measure that estimates the future value of an investment property after repair. <br /><br />Amount in <b>US Dollars (USD)</b>.',
                'help_html'  => true,
                'data'       => ($afterRepairValue === null) ? null : $afterRepairValue->amount()
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'purchase_price'     => null,
            'after_repair_value' => null
        ]);

        $resolver->addAllowedTypes('purchase_price', ['null', 'App\Domain\SharedKernel\Generic\ValueObject\Money\Money']);
        $resolver->addAllowedTypes('after_repair_value', ['null', 'App\Domain\SharedKernel\Generic\ValueObject\Money\Money']);
    }
}