<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Property;

use App\UserInterface\Web\Form\Type\Property\PropertyAddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CreatePropertyForm
 *
 * @package App\UserInterface\Web\Form\Property
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class CreatePropertyForm extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'Property Name',
                    'attr'  => [
                        'placeholder' => 'Rehab Property Hillswood, Rental Property n°65, etc...'
                    ]
                ]
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'label'    => 'Property Description',
                    'attr'     => [
                        'placeholder' => 'Add a description...'
                    ],
                    'help'     => 'Add a short description of this property to display in its reports.',
                    'required' => false
                ]
            )
            ->add('address', PropertyAddressType::class);
    }
}