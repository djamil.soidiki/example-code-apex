<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\Property;

use App\UserInterface\Web\Form\Type\Property\PropertyAddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class UpdatePropertyForm
 *
 * @package App\UserInterface\Web\Form\Property
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class UpdatePropertyForm extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'address',
                PropertyAddressType::class,
                [
                    'required' => false
                ]
            );
    }
}