<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\FlipAnalyzer;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddItemSubscriber
 *
 * @package App\UserInterface\Web\Form\FlipAnalyzer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class AddItemSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return [FormEvents::PRE_SUBMIT => 'preSetData'];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $product = $event->getData();
        $form    = $event->getForm();

        var_dump($product); die;

        $form->add('item', ClosingCostItemType::class);
    }
}