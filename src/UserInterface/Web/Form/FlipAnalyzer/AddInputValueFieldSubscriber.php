<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\FlipAnalyzer;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\Domain\SharedKernel\Generic\ValueObject\Money\Money;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

/**
 * Class AddInputValueFieldSubscriber
 *
 * @package App\UserInterface\Web\Form\FlipAnalyzer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class AddInputValueFieldSubscriber implements EventSubscriberInterface
{
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::POST_SUBMIT  => 'onPostSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        $this->formModifier($event->getForm()->getParent(), $event->getData());
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSubmit(FormEvent $event)
    {
        $this->formModifier($event->getForm()->getParent(), $event->getForm()->getData());
    }

    /**
     * @param FormInterface $form
     * @param null          $data
     */
    private function formModifier(FormInterface $form, $data = null)
    {
        if ($data === null || $data === ClosingCost::ITEMIZED_METHOD) {
            $form->add(
                'inputValue',
                CollectionType::class,
                [
                    'entry_type'     => ClosingCostItemType::class,
                    'entry_options'  => ['label' => false],
                    'allow_add'      => true,
                    'prototype_data' => [
                        'name'   => 'foo',
                        'type'   => ClosingCost::LUMP_SUM_METHOD,
                        'amount' => 0,
                    ],
                    'required' => false
                ]
            );

        } else if ($data === ClosingCost::LUMP_SUM_METHOD) {

            $form->add('inputValue', MoneyType::class, [
                'label'    => 'Lump Sum Amount',
                'currency' => 'USD',
            ]);

        } else if ($data === ClosingCost::PERCENTAGE_OF_PURCHASE_PRICE_METHOD) {

            $form->add('inputValue', PercentType::class, [
                'label' => 'Percentage of Purchase',
            ]);
        }
    }
}