<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\FlipAnalyzer;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ClosingCostItemType
 *
 * @package App\UserInterface\Web\Form\FlipAnalyzer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class ClosingCostItemType extends AbstractType
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * ClosingCostType constructor.
     *
     * @param Environment              $twig
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(Environment $twig, EventDispatcherInterface $eventDispatcher)
    {
        $this->twig            = $twig;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'attr'  => [
                    'placeholder' => 'Roof'
                ],
            ])
            ->add('type', ChoiceType::class, [
                'label'   => 'Type',
                'choices' => [
                    'Lump Sum'            => ClosingCost::LUMP_SUM_METHOD,
                    '% of Purchase Price' => ClosingCost::PERCENTAGE_OF_PURCHASE_PRICE_METHOD,
                ],
            ]);

        $builder->get('type')->addEventSubscriber(new AddAmountFieldSubscriber());
    }

    /**
     * @inheritDoc
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $javascriptCode = $this->twig->render('_form/type/closing_cost_item_type.html.twig', [
            'formType'             => $view->vars['id'],
            'closingCostItemType'  => $view->vars['id'] . "_type",
            'closingCostItemValue' => $view->vars['id'] . "_amount"
        ]);

        $this->eventDispatcher->addListener('kernel.response', function ($event) use ($javascriptCode) {

            $response = $event->getResponse();
            $content  = $response->getContent();
            // finding position of </body> tag to add content before the end of the tag
            $pos     = strripos($content, '</body>');
            $content = substr($content, 0, $pos) . $javascriptCode . substr($content, $pos);

            $response->setContent($content);
            $event->setResponse($response);
        });

        parent::buildView($view, $form, $options);
    }
}