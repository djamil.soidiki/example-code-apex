<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Form\FlipAnalyzer;

use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use Assert\AssertionFailedException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ClosingCostType
 *
 * @package App\UserInterface\Web\Form\FlipAnalyzer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
class ClosingCostType extends AbstractType implements DataMapperInterface
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * ClosingCostType constructor.
     *
     * @param Environment              $twig
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(Environment $twig, EventDispatcherInterface $eventDispatcher)
    {
        $this->twig            = $twig;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('inputMethod', ChoiceType::class, [
                'label'   => 'How do you want to calculate the closing cost?',
                'choices' => [
                    'Lump Sum'            => ClosingCost::LUMP_SUM_METHOD,
                    '% of Purchase Price' => ClosingCost::PERCENTAGE_OF_PURCHASE_PRICE_METHOD,
                    'Itemized'            => ClosingCost::ITEMIZED_METHOD,
                ],
            ]);

        $builder->get('inputMethod')->addEventSubscriber(new AddInputValueFieldSubscriber());
        $builder->setDataMapper($this);
    }

    /**
     * @inheritDoc
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $javascriptCode = $this->twig->render('_form/type/closing_cost_type.html.twig', [
            'formType'               => $view->vars['id'],
            'closingCostInputMethod' => $view->vars['id'] . "_inputMethod",
            'closingCostInputValue'  => $view->vars['id'] . "_inputValue"
        ]);

        $this->eventDispatcher->addListener('kernel.response', function ($event) use ($javascriptCode) {

            $response = $event->getResponse();
            $content  = $response->getContent();
            // finding position of </body> tag to add content before the end of the tag
            $pos     = strripos($content, '</body>');
            $content = substr($content, 0, $pos) . $javascriptCode . substr($content, $pos);

            $response->setContent($content);
            $event->setResponse($response);
        });

        parent::buildView($view, $form, $options);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => ClosingCost::class,
            'empty_data'      => null
        ]);
    }

    /**
     * @inheritDoc
     *
     * @param ClosingCost|null $viewData
     */
    public function mapDataToForms($viewData, $forms)
    {
        $forms = iterator_to_array($forms);

        if (array_key_exists('inputMethod', $forms))
            $forms['inputMethod']->setData($viewData ? $viewData->inputMethod() : ClosingCost::LUMP_SUM_METHOD);

        if (array_key_exists('inputValue', $forms)) {

            if ($forms['inputValue']->getConfig()->getType()->getInnerType() instanceof CollectionType)
                $forms['inputValue']->setData($viewData ? $viewData->inputValue() : []);
            else
                $forms['inputValue']->setData($viewData ? $viewData->inputValue() : 0);
        }
    }

    /**
     * @inheritDoc
     * @throws AssertionFailedException
     */
    public function mapFormsToData($forms, &$viewData)
    {
        $forms = iterator_to_array($forms);

        if ($forms['inputMethod']->getData() == null)
            $viewData = new ClosingCost(
                ClosingCost::LUMP_SUM_METHOD,
                '0'
            );
        else
            $viewData = new ClosingCost(
                $forms['inputMethod']->getData(),
                $forms['inputMethod']->getData() === ClosingCost::ITEMIZED_METHOD ? $forms['inputValue']->getData() : (string)$forms['inputValue']->getData()
            );
    }
}