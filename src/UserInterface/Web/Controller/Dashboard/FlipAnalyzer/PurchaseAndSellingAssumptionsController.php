<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\FlipAnalyzer;

use App\UserInterface\Web\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PurchaseAndSellingAssumptionsController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\FlipAnalyzer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/flip_analyzer/purchase_selling_assumptions")
 */
final class PurchaseAndSellingAssumptionsController extends AbstractController
{
    /**
     * @Route("/", name="app_dashboard_flip_analyzer_purchase_selling_assumptions", methods={"GET"})
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->render(
            'dashboard/flip_analyzer/purchase_selling_assumptions.html.twig'
        );
    }
}