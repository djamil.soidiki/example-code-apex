<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\FlipAnalyzer;

use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\FlipAnalyzer\ClosingCostType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ClosingCostsController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\FlipAnalyzer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/flip_analyzer/closing_costs")
 */
final class ClosingCostsController extends AbstractController
{
    /**
     * @Route("/", name="app_dashboard_flip_analyzer_closing_costs", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function index(): Response
    {
        $closingCostsForm = $this->createForm(ClosingCostType::class);
        $closingCostsForm->handleRequest($this->getRequest());

        if ($closingCostsForm->isSubmitted() && $closingCostsForm->isValid())
        {
            /*echo '<pre>';
            var_dump($closingCostsForm->getData());
            die;*/
        }

        return $this->render(
            'dashboard/flip_analyzer/closing_costs.html.twig',
            [
                'closingCostsForm' => $closingCostsForm->createView()
            ]
        );
    }
}