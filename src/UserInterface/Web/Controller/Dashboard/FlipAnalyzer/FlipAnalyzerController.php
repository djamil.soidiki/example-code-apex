<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\FlipAnalyzer;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetClosingCost;
use App\Application\Flipping\Analysis\Query\FindAnalysisByPropertyId;
use App\Application\Flipping\Analysis\Query\GetAnalysisByAnalysisId;
use App\Application\Flipping\Analysis\Query\GetAnalysisWorksheetByAnalysisId;
use App\Application\PropertyManagement\Query\FindPropertiesByManagerId;
use App\Application\PropertyManagement\Query\GetPropertyByPropertyId;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Analysis;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Worksheet;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\FlipAnalyzer\ClosingCostType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FlipAnalyzerController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\FlipAnalyzer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/flip_analyzer")
 */
final class FlipAnalyzerController extends AbstractController
{
    /**
     * @Route("/properties", name="app_dashboard_flip_analyzer", methods={"GET"})
     *
     * @return Response
     */
    public function index(): Response
    {
        /** @var Property[] $properties */
        $properties = $this->handleQuery(new FindPropertiesByManagerId($this->getUser()->getId()));

        return $this->render('dashboard/flip_analyzer/index.html.twig', [
            'properties' => $properties
        ]);
    }

    /**
     * @Route("/property/{propertyIdEncoded}", name="app_dashboard_flip_analyzer_property", methods={"GET", "POST"})
     *
     * @param $propertyIdEncoded
     *
     * @return Response
     */
    public function property($propertyIdEncoded): Response
    {
        $propertyId = PropertyId::decode($propertyIdEncoded);

        $property = $this->handleQuery(new GetPropertyByPropertyId($propertyId));

        /** @var Analysis[] $analysis */
        $analysis = $this->handleQuery(new FindAnalysisByPropertyId($propertyId));

        return $this->render('dashboard/flip_analyzer/property.html.twig', [
            'property' => $property,
            'analysis' => $analysis
        ]);
    }

    /**
     * @Route(
     *     "/property/{propertyIdEncoded}/analysis/{analysisIdEncoded}",
     *     name="app_dashboard_flip_analyzer_property_analysis",
     *     methods={"GET", "POST"}
     * )
     *
     * @param $propertyIdEncoded
     * @param $analysisIdEncoded
     *
     * @return Response
     */
    public function analysis($propertyIdEncoded, $analysisIdEncoded): Response
    {
        $propertyId = PropertyId::decode($propertyIdEncoded);
        $analysisId = AnalysisId::decode($analysisIdEncoded);

        /** @var Property $property */
        $property = $this->handleQuery(new GetPropertyByPropertyId($propertyId));

        /** @var Analysis $analysis */
        $analysis = $this->handleQuery(new GetAnalysisByAnalysisId($analysisId));

        /** @var Worksheet $worksheet */
        $worksheet = $this->handleQuery(new GetAnalysisWorksheetByAnalysisId($analysisId));

        $closingCost = ($worksheet->closingCost()->inputMethod() === null && $worksheet->closingCost()->inputValue() === null) ? null : $worksheet->closingCost();

        $closingCostForm = $this->createForm(ClosingCostType::class, $closingCost);
        $closingCostForm->handleRequest($this->getRequest());

        if ($closingCostForm->isSubmitted() && $closingCostForm->isValid()) {

            /** @var ClosingCost $closingCost */
            $closingCost = $closingCostForm->getData();

            $this->handleCommand(new ChangeWorksheetClosingCost($analysisId, $closingCost->inputMethod(), $closingCost->inputValue()));

            $this->addFlash('success', 'Closing cost updated!');
        }

        return $this->render('dashboard/flip_analyzer/analysis.html.twig', [
            'property'        => $property,
            'analysis'        => $analysis,
            'closingCostForm' => $closingCostForm->createView()
        ]);
    }
}