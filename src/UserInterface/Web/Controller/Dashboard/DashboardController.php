<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard;

use App\Application\Billing\Customer\Query\GetCustomerByUserIdQuery;
use App\Application\PropertyManagement\Command\CreateProperty;
use App\Application\PropertyManagement\Query\FindPropertiesByManagerId;
use App\Application\TrialPeriod\Query\GetTrialPeriodByCustomerId;
use App\Application\TrialPeriod\Query\IsCustomerOnTrialPeriod;
use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Property\CreatePropertyForm;
use Assert\AssertionFailedException;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 *
 * @package App\UserInterface\Web\Controller\Dashboard
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="app_dashboard", methods={"GET", "POST"})
     *
     * @return Response
     * @throws Exception
     * @throws AssertionFailedException
     */
    public function dashboard(): Response
    {
        /** @var Customer $customer */
        $customer = $this->handleQuery(new GetCustomerByUserIdQuery($this->security->getUser()->getId()));

        $isCustomerOnTrialPeriod = $this->handleQuery(new IsCustomerOnTrialPeriod($customer->getId()));

        if ($isCustomerOnTrialPeriod) {
            $trialPeriod              = $this->handleQuery(new GetTrialPeriodByCustomerId($customer->getId()));
            $trialPeriodDaysRemaining = $trialPeriod->endDate()->diff(new \DateTimeImmutable())->days;
        }

        $createPropertyForm = $this->createForm(CreatePropertyForm::class);
        $createPropertyForm->handleRequest($this->getRequest());

        if ($createPropertyForm->isSubmitted() && $createPropertyForm->isValid()) {

            $propertyName        = $createPropertyForm->getData()['name'];
            $propertyDescription = $createPropertyForm->getData()['description'];

            /** @var PropertyAddress $propertyAddress */
            $propertyAddress = $createPropertyForm->getData()['address'];

            $propertyId = $this->handleCommand(new CreateProperty($this->getUser()->getId(), $propertyName, $propertyDescription, $propertyAddress->serialize()));
            $this->addFlash('success', 'Property created!');

            return $this->redirectToRoute('app_dashboard_property_view', [
                'propertyIdEncoded' => (new PropertyId($propertyId))->encode()
            ]);
        }

        /** @var Property[] $properties */
        $properties = $this->handleQuery(new FindPropertiesByManagerId($this->security->getUser()->getId()));

        return $this->render('dashboard/dashboard.html.twig', [
            'properties'         => $properties,
            'createPropertyForm' => $createPropertyForm->createView(),
            'customer'           => [
                'isOnTrialPeriod'          => $isCustomerOnTrialPeriod,
                'trialPeriodDaysRemaining' => (isset($trialPeriodDaysRemaining)) ? $trialPeriodDaysRemaining : null
            ]
        ]);
    }
}