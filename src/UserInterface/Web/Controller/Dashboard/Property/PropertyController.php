<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Property;

use App\Application\Flipping\Analysis\Query\FindAnalysisByPropertyId;
use App\Application\PropertyManagement\Query\GetPropertyByPropertyId;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Analysis;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use Assert\AssertionFailedException;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropertyController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Property
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/property")
 */
final class PropertyController extends AbstractPropertyController
{
    /**
     * @Route("/{propertyIdEncoded}/view", name="app_dashboard_property_view", methods={"GET", "POST"})
     *
     * @param string $propertyIdEncoded
     *
     * @return Response
     * @throws Exception
     * @throws AssertionFailedException
     */
    public function propertyOverview(string $propertyIdEncoded): Response
    {
        $propertyId = PropertyId::decode($propertyIdEncoded);

        /** @var Property $property */
        $property = $this->handleQuery(new GetPropertyByPropertyId($propertyId));

        /** @var Analysis[] $propertyAnalysis */
        $propertyAnalysis = $this->handleQuery(new FindAnalysisByPropertyId($propertyId));

        /*$formAddAnalysis = $this->createForm(AddAnalysisType::class);
        $formAddAnalysis->handleRequest($this->getRequest());

        if ($formAddAnalysis->isSubmitted() && $formAddAnalysis->isValid()) {
            $analysisName       = $formAddAnalysis->getData()['name'];
            $propertyAnalysisId = $this->handleCommand(new AddPropertyAnalysisCommand($propertyId, $analysisName));

            $this->addFlash('success', "Congratulations! Analysis <b>$analysisName</b> has been added to your property");

            return $this->redirectToRoute(
                'app_dashboard_property_analysis_view',
                [
                    'propertyId'         => (new PropertyId($propertyId))->encode(),
                    'propertyAnalysisId' => (new AnalysisId($propertyAnalysisId))->encode()
                ]
            );
        }*/

        return $this->render(
            'dashboard/property/view.html.twig',
            [
                'property'         => $property,
                'propertyAnalysis' => $propertyAnalysis,
                //'formAddAnalysis'  => $formAddAnalysis->createView(),
            ]
        );
    }
}