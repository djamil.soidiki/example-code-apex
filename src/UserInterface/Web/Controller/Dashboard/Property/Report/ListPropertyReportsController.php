<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Property\Report;

use App\Application\Project\Query\FindProjectReportsByProjectIdQuery;
use App\Application\Project\Query\GetProjectByProjectIdQuery;
use App\Application\Project\Query\GetProjectReportByProjectReportIdQuery;
use App\Domain\Project\ValueObject\ProjectId;
use App\Domain\Project\ValueObject\ProjectReportId;
use App\Infrastructure\Project\Query\Model\ProjectModel;
use App\Infrastructure\Project\Query\Model\ProjectReportModel;
use App\UserInterface\Web\Controller\Dashboard\Property\AbstractPropertyController;
use App\UserInterface\Web\Form\Project\Report\SearchReportType;
use Assert\AssertionFailedException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProjectReportController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Property\Report
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/project/{projectId}/reports")
 */
final class ListPropertyReportsController extends AbstractPropertyController
{
    /**
     * @Route("/", name="app_dashboard_project_reports_list", methods={"GET", "POST"})
     *
     * @param $projectId
     *
     * @return Response
     * @throws AssertionFailedException
     */
    public function listReports($projectId): Response
    {
        $projectId = ProjectId::decode($projectId);

        /** @var ProjectModel $project */
        $project = $this->handleQuery(new GetProjectByProjectIdQuery($projectId));

        $queryParams = [];
        $queryParams = array_merge($queryParams, $this->getRequest()->query->all());

        if (isset($queryParams['analysisId']) && $queryParams['analysisId'] === "0") {
            unset($queryParams['analysisId']);
            $queryParams['projectId'] = $project->projectId()->encode();

            return $this->redirectToRoute('app_dashboard_project_reports_list', $queryParams);
        }

        /** @var ProjectReportModel[] $reports */
        $reports = $this->handleQuery(new FindProjectReportsByProjectIdQuery($projectId, $queryParams));

        $paramsFormSearchReport = [
            'project_id' => $projectId
        ];
        if (isset($queryParams['analysisId']) && !empty($queryParams['analysisId']))
            $paramsFormSearchReport['analysis_id'] = $queryParams['analysisId'];

        if (isset($queryParams['q']) && !empty($queryParams['q']))
            $paramsFormSearchReport['q'] = $queryParams['q'];

        $formSearchReport = $this->createForm(SearchReportType::class, null, $paramsFormSearchReport);
        $formSearchReport->handleRequest($this->getRequest());

        if ($formSearchReport->isSubmitted() && $formSearchReport->isValid()) {

            $queryParams['projectId'] = $project->projectId()->encode();

            $analysisId = $formSearchReport->getData()['analysisId'];
            $query      = $formSearchReport->getData()['q'];

            if ($analysisId !== null)
                $queryParams['analysisId'] = $analysisId;

            $queryParams['q'] = $query;

            return $this->redirectToRoute('app_dashboard_project_reports_list', $queryParams);
        }

        return $this->renderProjectView('dashboard/project/report/list.html.twig', [
            'project'          => $project,
            'reports'          => $reports,
            'formSearchReport' => $formSearchReport->createView(),
            'isSearching'      => false
        ]);
    }

    /**
     * @Route("/download-report/{reportId}", name="app_dashboard_project_reports_download")
     *
     * @param string $reportId
     *
     * @return Response
     */
    public function downloadPDFFile(string $reportId): Response
    {
        $projectReportId = ProjectReportId::decode($reportId);

        /** @var ProjectReportModel $report */
        $report = $this->handleQuery(new GetProjectReportByProjectReportIdQuery($projectReportId));

        $response = new BinaryFileResponse($report->filePath()->serialize() . $report->projectId()->serialize() . '/' . $report->fileName()->serialize());

        $response->headers->set('Content-Type','application/pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $report->fileName()->serialize());

        return $response;
    }
}