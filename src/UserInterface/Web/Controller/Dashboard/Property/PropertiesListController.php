<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Property;

use App\Application\PropertyManagement\Command\CreateProperty;
use App\Application\PropertyManagement\Query\FindPropertiesByManagerId;
use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use App\UserInterface\Web\Form\Property\CreatePropertyForm;
use Assert\AssertionFailedException;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropertiesListController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Property
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/properties")
 */
final class PropertiesListController extends AbstractPropertyController
{
    /**
     * @Route("/", name="app_dashboard_properties_list", methods={"GET","POST"})
     *
     * @return Response
     * @throws Exception
     * @throws AssertionFailedException
     */
    public function propertiesList(): Response
    {
        /** @var Property[] $properties */
        $properties = $this->handleQuery(new FindPropertiesByManagerId($this->getUser()->getId()));

        $createPropertyForm = $this->createForm(CreatePropertyForm::class);
        $createPropertyForm->handleRequest($this->getRequest());

        if ($createPropertyForm->isSubmitted() && $createPropertyForm->isValid()) {

            $propertyName        = $createPropertyForm->getData()['name'];
            $propertyDescription = $createPropertyForm->getData()['description'];

            /** @var PropertyAddress $propertyAddress */
            $propertyAddress = $createPropertyForm->getData()['address'];

            $propertyId = $this->handleCommand(new CreateProperty($this->getUser()->getId(), $propertyName, $propertyDescription, $propertyAddress->serialize()));

            $this->addFlash('success', "Your property named <b>$propertyName</b> has been created.");

            return $this->redirectToRoute('app_dashboard_property_view', [
                'propertyIdEncoded' => (new PropertyId($propertyId))->encode()
            ]);
        }

        return $this->render('dashboard/property/list.html.twig', [
            'properties'         => $properties,
            'createPropertyForm' => $createPropertyForm->createView(),
            'isSearching'        => false
        ]);
    }
}