<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Property\Analysis;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetAfterRepairValue;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetClosingCost;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetHoldingCost;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetHoldingPeriod;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetPurchasePrice;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetRehabCost;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetSellingCost;
use App\Application\Flipping\Analysis\Query\GetAnalysisByAnalysisId;
use App\Application\Flipping\Analysis\Query\GetAnalysisWorksheetByAnalysisId;
use App\Application\PropertyManagement\Query\GetPropertyByPropertyId;
use App\Domain\Flipping\Analysis\ValueObject\AnalysisId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Analysis;
use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Worksheet;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use App\UserInterface\Web\Controller\Dashboard\Property\AbstractPropertyController;
use App\UserInterface\Web\Form\Property\Analysis\AnalysisStepOneType;
use App\UserInterface\Web\Form\Property\Analysis\AnalysisStepThreeType;
use App\UserInterface\Web\Form\Type\Property\Analysis\ClosingCostType;
use App\UserInterface\Web\Form\Type\Property\Analysis\HoldingCostType;
use App\UserInterface\Web\Form\Type\Property\Analysis\SellingCostType;
use Assert\AssertionFailedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropertyAnalysisController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Property\Analysis
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/property/{propertyIdEncoded}")
 */
final class PropertyAnalysisController extends AbstractPropertyController
{
    /**
     * @Route("/analysis/{analysisIdEncoded}/view", name="app_dashboard_property_analysis_view", methods={"GET", "POST"})
     *
     * @param string $propertyIdEncoded
     * @param string $analysisIdEncoded
     *
     * @return Response
     * @throws AssertionFailedException
     */
    public function view(string $propertyIdEncoded, string $analysisIdEncoded): Response
    {
        $propertyId = PropertyId::decode($propertyIdEncoded);
        $analysisId = AnalysisId::decode($analysisIdEncoded);

        /** @var Property $property */
        $property = $this->handleQuery(new GetPropertyByPropertyId($propertyId));

        /** @var Analysis $analysis */
        $analysis = $this->handleQuery(new GetAnalysisByAnalysisId($analysisId));

        /** @var Worksheet $worksheet */
        $worksheet = $this->handleQuery(new GetAnalysisWorksheetByAnalysisId($analysisId));

        $formPurchaseAndSellingAssumptions = $this->createForm(AnalysisStepOneType::class, null, [
            'purchase_price'     => $worksheet->purchasePrice(),
            'after_repair_value' => $worksheet->afterRepairValue()
        ]);
        $formPurchaseAndSellingAssumptions->handleRequest($this->getRequest());

        if ($formPurchaseAndSellingAssumptions->isSubmitted() && $formPurchaseAndSellingAssumptions->isValid()) {

            $purchasePrice    = $formPurchaseAndSellingAssumptions->getData()['purchasePrice'];
            $afterRepairValue = $formPurchaseAndSellingAssumptions->getData()['afterRepairValue'];

            $this->handleCommand(new ChangeWorksheetPurchasePrice($analysisId, (int)$purchasePrice));
            $this->handleCommand(new ChangeWorksheetAfterRepairValue($analysisId, (int)$afterRepairValue));

            $this->addFlash('success', 'Purchase and selling assumptions has been updated.');
        }

        $formBuyingCost = $this->createForm(ClosingCostType::class, null, ['closing_cost' => $worksheet->closingCost()]);
        $formBuyingCost->handleRequest($this->getRequest());

        if ($formBuyingCost->isSubmitted() && $formBuyingCost->isValid()) {

            $buyingCost = $formBuyingCost->getData();

            $this->handleCommand(new ChangeWorksheetClosingCost($analysisId, $buyingCost['type'], (string)$buyingCost['value']));

            $this->addFlash('success', 'Buying cost has been updated.');
        }

        $formSellingCost = $this->createForm(SellingCostType::class, null, ['selling_cost' => $worksheet->sellingCost()]);
        $formSellingCost->handleRequest($this->getRequest());

        if ($formSellingCost->isSubmitted() && $formSellingCost->isValid()) {

            $sellingCost = $formSellingCost->getData();

            $this->handleCommand(new ChangeWorksheetSellingCost($analysisId, $sellingCost['type'], (string)$sellingCost['value']));

            $this->addFlash('success', 'Selling cost has been updated.');
        }

        $formHoldingCost = $this->createForm(HoldingCostType::class, null, ['holding_cost' => $worksheet->holdingCost()]);
        $formHoldingCost->handleRequest($this->getRequest());

        if ($formHoldingCost->isSubmitted() && $formHoldingCost->isValid()) {

            $holdingCost = $formHoldingCost->getData();

            $this->handleCommand(new ChangeWorksheetHoldingCost($analysisId, $holdingCost['type'], $holdingCost['value']));
            $this->handleCommand(new ChangeWorksheetHoldingPeriod($analysisId, $holdingCost['holdingPeriod']));

            $this->addFlash('success', 'Holding cost has been updated.');
        }

        $formRehabCost = $this->createForm(AnalysisStepThreeType::class, null, [
            'rehab_cost' => $worksheet->rehabCost()
        ]);
        $formRehabCost->handleRequest($this->getRequest());

        if ($formRehabCost->isSubmitted() && $formRehabCost->isValid()) {
            $rehabCostValue = $formRehabCost->getData()['rehabCost'];

            $this->handleCommand(new ChangeWorksheetRehabCost($analysisId, RehabCost::LUMP_SUM_METHOD, $rehabCostValue));

            $this->addFlash('success', 'Rehab cost has been updated.');
        }

        $progressBarPercent   = 0;
        $progressBarStepsDone = 0;
        $stepsDone            = [];

        if ($worksheet->purchasePrice() != null) {
            ++$progressBarStepsDone;
            $stepsDone[] = 'purchaseAndSellingAssumptions';
        }

        if ($worksheet->closingCost() != null) {
            ++$progressBarStepsDone;
            $stepsDone[] = 'buyingCost';
        }

        if ($worksheet->sellingCost() != null) {
            ++$progressBarStepsDone;
            $stepsDone[] = 'sellingCost';
        }

        if ($worksheet->holdingCost() != null) {
            ++$progressBarStepsDone;
            $stepsDone[] = 'holdingCost';
        }

        if ($worksheet->rehabCost() != null) {
            ++$progressBarStepsDone;
            $stepsDone[] = 'rehabCost';
        }

        if ($progressBarStepsDone > 0)
            $progressBarPercent = (100 * $progressBarStepsDone) / 5;

        return $this->renderPropertyView(
            'dashboard/project/analysis_view.html.twig',
            [
                'property'                          => $property,
                'analysis'                          => $analysis,
                'worksheet'                         => $worksheet,
                'closingCost'                       => 1337,
                'formPurchaseAndSellingAssumptions' => $formPurchaseAndSellingAssumptions->createView(),
                'formBuyingCost'                    => $formBuyingCost->createView(),
                'formSellingCost'                   => $formSellingCost->createView(),
                'formHoldingCost'                   => $formHoldingCost->createView(),
                'formRehabCost'                     => $formRehabCost->createView(),
                'progressBarPercent'                => $progressBarPercent,
                'stepsDone'                         => $stepsDone,
            ]
        );
    }
}
