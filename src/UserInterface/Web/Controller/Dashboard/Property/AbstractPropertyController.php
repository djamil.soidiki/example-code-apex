<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Property;

use App\Infrastructure\Flipping\Analysis\Query\Doctrine\Model\Analysis;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use App\UserInterface\Web\Controller\AbstractController;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Exception;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractPropertyController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Property
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractPropertyController extends AbstractController
{
    /**
     * @param string $view
     * @param array  $parameters
     *
     * @return Response
     * @throws AssertionFailedException
     * @throws Exception
     */
    protected function renderPropertyView(string $view, $parameters = []): Response
    {
        Assertion::keyExists($parameters, 'property');
        Assertion::isInstanceOf($parameters['property'], Property::class);

        $this->handleFormUpdateProperty($this->getFormUpdateProperty($parameters['property']), $parameters['property']);
        $this->handleFormArchiveProperty($this->getFormArchiveProperty(), $parameters['property']);

        if (isset($parameters['analysis'])) {
            Assertion::isInstanceOf($parameters['analysis'], Analysis::class);

            $this->handleFormGenerateReport($this->getFormGenerateReport(), $parameters['analysis']);
        }

        $parameters = array_merge(
            $parameters,
            [
                'no_shadow_navbar'    => true,
                'formUpdateProperty'  => $this->getFormUpdateProperty($parameters['property'])->createView(),
                'formArchiveProperty' => $this->getFormArchiveProperty()->createView(),
                'formGenerateReport'  => $this->getFormGenerateReport()->createView(),
            ]
        );

        return $this->render($view, $parameters);
    }

    /**
     * @param FormInterface $form
     * @param Property $property
     */
    private function handleFormUpdateProperty(FormInterface $form, Property $property): void
    {
        if ($form->isSubmitted() && $form->isValid()) {

            /*$this->handleCommand(new ChangePropertyNameCommand($property->getId(), $form->getData()['name']));
            $this->handleCommand(new ChangePropertyStrategyCommand($property->getId(), $form->getData()['strategy']));
            $this->handleCommand(new ChangePropertyAddressCommand(
                    $property->getId(),
                    $form->getData()['address']['line1'],
                    $form->getData()['address']['line2'],
                    $form->getData()['address']['city'],
                    $form->getData()['address']['state'],
                    $form->getData()['address']['postalCode'],
                    'US'
                )
            );*/

            $this->addFlash('success', "Property <b>" . $property->name() . "</b> has been updated.");
        }
    }

    /**
     * @param FormInterface $form
     * @param Property $property
     */
    private function handleFormArchiveProperty(FormInterface $form, Property $property): void
    {
        if ($form->isSubmitted() && $form->isValid()) {

            //$this->handleCommand(new ArchivePropertyCommand($property->getId()));

            $this->addFlash('success', "Property <b>" . $property->name() . "</b> has been archived.");
        }
    }

    /**
     * @param FormInterface         $form
     * @param Analysis $propertyAnalysis
     *
     * @throws Exception
     */
    private function handleFormGenerateReport(FormInterface $form, Analysis $propertyAnalysis): void
    {
        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form->getData()['name'];

            /*$filePath = $this->container->get('parameter_bag')->get('propertys_analysis_pdf_path');

            $propertyReportId = $this->handleCommand(new GeneratePropertyReportCommand($propertyAnalysis->propertyId()->serialize(), $propertyAnalysis->getId(), $name, $filePath));
            $propertyReportId = new PropertyReportId($propertyReportId);

            $urlForDownloadingPDF = $this->generateUrl('app_dashboard_property_reports_download', [
                'propertyId' => $propertyAnalysis->propertyId()->encode(),
                'reportId'   => $propertyReportId->encode(),
            ]);*/

            $urlForDownloadingPDF = "#";

            $link = '<a href="' . $urlForDownloadingPDF . '" target="_blank">Download Report</a>';

            $this->addFlash('success', "Property Report <b>$name</b> has been generated. <br >You can download your report by clicking the following link: $link");
        }
    }

    /**
     * @return FormInterface
     */
    private function getFormGenerateReport(): FormInterface
    {
        //$form = $this->createForm(GenerateReportType::class);
        $form = $this->createForm(TextareaType::class);
        $form->handleRequest($this->getRequest());

        return $form;
    }

    /**
     * @param Property $property
     *
     * @return FormInterface
     */
    private function getFormUpdateProperty(Property $property): FormInterface
    {
        //$form = $this->createForm(UpdatePropertyType::class, null, ['property' => $property]);
        $form = $this->createForm(TextareaType::class);
        $form->handleRequest($this->getRequest());

        return $form;
    }

    /**
     * @return FormInterface
     */
    private function getFormArchiveProperty(): FormInterface
    {
        $form = $this->createForm(FormType::class);
        $form->handleRequest($this->getRequest());

        return $form;
    }
}