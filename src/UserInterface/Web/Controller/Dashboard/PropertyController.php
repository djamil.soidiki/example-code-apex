<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetAfterRepairValue;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetClosingCost;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetHoldingCost;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetHoldingPeriod;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetPurchasePrice;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetRehabCost;
use App\Application\Flipping\Analysis\Command\ChangeWorksheetSellingCost;
use App\Application\PropertyManagement\Command\ChangePropertyAddress;
use App\Application\PropertyManagement\Command\ChangePropertyDescription;
use App\Application\PropertyManagement\Command\ChangePropertyDetails;
use App\Application\PropertyManagement\Command\ChangePropertyName;
use App\Application\PropertyManagement\Query\GetPropertyByPropertyId;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\HoldingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\RehabCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\SellingCost;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Property\UpdatePropertyForm;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropertyController
 *
 * @package App\UserInterface\Web\Controller\Dashboard
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/properties")
 */
final class PropertyController extends AbstractController
{
    /**
     * @Route("/{propertyIdEncoded}", name="app_dashboard_property_view_old", methods={"GET", "POST"})
     *
     * @param $propertyIdEncoded
     *
     * @return Response
     * @throws Exception
     */
    public function propertyView($propertyIdEncoded): Response
    {
        $propertyId = PropertyId::decode($propertyIdEncoded);

        /** @var Property $property */
        $property = $this->handleQuery(new GetPropertyByPropertyId($propertyId));

        $updatePropertyForm = $this->createForm(UpdatePropertyForm::class);
        $updatePropertyForm->handleRequest($this->getRequest());

        $this->handleCommand(new ChangeWorksheetHoldingPeriod(
            'e6ec1bfd-cf47-4ea6-9d6f-cbff390c0485',
            8
        ));

        /*$this->handleCommand(new ChangeWorksheetPurchasePrice(
            'e6ec1bfd-cf47-4ea6-9d6f-cbff390c0485',
            356000
        ));*/

        /*$this->handleCommand(new ChangeWorksheetAfterRepairValue(
            'e6ec1bfd-cf47-4ea6-9d6f-cbff390c0485',
            425000
        ));*/

        /*$this->handleCommand(new ChangeWorksheetSellingCost(
            'e6ec1bfd-cf47-4ea6-9d6f-cbff390c0485',
            SellingCost::ITEMIZED_METHOD,
            [
                [
                    'name'    => 'Commission',
                    'type'    => SellingCost::ITEM_PERCENT_TYPE,
                    'percent' => 1
                ],
                [
                    'name'   => 'Warranty',
                    'type'   => SellingCost::ITEM_AMOUNT_TYPE,
                    'amount' => 1500
                ],
                [
                    'name'   => 'Insurance',
                    'type'   => SellingCost::ITEM_AMOUNT_TYPE,
                    'amount' => 1000
                ],
            ]
        ));*/

        /*$this->handleCommand(new ChangeWorksheetClosingCost(
            'e6ec1bfd-cf47-4ea6-9d6f-cbff390c0485',
            ClosingCost::ITEMIZED_METHOD,
            [
                [
                    'name'    => 'Closing Costs',
                    'type'    => ClosingCost::ITEM_PERCENT_TYPE,
                    'percent' => 2
                ],
                [
                    'name'   => 'Mouf',
                    'type'   => ClosingCost::ITEM_AMOUNT_TYPE,
                    'amount' => 5200
                ],
                [
                    'name'    => 'taxes',
                    'type'    => ClosingCost::ITEM_PERCENT_TYPE,
                    'percent' => 0.5
                ],
            ]
        ));*/

        $this->handleCommand(new ChangeWorksheetRehabCost(
            'e6ec1bfd-cf47-4ea6-9d6f-cbff390c0485',
            RehabCost::ITEMIZED_METHOD,
            [
                [
                    'name'   => 'Wash',
                    'amount' => 350
                ],
                [
                    'name'   => 'Windows',
                    'amount' => 800
                ],
                [
                    'name'   => 'paint',
                    'amount' => 1100
                ],
                [
                    'name'   => 'carpet',
                    'amount' => 2500
                ],
                [
                    'name'   => 'hvac',
                    'amount' => 13000
                ],
            ]
        ));

        /*$this->handleCommand(new ChangeWorksheetHoldingCost(
            'e6ec1bfd-cf47-4ea6-9d6f-cbff390c0485',
            HoldingCost::ITEMIZED_METHOD,
            [
                [
                    'name'   => 'Property Taxes',
                    'type'   => HoldingCost::ITEM_AMOUNT_TYPE,
                    'amount' => [
                        'value'      => 790,
                        'recurrence' => HoldingCost::PER_YEAR_RECURRENCE
                    ]
                ],
                [
                    'name'   => 'Property Taxes',
                    'type'   => HoldingCost::ITEM_AMOUNT_TYPE,
                    'amount' => [
                        'value'      => 650,
                        'recurrence' => HoldingCost::PER_YEAR_RECURRENCE
                    ]
                ],
                [
                    'name'   => 'Property Taxes',
                    'type'   => HoldingCost::ITEM_AMOUNT_TYPE,
                    'amount' => [
                        'value'      => 305,
                        'recurrence' => HoldingCost::PER_MONTH_RECURRENCE
                    ]
                ],
                [
                    'name'   => 'Property Taxes',
                    'type'   => HoldingCost::ITEM_AMOUNT_TYPE,
                    'amount' => [
                        'value'      => 100,
                        'recurrence' => HoldingCost::PER_MONTH_RECURRENCE
                    ]
                ],
                [
                    'name'    => 'Property Taxes',
                    'type'    => HoldingCost::ITEM_PERCENT_TYPE,
                    'percent' => 2
                ],
            ]
        ));*/

        if ($updatePropertyForm->isSubmitted() && $updatePropertyForm->isValid()) {

            $name        = $updatePropertyForm->getData()['name'];
            $description = $updatePropertyForm->getData()['description'];

            $this->handleCommand(new ChangePropertyName($propertyId, $name));
            $this->handleCommand(new ChangePropertyDescription($propertyId, $description));
            $this->handleCommand(
                new ChangePropertyAddress(
                    $propertyId,
                    $updatePropertyForm->getData()['address']['streetAddress'],
                    $updatePropertyForm->getData()['address']['city'],
                    $updatePropertyForm->getData()['address']['state'],
                    $updatePropertyForm->getData()['address']['zipCode']
                )
            );

            $this->handleCommand(
                new ChangePropertyDetails(
                    $propertyId,
                    5,
                    5,
                    2,
                    6000,
                    2018,
                    1.53
                )
            );

            $this->addFlash('success', 'Property updated!');
        }

        return $this->render('dashboard/property_view.html.twig', [
            'property'           => $property,
            'updatePropertyForm' => $updatePropertyForm->createView()
        ]);
    }
}