<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Contact\Group;

use App\Application\ContactManagement\Group\Command\DeleteGroup;
use App\Application\ContactManagement\Group\Query\GetGroupByGroupId;
use App\Domain\ContactManagement\Group\ValueObject\GroupId;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\UserInterface\Web\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DeleteContactController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Contact\Group
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/contact/list/{groupId}")
 */
final class DeleteGroupController extends AbstractController
{
    /**
     * @Route("/delete", name="app_dashboard_contact_list_delete", methods={"GET"})
     *
     * @param $groupId
     *
     * @return Response
     */
    public function delete($groupId): Response
    {
        $groupId = GroupId::decode($groupId);

        /** @var Group $group */
        $group = $this->handleQuery(new GetGroupByGroupId($groupId));

        $groupName = $group->name();

        $this->handleCommand(new DeleteGroup($groupId));

        $this->addFlash('success', "The list '<b>$groupName</b>' has been deleted");

        return $this->redirectToRoute('app_dashboard_contact_list_list');
    }
}