<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Contact\Group;

use App\Application\ContactManagement\Group\Command\ChangeGroupDescription;
use App\Application\ContactManagement\Group\Command\ChangeGroupName;
use App\Application\ContactManagement\Group\Command\CreateGroup;
use App\Application\ContactManagement\Group\Query\FindGroupsByOwnerId;
use App\Domain\ContactManagement\Group\Exception\GroupNameIsNotUniqueException;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Contact\Group\CreateGroupType;
use App\UserInterface\Web\Form\Contact\Group\EditGroupType;
use Exception;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ListContactsController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Contact\Group
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/contact/lists")
 */
final class ListGroupsController extends AbstractController
{
    /**
     * @Route("/", name="app_dashboard_contact_list_list", methods={"GET", "POST"})
     *
     * @return Response
     * @throws Exception
     */
    public function list(): Response
    {
        $formCreateGroup = $this->createForm(CreateGroupType::class);
        $formCreateGroup->handleRequest($this->getRequest());

        if ($formCreateGroup->isSubmitted() && $formCreateGroup->isValid()) {

            $name        = $formCreateGroup->getData()['name'];
            $description = $formCreateGroup->getData()['description'];

            try {
                // TODO Add description in create group command

                $groupId = $this->handleCommand(new CreateGroup($this->getUser()->getId(), $name));

                if ($description !== null) {
                    $this->handleCommand(new ChangeGroupDescription($groupId, $description));
                }

                $this->addFlash('success', "Your contact list named <b>$name</b> has been created.");

                return $this->redirectToRoute('app_dashboard_contact_list_list');

            } catch (HandlerFailedException $exception) {
                $throwable = $this->getThrowable($exception);

                if ($throwable instanceof GroupNameIsNotUniqueException)
                    $this->addFlash('error', "A contact list with the name <b>$name</b> already exists. Contact list name must be unique.");
            }
        }

        $groups = $this->handleQuery(new FindGroupsByOwnerId($this->getUser()->getId()));

        $forms      = [];
        $formsViews = [];

        /** @var Group $group */
        foreach ($groups as $group) {

            $group    = $group['data'];
            $nameForm = "formEditGroup_" . $group->groupId()->serialize();

            $$nameForm = $this->createForm(EditGroupType::class, null, ['group' => $group]);
            $$nameForm->handleRequest($this->getRequest());

            if ($$nameForm->isSubmitted() && $$nameForm->isValid()) {

                $name        = $$nameForm->getData()['name'];
                $description = $$nameForm->getData()['description'];

                try {
                    $this->handleCommand(new ChangeGroupName($this->getUser()->getId(), $group->groupId()->serialize(), $name));

                    if ($description !== null) {
                        $this->handleCommand(new ChangeGroupDescription($group->groupId()->serialize(), $description));
                    }

                    $this->addFlash('success', "Your contact list named <b>$name</b> has been updated.");

                } catch (HandlerFailedException $exception) {
                    $throwable = $this->getThrowable($exception);

                    if ($throwable instanceof GroupNameIsNotUniqueException)
                        $this->addFlash('error', "A contact list with the name <b>$name</b> already exists. Contact list name must be unique.");
                }
            }

            $forms[$group->groupId()->serialize()] = $$nameForm;
        }

        /** @var FormInterface $form */
        foreach ($forms as $groupId => $form) {
            $formsViews[$groupId] = $form->createView();
        }

        return $this->render('dashboard/contact/contact_list/list.html.twig',
            [
                'groups'          => $groups,
                'formCreateGroup' => $formCreateGroup->createView(),
                'formsViews'      => $formsViews
            ]
        );
    }
}