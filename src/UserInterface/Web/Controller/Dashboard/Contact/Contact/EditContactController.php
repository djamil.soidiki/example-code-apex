<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Contact\Contact;

use App\Application\ContactManagement\Contact\Command\ChangeContactEmailAddress;
use App\Application\ContactManagement\Contact\Command\ChangeContactFullName;
use App\Application\ContactManagement\Contact\Command\ChangeContactPhoneNumber;
use App\Application\ContactManagement\Contact\Query\GetContactByContactId;
use App\Application\ContactManagement\Group\Command\AddContactToGroup;
use App\Application\ContactManagement\Group\Command\RemoveContactFromGroup;
use App\Application\ContactManagement\Group\Query\FindGroupsByOwnerId;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model\Contact;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Contact\Contact\EditContactType;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EditContactController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Contact\Contact
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/contact/{contactId}")
 */
final class EditContactController extends AbstractController
{
    /**
     * @Route("/view", name="app_dashboard_contact_edit", methods={"GET", "POST"})
     *
     * @param string $contactId
     *
     * @return Response
     * @throws Exception
     */
    public function view(string $contactId): Response
    {
        $contactId = ContactId::decode($contactId);

        /** @var Group[] $groups */
        $groups = $this->handleQuery(new FindGroupsByOwnerId($this->getUser()->getId()));

        $groupsIds = [];

        /** @var Group $group */
        foreach ($groups as $group) {
            $group = $group['data'];

            $groupsIds[] = $group->groupId()->serialize();
        }

        $formEditContact = $this->createForm(EditContactType::class, null, ['user_id' => $this->getUser()->getId(), 'contact_id' => $contactId]);
        $formEditContact->handleRequest($this->getRequest());

        if ($formEditContact->isSubmitted() && $formEditContact->isValid()) {
            $firstName       = $formEditContact->getData()['firstName'];
            $lastName        = $formEditContact->getData()['lastName'];
            $emailAddress    = $formEditContact->getData()['emailAddress'];
            $phoneNumber     = $formEditContact->getData()['phoneNumber'];
            $groupsOfContact = $formEditContact->getData()['groups'];

            $groupsToRemove = array_diff($groupsIds, $groupsOfContact);

            foreach ($groupsOfContact as $groupOfContact) {
                $this->handleCommand(new AddContactToGroup($groupOfContact, $contactId));
            }

            foreach ($groupsToRemove as $groupToRemove) {
                $this->handleCommand(new RemoveContactFromGroup($groupToRemove, $contactId));
            }

            $this->handleCommand(new ChangeContactFullName($contactId, $firstName, $lastName));
            $this->handleCommand(new ChangeContactEmailAddress($contactId, $emailAddress));

            if ($phoneNumber !== null)
                $this->handleCommand(new ChangeContactPhoneNumber($contactId, 'US', $phoneNumber));

            $this->addFlash('success', 'The contact has been updated');
        }

        /** @var Contact $contact */
        $contact = $this->handleQuery(new GetContactByContactId($contactId));

        return $this->render('dashboard/contact/contact/edit.html.twig', [
            'contact'         => $contact,
            'groups'          => $groups,
            'formEditContact' => $formEditContact->createView()
        ]);
    }
}