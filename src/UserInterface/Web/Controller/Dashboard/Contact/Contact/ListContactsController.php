<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Contact\Contact;

use App\Application\ContactManagement\Contact\Command\CreateContact;
use App\Application\ContactManagement\Contact\Query\FindContactsByOwnerId;
use App\Application\ContactManagement\Group\Command\AddContactToGroup;
use App\Application\ContactManagement\Group\Query\FindGroupsByContactId;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model\Contact;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Contact\Contact\CreateContactType;
use App\UserInterface\Web\Form\Contact\Contact\SearchContactType;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ListContactsController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Contact\Contact
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/contacts")
 */
final class ListContactsController extends AbstractController
{
    /**
     * @Route("/", name="app_dashboard_contact_list", methods={"GET", "POST"})
     *
     * @return Response
     * @throws Exception
     */
    public function list(): Response
    {
        $queryParams = [];
        $queryParams = array_merge($queryParams, $this->getRequest()->query->all());

        if (isset($queryParams['listId']) && $queryParams['listId'] === "0") {
            unset($queryParams['listId']);

            return $this->redirectToRoute('app_dashboard_contact_list', $queryParams);
        }

        /** @var Contact[] $contacts */
        $contacts = $this->handleQuery(new FindContactsByOwnerId($this->getUser()->getId(), $queryParams));

        $contactsData = [];

        /** @var Contact $contact */
        foreach ($contacts as $contact) {
            $lists = $this->handleQuery(new FindGroupsByContactId($contact->getId()));

            $contactsData[] = [
                'contact' => $contact,
                'lists'   => $lists
            ];
        }

        $formCreateContact = $this->createForm(CreateContactType::class, null, ['user_id' => $this->getUser()->getId()]);
        $formCreateContact->handleRequest($this->getRequest());

        if ($formCreateContact->isSubmitted() && $formCreateContact->isValid()) {

            $firstName     = $formCreateContact->getData()['firstName'];
            $lastName      = $formCreateContact->getData()['lastName'];
            $emailAddress  = $formCreateContact->getData()['emailAddress'];
            $contactGroups = $formCreateContact->getData()['groups'];

            $contactId = $this->handleCommand(new CreateContact($this->getUser()->getId(), $firstName, $lastName, $emailAddress));

            foreach ($contactGroups as $contactGroup) {
                $this->handleCommand(new AddContactToGroup($contactGroup, $contactId));
            }

            $this->addFlash('success', "Your contact named <b>$firstName $lastName</b> has been created.");

            return $this->redirectToRoute('app_dashboard_contact_list', $queryParams);
        }

        $paramsFormSearchContact = [
            'user_id' => $this->getUser()->getId()
        ];
        if (isset($queryParams['listId']) && !empty($queryParams['listId']))
            $paramsFormSearchContact['group_id'] = $queryParams['listId'];

        if (isset($queryParams['q']) && !empty($queryParams['q']))
            $paramsFormSearchContact['q'] = $queryParams['q'];

        $formSearchContact = $this->createForm(SearchContactType::class, null, $paramsFormSearchContact);
        $formSearchContact->handleRequest($this->getRequest());

        if ($formSearchContact->isSubmitted() && $formSearchContact->isValid()) {

            $groupId = $formSearchContact->getData()['groupId'];
            $query   = $formSearchContact->getData()['q'];

            if ($groupId !== null)
                $queryParams['listId'] = $groupId;

            $queryParams['q'] = $query;

            return $this->redirectToRoute('app_dashboard_contact_list', $queryParams);
        }

        $isSearching = false;
        if (isset($queryParams['listId']) || isset($queryParams['q']))
            $isSearching = true;

        return $this->render('dashboard/contact/contact/list.html.twig',
            [
                'contacts'          => $contactsData,
                'formCreateContact' => $formCreateContact->createView(),
                'formSearchContact' => $formSearchContact->createView(),
                'isSearching'       => $isSearching
            ]
        );
    }
}