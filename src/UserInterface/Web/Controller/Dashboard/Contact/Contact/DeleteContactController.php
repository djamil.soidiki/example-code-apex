<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard\Contact\Contact;

use App\Application\ContactManagement\Contact\Command\DeleteContact;
use App\Application\ContactManagement\Contact\Query\GetContactByContactId;
use App\Application\ContactManagement\Group\Command\RemoveContactFromGroup;
use App\Application\ContactManagement\Group\Query\FindGroupsByContactId;
use App\Domain\ContactManagement\Contact\ValueObject\ContactId;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model\Contact;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\UserInterface\Web\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DeleteContactController
 *
 * @package App\UserInterface\Web\Controller\Dashboard\Contact\Contact
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/dashboard/contact/{contactId}")
 */
final class DeleteContactController extends AbstractController
{
    /**
     * @Route("/delete", name="app_dashboard_contact_delete", methods={"GET"})
     *
     * @param $contactId
     *
     * @return Response
     */
    public function delete($contactId): Response
    {
        $contactId = ContactId::decode($contactId);

        $groups = $this->handleQuery(new FindGroupsByContactId($contactId));

        /** @var Group $group */
        foreach ($groups as $group) {
            $this->handleCommand(new RemoveContactFromGroup($group->groupId()->serialize(), $contactId));
        }

        /** @var Contact $contact */
        $contact = $this->handleQuery(new GetContactByContactId($contactId));

        $emailContact = $contact->emailAddress();

        $this->handleCommand(new DeleteContact($contactId));

        $this->addFlash('success', "<b>$emailContact</b> has been deleted");

        return $this->redirectToRoute('app_dashboard_contact_list');
    }
}