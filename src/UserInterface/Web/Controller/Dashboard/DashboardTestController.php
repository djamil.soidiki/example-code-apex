<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Dashboard;

use App\Application\Flipping\Analysis\Command\ChangeWorksheetClosingCost;
use App\Domain\Flipping\Analysis\ValueObject\WorksheetInput\ClosingCost;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\FlipAnalyzer\ClosingCostType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardTestController
 *
 * @package App\UserInterface\Web\Controller\Dashboard
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class DashboardTestController extends AbstractController
{
    /**
     * @Route("/dashboard_test", name="app_dashboard_test", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function dashboard(): Response
    {
        $closingCostForm = $this->createForm(ClosingCostType::class);
        $closingCostForm->handleRequest($this->getRequest());

        if ($closingCostForm->isSubmitted() && $closingCostForm->isValid()) {

            /** @var ClosingCost $closingCost */
            $closingCost = $closingCostForm->getData();

            $this->handleCommand(new ChangeWorksheetClosingCost());

            $this->addFlash('success', 'Closing cost updated!');
        }

        return $this->render('dashboard/dashboard_test.html.twig', [
            'closingCostForm' => $closingCostForm->createView()
        ]);
    }
}