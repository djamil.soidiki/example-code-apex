<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Home;

use App\UserInterface\Web\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PricingController
 *
 * @package App\UserInterface\Web\Controller\Home
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class PricingController extends AbstractController
{
    /**
     * @Route("/pricing", name="home_pricing", methods={"GET"})
     *
     * @return Response
     */
    public function pricing(): Response
    {
        return $this->render('home/pricing.html.twig');
    }
}