<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Home;

use App\Application\Authentication\Command\RegisterUser;
use App\Application\Billing\Customer\Command\RegisterCustomer;
use App\Application\TrialPeriod\Command\StartTrialPeriod;
use App\Domain\SharedKernel\Authentication\Exception\EmailAddressIsNotUniqueException;
use App\Domain\SharedKernel\Authentication\Exception\EmailAddressIsNotValidException;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Home\TryType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TryController
 *
 * @package App\UserInterface\Web\Controller\Home
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TryController extends AbstractController
{
    /**
     * @Route("/try", name="home_try", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function try(): Response
    {
        if ($this->security->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app_dashboard');
        }

        $formTry = $this->createForm(TryType::class);
        $formTry->handleRequest($this->getRequest());

        if ($formTry->isSubmitted() && $formTry->isValid()) {

            $firstName    = $formTry->getData()['firstName'];
            $lastName     = $formTry->getData()['lastName'];
            $emailAddress = $formTry->getData()['emailAddress'];
            $password     = $formTry->getData()['password'];

            try {
                /** @var string $userId */
                $userId = $this->handleCommand(new RegisterUser($emailAddress, $password));

                /** @var string $customerId */
                $customerId = $this->handleCommand(new RegisterCustomer($userId, $emailAddress, $firstName, $lastName));

                $this->handleCommand(new StartTrialPeriod($customerId));

                $this->authenticateUser($emailAddress);

                return $this->redirectToRoute('app_dashboard', ['from' => 'try']);

            } catch (HandlerFailedException $exception) {
                $throwable = $this->getThrowable($exception);

                if ($throwable instanceof EmailAddressIsNotUniqueException)
                    $this->addFlash('error', 'Email is already used by another user. Please choose another email.');
                else if ($throwable instanceof EmailAddressIsNotValidException)
                    $this->addFlash('error', 'Enter a valid email address.');
            }
        }

        return $this->render(
            'home/try.html.twig',
            [
                'formTry' => $formTry->createView()
            ]
        );
    }
}