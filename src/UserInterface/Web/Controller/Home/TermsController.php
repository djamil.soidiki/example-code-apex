<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Home;

use App\UserInterface\Web\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TermsController
 *
 * @package App\UserInterface\Web\Controller\Home
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class TermsController extends AbstractController
{
    /**
     * @Route("/terms", name="home_terms", methods={"GET"})
     * @return Response
     */
    public function terms(): Response
    {
        return $this->render('home/terms.html.twig');
    }

    /**
     * @Route("/privacy", name="home_privacy", methods={"GET"})
     * @return Response
     */
    public function privacyPolicy(): Response
    {
        return $this->render('home/privacy.html.twig');
    }
}