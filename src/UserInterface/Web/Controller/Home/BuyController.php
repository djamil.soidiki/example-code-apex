<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Home;

use App\Application\Authentication\Command\RegisterUser;
use App\Application\Billing\Customer\Command\RegisterCustomer;
use App\Application\Billing\Customer\Command\ChangeCustomerPaymentMethod;
use App\Application\Billing\Customer\Query\GetCustomerByUserIdQuery;
use App\Application\Billing\Customer\Query\IsUserACustomer;
use App\Application\Billing\Subscription\Command\SubscribeToPlan;
use App\Application\Billing\Subscription\Query\IsCustomerSubscribed;
use App\Domain\SharedKernel\Authentication\Exception\EmailAddressIsNotUniqueException;
use App\Domain\SharedKernel\Authentication\Exception\EmailAddressIsNotValidException;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Home\BuyType;
use Exception;
use Stripe\Exception\CardException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BuyController
 *
 * @package App\UserInterface\Web\Controller\Home
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class BuyController extends AbstractController
{
    // TODO get default plan and show default price to customer
    // TODO encrypt secret key

    /**
     * @var Customer|null;
     */
    private $customer;

    /**
     * @Route("/buy", name="home_buy", methods={"GET", "POST"})
     *
     * @return Response
     * @throws Exception
     */
    public function buy(): Response
    {
        if ($this->security->getUser() !== null && $this->handleQuery(new IsUserACustomer($this->security->getUser()->getId()))) {

            $this->customer = $this->handleQuery(new GetCustomerByUserIdQuery($this->security->getUser()->getId()));

            if ($this->handleQuery(new IsCustomerSubscribed($this->customer->getId()))) {
                $this->addFlash('info', 'You are already subscribed. To manage your subscription please go on your account settings.');
                return $this->redirectToRoute('app_dashboard');
            }
        }

        $formBuy = $this->getForm();
        if ($formBuy->isSubmitted() && $formBuy->isValid()) {

            $response = null;
            if (!$this->security->isGranted('ROLE_USER'))
                $response = $this->anonymousUserIsBuying($formBuy->getData());
            else
                $response = $this->authenticatedUserIsBuying($formBuy->getData());

            if ($response !== null) {
                $fullName = $formBuy->getData()['firstName'] . ' ' . $formBuy->getData()['lastName'];
                $this->addFlash('info', "Welcome $fullName! You successfully subscribe to Reavant.");

                return $this->redirectToRoute('app_dashboard', ['from' => 'buy']);
            } else {
                return $this->redirectToRoute('home_buy');
            }
        }

        return $this->render(
            'home/buy.html.twig',
            [
                'formBuy' => $formBuy->createView()
            ]
        );
    }

    /**
     * @return FormInterface
     */
    private function getForm(): FormInterface
    {
        if ($this->security->isGranted('ROLE_USER')) {

            $params['user_authenticated'] = true;

            try {
                $params['full_name'] = $this->customer->fullName();

            } catch (HandlerFailedException $exception) {
            }

            $form = $this->createForm(BuyType::class, null, $params);
        } else {
            $form = $this->createForm(BuyType::class);
        }

        $form->handleRequest($this->getRequest());

        return $form;
    }

    /**
     * @param array $formData
     *
     * @return Response|null
     */
    private function anonymousUserIsBuying(array $formData): ?Response
    {
        try {
            $userId     = $this->handleCommand(new RegisterUser($formData['emailAddress'], $formData['password']));
            $customerId = $this->handleCommand(new RegisterCustomer(
                    $userId,
                    $formData['emailAddress'],
                    $formData['firstName'],
                    $formData['lastName'],
                    $formData['paymentMethod']
                )
            );

            $this->handleCommand(new SubscribeToPlan($customerId, $formData['plan']));

            return $this->authenticateUser($formData['emailAddress']);
        } catch (HandlerFailedException $exception) {
            $throwable = $this->getThrowable($exception);

            if ($throwable instanceof EmailAddressIsNotUniqueException)
                $this->addFlash('error', 'Email is already used by another user. Please choose another email.');
            else if ($throwable instanceof EmailAddressIsNotValidException)
                $this->addFlash('error', 'Enter a valid email address.');
            else if ($throwable instanceof CardException) {
                $this->addFlash('warning', 'You successfully registered and you was logged in but something went wrong with the payment.');
                $this->addFlash('error', $throwable->getMessage());

                $this->authenticateUser($formData['emailAddress']);
            }

            return null;
        }
    }

    /**
     * @param array $formData
     *
     * @return Response|null
     */
    private function authenticatedUserIsBuying(array $formData): ?Response
    {
        try {
            if (!$this->customer) {
                $customerId = $this->handleCommand(new RegisterCustomer(
                        $this->getUser()->getId(),
                        $this->getUser()->getUsername(),
                        $formData['firstName'],
                        $formData['lastName'],
                        $formData['paymentMethod']
                    )
                );
            } else {
                $customerId = $this->customer->customerId()->serialize();
                $this->handleCommand(new ChangeCustomerPaymentMethod($customerId, $formData['paymentMethod']));
            }

            $this->handleCommand(new SubscribeToPlan($customerId, $formData['plan']));

            return $this->authenticateUser($this->getUser()->getUsername());
        } catch (HandlerFailedException $exception) {
            $throwable = $this->getThrowable($exception);

            if ($throwable instanceof CardException)
                $this->addFlash('error', $throwable->getMessage());

            return null;
        }
    }
}