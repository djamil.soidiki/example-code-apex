<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Home;

use App\UserInterface\Web\Controller\AbstractController;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 *
 * @package App\UserInterface\Web\Controller\Home
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     *
     * @return Response
     * @throws Exception
     */
    public function home()
    {
        return $this->render('home/home.html.twig');
    }
}