<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Account;

use App\Application\Authentication\Command\ChangeUserEmailAddress;
use App\Application\Authentication\Command\ChangeUserPassword;
use App\Application\Billing\Customer\Command\ChangeCustomerFullName;
use App\Application\Billing\Customer\Query\GetCustomerByUserIdQuery;
use App\Application\Billing\Subscription\Query\GetSubscriptionByCustomerId;
use App\Application\Billing\Subscription\Query\IsCustomerSubscribed;
use App\Application\TrialPeriod\Query\GetTrialPeriodByCustomerId;
use App\Application\TrialPeriod\Query\IsCustomerOnTrialPeriod;
use App\Domain\SharedKernel\Authentication\Exception\InvalidCurrentPasswordException;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Account\ChangeEmailType;
use App\UserInterface\Web\Form\Account\ChangeNameType;
use App\UserInterface\Web\Form\Account\ChangePasswordType;
use Exception;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 *
 * @package App\UserInterface\Web\Controller\Account
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/account")
 */
final class AccountController extends AbstractController
{
    /**
     * @Route("/general", name="home_account", methods={"GET", "POST"})
     *
     * @return Response
     * @throws Exception
     */
    public function account(): Response
    {
        $customer                = $this->handleQuery(new GetCustomerByUserIdQuery($this->security->getUser()->getId()));
        $isCustomerOnTrialPeriod = $this->handleQuery(new IsCustomerOnTrialPeriod($customer->getId()));
        $isCustomerSubscribed    = $this->handleQuery(new IsCustomerSubscribed($customer->getId()));

        $trialPeriod = null;
        if ($isCustomerOnTrialPeriod)
            $trialPeriod = $this->handleQuery(new GetTrialPeriodByCustomerId($customer->getId()));

        $subscription = null;
        if ($isCustomerSubscribed)
            $subscription = $this->handleQuery(new GetSubscriptionByCustomerId($customer->getId()));

        $changeFullNameForm     = $this->createAndHandleChangeFullNameForm($customer);
        $changeEmailAddressForm = $this->createAndHandleChangeEmailAddressForm($customer);
        $changePasswordForm     = $this->createAndHandleChangePasswordForm();

        // TODO get stripe data for next billing info

        return $this->render(
            'account/account.html.twig',
            [
                'customer'                => $customer,
                'isCustomerOnTrialPeriod' => $isCustomerOnTrialPeriod,
                'isCustomerSubscribed'    => $isCustomerSubscribed,
                'trial'                   => $trialPeriod,
                'subscription'            => $subscription,
                'changeFullNameForm'      => $changeFullNameForm->createView(),
                'changeEmailAddressForm'  => $changeEmailAddressForm->createView(),
                'changePasswordForm'      => $changePasswordForm->createView(),
            ]
        );
    }

    /**
     * @param Customer $customer
     *
     * @return FormInterface
     */
    private function createAndHandleChangeFullNameForm(Customer $customer): FormInterface
    {
        $changeFullNameForm = $this->createForm(ChangeNameType::class, null, ['full_name' => $customer->fullName()]);
        $changeFullNameForm->handleRequest($this->getRequest());

        if ($changeFullNameForm->isSubmitted() && $changeFullNameForm->isValid()) {
            $firstName = $changeFullNameForm->getData()['firstName'];
            $lastName  = $changeFullNameForm->getData()['lastName'];

            $this->handleCommand(new ChangeCustomerFullName($customer->customerId()->serialize(), $firstName, $lastName));

            $changeFullNameForm = $this->createForm(ChangeNameType::class, null, ['full_name' => $customer->fullName()]);
        }

        return $changeFullNameForm;
    }

    /**
     * @param Customer $customer
     *
     * @return FormInterface
     */
    private function createAndHandleChangeEmailAddressForm(Customer $customer): FormInterface
    {
        $changeEmailAddressForm = $this->createForm(ChangeEmailType::class, null, ['email_address' => $customer->emailAddress()]);
        $changeEmailAddressForm->handleRequest($this->getRequest());

        if ($changeEmailAddressForm->isSubmitted() && $changeEmailAddressForm->isValid()) {
            $emailAddress = $changeEmailAddressForm->getData()['emailAddress'];

            $this->handleCommand(new ChangeUserEmailAddress($customer->userId()->serialize(), $emailAddress));

            $this->authenticateUser($emailAddress);

            $changeEmailAddressForm = $this->createForm(ChangeEmailType::class, null, ['email_address' => $customer->emailAddress()]);
        }

        return $changeEmailAddressForm;
    }

    /**
     * @return FormInterface
     */
    private function createAndHandleChangePasswordForm(): FormInterface
    {
        $changePasswordForm = $this->createForm(ChangePasswordType::class);
        $changePasswordForm->handleRequest($this->getRequest());

        if ($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()) {

            try {
                $this->handleCommand(new ChangeUserPassword(
                    $this->getUser()->getId(),
                    $changePasswordForm->getData()['currentPassword'],
                    $changePasswordForm->getData()['newPassword']
                ));

                $this->authenticateUser($this->getUser()->getUsername());

                $changePasswordForm = $this->createForm(ChangePasswordType::class);

                $this->addFlash('success', 'Your password has been changed successfully.');
            } catch (HandlerFailedException $exception) {
                $throwable = $this->getThrowable($exception);

                if ($throwable instanceof InvalidCurrentPasswordException)
                    $this->addFlash('error', 'Your current password was incorrect. Please try again.');
            }
        }

        return $changePasswordForm;
    }
}