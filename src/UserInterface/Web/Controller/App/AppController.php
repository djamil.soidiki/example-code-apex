<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\App;

use App\Application\PropertyManagement\Command\CreateProperty;
use App\Application\PropertyManagement\Query\FindPropertiesByManagerId;
use App\Domain\PropertyManagement\ValueObject\PropertyAddress;
use App\Domain\PropertyManagement\ValueObject\PropertyId;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Property\CreatePropertyForm;
use Assert\AssertionFailedException;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AppController
 *
 * @package App\UserInterface\Web\Controller\App
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/app")
 */
final class AppController extends AbstractController
{
    /**
     * @Route("/", name="app", methods={"GET"})
     *
     * @return RedirectResponse|Response
     * @throws AssertionFailedException
     * @throws Exception
     */
    public function app()
    {
        $createPropertyForm = $this->createForm(CreatePropertyForm::class);
        $createPropertyForm->handleRequest($this->getRequest());

        if ($createPropertyForm->isSubmitted() && $createPropertyForm->isValid()) {

            $propertyName        = $createPropertyForm->getData()['name'];
            $propertyDescription = $createPropertyForm->getData()['description'];

            /** @var PropertyAddress $propertyAddress */
            $propertyAddress = $createPropertyForm->getData()['address'];

            $propertyId = $this->handleCommand(new CreateProperty($this->getUser()->getId(), $propertyName, $propertyDescription, $propertyAddress->serialize()));
            $this->addFlash('success', 'Property created!');

            return $this->redirectToRoute('app_dashboard_property_view', [
                'propertyIdEncoded' => (new PropertyId($propertyId))->encode()
            ]);
        }

        /** @var Property[] $properties */
        $properties = $this->handleQuery(new FindPropertiesByManagerId($this->security->getUser()->getId()));

        return $this->render('app/app.html.twig', [
            'properties'         => $properties,
            'createPropertyForm' => $createPropertyForm->createView(),
        ]);
    }
}