<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\App\ContactManagement;

use App\Application\ContactManagement\Group\Query\FindGroupsByOwnerId;
use App\Infrastructure\ContactManagement\Group\Query\Doctrine\Model\Group;
use App\UserInterface\Web\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GroupsController
 *
 * @package App\UserInterface\Web\Controller\App\ContactManagement
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/app/contact/groups")
 */
final class GroupsController extends AbstractController
{
    /**
     * @Route("/", name="app_contact_management_groups", methods={"GET"})
     * @return Response
     */
    public function contacts()
    {
        /** @var Group[] $groups */
        $groups = $this->handleQuery(new FindGroupsByOwnerId($this->security->getUser()->getId()));

        return $this->render('app/contact_management/groups.html.twig', [
            'groups' => $groups,
        ]);
    }
}