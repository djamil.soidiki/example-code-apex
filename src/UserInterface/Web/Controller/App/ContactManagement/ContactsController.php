<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\App\ContactManagement;

use App\Application\ContactManagement\Contact\Query\FindContactsByOwnerId;
use App\Infrastructure\ContactManagement\Contact\Query\Doctrine\Model\Contact;
use App\UserInterface\Web\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactsController
 *
 * @package App\UserInterface\Web\Controller\App\ContactManagement
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/app/contacts")
 */
final class ContactsController extends AbstractController
{
    /**
     * @Route("/", name="app_contact_management_contacts", methods={"GET"})
     * @return Response
     */
    public function contacts()
    {
        /** @var Contact[] $contacts */
        $contacts = $this->handleQuery(new FindContactsByOwnerId($this->security->getUser()->getId()));

        return $this->render('app/contact_management/contacts.html.twig', [
            'contacts' => $contacts,
        ]);
    }
}