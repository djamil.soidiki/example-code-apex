<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\App\PropertyManagement;

use App\Application\PropertyManagement\Query\FindPropertiesByManagerId;
use App\Infrastructure\PropertyManagement\Query\Doctrine\Model\Property;
use App\UserInterface\Web\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropertiesController
 *
 * @package App\UserInterface\Web\Controller\App\PropertyManagement
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/app/properties")
 */
final class PropertiesController extends AbstractController
{
    /**
     * @Route("/", name="app_property_management_properties", methods={"GET"})
     * @return Response
     */
    public function properties()
    {
        /** @var Property[] $properties */
        $properties = $this->handleQuery(new FindPropertiesByManagerId($this->security->getUser()->getId()));

        return $this->render('app/property/properties.html.twig', [
            'properties' => $properties,
        ]);
    }
}