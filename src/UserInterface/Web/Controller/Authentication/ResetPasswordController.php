<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Authentication;

use App\Application\Authentication\Command\ResetPassword;
use App\Application\Authentication\Query\GetPasswordReminderByToken;
use App\Domain\SharedKernel\Generic\Exception\NotFoundException;
use App\Infrastructure\SharedKernel\Authentication\Query\Doctrine\Model\PasswordReminder;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Authentication\ResetPasswordType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class ResetPasswordController
 *
 * @package App\UserInterface\Web\Controller\Authentication
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/user")
 */
final class ResetPasswordController extends AbstractController
{
    /**
     * @Route("/reset-password/{token}", name="authentication_reset_password", methods={"GET", "POST"})
     *
     * @param string   $token
     * @param Security $security
     * @param Request  $request
     *
     * @return RedirectResponse|Response
     */
    public function resetPassword(string $token, Security $security, Request $request): Response
    {
        // if user is already logged in, don't display the login page again
        if ($security->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('home');
        }

        try {
            /** @var PasswordReminder $passwordReminder */
            $passwordReminder = $this->handleQuery(new GetPasswordReminderByToken($token));
        } catch (HandlerFailedException $exception) {
            $throwable = $this->getThrowable($exception);

            if ($throwable instanceof NotFoundException)
                $this->addFlash('error', 'Your reset link is incorrect or expired. Please check your email again and follow the instructions or request a new one.');

            return $this->redirectToRoute('authentication_account_recovery');
        }

        $formResetPassword = $this->createForm(ResetPasswordType::class);
        $formResetPassword->handleRequest($request);

        if ($formResetPassword->isSubmitted() && $formResetPassword->isValid()) {

            $this->handleCommand(new ResetPassword(
                                     $passwordReminder->userId()->serialize(),
                                     $formResetPassword->getData()['password']
                                 )
            );

            $this->addFlash('info', 'New password set successfully.');
            return $this->redirectToRoute('authentication_login');
        }

        return $this->render(
            'authentication/account_recover/reset_password.html.twig',
            [
                'formResetPassword' => $formResetPassword->createView()
            ]
        );
    }
}