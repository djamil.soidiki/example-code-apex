<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Authentication;

use App\UserInterface\Web\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class LoginController
 *
 * @package App\UserInterface\Web\Controller\Authentication
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="authentication_login", methods={"GET", "POST"})
     *
     * @param Security            $security
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(Security $security, AuthenticationUtils $authenticationUtils): Response
    {
        // if user is already logged in, don't display the login page again
        if ($security->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app_dashboard');
        }

        return $this->render('authentication/login.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(),
            'error'         => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }
}