<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Authentication;

use App\Application\Authentication\Command\RequestPasswordReminder;
use App\Domain\SharedKernel\Authentication\Exception\EmailAddressNotFoundException;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Authentication\RequestPasswordResetType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class AccountRecoveryController
 *
 * @package App\UserInterface\Web\Controller\Authentication
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/user")
 */
final class AccountRecoveryController extends AbstractController
{
    /**
     * @Route("/account-recovery", name="authentication_account_recovery", methods={"GET", "POST"})
     *
     * @param Security $security
     * @param Request  $request
     *
     * @return RedirectResponse|Response
     */
    public function accountRecovery(Security $security, Request $request): Response
    {
        // TODO Use attribute from AbstractController instead of DI

        // if user is already logged in, don't display the login page again
        if ($security->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('home');
        }

        $formRequestPasswordReset = $this->createForm(RequestPasswordResetType::class);
        $formRequestPasswordReset->handleRequest($request);

        if ($formRequestPasswordReset->isSubmitted() && $formRequestPasswordReset->isValid()) {
            $emailAddress = $formRequestPasswordReset->getData()['emailAddress'];

            try {
                $this->handleCommand(new RequestPasswordReminder($emailAddress));

                return $this->render('authentication/account_recover/request_confirmation.html.twig');
            } catch (HandlerFailedException $exception) {
                $throwable = $this->getThrowable($exception);

                if ($throwable instanceof EmailAddressNotFoundException)
                    $this->addFlash('error', 'We\'re sorry. We weren\'t able to identify you given the information provided.');
            }
        }

        return $this->render(
            'authentication/account_recover/request.html.twig',
            [
                'formRequestPasswordReset' => $formRequestPasswordReset->createView()
            ]
        );
    }
}