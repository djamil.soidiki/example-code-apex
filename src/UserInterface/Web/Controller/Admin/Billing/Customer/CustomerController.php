<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Admin\Billing\Customer;

use App\Application\Billing\Customer\Command\RegisterCustomer;
use App\Application\Billing\Customer\Query\FindCustomersQuery;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Admin\Billing\Customer\CreateCustomerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CustomerController
 *
 * @package App\UserInterface\Web\Controller\Admin\Billing\Customer
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/admin/billing/customers")
 */
final class CustomerController extends AbstractController
{
    /**
     * @Route("/", name="admin_billing_customers", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function customers(Request $request)
    {
        $createCustomerForm = $this->createForm(CreateCustomerType::class);
        $createCustomerForm->handleRequest($request);

        $customers = $this->handleQuery(new FindCustomersQuery());

        if ($createCustomerForm->isSubmitted() && $createCustomerForm->isValid()) {
            $userId       = $createCustomerForm->getData()['user'];
            $emailAddress = $createCustomerForm->getData()['emailAddress'];
            $firstName    = $createCustomerForm->getData()['firstName'];
            $lastName     = $createCustomerForm->getData()['lastName'];

            $this->handleCommand(new RegisterCustomer($userId, $emailAddress, $firstName, $lastName));

            $this->addFlash('success', 'Customer created');

            return $this->redirectToRoute('admin_billing_customers');
        }

        return $this->render(
            'admin/billing/customer/create.html.twig',
            [
                'createCustomerForm' => $createCustomerForm->createView(),
                'customers'          => $customers
            ]
        );
    }
}