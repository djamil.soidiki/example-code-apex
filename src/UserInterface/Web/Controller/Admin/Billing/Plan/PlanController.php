<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Admin\Billing\Plan;

use App\Application\Billing\Plan\Command\CreatePlan;
use App\Application\Billing\Plan\Query\FindPlansQuery;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Admin\Billing\Plan\CreatePlanType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PlanController
 *
 * @package App\UserInterface\Web\Controller\Admin\Billing\Plan
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/admin/billing/plans")
 */
final class PlanController extends AbstractController
{
    /**
     * @Route("/", name="admin_billing_plans", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function plans(Request $request)
    {
        $createPlanForm = $this->createForm(CreatePlanType::class);
        $createPlanForm->handleRequest($request);

        $plans = $this->handleQuery(new FindPlansQuery());

        if ($createPlanForm->isSubmitted() && $createPlanForm->isValid()) {
            $productId    = $createPlanForm->getData()['product'];
            $planName     = $createPlanForm->getData()['name'];
            $planPrice    = $createPlanForm->getData()['price'];
            $planInterval = $createPlanForm->getData()['interval'];

            $this->handleCommand(new CreatePlan($productId, $planName, $planPrice, $planInterval));

            $this->addFlash('success', 'Plan created');

            return $this->redirectToRoute('admin_billing_plans');
        }

        return $this->render(
            'admin/billing/plan/create.html.twig',
            [
                'createPlanForm' => $createPlanForm->createView(),
                'plans'          => $plans
            ]
        );
    }
}