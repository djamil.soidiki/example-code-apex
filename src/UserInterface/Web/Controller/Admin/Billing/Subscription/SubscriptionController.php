<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller\Admin\Billing\Subscription;

use App\Application\Billing\Subscription\Command\SubscribeToPlan;
use App\Application\Billing\Subscription\Query\FindSubscriptions;
use App\UserInterface\Web\Controller\AbstractController;
use App\UserInterface\Web\Form\Admin\Billing\Subscription\CreateSubscriptionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SubscriptionController
 *
 * @package App\UserInterface\Web\Controller\Admin\Billing\Subscription
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/admin/billing/subscriptions")
 */
final class SubscriptionController extends AbstractController
{
    /**
     * @Route("/", name="admin_billing_subscriptions", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function subscriptions(Request $request)
    {
        $createSubscriptionForm = $this->createForm(CreateSubscriptionType::class);
        $createSubscriptionForm->handleRequest($request);

        $subscriptions = $this->handleQuery(new FindSubscriptions());

        if ($createSubscriptionForm->isSubmitted() && $createSubscriptionForm->isValid()) {
            $customerId = $createSubscriptionForm->getData()['customer'];
            $planId     = $createSubscriptionForm->getData()['plan'];

            $this->handleCommand(new SubscribeToPlan($customerId, $planId));

            $this->addFlash('success', 'Subscription created');

            return $this->redirectToRoute('admin_billing_subscriptions');
        }

        return $this->render(
            'admin/billing/subscription/create.html.twig',
            [
                'createSubscriptionForm' => $createSubscriptionForm->createView(),
                'subscriptions'          => $subscriptions
            ]
        );
    }
}