<?php

declare(strict_types=1);

namespace App\UserInterface\Web\Controller;

use App\Application\Authentication\Query\GetUserByEmailAddress;
use App\Application\Authorization\Query\FindUserRoles;
use App\Application\Billing\Customer\Query\GetCustomerByUserIdQuery;
use App\Application\Billing\Customer\Query\IsUserACustomer;
use App\Application\TrialPeriod\Query\GetTrialPeriodByCustomerId;
use App\Application\TrialPeriod\Query\IsCustomerOnTrialPeriod;
use App\Infrastructure\SharedKernel\Billing\Customer\Query\Doctrine\Model\Customer;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use App\Infrastructure\SharedKernel\Generic\Security\Core\User;
use App\Infrastructure\SharedKernel\Generic\Security\Guard\LoginFormAuthenticator;
use App\Infrastructure\SharedKernel\Generic\Security\Translator\RoleTranslator;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

/**
 * Class AbstractController
 *
 * @package App\UserInterface\Web\Controller
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractController extends BaseController
{
    use BusTrait;

    /**
     * @var Security
     */
    protected $security;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var GuardAuthenticatorHandler
     */
    private $authenticatorHandler;

    /**
     * @var LoginFormAuthenticator
     */
    private $formAuthenticator;

    /**
     * @var Customer|null
     */
    private $customer = null;

    /**
     * AbstractController constructor.
     *
     * @param MessageBusInterface       $commandBus
     * @param MessageBusInterface       $queryBus
     * @param Security                  $security
     * @param RequestStack              $requestStack
     * @param GuardAuthenticatorHandler $authenticatorHandler
     * @param LoginFormAuthenticator    $formAuthenticator
     *
     * @throws Exception
     */
    public function __construct(
        MessageBusInterface $commandBus,
        MessageBusInterface $queryBus,
        Security $security,
        RequestStack $requestStack,
        GuardAuthenticatorHandler $authenticatorHandler,
        LoginFormAuthenticator $formAuthenticator
    )
    {
        $this->commandBus           = $commandBus;
        $this->queryBus             = $queryBus;
        $this->security             = $security;
        $this->requestStack         = $requestStack;
        $this->authenticatorHandler = $authenticatorHandler;
        $this->formAuthenticator    = $formAuthenticator;
    }

    /**
     * @return Request|null
     */
    protected function getRequest(): ?Request
    {
        return $this->requestStack->getCurrentRequest();
    }

    /**
     * @param string $emailAddress
     *
     * @return Response|null
     */
    protected function authenticateUser(string $emailAddress): ?Response
    {
        $userModel = $this->handleQuery(new GetUserByEmailAddress($emailAddress));

        /** @var Customer $customer */
        $customer = $this->handleQuery(new GetCustomerByUserIdQuery($userModel->getId()));

        $roles = $this->handleQuery(new FindUserRoles($userModel->getId()));

        $roleTranslator = new RoleTranslator();

        $user = new User(
            $userModel->userId()->serialize(),
            $customer->fullName()->firstName(),
            $customer->fullName()->lastName(),
            $userModel->emailAddress()->serialize(),
            $userModel->password()->serialize(),
            $roleTranslator->toRoleForSymfony($roles)
        );

        return $this->authenticatorHandler->authenticateUserAndHandleSuccess(
            $user,
            $this->requestStack->getCurrentRequest(),
            $this->formAuthenticator,
            'main'
        );
    }

    /**
     * @throws Exception
     */
    protected function trialPeriodFlashInfo(): void
    {
        $isUserACustomer = $this->handleQuery(new IsUserACustomer($this->security->getUser()->getId()));

        if (!$isUserACustomer)
            return;

        $customer = $this->handleQuery(new GetCustomerByUserIdQuery($this->security->getUser()->getId()));
        if ($customer === null)
            return;

        $isCustomerOnTrialPeriod = $this->handleQuery(new IsCustomerOnTrialPeriod($this->customer->getId()));

        if ($isCustomerOnTrialPeriod /*&& !$this->isSubscribed()*/) {

            $trialPeriod = $this->handleQuery(new GetTrialPeriodByCustomerId($this->customer->getId()));

            $daysRemaining          = $trialPeriod->endDate()->diff(new \DateTimeImmutable())->days;
            $linkToSubscriptionPage = $this->generateUrl('home_buy');

            $message = "Your trial will end in <b>$daysRemaining days.</b> To keep using Reavant, <a href=\"$linkToSubscriptionPage\">add billing info</a>.";

            $this->addFlash('info', $message);
        }
    }

    /**
     * @param string $type
     * @param string $title
     * @param string $message
     */
    protected function addCustomFlashMessage(string $type, string $message, string $title = null): void
    {
        if (isset($title) && !empty($title))
            $this->addFlash($type, "$title#~#$message");
        else
            $this->addFlash($type, "$message");
    }
}