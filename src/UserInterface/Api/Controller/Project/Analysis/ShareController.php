<?php

declare(strict_types=1);

namespace App\UserInterface\Api\Controller\Project\Analysis;

use App\Application\Project\Command\ShareProjectAnalysisCommand;
use App\UserInterface\Api\Controller\AbstractController;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShareController
 *
 * @package App\UserInterface\Api\Controller\Property\Analysis
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/api")
 */
final class ShareController extends AbstractController
{
    /**
     * @Route("/project/analysis/share", options={"expose"=true}, name="api_project_analysis_share", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function shareAnalysis(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        try {
            Assertion::keyExists($data, 'project_id');
            Assertion::keyExists($data, 'project_analysis_id');
            Assertion::keyExists($data, 'contacts_ids');

            $this->handleCommand(new ShareProjectAnalysisCommand(
                $data['project_id'],
                $data['project_analysis_id'],
                $data['contacts_ids']
            ));
        } catch (AssertionFailedException $exception) {

            return new JsonResponse(
                [
                    'message' => $exception->getMessage()
                ],
                Response::HTTP_BAD_REQUEST
            );
        } catch (HandlerFailedException $exception) {
            $throwable = $this->getThrowable($exception);

            return new JsonResponse(
                [
                    'message' => $throwable->getMessage()
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->jsonResponse([
            'message' => 'success'
        ]);
    }
}