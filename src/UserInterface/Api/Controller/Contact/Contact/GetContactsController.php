<?php

declare(strict_types=1);

namespace App\UserInterface\Api\Controller\Contact\Contact;

use App\Application\Contact\Contact\Query\FindContactsByUserIdQuery;
use App\UserInterface\Api\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GetContactsController
 *
 * @package App\UserInterface\Api\Controller\Contact\Contact
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/api")
 */
final class GetContactsController extends AbstractController
{
    /**
     * @Route("/contacts", options={"expose"=true}, name="api_contacts_get", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getContacts(): JsonResponse
    {
        try {
            $userId = $this->getUser()->getId();

            $contacts = $this->handleQuery(new FindContactsByUserIdQuery($userId));

        } catch (HandlerFailedException $exception) {
            $throwable = $this->getThrowable($exception);

            return new JsonResponse(
                [
                    'message' => $throwable->getMessage()
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->jsonResponse($contacts);
    }
}