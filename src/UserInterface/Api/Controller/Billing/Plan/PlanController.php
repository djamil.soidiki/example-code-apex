<?php

declare(strict_types=1);

namespace App\UserInterface\Api\Controller\Billing\Plan;

use App\Application\Billing\Plan\Query\GetPlanByPlanIdQuery;
use App\Infrastructure\SharedKernel\Billing\Plan\Query\Model\Plan;
use App\UserInterface\Api\Controller\AbstractController;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PlanController
 *
 * @package App\UserInterface\Api\Controller\Billing\Plan
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 *
 * @Route("/api/plan")
 */
final class PlanController extends AbstractController
{
    /**
     * @Route("/{id}", options={"expose"=true}, name="api_plan_get_item", methods={"GET"})
     *
     * @param string $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getPlan(string $id): JsonResponse
    {
        try {
            /** @var Plan $plan */
            $plan = $this->handleQuery(new GetPlanByPlanIdQuery($id));
        } catch (HandlerFailedException $exception) {
            $throwable = $this->getThrowable($exception);

            return new JsonResponse(
                [
                    'message' => $throwable->getMessage()
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->jsonResponse($plan);
    }
}