<?php

declare(strict_types=1);

namespace App\UserInterface\Api\Controller;

use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class AbstractController
 *
 * @package App\UserInterface\Api\Controller
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
abstract class AbstractController extends BaseController
{
    use BusTrait;

    /**
     * AbstractController constructor.
     *
     * @param MessageBusInterface $commandBus
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $commandBus, MessageBusInterface $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus   = $queryBus;
    }

    /**
     * @param $data
     *
     * @return JsonResponse
     */
    protected function jsonResponse($data)
    {
        if (is_object($data))
            return new JsonResponse($data->serialize());
        else {

            return new JsonResponse($this->jsonFormatter($data));
        }
    }

    /**
     * @param array $collection
     *
     * @return array
     */
    private function jsonFormatter(array $collection): array
    {
        $jsonFormat = [];

        foreach ($collection as $key => $item) {
            if (is_object($item))
                $jsonFormat[$key] = $item->serialize();
            else
                $jsonFormat[$key] = $item;
        }

        return $jsonFormat;
    }
}