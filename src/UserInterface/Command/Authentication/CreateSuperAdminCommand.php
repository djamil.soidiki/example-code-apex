<?php

declare(strict_types=1);

namespace App\UserInterface\Command\Authentication;

use App\Application\Authentication\Command\RegisterUser;
use App\Application\Billing\Customer\Command\RegisterCustomer;
use App\Domain\SharedKernel\Authentication\ValueObject\Roles\UserRole;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class CreateSuperAdminCommand
 *
 * @package App\UserInterface\Command\Authentication
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateSuperAdminCommand extends Command
{
    use BusTrait;

    protected static $defaultName = 'app:authentication:super-admin';

    /**
     * CreateSuperAdminCommand constructor.
     *
     * @param MessageBusInterface $commandBus
     */
    public function __construct(MessageBusInterface $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Creates a new super admin user.')
            ->setHelp('This command allows you to create a super admin user ...')
            ->addArgument('firstName', InputArgument::REQUIRED, 'The first name')
            ->addArgument('lastName', InputArgument::REQUIRED, 'The last name')
            ->addArgument('emailAddress', InputArgument::REQUIRED, 'The email address')
            ->addArgument('password', InputArgument::REQUIRED, 'The password');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $firstName    = $input->getArgument('firstName');
        $lastName     = $input->getArgument('lastName');
        $emailAddress = $input->getArgument('emailAddress');
        $password     = $input->getArgument('password');

        $userId = $this->handleCommand(new RegisterUser($emailAddress, $password, [UserRole::ROLE_SUPER_ADMIN]));
        $this->handleCommand(new RegisterCustomer($userId, $emailAddress, $firstName, $lastName));

        $output->writeln(sprintf('Created user <comment>%s</comment>', $emailAddress));

        return 0;
    }

    /**
     * @inheritDoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];

        if (!$input->getArgument('firstName')) {
            $question = new Question('Please enter a first name:');
            $question->setValidator(function ($firstName) {
                if (empty($firstName)) {
                    throw new \Exception('First name can not be empty');
                }

                return $firstName;
            }
            );
            $questions['firstName'] = $question;
        }

        if (!$input->getArgument('lastName')) {
            $question = new Question('Please enter a last name:');
            $question->setValidator(function ($lastName) {
                if (empty($lastName)) {
                    throw new \Exception('Last name can not be empty');
                }

                return $lastName;
            }
            );
            $questions['lastName'] = $question;
        }

        if (!$input->getArgument('emailAddress')) {
            $question = new Question('Please choose an email address:');
            $question->setValidator(function ($emailAddress) {
                if (empty($emailAddress)) {
                    throw new \Exception('Email address can not be empty');
                }

                $filteredEmailAddress = filter_var($emailAddress, FILTER_VALIDATE_EMAIL);

                if ($filteredEmailAddress === false) {
                    throw new \Exception(sprintf('"%s" is not a valid email address', $emailAddress));
                }

                return $emailAddress;
            }
            );
            $questions['emailAddress'] = $question;
        }

        if (!$input->getArgument('password')) {
            $question = new Question('Please choose a password:');
            $question->setValidator(function ($password) {
                if (empty($password)) {
                    throw new \Exception('Password can not be empty');
                }

                return $password;
            }
            );
            $question->setHidden(true);
            $questions['password'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}