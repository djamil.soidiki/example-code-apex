<?php

declare(strict_types=1);

namespace App\UserInterface\Command\Authorization;

use App\Application\Authorization\Command\CreateRole;
use App\Infrastructure\SharedKernel\Generic\Bus\BusTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class CreateRoleAdminCommand
 *
 * @package App\UserInterface\Command\Authorization
 * @author  Djamil Soidiki <djamil.soidiki@gmail.com>
 */
final class CreateRoleAdminCommand extends Command
{
    use BusTrait;

    protected static $defaultName = 'app:authorization:role-admin';

    /**
     * CreateSuperAdminCommand constructor.
     *
     * @param MessageBusInterface $commandBus
     */
    public function __construct(MessageBusInterface $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Creates a new role admin.')
            ->setHelp('This command allows you to create a role admin ...');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $roleId = $this->handleCommand(new CreateRole());

        $output->writeln(sprintf('Role created. Here is the id: <comment>%s</comment>', $roleId));

        return 0;
    }
}