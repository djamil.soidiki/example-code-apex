import React, {useCallback, useState} from 'react';
import http from '../common/http-common';

const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
};

const getAll = () => {
    const [loading, setLoading] = useState(false);
    const [contacts, setContacts] = useState([]);

    const load = useCallback(async () => {
        setLoading(true);

        await http.get('/contacts')
            .then(response => {
                setContacts(response.data);
            })
            .catch(e => {
                console.log(e);
            });

        setLoading(false);
    });

    return {
        loading,
        contacts,
        load
    }
};

const sendEmails = () => {
    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState({});
    const [success, setSuccess] = useState(false);

    const send = useCallback(async (projectId, projectAnalysisId, users) => {
        setLoading(true);

        const data = {
            project_id: projectId,
            project_analysis_id: projectAnalysisId,
            contacts_ids: users,
        };

        await http.post('/project/analysis/share', data)
            .then(response => {
                setSuccess(true);
            })
            .catch(e => {
                console.log(e.response.data);
            })
        ;

        setLoading(false);
    });

    return {
        loading,
        send,
        success
    }
};

export default {
    getAll,
    sendEmails
};