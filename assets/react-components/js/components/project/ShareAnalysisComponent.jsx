import React, {useEffect, useState} from 'react';
import {render} from 'react-dom';
import BootstrapTable from "react-bootstrap-table-next";
import ContactService from "../../services/ContactService";

function linkNameFormatter(cell, row) {
    return (
        <a href={'#'} className={'font-weight-bold'}>{cell.first_name} {cell.last_name}</a>
    );
}

function linkEmailFormatter(cell, row) {
    return (
        <a href={'#'}>{cell}</a>
    );
}

function noDataIndication() {
    return (
        <div className={'mt-3 col-md-5 text-center mx-auto text-muted'}>

            <img src={'/assets/app/img/contact/icon-contacts-empty.png'} className={'col-md-4 mb-4 img'}/>

            <h6>You do not have any contacts</h6>

            Add or import new contacts by clicking the "Import" button or "Add a contact" on the <a href={'#'}>contact
            management page.</a>
        </div>
    );
}

function ShareAnalysis({projectId, projectAnalysisId}) {
    const {contacts, load, loading} = ContactService.getAll();
    const {send, loading: loadingSendEmail, success} = ContactService.sendEmails();
    const [contactsSelected, setContactsSelected] = useState([]);
    const [buttonDisabled, setButtonDisabled] = useState(true);

    useEffect(() => {
        load()
    }, []);

    const handleOnSelect = (row, isSelect) => {

        const contacts = contactsSelected.slice();

        if (isSelect) {
            contacts.push(row);
        } else {
            const index = contacts.findIndex(function (contact) {
                return contact === row;
            });

            contacts.splice(index, 1);
        }

        if (contacts.length > 0) {
            setButtonDisabled(false);
        } else {
            setButtonDisabled(true);
        }

        setContactsSelected(contacts);
    };

    const handleOnSelectAll = (isSelect, rows) => {

        let contacts = contactsSelected.slice();

        if (isSelect) {
            contacts = rows;
            setButtonDisabled(false);
        } else {
            contacts = [];
            setButtonDisabled(true);
        }

        setContactsSelected(contacts);
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        let contacts = contactsSelected.slice();
        let contactsIds = [];

        contacts.forEach(contact => {
            contactsIds.push(contact.contact_id);
        });

        //handleOnSelectAll(true, []);

        send(projectId, projectAnalysisId, contactsIds);
    };

    const columns = [
        {
            dataField: 'full_name',
            text: 'Full Name',
            formatter: linkNameFormatter
        },
        {
            dataField: 'email_address',
            text: 'Email Address',
            formatter: linkEmailFormatter
        }
    ];

    const classes = 'table-borderless';

    const selectRow = {
        mode: 'checkbox',
        clickToSelect: true,
        onSelect: handleOnSelect,
        onSelectAll: handleOnSelectAll
    };

    const renderSendButton = () => {

        if (loadingSendEmail) {
            return <button className="btn btn-sm btn-success" type="button" disabled>
                <span className="spinner-border spinner-border-sm mr-3" role="status" aria-hidden="true"></span>
                Sending...
            </button>
        } else {
            return <button className="btn btn-sm btn-success px-5" disabled={buttonDisabled}>Send</button>
        }
    };

    const renderConfirmationMessage = () => {

        if (success) {
            return <div className="alert alert-success" role="alert">
                Your analysis was shared with your contacts
            </div>
        }
    };

    return (
        <div className={'row'}>
            <div className={'col-md-8'}>
                <BootstrapTable keyField='contact_id' data={contacts} columns={columns}
                                selectRow={selectRow}
                                bordered={false} classes={classes} noDataIndication={noDataIndication} bootstrap4/>
            </div>

            <div className={'col-md-4 border-left pt-2'}>
                <div className="pb-4 mb-3 border-bottom">
                    <h6>Contacts Selected ({contactsSelected.length})</h6>

                    <div>
                        {contactsSelected.map(contactSelected => (
                            <div key={contactSelected.contact_id}>{contactSelected.email_address}</div>
                        ))}
                    </div>
                </div>

                <div className={'text-center mt-4'}>
                    {renderConfirmationMessage()}

                    <form onSubmit={handleSubmit}>
                        {renderSendButton()}
                    </form>
                </div>
            </div>
        </div>
    );
}

class ShareAnalysisComponent extends HTMLElement {
    connectedCallback() {
        const projectId = this.dataset.projectId;
        const projectAnalysisId = this.dataset.projectAnalysisId;

        render(<ShareAnalysis projectId={projectId} projectAnalysisId={projectAnalysisId}/>, this)
    }
}

customElements.define('share-analysis-component', ShareAnalysisComponent);